
### Preparation of the Cordex data
conda activate parc
chmod 777 /home/hola/project/nacional_parques/script/nivo_parc/toolbox/*

# Variable array
declare -a var_arr=("hurs" "tas" "uas" "vas" "pr")

# Site array
declare -a site_arr=("AIGUE" "GUADA" "ORDES" "PICOS" "SIERR") # "TEIDE")

res_simu="250m"

# Init folders
dataCordexFolder="/home/hola/datos_seis/nacional_parques/data/cordex/EUR11"
prodFolder0="/home/hola/datos_seis/nacional_parques/prod/cordex"
CordexToolbox="/home/hola/project/nacional_parques/script/nivo_parc/toolbox"

parallelTxtFolder="/home/hola/project/nacional_parques/script/parallelTxt"
rm ${parallelTxtFolder}/*.txt
mkdir -p ${parallelTxtFolder}


####################################
####################################
# Quantile-mapping correction
####################################

# PRECIPITATION
inRefClim_pr="/home/hola/datos_seis/nacional_parques/prod/STEAD_SPREAD/SPREAD_pen_pcp_latlon_1950_2012_011.nc"

# Resampling t 0.11º grid (the one from Iberia)
# see ./misc/prep_SPREAD_file.py

# historical
run_name="historical" # historical rcp85 rcp26
source $CordexToolbox/exp_list_${run_name}.txt


for exp_name in "${exp_arr[@]}"
do

for site_name in "${site_arr[@]}"
do
workFolder1=${prodFolder0}/${exp_name}/${run_name}/${site_name}
echo -e "\nTreating "$workFolder1
metfile=$(ls ${workFolder1}/pr/pr*.nc) # | head -n 1)

if [ ! -f ${workFolder1}/pr_qm_S/*.nc ]
#if [ ! -f "/home/hola/datos_seis/nacional_parques/prod/pkl/QM_SS/pr"*$exp_name*$site_name*".p" ]
then
echo python ${CordexToolbox}/QM_correction_pr.py -inRefClim ${inRefClim_pr} -inCordex  ${metfile} -out_QM_folder "/home/hola/datos_seis/nacional_parques/prod/pkl/QM_SS" -out_fig_folder "/home/hola/datos_seis/nacional_parques/fig/QM_SS" -out_NC_folder "pr_qm_S" -calc_read "calc_apply" -method "interp_point" -daymean_monthsum "monthsum" -remove_drizzle_day 1  >> ${parallelTxtFolder}/tmp_par_hist_qm_pr.txt
fi
done

done
parallel --jobs 1 :::: ${parallelTxtFolder}/tmp_par_hist_qm_pr.txt 


# rcp
run_name="rcp85" # historical rcp85 rcp26
source $CordexToolbox/exp_list_${run_name}.txt

for exp_name in "${exp_arr[@]}"
do

for site_name in "${site_arr[@]}"
do
workFolder1=${prodFolder0}/${exp_name}/${run_name}/${site_name}
echo -e "\nTreating "$workFolder1
metfile=$(ls ${workFolder1}/pr/pr*.nc) # | head -n 1)

if [ ! -f ${workFolder1}/pr_qm_S/*.nc ]
then
echo python ${CordexToolbox}/QM_correction_pr.py -inRefClim ${inRefClim_pr} -inCordex  ${metfile} -out_QM_folder "/home/hola/datos_seis/nacional_parques/prod/pkl/QM_SS" -out_fig_folder "/home/hola/datos_seis/nacional_parques/fig/QM_SS"  -out_NC_folder "pr_qm_S" -calc_read "read_apply" -method "interp_point"  -daymean_monthsum "monthsum" -remove_drizzle_day 1  >> ${parallelTxtFolder}/tmp_par_rcp_qm_pr.txt
fi
done

done
parallel --jobs 2 :::: ${parallelTxtFolder}/tmp_par_rcp_qm_pr.txt


# TEMPERATURE
inRefClim_tas="/home/hola/datos_seis/nacional_parques/prod/STEAD_SPREAD/tmean_pen_comp_011.nc"

# historical
run_name="historical" # historical rcp85 rcp26
source $CordexToolbox/exp_list_${run_name}.txt

for exp_name in "${exp_arr[@]}"
do

for site_name in "${site_arr[@]}"
do
workFolder1=${prodFolder0}/${exp_name}/${run_name}/${site_name}
echo -e "\nTreating "$workFolder1
metfile=$(ls ${workFolder1}/tas/tas*.nc) # | head -n 1)

if [ ! -f ${workFolder1}/tas_qm_S/*.nc ]
then
echo python ${CordexToolbox}/QM_correction_tas.py -inRefClim ${inRefClim_tas} -inCordex  ${metfile} -out_QM_folder "/home/hola/datos_seis/nacional_parques/prod/pkl/QM_SS" -out_fig_folder "/home/hola/datos_seis/nacional_parques/fig/QM_SS" -out_NC_folder "tas_qm_S" -calc_read "calc_apply" -method "interp_point"  >> ${parallelTxtFolder}/tmp_par_hist_qm_tas.txt
fi
done

done
parallel --jobs 2 :::: ${parallelTxtFolder}/tmp_par_hist_qm_tas.txt 


# rcp
run_name="rcp85" # historical rcp85 rcp26
source $CordexToolbox/exp_list_${run_name}.txt

for exp_name in "${exp_arr[@]}"
do

for site_name in "${site_arr[@]}"
do
workFolder1=${prodFolder0}/${exp_name}/${run_name}/${site_name}
echo -e "\nTreating "$workFolder1
metfile=$(ls ${workFolder1}/tas/tas*.nc) # | head -n 1)
if [ ! -f ${workFolder1}/tas_qm_S/*.nc ]
then
echo python ${CordexToolbox}/QM_correction_tas.py -inRefClim ${inRefClim_tas} -inCordex  ${metfile} -out_QM_folder "/home/hola/datos_seis/nacional_parques/prod/pkl/QM_SS" -out_fig_folder "/home/hola/datos_seis/nacional_parques/fig/QM_SS" -out_NC_folder "tas_qm_S" -calc_read "read_apply" -method "interp_point"  >> ${parallelTxtFolder}/tmp_par_rcp_qm_tas.txt
fi
done

done
parallel --jobs 2 :::: ${parallelTxtFolder}/tmp_par_rcp_qm_tas.txt





##################################################
######## Merge all variables in one file #########
##################################################
run_name="historical" # historical rcp85 rcp26
source $CordexToolbox/exp_list_${run_name}.txt

###########################
#### PRCOR 0; TASCOR 0 ####
for exp_name in "${exp_arr[@]}"
do

for site_name in "${site_arr[@]}"
do
workFolder1=${prodFolder0}/${exp_name}/${run_name}/${site_name}
echo -e "\nTreating "$workFolder1
metfile0=$(ls ${workFolder1}/tas/tas*.nc) # | head -n 1)
outNCName=$(${CordexToolbox}/get_Cordex_name_with_prefix_suffix.py -inNC ${metfile0[0]} -prefix allvar_ -str2rem tas_ )


mkdir -p ${workFolder1}/allvar_qm_T0_P0/

#if [ ! -f ${workFolder1}/allvar_qm_T0_P0/allvar*.nc ]
#then

cp -rf ${metfile0} ${workFolder1}/allvar_qm_T0_P0/${outNCName} 

metfile_list=$(ls ${workFolder1}/{pr,hurs,uas,vas}/*nc)
for metfile in ${metfile_list[@]}
do
ncks -A ${metfile} ${workFolder1}/allvar_qm_T0_P0/${outNCName}
done
#fi

mkdir ${workFolder1}/dat_qm_T0_P0/
#if [ ! -f ${workFolder1}/dat_qm_T0_P0/*end.dat ]
#then
echo ${CordexToolbox}/prep_netcdf_meteo2micromet.py -inNC ${workFolder1}/allvar_qm_T0_P0/${outNCName} -outFolder ${workFolder1}/dat_qm_T0_P0/ -resDEM ${res_simu} -dist_buffer 15000 -EUR_AFR "EUR" >> ${parallelTxtFolder}/tmp_par_nc2mm_qm.txt
#fi

done
done



#############################
#### PR COR 1; TAS COR 1 ####
for exp_name in "${exp_arr[@]}"
do

for site_name in "${site_arr[@]}"
do
workFolder1=${prodFolder0}/${exp_name}/${run_name}/${site_name}
echo -e "\nTreating "$workFolder1
metfile0=$(ls ${workFolder1}/tas_qm_S/tas*.nc) # | head -n 1)
outNCName=$(${CordexToolbox}/get_Cordex_name_with_prefix_suffix.py -inNC ${metfile0[0]} -prefix allvar_ -str2rem tas_ )

mkdir -p ${workFolder1}/allvar_qm_T1_P1_S/

if [ ! -f ${workFolder1}/dat_qm_T1_P1_S/allvar*.dat ]
then

cp -rf ${metfile0} ${workFolder1}/allvar_qm_T1_P1_S/${outNCName} 

metfile_list=$(ls ${workFolder1}/{pr_qm_S,hurs,uas,vas}/*nc)
for metfile in ${metfile_list[@]}
do
ncks -A ${metfile} ${workFolder1}/allvar_qm_T1_P1_S/${outNCName}
done
fi

mkdir ${workFolder1}/dat_qm_T1_P1_S/
if [ ! -f ${workFolder1}/dat_qm_T1_P1_S/*end.dat ]
then
#echo ${CordexToolbox}/prep_netcdf_meteo2micromet.py -inNC ${workFolder1}/allvar_qm_T1_P1_S/${outNCName} -outFolder ${workFolder1}/dat_qm_T1_P1_S/ -resDEM ${res_simu} -dist_buffer 15000 -EUR_AFR "EUR" >> ${parallelTxtFolder}/tmp_par_nc2mm_qm.txt
${CordexToolbox}/prep_netcdf_meteo2micromet.py -inNC ${workFolder1}/allvar_qm_T1_P1_S/${outNCName} -outFolder ${workFolder1}/dat_qm_T1_P1_S/ -resDEM ${res_simu} -dist_buffer 15000 -EUR_AFR "EUR"
fi

done
done


parallel --jobs 2 :::: ${parallelTxtFolder}/tmp_par_nc2mm_qm.txt  # jobs 50 makes that some job are not done...?!¡









### ARCHIVE

#############################
#### PR COR 0; TAS COR 1 ####
for exp_name in "${exp_arr[@]}"
do

for site_name in "${site_arr[@]}"
do
workFolder1=${prodFolder0}/${exp_name}/${run_name}/${site_name}
echo -e "\nTreating "$workFolder1
metfile0=$(ls ${workFolder1}/tas_qm_S/tas*.nc) # | head -n 1)
outNCName=$(${CordexToolbox}/get_Cordex_name_with_prefix_suffix.py -inNC ${metfile0[0]} -prefix allvar_ -str2rem tas_ )


mkdir -p ${workFolder1}/allvar_qm_T1_P0_S/

if [ ! -f ${workFolder1}/allvar_qm_T1_P0_S/allvar*.nc ]
then

cp -rf ${metfile0} ${workFolder1}/allvar_qm_T1_P0_S/${outNCName} 

metfile_list=$(ls ${workFolder1}/{pr,hurs,uas,vas}/*nc)
for metfile in ${metfile_list[@]}
do
ncks -A ${metfile} ${workFolder1}/allvar_qm_T1_P0_S/${outNCName}
done
#fi

mkdir ${workFolder1}/dat_qm_T1_P0_S/
#if [ ! -f ${workFolder1}/dat_qm_T1_P0/*end.dat ]
#then
###echo ${CordexToolbox}/prep_netcdf_meteo2micromet.py -inNC ${workFolder1}/allvar_qm_T1_P0_S/${outNCName} -outFolder ${workFolder1}/dat_qm_T1_P0_S/ -resDEM ${res_simu} -dist_buffer 15000 -EUR_AFR "EUR" >> ${parallelTxtFolder}/tmp_par_nc2mm_qm.txt
${CordexToolbox}/prep_netcdf_meteo2micromet.py -inNC ${workFolder1}/allvar_qm_T1_P0_S/${outNCName} -outFolder ${workFolder1}/dat_qm_T1_P0_S/ -resDEM ${res_simu} -dist_buffer 15000 -EUR_AFR "EUR"

fi

done
done


#############################
#### PR COR 1; TAS COR 0 ####
for exp_name in "${exp_arr[@]}"
do

for site_name in "${site_arr[@]}"
do
workFolder1=${prodFolder0}/${exp_name}/${run_name}/${site_name}
echo -e "\nTreating "$workFolder1
metfile0=$(ls ${workFolder1}/tas/tas*.nc) # | head -n 1)
outNCName=$(${CordexToolbox}/get_Cordex_name_with_prefix_suffix.py -inNC ${metfile0[0]} -prefix allvar_ -str2rem tas_ )


mkdir -p ${workFolder1}/allvar_qm_T0_P1_S/

if [ ! -f ${workFolder1}/dat_qm_T0_P1_S/allvar*.dat ]
then

cp -rf ${metfile0} ${workFolder1}/allvar_qm_T0_P1_S/${outNCName} 

metfile_list=$(ls ${workFolder1}/{pr_qm_S,hurs,uas,vas}/*nc)
for metfile in ${metfile_list[@]}
do
ncks -A ${metfile} ${workFolder1}/allvar_qm_T0_P1_S/${outNCName}
done
#fi

mkdir ${workFolder1}/dat_qm_T0_P1_S
#if [ ! -f ${workFolder1}/dat_qm_T0_P1/*end.dat ]
#then
###echo ${CordexToolbox}/prep_netcdf_meteo2micromet.py -inNC ${workFolder1}/allvar_qm_T0_P1_S/${outNCName} -outFolder ${workFolder1}/dat_qm_T0_P1_S/ -resDEM ${res_simu} -dist_buffer 15000 -EUR_AFR "EUR" >> ${parallelTxtFolder}/tmp_par_nc2mm_qm.txt
${CordexToolbox}/prep_netcdf_meteo2micromet.py -inNC ${workFolder1}/allvar_qm_T0_P1_S/${outNCName} -outFolder ${workFolder1}/dat_qm_T0_P1_S/ -resDEM ${res_simu} -dist_buffer 15000 -EUR_AFR "EUR"
fi

done
done



