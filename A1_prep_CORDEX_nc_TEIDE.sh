#!/bin/sh

### Preparation of the Cordex data
conda activate parc
chmod 777 /home/hola/project/nacional_parques/script/nivo_parc/toolbox/*


# Variable array
declare -a var_arr=("hurs" "tas" "uas" "vas" "pr")

# Site array
declare -a site_arr=("TEIDE")

res_simu="250m"

# Scenarios array
#declare -a run_arr=("rcp26") # "historical" "rcp26" "rcp45" "rcp85")
# Init folders
dataCordexFolder="/home/hola/datos_seis/nacional_parques/data/cordex/AFR22"
prodFolder0="/home/hola/datos_seis/nacional_parques/prod/cordex"
CordexToolbox="/home/hola/project/nacional_parques/script/nivo_parc/toolbox"

parallelTxtFolder="/home/hola/project/nacional_parques/script/parallelTxt"
rm ${parallelTxtFolder}/*.txt
mkdir -p ${parallelTxtFolder}

run_name="historical" # rcp85 historical
source $CordexToolbox/exp_list_${run_name}_TEIDE.txt

## to find files lower than 10ko (often corrupted)
#find . -size -10k -exec ls -lh {} \+


######################################
#####     Merge temporally       #####
######################################


for exp_name in "${exp_arr[@]}"
do
         
for site_name in "${site_arr[@]}"
do

for var_name in "${var_arr[@]}"
do

echo $exp_name" - "$site_name" - "$var_name
dataFolder1=${dataCordexFolder}/${exp_name}/${run_name}/${site_name}/${var_name}
workFolder1=${prodFolder0}/${exp_name}/${run_name}/${site_name}/${var_name}

mkdir -p $workFolder1
outNCName=$(${CordexToolbox}/get_Cordex_full_period_name.py -inNCFolder $dataFolder1/ -inTimeStep 3hr)
year0=$(echo ${outNCName} | rev | cut -d_ -f1 | rev | head -c 4)
month0=$(echo ${outNCName} | rev | cut -d_ -f1 | rev | head -c 6 | rev | head -c 2 | rev)
day0=$(echo ${outNCName} | rev | cut -d_ -f1 | rev | head -c 8 | rev | head -c 2 | rev)
#if [ ! -z "$outNCName" ]
#then
# only if outNCName is not empty and files were found in the folder

# merge time
#     cdo -O mergetime $dataFolder1/*.nc ${workFolder1}/tmp/${outNCName}
echo cdo -O mergetime $dataFolder1/*.nc ${workFolder1}/tmp/${outNCName} >> ${parallelTxtFolder}/tmp_par_merge_time.txt
mkdir -p ${workFolder1}/tmp/

# interpolate in the same time line !! all var dont have the same last time step => pb when merging files
outNCName1=$(${CordexToolbox}/get_Cordex_basename.py -inNCName $outNCName -inTimeStep 3hr)
#     cdo -O inttime,${year0}-${month0}-${day0},03:00,3hour ${workFolder1}/tmp/${outNCName} ${workFolder1}/${outNCName1}"${year0}${month0}${day0}0300_end.nc"
echo cdo -O inttime,${year0}-${month0}-${day0},03:00,3hour ${workFolder1}/tmp/${outNCName} ${workFolder1}/${outNCName1}"${year0}${month0}${day0}0300_end.nc" >> ${parallelTxtFolder}/tmp_par_interpolate_time.txt

### crop to the same time period (to differenciate between rcp and historical)
##echo cdo seldate,1951-01-01T03:00:00,2005-12-31T21:00:00 ${workFolder1}/tmp/${outNCName1}"195101010300_end.nc" ${workFolder1}/${outNCName1}"195101010300_200512312100.nc" >> ${parallelTxtFolder}/tmp_par_select_time.txt

#fi

done # var
done # site 
done # exp

parallel --jobs 5 :::: ${parallelTxtFolder}/tmp_par_merge_time.txt 
parallel --jobs 5 :::: ${parallelTxtFolder}/tmp_par_interpolate_time.txt 
##parallel --jobs 50 :::: ${parallelTxtFolder}/tmp_par_select_time.txt 



# some file dont have lon lat...dont know why, add a check when downloading?
# ncdump -h /home/hola/datos_seis/nacional_parques/data/cordex/EUR11/NCC-NorESM1-M__ALADIN63/historical/SIERR/pr/pr_EUR-11_NCC-NorESM1-M_historical_r1i1p1_CNRM-ALADIN63_v1_3hr_195501010130-195512312230.nc


##################################################
######## Merge all variables in one file #########
##################################################


for exp_name in "${exp_arr[@]}"
do
  
for site_name in "${site_arr[@]}"
do
workFolder1=${prodFolder0}/${exp_name}/${run_name}/${site_name}
echo -e "\nTreating "$workFolder1
metfile0=$(ls ${workFolder1}/tas/tas*nc) # | head -n 1)
outNCName=$(${CordexToolbox}/get_Cordex_name_with_prefix_suffix.py -inNC ${metfile0[0]} -prefix allvar_ -str2rem tas_ )

mkdir -p ${workFolder1}/allvar/

cp -rf ${metfile0} ${workFolder1}/allvar/${outNCName} 

metfile_list=$(ls ${workFolder1}/{pr,hurs,uas,vas}/*nc)


for metfile in ${metfile_list[@]}
do
ncks -A ${metfile} ${workFolder1}/allvar/${outNCName}
done


mkdir ${workFolder1}/dat/
#inSHP=$(ls ~/datos_seis/nacional_parques/data/contour/limite_${site_name}.shp)

# ${CordexToolbox}/prep_netcdf_meteo2micromet.py -inNC ${workFolder1}/allvar/${outNCName} -outFolder ${workFolder1}/dat/ -resDEM ${res_simu} -dist_buffer 25000 -EUR_AFR "AFR"
echo ${CordexToolbox}/prep_netcdf_meteo2micromet.py -inNC ${workFolder1}/allvar/${outNCName} -outFolder ${workFolder1}/dat/ -resDEM ${res_simu} -dist_buffer 25000 -EUR_AFR "AFR" >> ${parallelTxtFolder}/tmp_par_nc2mm.txt


done
done

parallel --jobs 10 :::: ${parallelTxtFolder}/tmp_par_nc2mm.txt  # jobs 50 makes that some job are not done...?!¡



##################################################
######## Merge all variables in one file - CORRECTED TEMPERATURE AND PRECIP #########
##################################################

run_name="rcp85" # rcp85 rcp26 historical

for exp_name in "${exp_arr[@]}"
do
  
for site_name in "${site_arr[@]}"
do
workFolder1=${prodFolder0}/${exp_name}/${run_name}/${site_name}
echo -e "\nTreating "$workFolder1
metfile0=$(ls ${workFolder1}/tas_corr/tas*nc) # | head -n 1)
outNCName=$(${CordexToolbox}/get_Cordex_name_with_prefix_suffix.py -inNC ${metfile0[0]} -prefix allvar_ -str2rem tas_ )

mkdir -p ${workFolder1}/allvar_corr/

cp -rf ${metfile0} ${workFolder1}/allvar_corr/${outNCName} 

metfile_list=$(ls ${workFolder1}/{pr_corr,hurs,uas,vas}/*nc)


for metfile in ${metfile_list[@]}
do
ncks -A ${metfile} ${workFolder1}/allvar_corr/${outNCName}
done


mkdir ${workFolder1}/dat_corr/
#inSHP=$(ls ~/datos_seis/nacional_parques/data/contour/limite_${site_name}.shp)

${CordexToolbox}/prep_netcdf_meteo2micromet.py -inNC ${workFolder1}/allvar_corr/${outNCName} -outFolder ${workFolder1}/dat_corr/ -resDEM ${res_simu} -dist_buffer 25000 -EUR_AFR "AFR"
#echo ${CordexToolbox}/prep_netcdf_meteo2micromet.py -inNC ${workFolder1}/allvar/${outNCName} -outFolder ${workFolder1}/dat/ -resDEM ${res_simu} -dist_buffer 15000 >> ${parallelTxtFolder}/tmp_par_nc2mm.txt


done
done

#parallel --jobs 10 :::: ${parallelTxtFolder}/tmp_par_nc2mm.txt  # jobs 50 makes that some job are not done...?!¡




