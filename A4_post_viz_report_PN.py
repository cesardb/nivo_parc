import glob
import datetime as dt
import numpy as np    
import xarray as xr
import rioxarray as rioxr
import matplotlib.pyplot as plt
plt.rcParams.update({'font.size': 10, 'font.family' : 'Arial'})
from matplotlib_scalebar.scalebar import ScaleBar
import pandas as pd
import csv
import os
import pickle
# # edit of sys.path required due to a weird bug/feature of python3.6 https://github.com/conda/conda/issues/6018
# sys.path.append('/home/hola/project/nacional_parques/script')
from nivo_parc.toolbox import post_tools

    
def read_data(reg_list, var_list):            
    # fixed parameters
    cordex_domain = "EUR-11"  # CORDEX domain [STR] eg "EUR-44"
    project = 'CORDEX' # eg CMIP6
    time_frequency = "3hr" #'day'  # eg 3hr
    res = int(250)      
    pixel_size = res*res/1e6
    
    run_dict = post_tools.read_run_list("/home/hola/project/nacional_parques/script/nivo_parc/toolbox/exp_list_EXP.txt")
    out_dir = "/home/hola/datos_seis/nacional_parques/prod"
    
    epsg_dict = {"AIGUE":"EPSG:32631", "GUADA":"EPSG:32630", "ORDES":"EPSG:32631", "PICOS":"EPSG:32630", "SIERR":"EPSG:32630", "TEIDE":"EPSG:32628"}

    # variables
    exp_list = ['historical', "rcp85"] #,'historical' 'rcp26', 'rcp45', 'rcp85']
    
    ################################
    ########    LOAD DATA   ########
    ################################
    # Load data for each run
    #data_all_run_qm =  {}
    qm_exp="_T1_P1_S"
    sm_par_dir = "/home/hola/project/nacional_parques/sm_2024_01_28_4pn/par_qm"+qm_exp
    sm_par_dir_TEIDE = "/home/hola/project/nacional_parques/sm_2024_01_28_4pn/par"
    
    sm_out_dir = out_dir+"/sm/sm_20240128_Tlr_53_qm"+qm_exp
    sm_out_dir_TEIDE = out_dir+"/sm/sm_20240128_Tlr_53"

    # Open MODIS data
    data_MODIS = {}
    for reg in reg_list:
        print(reg)
        data_MODIS[reg] = {}
    
        MODIS_list = []
    
        for in_MODIS_im, in_MODIS_date in zip(sorted(glob.glob( "/home/hola/datos_seis/nacional_parques/data/MODIS/"+reg+"*.tif")), sorted(glob.glob( "/home/hola/datos_seis/nacional_parques/data/MODIS/"+reg+"*.csv"))):
            MODIS_tmp = rioxr.open_rasterio(in_MODIS_im)
            MODIS_date_raw = np.genfromtxt(in_MODIS_date, dtype = str, delimiter = ",", skip_header=1)
            MODIS_date = [dt.datetime.strptime("".join(ll.split('_')[1:4]),"%Y%m%d") for ll in MODIS_date_raw]
            MODIS_out = xr.DataArray( data = MODIS_tmp.data, dims = ('time', 'y', 'x'), coords = {'time': MODIS_date, 'y': MODIS_tmp.y, 'x': MODIS_tmp.x})
            
            DEM = np.loadtxt("/home/hola/datos_seis/nacional_parques/data/MODIS/"+reg+"_MODIS_DEM.asc", skiprows=6)
            MODIS_out = MODIS_out.assign_coords(DEM=(["y","x"], DEM)) # elevation
    
            MODIS_list.append( MODIS_out )
            
        MODIS = xr.concat(MODIS_list, dim='time')
    #    var_concat_clean = post_tools.clean_uncomplete_hyear(var_concat)
    
        hyear = [ dd.dt.year.item() if (dd.dt.month>=9) else dd.dt.year.item()-1 for dd in MODIS.time]
        MODIS = MODIS.assign_coords(hyear=("time", np.array(hyear))) # day of hydrological year (0=1st september) 
                
        MODIS_sca = (MODIS>50).astype(int)
    
        data_MODIS[reg]["scf"] = MODIS
        data_MODIS[reg]["sca"] = MODIS_sca
        
        SMOD_list = []
        sca_gp = MODIS_sca.groupby("hyear")        
        for sca_hy in sca_gp:
            SMOD_list.append( xr.apply_ufunc(
                post_tools.calc_SMOD, 
                sca_hy[1].chunk(dict(time=-1)), 
                input_core_dims=[['time']], 
                vectorize=True,
                dask="parallelized"
            ))
        data_MODIS[reg]["smod"] = xr.concat(SMOD_list, pd.Index(sca_gp.groups.keys(), name='hyear')) #.rename({"sca":"smod"})
    
    # Load data for ensemble mean
#    var_list = ["snod", "swed", "tair", "prec"] #, "smlt", "ssub"] # , "rpre", 
    stat_name_list = [ "mean", "min", "max"] #"std",
    
    data_ens_qm =  {}
    data_ens_qm_tmp = {}
    for stat_name in stat_name_list:
        data_ens_qm_tmp[stat_name] = {}
    #sm_par_dir = "/home/hola/project/nacional_parques/sm_Tlr_53/par_qm"
    
    if ("TEIDE" in reg_list) and (len(reg_list) > 1):
        print("CASE1")
        reg_list_without_teide = [reg for reg in reg_list if not (reg == "TEIDE")]

        for exp in ["historical", "rcp85"]:
            run_list = run_dict[exp]
        
            print(exp)
            for stat_name in stat_name_list:
                data = post_tools.get_simu_ens( exp, res, sm_out_dir, sm_par_dir, reg_list_without_teide, var_list, stat_name)
                data_ens_qm_tmp[stat_name] = post_tools.merge_data_runs( data_ens_qm_tmp[stat_name], data)
    
                data = post_tools.get_simu_ens( exp, res, sm_out_dir_TEIDE, sm_par_dir_TEIDE, ["TEIDE"], var_list, stat_name)
                #tt = post_tools.merge_data_runs( data_ens_qm_tmp[stat_name], data)
                data_ens_qm_tmp[stat_name]["ens"][exp]["TEIDE"] = data["ens"][exp]["TEIDE"]
    elif ("TEIDE" in reg_list) and (len(reg_list) == 1):
        print("CASE2")
        for exp in ["historical", "rcp85"]:
            run_list = run_dict[exp]
            sm_out_dir = sm_out_dir_TEIDE
            sm_par_dir = sm_par_dir_TEIDE 
            print(exp)
            for stat_name in stat_name_list:
                data = post_tools.get_simu_ens( exp, res, sm_out_dir, sm_par_dir, reg_list, var_list, stat_name)
                data_ens_qm_tmp[stat_name] = post_tools.merge_data_runs( data_ens_qm_tmp[stat_name], data)
    else:
        print("CASE3")
        for exp in ["historical", "rcp85"]:
            run_list = run_dict[exp]
        
            print(exp)
            for stat_name in stat_name_list:
                data = post_tools.get_simu_ens( exp, res, sm_out_dir, sm_par_dir, reg_list, var_list, stat_name)
                data_ens_qm_tmp[stat_name] = post_tools.merge_data_runs( data_ens_qm_tmp[stat_name], data)
                
    # Merge historical - rcp85
    run = "ens"
    data_ens_qm[run] = {"all":{}}
    
    
    for reg in reg_list:
        data_ens_qm[run]["all"][reg] = {}
        for var in var_list:
            data_ens_qm[run]["all"][reg][var] = {}
            for stat_name in stat_name_list: 
                data_ens_qm[run]["all"][reg][var][stat_name] = {}
    
                var_hist = data_ens_qm_tmp[stat_name][run]["historical"][reg][var]
                var_rcp = data_ens_qm_tmp[stat_name][run]["rcp85"][reg][var]
                var_concat = xr.concat([var_hist, var_rcp], dim="time").sortby("time")
                            
                # Identify duplicate time values
                time_values = var_concat["time"].values
                unique_indices = np.unique(time_values, return_index=True)[1]
                
                # Select the first value for each duplicate
                var_concat = var_concat.isel(time=unique_indices) # only for MOHC-HadGEM2-ES__ICTP-RegCM4-6 which has december 2005 in both historical AND rcp85
                
                var_concat_clean = post_tools.clean_uncomplete_hyear(var_concat)
                data_ens_qm["ens"]["all"][reg][var][stat_name] = var_concat_clean
    del(data_ens_qm_tmp)
    
    # Calculate SCA
    snod_threshold = 0.1
    for exp in ["all"]: #["historical", "rcp85"]: 
        for reg in reg_list:
            print(reg)
            for run in data_ens_qm.keys():
                data_ens_qm[run][exp][reg]["sca"] = { "mean":{}, "mean_mstd":{}, "mean_pstd":{}} #"min":{}, "max":{}, 
    
                for stat_name in [ "mean", "min", "max"]: #"min", "max",, "mean_mstd", "mean_pstd" 
    
                    snod = data_ens_qm[run][exp][reg]["snod"][stat_name]
                    if len(snod)>0:
                        data_ens_qm[run][exp][reg]["sca"][stat_name] = snod.copy(deep=True).rename({"snod":"sca"})
                    
                        # Create the binary mask using Xarray operations
                        mask = (snod['snod'] > snod_threshold).astype(int)
                        data_ens_qm[run][exp][reg]["sca"][stat_name]["sca"] = mask
                    else:
                        data_ens_qm[run][exp][reg]["sca"][stat_name] = {}
            
    # Calculate SMOD
    for exp in ["all"]: #["historical", "rcp85"]: 
        for reg in reg_list:
            for run in data_ens_qm.keys():
                print(reg+" - "+run)
    
                data_ens_qm[run][exp][reg]["smod"] = { "mean":{}, "min":{}, "max":{}, "mean_mstd":{}, "mean_pstd":{}} # "min":{}, "max":{}, 
                for stat_name in ["mean", "min", "max" ]: #"min", "max","mean_mstd", "mean_pstd"  
                    sca = data_ens_qm[run][exp][reg]["sca"][stat_name]
                    
                    SMOD_list = []
                    sca_gp = sca.groupby("hyear")        
                    for sca_hy in sca_gp:
                        SMOD_list.append( xr.apply_ufunc(
                            post_tools.calc_SMOD, 
                            sca_hy[1].chunk(dict(time=-1)), 
                            input_core_dims=[['time']], 
                            vectorize=True,
                            dask="parallelized"
                        ))
                    data_ens_qm[run][exp][reg]["smod"][stat_name] = xr.concat(SMOD_list, pd.Index(sca_gp.groups.keys(), name='hyear')).rename({"sca":"smod"})
    
    if "var5" in var_list:
        for reg in reg_list:
            for run in data_ens_qm.keys():
                data_ens_qm[run][exp][reg]["csub"] = data_ens_qm[run][exp][reg].pop("var5")
                for stat_name in data_ens_qm[run][exp][reg]["csub"].keys():
                    data_ens_qm[run][exp][reg]["csub"][stat_name] = -data_ens_qm[run][exp][reg]["csub"][stat_name].rename({"var5":"csub"})
                    
    if "var6" in var_list:
        for reg in reg_list:
            for run in data_ens_qm.keys():
                data_ens_qm[run][exp][reg]["wsub"] = data_ens_qm[run][exp][reg].pop("var6")
                for stat_name in data_ens_qm[run][exp][reg]["wsub"].keys():
                    data_ens_qm[run][exp][reg]["wsub"][stat_name] = -data_ens_qm[run][exp][reg]["wsub"][stat_name].rename({"var6":"wsub"})
                
    # Open Parc contour
    contour_parc = {}            
    for reg in reg_list:
        x_parc, y_parc = post_tools.open_parc_shp(reg)
        contour_parc[reg] = (x_parc , y_parc)
    
    return data_MODIS, data_ens_qm, contour_parc

def def_misc():
    common_years = list( np.arange(2000, 2022)) # years with MODIS observations and simu (reanalysis + clim proj)
    bin_edges = np.arange(900, 3900, 300)
    return common_years, bin_edges
 
def plot_sca(reg, data_MODIS, data_ens_qm, contour_parc, plt_dict, out_folder):
    # all run plot SCA
    print(reg)
        
    common_years, bin_elev_edges = def_misc()
    
    # Read MODIS data
    in_SCA_MODIS="/home/hola/datos_seis/nacional_parques/data/MODIS/Snow_Cover_Area_MODIS_"+reg+".csv"
    SCA = pd.read_csv(in_SCA_MODIS,names=["time","sca"],header=0)
    time_dt = [dt.datetime.strptime(dd, "%Y_%m_%d") for dd in SCA.time]
    SCA["time"]=time_dt
    SCA["hyear"] = [yy if mm<12 else yy+1 for yy,mm in zip(SCA.time.dt.year, SCA.time.dt.month) ]
    ts_MODIS = SCA.groupby("hyear").mean()
    
    # mean simu sca
    sca = data_ens_qm["ens"]["all"][reg]["sca"]["mean"]
    ts_sca = sca.sum(dim=("lat","lon")).groupby("hyear").mean()*250*250/1e6
    
    MODIS_filtered = ts_MODIS[ts_MODIS.index.isin(common_years)]
    SIMU_filtered  = ts_sca.where(ts_sca.hyear.isin(common_years), drop = True)
    
    # Calculate statistics - mean bias, rmse
    out_stat = {}
    out_stat["mean_bias_all"] = (SIMU_filtered-MODIS_filtered).mean().round(2)["sca"].values.item()
    out_stat["median_bias_all"] = (SIMU_filtered-MODIS_filtered).median().round(2)["sca"].values.item()
    out_stat["rmse_all"] = np.sqrt(np.square(np.subtract(MODIS_filtered.sca, SIMU_filtered.sca)).mean()).round(2)
            
    out_stat["mean_bias_rel_all"] = (100*((SIMU_filtered-MODIS_filtered).mean() / MODIS_filtered.mean()).round(2)["sca"]).values
    out_stat["median_bias_rel_all"] = (100*((SIMU_filtered-MODIS_filtered).median() / MODIS_filtered.mean()).round(2)["sca"]).values
    out_stat["nrmse_all"] = 100*(np.sqrt(np.square(np.subtract(MODIS_filtered.sca, SIMU_filtered.sca)).mean())/(MODIS_filtered.mean().round(2)["sca"])).round(2)
    
    with open(out_folder+"/sca_"+reg+"_qm_report.csv", 'w') as csv_file:  
        writer = csv.writer(csv_file)
        for key, value in out_stat.items():
           writer.writerow([key, value])

    # idem but with min-max
    plt.figure(figsize=(8,3))

    # plot ens mean
    sca = data_ens_qm["ens"]["all"][reg]["sca"]["mean"]
    ts_sca = sca.sum(dim=("lat","lon")).groupby("hyear").mean()*250*250/1e6
    plt.plot(ts_sca.hyear, ts_sca.sca, lw=2, ms=6, mew=0, color=plt_dict["SIMU"]["col"]) #[0.1,0.1,0.9])

    sca = data_ens_qm["ens"]["all"][reg]["sca"]["max"]
    ts_sca_plus = sca.sum(dim=("lat","lon")).groupby("hyear").mean()*250*250/1e6
    
    sca = data_ens_qm["ens"]["all"][reg]["sca"]["min"]
    ts_sca_minus = sca.sum(dim=("lat","lon")).groupby("hyear").mean()*250*250/1e6
    plt.fill_between(ts_sca_plus.hyear, ts_sca_minus.sca, ts_sca_plus.sca, color=plt_dict["SIMU"]["col"], alpha=0.5)
    # plot MODIS
    plt.plot(ts_MODIS.index, ts_MODIS.sca, lw=2, color=plt_dict["MODIS"]["col"])
    
    plt.ylim((0,plt.gca().get_ylim()[1]))
    plt.ylabel("Yearly mean SCA (km2)")
    plt.grid(True, lw=0.4)
    
    plt.savefig(out_folder+"/sca_minmax_"+reg+"_qm_report_new.png")
    plt.savefig(out_folder+"/sca_minmax_"+reg+"_qm_report_new.svg")
    plt.close()        
    
    ########################## 
    # all run map - VS SCA MODIS
    (x_parc, y_parc) = contour_parc[reg]
    
    # Open Parc hillshade
    in_HS = "/home/hola/datos_seis/nacional_parques/data/COP30/250m/limite_"+reg+"_COP30_250m_UTM_DEM_HS.tif"
    HS = xr.open_dataset( in_HS )

    # load simu data
    var_data = 100*data_ens_qm["ens"]["all"][reg]["sca"]["mean"].sel(time=slice("2000-09-01","2023-08-31")).sum(dim={"time"})/(23*365)
    data2im = np.flipud(var_data.to_array().squeeze())
    data2im = np.ma.masked_where( data2im < 5, data2im)     
        
    data2im_MODIS = 100*data_MODIS[reg]["sca"].sel(time=slice("2000-09-01","2023-08-31")).sum(dim={"time"})/(23*365)
    data2im_MODIS = np.ma.masked_where(data2im_MODIS < 5, data2im_MODIS)    

    # Plot
    fig = plt.figure(figsize=(8,3))
    ax1 = plt.subplot(121)

    im1 = ax1.imshow( HS.band_data.squeeze(), cmap='Greys_r', vmin=0, vmax=255, extent = post_tools.get_xr_im_extent( HS ), origin="upper", interpolation="nearest", alpha=0.8)
    im2 = ax1.imshow( data2im, cmap='viridis', vmin = 0 , vmax = 100, extent = post_tools.get_xr_im_extent( var_data), origin="upper", interpolation="nearest")
    ax1.plot(x_parc, y_parc, color="#f6cd61", lw=0.5 )
    ax1.set_title("Simulation")
    
    ax2 = plt.subplot(122)
    im1 = ax2.imshow(HS.band_data.squeeze(), cmap = 'Greys_r', vmin=0, vmax=255, extent=post_tools.get_xr_im_extent( HS ), origin = "upper", interpolation = "nearest", alpha = 0.8)
    im3 = ax2.imshow( data2im_MODIS, cmap='viridis', vmin=0 , vmax=100, extent=post_tools.get_xr_im_extent( data_MODIS[reg]["sca"] ), origin = "upper", interpolation = "nearest")
    ax2.plot(x_parc, y_parc, color="#f6cd61", lw=0.5 )
    ax2.set_title("Observation")
    
    for ax in fig.get_axes():
        ax.set_xticks([])
        ax.set_yticks([])
    
    scalebar = ScaleBar(0.001, "km", length_fraction=0.25, width_fraction=0.03, fixed_value=10, font_properties={"size":9})
    plt.gca().add_artist(scalebar)
    
    fig.subplots_adjust(right=0.8)
    cbar_ax = fig.add_axes([0.90, 0.3, 0.02, 0.3])
    cb = fig.colorbar(im2, cax=cbar_ax)
    cb.set_ticks([0, 25, 50, 75, 100], labels=[0, 25, 50, 75, 100], fontsize=10)
    cb.ax.set_title('Snow probability\n2000-2020 (%)',fontsize=10, horizontalalignment="center")

    plt.savefig(out_folder+"/sca_"+reg+"_qm_map_report_new.png")
    plt.savefig(out_folder+"/sca_"+reg+"_qm_map_report_new.svg")
    plt.close()
        
    ########################## 
    # all mean run - VS SCA = f(ELEVATION)

    # load simu data
    var_data_mean = data_ens_qm["ens"]["all"][reg]["sca"]["mean"].sel(time=slice("2000-09-01","2023-08-31")).sum(dim={"time"})/(23*365)
    
    # SIMU
    data_grouped = var_data_mean.sca.groupby_bins( 'DEM', bin_elev_edges)
    boxplot_data, bin2plot = post_tools.get_group_values( data_grouped, 30 )
        
    # Create the boxplot
    fig, ax = plt.subplots()
    bp_simu = ax.boxplot(boxplot_data, positions=np.array(bin2plot)+35, widths=55, patch_artist=True, whis=1e9,
                boxprops = { "facecolor":plt_dict["SIMU"]["col"], "color":plt_dict["SIMU"]["col"]},
                capprops = { "color":plt_dict["SIMU"]["col"]},
                whiskerprops = { "color":plt_dict["SIMU"]["col"]},
                medianprops = { "color":"w"},
                )

    # MODIS
    data2im_MODIS = data_MODIS[reg]["sca"].sel(time=slice("2000-09-01","2023-08-31")).sum(dim={"time"})/(23*365)
    data_grouped = data2im_MODIS.groupby_bins( 'DEM', bin_elev_edges)
    boxplot_data, bin2plot = post_tools.get_group_values( data_grouped, 30 )

    # Create the boxplot
    bp_MODIS = ax.boxplot(boxplot_data, positions=np.array(bin2plot)-35, widths=55, patch_artist=True, whis=1e9,
                boxprops = { "facecolor":plt_dict["MODIS"]["col"], "color":plt_dict["MODIS"]["col"]},
                capprops = { "color":plt_dict["MODIS"]["col"]},
                whiskerprops = { "color":plt_dict["MODIS"]["col"]},
                medianprops = { "color":"w"},
                )

    # Customize the appearance of the boxplot
    ax.set_xlabel('Elevation (m)')
    ax.set_ylabel('Snow probability')
    ax.set_title('Snow probability per elevation - '+reg)
    ax.set_xticks(bin_elev_edges)
    ax.set_xticklabels([str(int(x)) for x in bin_elev_edges])
    ax.legend([bp_simu["boxes"][0], bp_MODIS["boxes"][0]], ['Simu.', 'Obs. MODIS'], loc='upper right')
    ax.axis([600 ,3300, 0, 1])
    ax.grid(lw=0.4)    
    
    plt.savefig(out_folder+"/sca_"+reg+"_elevation_report_new.png")
    plt.savefig(out_folder+"/sca_"+reg+"_elevation_report_new.svg")
    plt.close()
        

def plot_smod(reg, data_MODIS, data_ens_qm, contour_parc, plt_dict, out_folder):

    common_years, bin_elev_edges = def_misc()
    ########################## 
    # all run map - VS SMOD MODIS


    exp = "all" 
    print(reg)
    (x_parc, y_parc) = contour_parc[reg]
    
    # Open Parc hillshade
    
    in_HS = "/home/hola/datos_seis/nacional_parques/data/COP30/250m/limite_"+reg+"_COP30_250m_UTM_DEM_HS.tif"
    HS = xr.open_dataset( in_HS )
    
    # load simu data
    var_data = data_ens_qm["ens"]["all"][reg]["smod"]["mean"].sel(hyear=slice("2000","2022")).mean( dim = {"hyear"} )
    data2im = np.flipud(var_data.to_array().squeeze())
    data2im = np.ma.masked_where(data2im<0.05, data2im)   
    
    data_MODIS_r = data_MODIS[reg]["smod"].rename({'x': 'lon', 'y': 'lat'})
    # Resample data_MODIS onto the grid of var_data using interpolation
    resampled_data_MODIS = data_MODIS_r.interp(lat=var_data.lat, lon=var_data.lon, method="nearest")

    data2im_MODIS = resampled_data_MODIS.sel(hyear=slice("2000","2022")).mean(dim={"hyear"}) # data_MODIS[reg]["smod"].sel(hyear=slice("2000","2022")).mean(dim={"hyear"})
    data2im_MODIS = np.flipud(data2im_MODIS)
    data2im_MODIS = np.ma.masked_where(data2im_MODIS < 0.05, data2im_MODIS)    
#    ymin_MODIS, ymax_MODIS, xmin_MODIS, xmax_MODIS = np.min(resampled_data_MODIS.lat), np.max(resampled_data_MODIS.lat), np.min(resampled_data_MODIS.lon), np.max(resampled_data_MODIS.lon)
#    ymin_MODIS, ymax_MODIS, xmin_MODIS, xmax_MODIS = np.min(data_MODIS[reg]["smod"].y), np.max(data_MODIS[reg]["smod"].y), np.min(data_MODIS[reg]["smod"].x), np.max(data_MODIS[reg]["smod"].x)

    # Calculate statistics - mean bias, rmse

    #MODIS_filtered = ts_MODIS[ts_MODIS.index.isin(common_years)]
    #SIMU_filtered  = ts_sca.where(ts_sca.hyear.isin(common_years), drop = True)
    var4stat = data_ens_qm["ens"]["all"][reg]["smod"]["mean"].sel(hyear=slice("2000","2022"))
    MODIS4stat = resampled_data_MODIS.sel(hyear=slice("2000","2022"))

    mask = (~np.isnan(var4stat*MODIS4stat)).astype(float).smod.values
    mask[mask==0] = np.nan
    var4stat = (var4stat*mask).mean( dim = {"lon", "lat"} )
    MODIS4stat = (MODIS4stat*mask).mean( dim = {"lon", "lat"} )
#    MODIS4stat = data_MODIS[reg]["smod"].sel(hyear=slice("2000","2022")).mean(dim={"x", "y"})
    
    out_stat = {}
    out_stat["std_obs"] = np.std(MODIS4stat.values).round(2)
    out_stat["std_simu"] = np.std(var4stat.smod.values).round(2)
    out_stat["diff_median_all"] = np.round(np.median(var4stat.smod.values) - np.median(MODIS4stat.values),2)
    out_stat["diff_mean_all"] = np.round(np.mean(var4stat.smod.values) - np.mean(MODIS4stat.values),2)
    out_stat["rmse_all"] = np.sqrt(np.square(np.subtract(var4stat.smod.values, MODIS4stat.values)).mean()).round(2)
    
    diff_simu_obs = (data2im-data2im_MODIS).flatten()
    n_data = len(np.where(np.isnan(diff_simu_obs) == False )[0])
    out_stat["pct_nodata"] = np.round(100*len(np.where(np.isnan(diff_simu_obs) == True )[0])/diff_simu_obs.size, 2)
    out_stat["pct_within_10d"] = np.round((100*len(np.where( abs(diff_simu_obs) < 10)[0]) / n_data), 2)
    out_stat["pct_within_15d"] = np.round((100*len(np.where( abs(diff_simu_obs) < 15)[0]) / n_data), 2)
    out_stat["pct_within_20d"] = np.round((100*len(np.where( abs(diff_simu_obs) < 20)[0]) / n_data), 2)
    out_stat["pct_inf_m30d"] = np.round((100*len(np.where( diff_simu_obs < -30)[0]) / n_data), 2)
    out_stat["pct_inf_m20d"] = np.round((100*len(np.where( diff_simu_obs < -20)[0]) / n_data), 2)
    out_stat["pct_inf_m10d"] = np.round((100*len(np.where( diff_simu_obs < -10)[0]) / n_data), 2)
    out_stat["pct_sup_p10d"] = np.round((100*len(np.where( diff_simu_obs > 10)[0]) / n_data), 2)
    out_stat["pct_sup_p20d"] = np.round((100*len(np.where( diff_simu_obs > 20)[0]) / n_data), 2)
    out_stat["pct_sup_p30d"] = np.round((100*len(np.where( diff_simu_obs > 30)[0]) / n_data), 2)
    
    with open(out_folder+"/smod_"+reg+"_qm_report.csv", 'w') as csv_file:  
        writer = csv.writer(csv_file)
        for key, value in out_stat.items():
           writer.writerow([key, value])
           
    # Scatter plot simu SMOD againt obs SMOD
    fig = plt.figure(figsize=(4,4))
    #plt.plot(resampled_data_MODIS.sel(hyear=slice("2000","2022")).values.flatten(), data_ens_qm["ens"]["all"][reg]["smod"]["mean"].sel(hyear=slice("2000","2022")).to_array().squeeze().values.flatten(), ".k")
#    plt.plot(data2im_MODIS, data2im, ".k")
    plt.hist2d(data2im_MODIS.data.flatten(), data2im.data.flatten(), cmin=1, vmin=0, vmax = 150, bins=40, range = [[100,365],[100,365]])

    plt.plot([100,365],[100,365],"k",lw=0.5)
    plt.axis("square")
    plt.grid(True, lw=0.1)
    plt.xticks([ 0, 30, 60, 90, 120, 150, 180, 210, 240, 270, 300, 330, 365],
                  labels=[ "sep", "oct", "nov", "dec", "jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep"])
    plt.yticks([ 0, 30, 60, 90, 120, 150, 180, 210, 240, 270, 300, 330, 365],
                  labels=[ "sep", "oct", "nov", "dec", "jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep"])
    plt.axis([100,365,100,365])

    plt.title(reg)
    plt.xlabel("MODIS Snow Melt-Out Date")
    plt.ylabel("Simu. Snow Melt-Out Date")
    
    plt.savefig(out_folder+"/smod_"+reg+"_qm_scatter_new.png")
    plt.savefig(out_folder+"/smod_"+reg+"_qm_scatter_new.svg")
    plt.close()
    
        
    # Plot
    fig = plt.figure(figsize=(8,3))
    ax1 = plt.subplot(121)

    im1 = ax1.imshow( HS.band_data.squeeze(), cmap='Greys_r', vmin=0, vmax=255, extent = post_tools.get_xr_im_extent( HS ), origin="upper", interpolation="nearest", alpha=0.8)
    im2 = ax1.imshow( data2im, cmap='viridis', vmin = 0 , vmax = 365, extent = post_tools.get_xr_im_extent( var_data), origin="upper", interpolation="nearest")
    ax1.plot(x_parc, y_parc, color="#f6cd61", lw=0.5 )
    ax1.set_xticklabels("")
    ax1.set_yticklabels("")
    ax1.set_title("Simulation")
    
    ax2 = plt.subplot(122)
    im1 = ax2.imshow(HS.band_data.squeeze(), cmap='Greys_r', vmin=0, vmax=255, extent = post_tools.get_xr_im_extent( HS ), origin="upper", interpolation="nearest", alpha=0.8)
    im3 = ax2.imshow( data2im_MODIS, cmap='viridis', vmin = 0 , vmax = 365, extent = post_tools.get_xr_im_extent( resampled_data_MODIS ), origin="upper", interpolation="nearest")
    ax2.plot(x_parc, y_parc, color="#f6cd61", lw=0.5 )
    ax2.set_title("Observation")

    for ax in fig.get_axes():
        ax.set_xticks([])
        ax.set_yticks([])
        
    scalebar = ScaleBar(0.001, "km", length_fraction=0.25, width_fraction=0.03, fixed_value=10, font_properties={"size":9})
    plt.gca().add_artist(scalebar)
    
    fig.subplots_adjust(right=0.8)
    cbar_ax = fig.add_axes([0.90, 0.3, 0.02, 0.3])
    cb = fig.colorbar(im2, cax=cbar_ax, label="day of year")
    cb.ax.set_title('Snow melt-out date\n2000-2022',fontsize=10, horizontalalignment="center")
    cb.set_ticks([ 0, 100, 200, 300], labels=[ 0, 100, 200, 300])

    
    cbar_axt = cbar_ax.twinx()
    cbt = fig.colorbar(im2, cax=cbar_axt, label='date')
    cbt.set_ticks([ 0, 90, 180, 270, 365], labels=[ "sep", "dec", "mar", "jun", "sep"])

    cbt.ax.yaxis.set_ticks_position('left')
    cbt.ax.yaxis.set_label_position('left')
    
    cb.ax.yaxis.set_ticks_position('right')
    cb.ax.yaxis.set_label_position('right')
    
    plt.savefig(out_folder+"/smod_"+reg+"_qm_map_report_new.png")
    plt.savefig(out_folder+"/smod_"+reg+"_qm_map_report_new.svg")
    plt.close()
    
    ########################## 
    # all mean run - VS SMOD = f(ELEVATION)

    # load simu data
    var_data_mean = data_ens_qm["ens"]["all"][reg]["smod"]["mean"].sel(hyear=slice("2000","2022")).mean( dim = {"hyear"} )
    
    # SIMU
    data_grouped = var_data_mean.smod.groupby_bins( 'DEM', bin_elev_edges)
    boxplot_data, bin2plot = post_tools.get_group_values( data_grouped, 30 )
    boxplot_data_S, bin2plot = post_tools.get_group_values( data_grouped, 30 )


        
    # Create the boxplot
    fig, ax = plt.subplots()
    bp_simu = ax.boxplot(boxplot_data, positions=np.array(bin2plot)+35, widths=55, patch_artist=True, whis=1e9,
                boxprops = { "facecolor":plt_dict["SIMU"]["col"], "color":plt_dict["SIMU"]["col"]},
                capprops = { "color":plt_dict["SIMU"]["col"]},
                whiskerprops = { "color":plt_dict["SIMU"]["col"]},
                medianprops = { "color":"w"},
                )

    # MODIS
    data2im_MODIS = data_MODIS[reg]["smod"].sel(hyear=slice("2000","2022")).mean(dim={"hyear"})
    data_grouped = data2im_MODIS.groupby_bins( 'DEM', bin_elev_edges)
    boxplot_data, bin2plot = post_tools.get_group_values( data_grouped, 30 )
    boxplot_data_m, bin2plot = post_tools.get_group_values( data_grouped, 30 )

        
    # Create the boxplot
    bp_MODIS = ax.boxplot(boxplot_data, positions=np.array(bin2plot)-35, widths=55, patch_artist=True, whis=1e9,
                boxprops = { "facecolor":plt_dict["MODIS"]["col"], "color":plt_dict["MODIS"]["col"]},
                capprops = { "color":plt_dict["MODIS"]["col"]},
                whiskerprops = { "color":plt_dict["MODIS"]["col"]},
                medianprops = { "color":"w"},
                )


    # Customize the appearance of the boxplot
    ax.set_xlabel('Elevation (m)')
    ax.set_ylabel('Snow melt-out date')
    ax.set_yticks([ 0, 30, 60, 90, 120, 150, 180, 210, 240, 270, 300, 330, 365],
                  labels=[ "sep", "oct", "nov", "dec", "jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep"])
    ax.set_title('Snow melt-out date - '+reg)
    ax.set_xticks(bin_elev_edges)
    ax.set_xticklabels([str(int(x)) for x in bin_elev_edges])
    ax.legend([bp_simu["boxes"][0], bp_MODIS["boxes"][0]], ['Simu.', 'Obs. MODIS'], loc='upper right')
    ax.axis([600 ,3300, 0, 365])
    ax.grid(lw=0.4)    
    
    plt.savefig(out_folder+"/smod_"+reg+"_elevation_report_new.png")
    plt.savefig(out_folder+"/smod_"+reg+"_elevation_report_new.svg")
    plt.close()
  
def calc_data_2050(reg_list, data_ens_qm, var_list, dT_2050):    
    data_2050 = {}
    central_year_ref = 2005
    period_year_ref = (1990, 2020)
    for dT_stat in dT_2050.keys():
        print(dT_stat)
        data_2050[dT_stat] = {}
        for reg in reg_list:
            print(reg)
            data_2050[dT_stat][reg] = {}
    
            for var in var_list:
                data_2050[dT_stat][reg][var] = {}
            tair = data_ens_qm["ens"]["all"][reg]["tair"]["mean"]
            
            tair_mean_30yr =  tair.mean(dim=("lat","lon")).rolling(time=30*365, center=True).mean()
            
            tair_ref = tair_mean_30yr.where( tair_mean_30yr.hyear == central_year_ref, drop=True ).tair[0].values
            dtair_mean_30yr = tair_mean_30yr - tair_ref 
            
            for var_name in data_2050[dT_stat][reg].keys():
                print(var_name)
                var = data_ens_qm["ens"]["all"][reg][var_name]["mean"]
        
                # why stricly > AND < ? must not make 30 yr...
                data_2050[dT_stat][reg][var_name]["ref"] = var.where( (var.hyear> period_year_ref[0]) & (var.hyear < period_year_ref[1]), drop = True)
            
                for exp in dT_2050[dT_stat][reg].keys():
                    print(exp)
                    first_date_above_dT = np.where( dtair_mean_30yr.to_array() > dT_2050[dT_stat][reg][exp] )[1][0]
                    if tair_mean_30yr.time[first_date_above_dT].dohy> 182:
                        first_hyear_above_dT = tair_mean_30yr.hyear[first_date_above_dT].data.flatten()[0] + 1
                    else:
                        first_hyear_above_dT = tair_mean_30yr.hyear[first_date_above_dT].data.flatten()[0]
                    period_year_rcp = (first_hyear_above_dT-15, first_hyear_above_dT+15)
            
                    data_2050[dT_stat][reg][var_name][exp] = var.where( (var.hyear>= period_year_rcp[0]) & (var.hyear < period_year_rcp[1]), drop = True)
        
    return(data_2050)

def plot_swe_2050(reg, data_2050, dT_stat, out_folder):
    # PLOT FOR SWE WINTER-SPRING
    min_swe_mask = 0.01
    months_to_select_winter_spring = [12, 1, 2, 3, 4, 5]
    print(reg)
    (x_parc, y_parc) = contour_parc[reg]
    
    in_HS = "/home/hola/datos_seis/nacional_parques/data/COP30/250m/limite_"+reg+"_COP30_250m_UTM_DEM_HS.tif"
    HS = xr.open_dataset( in_HS )
    
    tt = data_2050[dT_stat][reg]["swed"]["ref"]
    SWE_0 = tt.sel(time=tt['time.month'].isin( months_to_select_winter_spring )).mean(dim="time").swed

    for exp in ["rcp26", "rcp45", "rcp85"]: #(SWE, dSWE) in enumerate([(SWE_rcp26, dSWE_rcp26), (SWE_rcp45, dSWE_rcp45), (SWE_rcp85, dSWE_rcp85)]):
        tt = data_2050[dT_stat][reg]["swed"][exp]
        SWE = tt.sel(time=tt['time.month'].isin( months_to_select_winter_spring )).mean(dim="time").swed
        dSWE = 100*(SWE - SWE_0)/SWE_0
        print(exp)
        fig = plt.figure( figsize=( 9, 4) )
        
        ax1 = plt.subplot(131)
        data2im = np.flipud(SWE_0.to_numpy())
        data2im = np.ma.masked_where(data2im < min_swe_mask, data2im)    
        data2im2mask = data2im.copy() 
        im1 = ax1.imshow( HS.band_data.squeeze(), cmap='Greys_r', vmin=0, vmax=255, extent = post_tools.get_xr_im_extent(HS), origin="upper", interpolation="nearest", alpha=0.8)
        im2 = ax1.imshow( data2im, cmap='viridis', vmin = 0 , vmax = plt_dict[reg]["max_swe"], extent = post_tools.get_xr_im_extent(HS), origin="upper", interpolation="nearest")
        ax1.set_title('Reference period (1990-2020)',fontsize=10, horizontalalignment="center")
    
        ax2 = plt.subplot(132)
        data2im = np.flipud(SWE.to_numpy())
        data2im = np.ma.masked_where(data2im < min_swe_mask, data2im)    
        im1 = ax2.imshow( HS.band_data.squeeze(), cmap='Greys_r', vmin=0, vmax=255, extent = post_tools.get_xr_im_extent(HS), origin="upper", interpolation="nearest", alpha=0.8)
        im3 = ax2.imshow( data2im, cmap='viridis', vmin = 0 , vmax = plt_dict[reg]["max_swe"], extent = post_tools.get_xr_im_extent(HS), origin="upper", interpolation="nearest")
        ax2.set_title('2050',fontsize=10, horizontalalignment="center")
    
        ax3 = plt.subplot(133)
        data2im = np.flipud(dSWE.to_numpy())
        data2im = np.ma.masked_where(data2im2mask < min_swe_mask, data2im)    
        im1 = ax3.imshow( HS.band_data.squeeze(), cmap='Greys_r', vmin=0, vmax=255, extent = post_tools.get_xr_im_extent(HS), origin="upper", interpolation="nearest", alpha=0.8)
        im4 = ax3.imshow( data2im, cmap='RdYlBu', vmin = -50, vmax = 50, extent = post_tools.get_xr_im_extent(HS), origin="upper", interpolation="nearest")
        ax3.set_title('2050 minus reference period',fontsize=10, horizontalalignment="center")
    
        for ax in fig.get_axes():
            ax.set_xticks([])
            ax.set_yticks([])
            ax.plot(x_parc, y_parc, color=plt_dict[reg]["col"], lw=0.8) #color="#f6cd61", lw=0.5 )
    
        scalebar = ScaleBar(0.001, "km", length_fraction=0.25, width_fraction=0.03, fixed_value=10, font_properties={"size":9})
        plt.gca().add_artist(scalebar)
        
        fig.subplots_adjust(bottom=0.1) #, right=0.8)  # Adjust as needed
        cbar_ax = fig.add_axes([0.15, 0.25, 0.3, 0.02])
        cb = fig.colorbar(im2, cax=cbar_ax, orientation='horizontal')
        cb.ax.set_title('Snow mass (m.w.e.)',fontsize=10, horizontalalignment="center")
    
        cbar_ax = fig.add_axes([0.50, 0.25, 0.3, 0.02])
        cb = fig.colorbar(im4, cax=cbar_ax, orientation='horizontal')
        cb.ax.set_title('Snow change (%)',fontsize=10, horizontalalignment="center")
        
        plt.tight_layout()    
        
        plt.savefig(out_folder+"/swed_dswed_2050_"+exp+"_"+reg+"_winspr_report_new.png")
        plt.savefig(out_folder+"/swed_dswed_2050_"+exp+"_"+reg+"_winspr_report_new.svg")
        plt.close()
        
    # PLOT FOR SWE 1st APRIL

    tt = data_2050[dT_stat][reg]["swed"]["ref"]
    SWE_0 = tt.sel(time=tt['dohy'].isin( [212] )).mean(dim="time").swed

    for exp in ["rcp26", "rcp45", "rcp85"]: #(SWE, dSWE) in enumerate([(SWE_rcp26, dSWE_rcp26), (SWE_rcp45, dSWE_rcp45), (SWE_rcp85, dSWE_rcp85)]):
        tt = data_2050[dT_stat][reg]["swed"][exp]
        SWE = tt.sel(time=tt['dohy'].isin( [212] )).mean(dim="time").swed
        dSWE = 100*(SWE - SWE_0)/SWE_0
        print(exp)
        fig = plt.figure( figsize=( 9, 4) )
        
        ax1 = plt.subplot(131)
        data2im = np.flipud(SWE_0.to_numpy())
        data2im = np.ma.masked_where(data2im < min_swe_mask, data2im)    
        data2im2mask = data2im.copy() 
        im1 = ax1.imshow( HS.band_data.squeeze(), cmap='Greys_r', vmin=0, vmax=255, extent = post_tools.get_xr_im_extent(HS), origin="upper", interpolation="nearest", alpha=0.8)
        im2 = ax1.imshow( data2im, cmap='viridis', vmin = 0 , vmax = plt_dict[reg]["max_swe"], extent = post_tools.get_xr_im_extent(HS), origin="upper", interpolation="nearest")
        ax1.set_title('Reference period (1990-2020)',fontsize=10, horizontalalignment="center")
    
        ax2 = plt.subplot(132)
        data2im = np.flipud(SWE.to_numpy())
        data2im = np.ma.masked_where(data2im < min_swe_mask, data2im)    
        im1 = ax2.imshow( HS.band_data.squeeze(), cmap='Greys_r', vmin=0, vmax=255, extent = post_tools.get_xr_im_extent(HS), origin="upper", interpolation="nearest", alpha=0.8)
        im3 = ax2.imshow( data2im, cmap='viridis', vmin = 0 , vmax = plt_dict[reg]["max_swe"], extent = post_tools.get_xr_im_extent(HS), origin="upper", interpolation="nearest")
        ax2.set_title('2050',fontsize=10, horizontalalignment="center")
    
        ax3 = plt.subplot(133)
        data2im = np.flipud(dSWE.to_numpy())
        data2im = np.ma.masked_where(data2im2mask < min_swe_mask, data2im)    
        im1 = ax3.imshow( HS.band_data.squeeze(), cmap='Greys_r', vmin=0, vmax=255, extent = post_tools.get_xr_im_extent(HS), origin="upper", interpolation="nearest", alpha=0.8)
        im4 = ax3.imshow( data2im, cmap='RdYlBu', vmin = -50, vmax = 50, extent = post_tools.get_xr_im_extent(HS), origin="upper", interpolation="nearest")
        ax3.set_title('2050 minus reference period',fontsize=10, horizontalalignment="center")
    
        for ax in fig.get_axes():
            ax.set_xticks([])
            ax.set_yticks([])
            ax.plot(x_parc, y_parc, color=plt_dict[reg]["col"], lw=0.8) #color="#f6cd61", lw=0.5 )
    
        scalebar = ScaleBar(0.001, "km", length_fraction=0.25, width_fraction=0.03, fixed_value=10, font_properties={"size":9})
        plt.gca().add_artist(scalebar)
        
        fig.subplots_adjust(bottom=0.1) #, right=0.8)  # Adjust as needed
        cbar_ax = fig.add_axes([0.15, 0.25, 0.3, 0.02])
        cb = fig.colorbar(im2, cax=cbar_ax, orientation='horizontal')
        cb.ax.set_title('Snow mass (m.w.e.)',fontsize=10, horizontalalignment="center")
    
        cbar_ax = fig.add_axes([0.50, 0.25, 0.3, 0.02])
        cb = fig.colorbar(im4, cax=cbar_ax, orientation='horizontal')
        cb.ax.set_title('Snow change (%)',fontsize=10, horizontalalignment="center")
        
        plt.tight_layout()    
        
        plt.savefig(out_folder+"/swed_dswed_2050_"+exp+"_"+reg+"_1stapril_report_new.png")
        plt.savefig(out_folder+"/swed_dswed_2050_"+exp+"_"+reg+"_1stapril_report_new.svg")
        plt.close()
    
    # dohy
    ddd = data_2050[dT_stat][reg]["swed"]["ref"].chunk(dict(time=-1))
    mean_swe_0 = ddd.sel(time=ddd['time.month'].isin( months_to_select_winter_spring )).mean(dim={"lat","lon"}).groupby("dohy").mean()
    dd = ddd["swed"].mean(dim=("lat","lon")).groupby(ddd.time.dohy)
    mean_swe_0_q1 = dd.quantile(0.25).values
    mean_swe_0_q3 = dd.quantile(0.75).values

    plt.figure()
    
    # Find the index of the maximum value of 'swed'
    plt.plot( mean_swe_0.dohy, mean_swe_0.swed, lw=2, color=plt_dict["SIMU"]["col"])
    max_swed = mean_swe_0['swed'].max().values.flatten()
    max_swed_index = mean_swe_0['swed'].argmax().compute()
    max_swed_dohy = mean_swe_0['dohy'][max_swed_index].compute().values.flatten()[0] # Use this index to get the corresponding 'dohy'
    plt.text( max_swed_dohy, max_swed, str(max_swed_index.values.flatten()[0]), color=plt_dict[exp]["col"])
    plt.vlines(max_swed_dohy, 0, max_swed,lw=0.5)  
        
    for exp in ["rcp26", "rcp45", "rcp85"]:       
        ddd = data_2050[dT_stat][reg]["swed"][exp].chunk(dict(time=-1))
        mean_swe = ddd.sel(time=ddd['time.month'].isin( months_to_select_winter_spring )).mean(dim={"lat","lon"}).groupby("dohy").mean()

        plt.plot( mean_swe.dohy, mean_swe.swed, lw=2, color=plt_dict[exp]["col"])
        plt.fill_between(np.array(list(dd.groups.keys())), mean_swe_0_q1, mean_swe_0_q3, color=plt_dict["SIMU"]["col"], alpha=0.5)

        # trick to get quantiles

        dd = ddd["swed"].mean(dim=("lat","lon")).groupby(ddd.time.dohy)
        mean_swe_q1 = dd.quantile(0.25).values
        mean_swe_q3 = dd.quantile(0.75).values
        plt.fill_between(np.array(list(dd.groups.keys())), mean_swe_q1, mean_swe_q3, color=plt_dict[exp]["col"], alpha=0.5)
        
        # Find the index of the maximum value of 'swed'
        max_swed = mean_swe['swed'].max().values.flatten()
        max_swed_index = mean_swe['swed'].argmax().compute()
        max_swed_dohy = mean_swe['dohy'][max_swed_index].compute().values.flatten()[0] # Use this index to get the corresponding 'dohy'
        plt.text( max_swed_dohy, max_swed, str(max_swed_index.values.flatten()[0]), color=plt_dict[exp]["col"])
        plt.vlines(max_swed_dohy, 0, max_swed,lw=0.5)        
        plt.grid(True, lw=0.2)
        plt.ylabel("Mean SWE (m w.e.)")
        plt.ylim([0, plt.gca().get_ylim()[1]])
        plt.xlabel("Time of year")
        plt.xticks([ 0, 30, 60, 90, 120, 150, 180, 210, 240, 270, 300, 330, 365],
                      labels=[ "sep", "oct", "nov", "dec", "jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep"])
        plt.xlim([30, 300])
        
    plt.savefig(out_folder+"/swed_mean_dohy_2050_"+reg+"_allexp_report_new.png")
    plt.savefig(out_folder+"/swed_mean_dohy_2050_"+reg+"_allexp_report_new.svg")
    plt.close()
            
            


def plot_smod_2050(reg, data_2050, dT_stat, out_folder):
    # PLOT FOR SMOD
    min_smod_mask = 100
    print(reg)
    (x_parc, y_parc) = contour_parc[reg]
    
    in_HS = "/home/hola/datos_seis/nacional_parques/data/COP30/250m/limite_"+reg+"_COP30_250m_UTM_DEM_HS.tif"
    HS = xr.open_dataset( in_HS )

    common_years, bin_elev_edges = def_misc()

    smod_0 = data_2050[dT_stat][reg]["smod"]["ref"].chunk({'lat': 50, 'lon': 50, 'hyear':5}).mean(dim="hyear").smod
    smod_0 = smod_0.chunk({'lat': 50, 'lon': 50})

    for exp in ["rcp26", "rcp45", "rcp85"]: #(SWE, dSWE) in enumerate([(SWE_rcp26, dSWE_rcp26), (SWE_rcp45, dSWE_rcp45), (SWE_rcp85, dSWE_rcp85)]):
        print(exp)
        smod = data_2050[dT_stat][reg]["smod"][exp].mean(dim="hyear").smod
        smod = smod.chunk({'lat': 50, 'lon': 50})
        
        dsmod = smod - smod_0 
        fig = plt.figure( figsize=( 9, 4) )
        
        ax1 = plt.subplot(131)
        data2im = np.flipud(smod_0.to_numpy())
        data2im = np.ma.masked_where(data2im < min_smod_mask, data2im)    
        im1 = ax1.imshow( HS.band_data.squeeze(), cmap='Greys_r', vmin=0, vmax=255, extent = post_tools.get_xr_im_extent(HS), origin="upper", interpolation="nearest", alpha=0.8)
        im2 = ax1.imshow( data2im, cmap='viridis', vmin = 100 , vmax = 365, extent = post_tools.get_xr_im_extent(HS), origin="upper", interpolation="nearest")
        ax1.set_title('Reference period (1990-2020)',fontsize=10, horizontalalignment="center")
    
        ax2 = plt.subplot(132)
        data2im = np.flipud(smod.to_numpy())
        data2im = np.ma.masked_where(data2im < min_smod_mask, data2im)   
        data2im2mask = data2im.copy() 
        im1 = ax2.imshow( HS.band_data.squeeze(), cmap='Greys_r', vmin=0, vmax=255, extent = post_tools.get_xr_im_extent(HS), origin="upper", interpolation="nearest", alpha=0.8)
        im3 = ax2.imshow( data2im, cmap='viridis', vmin = 100 , vmax = 365, extent = post_tools.get_xr_im_extent(HS), origin="upper", interpolation="nearest")
        ax2.set_title('2050',fontsize=10, horizontalalignment="center")
    
        ax3 = plt.subplot(133)
        data2im = np.flipud(dsmod.to_numpy())
        data2im = np.ma.masked_where(data2im2mask < min_smod_mask, data2im)    
        im1 = ax3.imshow( HS.band_data.squeeze(), cmap='Greys_r', vmin=0, vmax=255, extent = post_tools.get_xr_im_extent(HS), origin="upper", interpolation="nearest", alpha=0.8)
        im4 = ax3.imshow( data2im, cmap='RdYlBu', vmin = -50, vmax = 50, extent = post_tools.get_xr_im_extent(HS), origin="upper", interpolation="nearest")
        ax3.set_title('2050 minus reference period',fontsize=10, horizontalalignment="center")
    
        for ax in fig.get_axes():
            ax.set_xticks([])
            ax.set_yticks([])
            ax.plot(x_parc, y_parc, color=plt_dict[reg]["col"], lw=0.8) #color="#f6cd61", lw=0.5 )
    
        scalebar = ScaleBar(0.001, "km", length_fraction=0.25, width_fraction=0.03, fixed_value=10, font_properties={"size":9})
        plt.gca().add_artist(scalebar)
        
        #fig.subplots_adjust(right=0.7)
        fig.subplots_adjust(bottom=0.1) #, right=0.8)  # Adjust as needed
        cbar_ax = fig.add_axes([0.15, 0.25, 0.3, 0.02])
        cb = fig.colorbar(im2, cax=cbar_ax, orientation='horizontal')
#        cb.set_ticks([100, 150, 200, 250, 300, 350], labels=[100, 150, 200, 250, 300, 350], fontsize=10)
        cb.ax.set_title('Snow melt-out date',fontsize=10, horizontalalignment="center")
 #       cbar_axt = cbar_ax.twiny()
 #       cbt = fig.colorbar(im2, cax=cbar_axt, orientation='horizontal', label='date')
        cb.set_ticks([ 120, 180, 240, 300, 364], labels=[ "jan", "mar", "may", "jul", "sep"])
        
        cbar_ax = fig.add_axes([0.50, 0.25, 0.3, 0.02])
        cb = fig.colorbar(im4, cax=cbar_ax, orientation='horizontal')
    #    cb.set_ticks([0, 25, 50, 75, 100], labels=[0, 25, 50, 75, 100], fontsize=10)
        cb.ax.set_title('Snow melt-out day change (day)',fontsize=10, horizontalalignment="center")
        
        plt.tight_layout()    
            
        plt.savefig(out_folder+"/smod_dsmod_2050_"+exp+"_temp_"+reg+"_report_new.png")
        plt.savefig(out_folder+"/smod_dsmod_2050_"+exp+"_temp_"+reg+"_report_new.svg")
        plt.close()
            
    # All that but with elevation
    # PLOT AGAINST ELEVATION
    # PLOT FOR SMOD
       
    # REF PERIOD    
    data_grouped = smod_0.groupby_bins( 'DEM', bin_elev_edges)
    boxplot_data0, bin2plot0 = post_tools.get_group_values(data_grouped, 30)
    
    for exp in ["rcp26", "rcp45", "rcp85"]: #(SWE, dSWE) in enumerate([(SWE_rcp26, dSWE_rcp26), (SWE_rcp45, dSWE_rcp45), (SWE_rcp85, dSWE_rcp85)]):
        print(exp)
        smod = data_2050[dT_stat][reg]["smod"][exp].mean(dim="hyear").smod    
                
        # Create the boxplot
        fig, ax = plt.subplots()
        bp_simu_ref = ax.boxplot(boxplot_data0, positions=np.array(bin2plot0)-35, widths=55, patch_artist=True, whis=1e9,
                    boxprops = { "facecolor":plt_dict["SIMU"]["col"], "color":plt_dict["SIMU"]["col"]},
                    capprops = { "color":plt_dict["SIMU"]["col"]},
                    whiskerprops = { "color":plt_dict["SIMU"]["col"]},
                    medianprops = { "color":"w"},
                    )
        
        # 2050 PERIOD    
        data_grouped = smod.groupby_bins( 'DEM', bin_elev_edges)
        boxplot_data, bin2plot = post_tools.get_group_values(data_grouped, 30)
            
        # Create the boxplot
        bp_simu = ax.boxplot(boxplot_data, positions=np.array(bin2plot)+35, widths=55, patch_artist=True, whis=1e9,
                    boxprops = { "facecolor":plt_dict["SIMU"]["col"], "color":plt_dict[exp]["col"]},
                    capprops = { "color":plt_dict[exp]["col"]},
                    whiskerprops = { "color":plt_dict[exp]["col"]},
                    medianprops = { "color":"w"},
                    )
    
        # Customize the appearance of the boxplot
        ax.set_xlabel('Elevation (m)')
        ax.set_ylabel('Snow melt-out date')
        ax.set_yticks([ 0, 30, 60, 90, 120, 150, 180, 210, 240, 270, 300, 330, 365],
                      labels=[ "sep", "oct", "nov", "dec", "jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep"])
        ax.set_title('Snow melt-out date per elevation - '+reg)
        ax.set_xticks(bin_elev_edges)
        ax.set_xticklabels([str(int(x)) for x in bin_elev_edges])
        ax.legend([bp_simu_ref["boxes"][0], bp_simu["boxes"][0]], ['Ref. period', '2050'], loc='upper right')
        ax.axis([600 ,3300, 0, 365])
        ax.grid(lw=0.4)  
        
        plt.savefig(out_folder+"/smod_ref_2050_elevation_"+reg+"_"+exp+"_report_new.png")
        plt.savefig(out_folder+"/smod_ref_2050_elevation_"+reg+"_"+exp+"_report_new.svg")
        
def write_stat_2050(reg_list, var_list, data_2050, dT_2050, out_folder):
    # EXPORT STAT   
    #data_2050 = {}
    central_year_ref = 2005
    period_year_ref = (1990, 2020)
    out_stat = {}
    months_to_select_winter_spring = [12,1,2,3,4,5]

    # OUTPUT STAT
    for var in var_list:
        out_stat[var] = {}
        for dT_stat in dT_2050.keys(): # ["median"]: #
            out_stat[var][dT_stat] = {}
            for reg in reg_list:
                print(reg)
                out_stat[var][dT_stat][reg] = {}
                out_stat[var][dT_stat][reg] = {"in_PN":{}, "all":{} }
        
                # dswed_dtair = xr.concat(delta_data_map[reg][var]["all"]["dSWE_rel"], dim = "dtair") # , "dprec"] )
                if "time" in data_2050[dT_stat][reg][var]["ref"].coords:
                    if var in ["prec", "rpre"]:
                        ddd = data_2050[dT_stat][reg][var]["ref"].chunk(dict(time=-1))
                        mean_var_0 = ddd.sel(time=ddd['time.month'].isin( months_to_select_winter_spring )).sum(dim={"time"})[var] #.swed
                
                        # dSWE_0 = dswed_dtair.interp( coords = {"dtair":0}, method = "nearest")
                        out_stat[var][dT_stat][reg]["in_PN"]["ref"] = mean_var_0.where(mean_var_0.MSK==1).mean().values.flatten()[0]
                        out_stat[var][dT_stat][reg]["all"]["ref"] = mean_var_0.mean().values.flatten()[0]
                
                        for exp in ["rcp26", "rcp45", "rcp85"]:
                            ddd = data_2050[dT_stat][reg][var][exp].chunk(dict(time=-1))
                            mean_var = ddd.sel(time=ddd['time.month'].isin( months_to_select_winter_spring )).sum(dim={"time"})[var]
                            # SIMU
                            out_stat[var][dT_stat][reg]["in_PN"][exp] = mean_var.where(mean_var.MSK==1).mean().values.flatten()[0]
                            out_stat[var][dT_stat][reg]["all"][exp]   = mean_var.mean().values.flatten()[0]# / mean_var_.where(mean_var.MSK==1).count().values.flatten()[0]
                
                        pd.DataFrame(out_stat[var]).T.to_csv(out_folder+"/"+var+"_PN_2050_"+reg+"_"+dT_stat+".csv")
                    else:
                        ddd = data_2050[dT_stat][reg][var]["ref"].chunk(dict(time=-1))
                        mean_var_0 = ddd.sel(time=ddd['time.month'].isin( months_to_select_winter_spring )).mean(dim={"time"})[var] #.swed
                
                        # dSWE_0 = dswed_dtair.interp( coords = {"dtair":0}, method = "nearest")
                        out_stat[var][dT_stat][reg]["in_PN"]["ref"] = mean_var_0.where(mean_var_0.MSK==1).mean().values.flatten()[0]
                        out_stat[var][dT_stat][reg]["all"]["ref"] = mean_var_0.mean().values.flatten()[0]
                
                        for exp in ["rcp26", "rcp45", "rcp85"]:
                            ddd = data_2050[dT_stat][reg][var][exp].chunk(dict(time=-1))
                            mean_var = ddd.sel(time=ddd['time.month'].isin( months_to_select_winter_spring )).mean(dim={"time"})[var]
                            # SIMU
                            out_stat[var][dT_stat][reg]["in_PN"][exp] = mean_var.where(mean_var.MSK==1).mean().values.flatten()[0]
                            out_stat[var][dT_stat][reg]["all"][exp]   = mean_var.mean().values.flatten()[0]# / mean_var_.where(mean_var.MSK==1).count().values.flatten()[0]
                
                        pd.DataFrame(out_stat[var]).T.to_csv(out_folder+"/"+var+"_PN_2050_"+reg+"_"+dT_stat+".csv")
                elif "hyear" in data_2050[dT_stat][reg][var]["ref"].coords:
                    if var == "smod":
                        ddd = data_2050[dT_stat][reg][var]["ref"].chunk(dict(hyear=-1))
                        mean_var_0 = ddd.mean(dim={"hyear"})[var] #.swed
                
                        # dSWE_0 = dswed_dtair.interp( coords = {"dtair":0}, method = "nearest")
                        out_stat[var][dT_stat][reg]["in_PN"]["ref"] = np.percentile( mean_var_0.where(mean_var_0.MSK==1).fillna(0).values, 95)
                        out_stat[var][dT_stat][reg]["all"]["ref"] = np.percentile( mean_var_0.fillna(0).values, 95)
                
                        for exp in ["rcp26", "rcp45", "rcp85"]:
                            ddd = data_2050[dT_stat][reg][var][exp].chunk(dict(hyear=-1))
                            mean_var = ddd.mean(dim={"hyear"})[var]
                            # SIMU
                            out_stat[var][dT_stat][reg]["in_PN"][exp] = np.percentile( mean_var.where(mean_var.MSK==1).fillna(0).values, 95)
                            out_stat[var][dT_stat][reg]["all"][exp]   = np.percentile( mean_var.fillna(0).values, 95) # / mean_var_.where(mean_var.MSK==1).count().values.flatten()[0]
                                    
                    else:
                        ddd = data_2050[dT_stat][reg][var]["ref"].chunk(dict(hyear=-1))
                        mean_var_0 = ddd.mean(dim={"hyear"})[var] #.swed
                
                        # dSWE_0 = dswed_dtair.interp( coords = {"dtair":0}, method = "nearest")
                        out_stat[var][dT_stat][reg]["in_PN"]["ref"] = mean_var_0.where(mean_var_0.MSK==1).mean().values.flatten()[0]
                        out_stat[var][dT_stat][reg]["all"]["ref"] = mean_var_0.mean().values.flatten()[0]
                
                        for exp in ["rcp26", "rcp45", "rcp85"]:
                            ddd = data_2050[dT_stat][reg][var][exp].chunk(dict(hyear=-1))
                            mean_var = ddd.mean(dim={"hyear"})[var]
                            # SIMU
                            out_stat[var][dT_stat][reg]["in_PN"][exp] = mean_var.where(mean_var.MSK==1).mean().values.flatten()[0]
                            out_stat[var][dT_stat][reg]["all"][exp]   = mean_var.mean().values.flatten()[0]# / mean_var_.where(mean_var.MSK==1).count().values.flatten()[0]
            
                pd.DataFrame(out_stat[var][dT_stat][reg]).T.to_csv(out_folder+"/"+var+"_PN_2050_"+reg+"_"+dT_stat+".csv")
    return out_stat

def print_stat_2050():
    for reg in reg_list:
        print(reg + " SWE change RCP 4.5")
        stat_of_interest_median = int(100*(out_stat["swed"]["median"][reg]["mean"]["in_PN"]["rcp45"]-out_stat["swed"]["median"][reg]["mean"]["in_PN"]["ref"])/out_stat["swed"]["median"][reg]["mean"]["in_PN"]["ref"])
        stat_of_interest_1Q = int(100*(out_stat["swed"]["1Q"][reg]["mean"]["in_PN"]["rcp45"]-out_stat["swed"]["1Q"][reg]["mean"]["in_PN"]["ref"])/out_stat["swed"]["1Q"][reg]["mean"]["in_PN"]["ref"])
        stat_of_interest_3Q = int(100*(out_stat["swed"]["3Q"][reg]["mean"]["in_PN"]["rcp45"]-out_stat["swed"]["3Q"][reg]["mean"]["in_PN"]["ref"])/out_stat["swed"]["3Q"][reg]["mean"]["in_PN"]["ref"])

        print(str(stat_of_interest_median) + "[ "+str(stat_of_interest_1Q)+"; "+str(stat_of_interest_3Q)+" ]")
          
    for reg in reg_list:
        print(reg + " SMOD change RCP 4.5")
        stat_of_interest_median = int((out_stat["smod"]["median"][reg]["mean"]["in_PN"]["rcp45"]-out_stat["smod"]["median"][reg]["mean"]["in_PN"]["ref"]))
        stat_of_interest_1Q = int((out_stat["smod"]["1Q"][reg]["mean"]["in_PN"]["rcp45"]-out_stat["smod"]["1Q"][reg]["mean"]["in_PN"]["ref"]))
        stat_of_interest_3Q = int((out_stat["smod"]["3Q"][reg]["mean"]["in_PN"]["rcp45"]-out_stat["smod"]["3Q"][reg]["mean"]["in_PN"]["ref"]))

        print(str(stat_of_interest_median) + "[ "+str(stat_of_interest_1Q)+"; "+str(stat_of_interest_3Q)+" ]")        

    for reg in reg_list:
        print(reg + " SMOD change RCP 4.5")
        stat_of_interest_median = int((out_stat["smod"]["median"][reg]["mean"]["in_PN"]["rcp85"]-out_stat["smod"]["median"][reg]["mean"]["in_PN"]["ref"]))
        stat_of_interest_1Q = int((out_stat["smod"]["1Q"][reg]["mean"]["in_PN"]["rcp85"]-out_stat["smod"]["1Q"][reg]["mean"]["in_PN"]["ref"]))
        stat_of_interest_3Q = int((out_stat["smod"]["3Q"][reg]["mean"]["in_PN"]["rcp85"]-out_stat["smod"]["3Q"][reg]["mean"]["in_PN"]["ref"]))

        print(str(stat_of_interest_median) + "[ "+str(stat_of_interest_1Q)+"; "+str(stat_of_interest_3Q)+" ]")    
        
    for reg in reg_list:
        print(reg + " Precip change RCP 4.5")
        stat_of_interest_median = int(100*(out_stat["prec"]["median"][reg]["mean"]["in_PN"]["rcp45"]-out_stat["prec"]["median"][reg]["mean"]["in_PN"]["ref"])/out_stat["prec"]["median"][reg]["mean"]["in_PN"]["ref"])
        stat_of_interest_1Q = int(100*(out_stat["prec"]["1Q"][reg]["mean"]["in_PN"]["rcp45"]-out_stat["prec"]["1Q"][reg]["mean"]["in_PN"]["ref"])/out_stat["prec"]["1Q"][reg]["mean"]["in_PN"]["ref"])
        stat_of_interest_3Q = int(100*(out_stat["prec"]["3Q"][reg]["mean"]["in_PN"]["rcp45"]-out_stat["prec"]["3Q"][reg]["mean"]["in_PN"]["ref"])/out_stat["prec"]["3Q"][reg]["mean"]["in_PN"]["ref"])

        print(str(stat_of_interest_median) + "[ "+str(stat_of_interest_1Q)+"; "+str(stat_of_interest_3Q)+" ]")
    
    for reg in reg_list:
        print(reg + " Precip change RCP 4.5")
        stat_of_interest_median = int(100*((out_stat["prec"]["median"][reg]["mean"]["all"]["rcp45"]-out_stat["prec"]["median"][reg]["mean"]["all"]["ref"])/out_stat["prec"]["median"][reg]["mean"]["all"]["ref"]))
        stat_of_interest_1Q = int(100*((out_stat["prec"]["1Q"][reg]["mean"]["all"]["rcp45"]-out_stat["prec"]["1Q"][reg]["mean"]["all"]["ref"])/out_stat["prec"]["1Q"][reg]["mean"]["all"]["ref"]))
        stat_of_interest_3Q = int(100*((out_stat["prec"]["3Q"][reg]["mean"]["all"]["rcp45"]-out_stat["prec"]["3Q"][reg]["mean"]["all"]["ref"])/out_stat["prec"]["3Q"][reg]["mean"]["all"]["ref"]))

        print(str(stat_of_interest_median) + "[ "+str(stat_of_interest_1Q)+"; "+str(stat_of_interest_3Q)+" ]")
    
plt_dict = {
    "historical":{"col":"#4a4e4d", "mrk":"v"},
    "rcp26":{"col":"#f6cd61", "mrk":"^"},
    "rcp45":{"col":"#fe8a71", "mrk":"^"},
    "rcp85":{"col":"#851e3e", "mrk":">"},
    "AIGUE":{"col":"#ee4035","mrk":"H","max_swe":0.5},
    "GUADA":{"col":"#f37736","mrk":"X","max_swe":0.5},
    "ORDES":{"col":"#ffe800","mrk":"o","max_swe":2},
    "PICOS":{"col":"#7bc043","mrk":"d","max_swe":2},
    "SIERR":{"col":"#0392cf","mrk":"s","max_swe":1},
    "TEIDE":{"col":"#8874a3","mrk":"s","max_swe":0.3},
    "DJF":{"col":"#0057e7","mrk":"*"},
    "MAM":{"col":"#008744","mrk":"s"},
    "JJA":{"col":"#d62d20","mrk":"."},
    "SON":{"col":"#ffa700","mrk":"^"},
    "MODIS":{"col":"#283655"},
    "SIMU": {"col":"#35a79c"},
    }
    
cm2in = 1/2.54 

    
# ALL YEAR - CALCULATED LOCALLY CALCULATED ON 2023-03-05
dT_2050 = {"median":
 {'AIGUE': {'rcp26': 0.804, 'rcp45': 1.095, 'rcp85': 1.588},
  'GUADA': {'rcp26': 0.798, 'rcp45': 1.095, 'rcp85': 1.567},
  'ORDES': {'rcp26': 0.779, 'rcp45': 1.059, 'rcp85': 1.521},
  'PICOS': {'rcp26': 0.564, 'rcp45': 0.765, 'rcp85': 1.115},
  'SIERR': {'rcp26': 0.786, 'rcp45': 1.094, 'rcp85': 1.578},
  'TEIDE': {'rcp26': 0.776, 'rcp45': 0.972, 'rcp85': 1.321}},
 "1Q":
{'AIGUE': {'rcp26': 0.662, 'rcp45': 0.979, 'rcp85': 1.388},
 'GUADA': {'rcp26': 0.658, 'rcp45': 0.976, 'rcp85': 1.375},
 'ORDES': {'rcp26': 0.644, 'rcp45': 0.947, 'rcp85': 1.333},
 'PICOS': {'rcp26': 0.466, 'rcp45': 0.685, 'rcp85': 0.967},
 'SIERR': {'rcp26': 0.653, 'rcp45': 0.971, 'rcp85': 1.379},
 'TEIDE': {'rcp26': 0.581, 'rcp45': 0.864, 'rcp85': 1.154}},
"3Q":
{'AIGUE': {'rcp26': 0.930, 'rcp45': 1.248, 'rcp85': 1.872},
 'GUADA': {'rcp26': 0.926, 'rcp45': 1.241, 'rcp85': 1.840},
 'ORDES': {'rcp26': 0.900, 'rcp45': 1.202, 'rcp85': 1.788},
 'PICOS': {'rcp26': 0.652, 'rcp45': 0.871, 'rcp85': 1.326},
 'SIERR': {'rcp26': 0.920, 'rcp45': 1.243, 'rcp85': 1.858},
 'TEIDE': {'rcp26': 0.886, 'rcp45': 1.114, 'rcp85': 1.391}
 }}
    
out_folder = "/home/hola/datos_seis/port_transfert/report"
reg_list = ["AIGUE", "GUADA", "ORDES", "PICOS", "SIERR", "TEIDE"]

var_list = ["snod", "swed", "prec", "relh", "rpre", "smlt", "ssub", "tair", "var5", "var6"]


if os.path.isfile("/home/hola/datos_seis/nacional_parques/prod/pkl/sm4viz/data_ens_qm.pkl") == False:
    data_MODIS, data_ens_qm, contour_parc = read_data(reg_list, var_list)
    # writing data
    with open("/home/hola/datos_seis/nacional_parques/prod/pkl/sm4viz/data_MODIS.pkl", "wb") as handle:
        pickle.dump( data_MODIS, handle, protocol = pickle.HIGHEST_PROTOCOL) 
        
    with open("/home/hola/datos_seis/nacional_parques/prod/pkl/sm4viz/data_ens_qm.pkl", "wb") as handle:
        pickle.dump( data_ens_qm, handle, protocol = pickle.HIGHEST_PROTOCOL) 
        
    with open("/home/hola/datos_seis/nacional_parques/prod/pkl/sm4viz/contour_parc.pkl", "wb") as handle:
        pickle.dump( contour_parc, handle, protocol = pickle.HIGHEST_PROTOCOL)  
else:
    # reading data
    with open("/home/hola/datos_seis/nacional_parques/prod/pkl/sm4viz/data_MODIS.pkl", "rb") as handle:
        data_MODIS = pickle.load( handle)
        
    with open("/home/hola/datos_seis/nacional_parques/prod/pkl/sm4viz/data_ens_qm.pkl", "rb") as handle:
        data_ens_qm = pickle.load( handle) 
        
    with open("/home/hola/datos_seis/nacional_parques/prod/pkl/sm4viz/contour_parc.pkl", "rb") as handle:
        contour_parc = pickle.load( handle)  
        

if os.path.isfile("/home/hola/datos_seis/nacional_parques/prod/pkl/sm4viz/data_2050.pkl") == False:
    var_list = ["snod", "swed", "prec", "relh", "rpre", "smlt", "ssub", "tair", "csub", "wsub"]
    data_2050 = calc_data_2050( reg_list, data_ens_qm, var_list+["sca", "smod"], dT_2050)
    # writing data_2050
    with open("/home/hola/datos_seis/nacional_parques/prod/pkl/sm4viz/data_2050.pkl", "wb") as handle:
        pickle.dump( data_2050, handle, protocol = pickle.HIGHEST_PROTOCOL)  
else:
    with open("/home/hola/datos_seis/nacional_parques/prod/pkl/sm4viz/data_2050.pkl", "rb") as handle:
        data_2050 = pickle.load( handle)
    
    
    
var_list = ["snod", "swed", "prec", "relh", "rpre", "smlt", "ssub", "tair", "csub", "wsub", "sca", "smod"]
var_list = ["prec", "rpre", "smlt", "ssub", "tair", "csub", "wsub"]

#var_list = ["sca", "smod"]
var_list = [ "prec", "rpre", "smlt", "ssub", "tair", "csub", "wsub"] 

if os.path.isfile("/home/hola/datos_seis/nacional_parques/prod/pkl/sm4viz/out_stat_2050_meteo_process.pkl") == False:
    out_stat_2050 = write_stat_2050(reg_list, var_list, data_2050, dT_2050, out_folder)
    with open("/home/hola/datos_seis/nacional_parques/prod/pkl/sm4viz/out_stat_2050_meteo_process.pkl", "wb") as handle:
        pickle.dump( out_stat_2050, handle, protocol = pickle.HIGHEST_PROTOCOL)  
else:
    with open("/home/hola/datos_seis/nacional_parques/prod/pkl/sm4viz/out_stat_2050_meteo_process.pkl", "rb") as handle:
        out_stat_2050 = pickle.load( handle)

var_list = [ "swed", "sca", "smod"]
if os.path.isfile("/home/hola/datos_seis/nacional_parques/prod/pkl/sm4viz/out_stat_2050_meteo_snow.pkl") == False:
    out_stat_2050 = write_stat_2050(reg_list, var_list, data_2050, dT_2050, out_folder)
    with open("/home/hola/datos_seis/nacional_parques/prod/pkl/sm4viz/out_stat_2050_meteo_snow.pkl", "wb") as handle:
        pickle.dump( out_stat_2050, handle, protocol = pickle.HIGHEST_PROTOCOL)  
else:
    with open("/home/hola/datos_seis/nacional_parques/prod/pkl/sm4viz/out_stat_2050_meteo_snow.pkl", "rb") as handle:
        out_stat_2050 = pickle.load( handle)

for reg in reg_list:
    plot_sca(reg, data_MODIS, data_ens_qm, contour_parc, plt_dict, out_folder)
    plot_smod(reg, data_MODIS, data_ens_qm, contour_parc, plt_dict, out_folder)
    plot_swe_2050( reg, data_2050, "median", out_folder)
    plot_smod_2050(reg, data_2050, "median", out_folder)
     
    
# plot change of processes
for reg in reg_list:
    prec_ref = out_stat_2050["prec"]["median"][reg]["in_PN"]["ref"]
    prec_2050  = out_stat_2050["prec"]["median"][reg]["in_PN"]["rcp45"]
    
    accu_ref = out_stat_2050["prec"]["median"][reg]["in_PN"]["ref"] - out_stat_2050["rpre"]["median"][reg]["in_PN"]["ref"]
    accu_2050  = out_stat_2050["prec"]["median"][reg]["in_PN"]["rcp45"] - out_stat_2050["rpre"]["median"][reg]["in_PN"]["rcp45"]
    
    ssub_ref = out_stat_2050["ssub"]["median"][reg]["in_PN"]["ref"]
    ssub_2050 = out_stat_2050["ssub"]["median"][reg]["in_PN"]["rcp45"]
    
    csub_ref = out_stat_2050["csub"]["median"][reg]["in_PN"]["ref"]
    csub_2050 = out_stat_2050["csub"]["median"][reg]["in_PN"]["rcp45"]
    
    wsub_ref = out_stat_2050["wsub"]["median"][reg]["in_PN"]["ref"]
    wsub_2050 = out_stat_2050["wsub"]["median"][reg]["in_PN"]["rcp45"]
    
    smlt_ref  = accu_ref  - ssub_ref  - csub_ref  - wsub_ref # out_stat_2050["smlt"]["median"][reg]["in_PN"]["ref"] - accu_ref - ssub_ref - csub_ref - wsub_ref
    smlt_2050 = accu_2050 - ssub_2050 - csub_2050 - wsub_2050 # out_stat_2050["smlt"]["median"][reg]["in_PN"]["rcp45"] - accu_2050 - ssub_2050 - csub_2050 -wsub_2050
    
    refr_ref  = out_stat_2050["smlt"]["median"][reg]["in_PN"]["ref"]   - smlt_ref
    refr_2050 = out_stat_2050["smlt"]["median"][reg]["in_PN"]["rcp45"] - smlt_2050
    
    s0ref = out_stat_2050["smlt"]["median"][reg]["in_PN"]["ref"]
    s02050 = out_stat_2050["smlt"]["median"][reg]["in_PN"]["rcp45"]
    
    norm_factor = 100./accu_ref
    
    out_stat = {"prec_ref":prec_ref,
                "prec_2050":prec_2050,
                "accu_ref":accu_ref,
                "accu_2050":accu_2050,
                "ssub_ref":ssub_ref,
                "ssub_2050":ssub_2050,
                "csub_ref":csub_ref,
                "csub_2050":csub_2050,
                "wsub_ref":wsub_ref,
                "wsub_2050":wsub_2050,
                "smlt_ref":smlt_ref,
                "smlt_2050":smlt_2050
                }
            
    with open(out_folder+"/process_"+reg+"_qm_report.csv", 'w') as csv_file:  
        writer = csv.writer(csv_file)
        for key, value in out_stat.items():
           writer.writerow([key, value])
           
    plt.figure()
    
    # ref period
    plt.bar(1, norm_factor*accu_ref, color="#03396c") # solid precip 
    plt.bar(2, norm_factor*refr_ref, color="#6497b1") # refreezing
    plt.bar(3, norm_factor*(smlt_ref+ssub_ref+csub_ref+wsub_ref), color="#851e3e") # melt, sublimation components
    plt.bar(3, norm_factor*(smlt_ref+ssub_ref+csub_ref), color="#451e3e")
    plt.bar(3, norm_factor*(smlt_ref+ssub_ref), color="#051e3e")
    plt.bar(3, norm_factor*(smlt_ref), color="#b3cde0")
        
    # 2050
    plt.bar(4.5, norm_factor*accu_2050, color="#03396c") # solid precip 
    plt.bar(5.5, norm_factor*refr_2050, color="#6497b1") # refreezing
    plt.bar(6.5, norm_factor*(smlt_2050+ssub_2050+csub_2050+wsub_2050), color="#851e3e") # melt, sublimation components
    plt.bar(6.5, norm_factor*(smlt_2050+ssub_2050+csub_2050), color="#451e3e")
    plt.bar(6.5, norm_factor*(smlt_2050+ssub_2050), color="#051e3e")
    plt.bar(6.5, norm_factor*(smlt_2050), color="#b3cde0")
    
    #plt.xticks([1,2,3,4,5,6],["Snow precip.", ""])
    plt.savefig(out_folder + "/process_accu_abla_"+reg+".jpg")
    plt.savefig(out_folder + "/process_accu_abla_"+reg+".svg")

def write_raster():
        
    # export raster
    for reg in reg_list:
        print(reg)
   
        smod_0 = data_2050[dT_stat][reg]["smod"]["ref"].mean(dim="hyear").smod
        tmp = smod_0.rio.set_crs(epsg_dict[reg])
        tmp.rio.to_raster(out_folder+"/snow_melt_out_date__ref_period__"+reg+".tif")

        ddd = data_2050[dT_stat][reg]["swed"]["ref"].chunk(dict(time=-1))
        SWE_0 = ddd.sel(time=ddd['time.month'].isin( months_to_select_winter_spring )).mean(dim={"time"}).swed
        tmp = SWE_0.rio.set_crs(epsg_dict[reg])
        tmp.rio.to_raster(out_folder+"/snow_water_equivalent__ref_period__"+reg+".tif")


        for exp in ["rcp26", "rcp45", "rcp85"]: #(SWE, dSWE) in enumerate([(SWE_rcp26, dSWE_rcp26), (SWE_rcp45, dSWE_rcp45), (SWE_rcp85, dSWE_rcp85)]):
            print(exp)
            smod = data_2050[dT_stat][reg]["smod"][exp].mean(dim="hyear").smod
            tmp = smod.rio.set_crs(epsg_dict[reg])
            tmp.rio.to_raster(out_folder+"/snow_melt_out_date__2050_"+exp+"__"+reg+".tif")
            
            ddd = data_2050[dT_stat][reg]["swed"][exp].chunk(dict(time=-1))
            SWE = ddd.sel(time=ddd['time.month'].isin( months_to_select_winter_spring )).mean(dim={"time"}).swed
            tmp = SWE.rio.set_crs(epsg_dict[reg])
            tmp.rio.to_raster(out_folder+"/snow_water_equivalent__2050_"+exp+"__"+reg+".tif")

    
out_folder = "/home/hola/datos_seis/port_transfert/report"
        
for reg_list in [["AIGUE"], ["GUADA"], ["ORDES"], ["PICOS"], ["SIERR"]]:
    make_fig( reg_list, out_folder)








    for reg in reg_list:
        print(reg + " SWE change RCP 4.5")
        print(str(dT_2050["median"][reg]["rcp45"]) + " [ "+str(dT_2050["1Q"][reg]["rcp45"])+"; "+str(dT_2050["3Q"][reg]["rcp45"])+" ]")        






    ################################
    ########    LOAD DATA   ########
    ################################
    # Load data for each run
    #data_all_run_qm =  {}
    qm_exp="_T1_P1_S"
    sm_par_dir = "/home/hola/project/nacional_parques/sm_2024_01_28_4pn/par_qm"+qm_exp
    

    # Load data for ensemble mean
    var_list = ["prec", "relh", "rpre", "smlt", "ssub", "tair", "var5", "var6"]

    stat_name_list = [ "mean",  "min", "max"] #
    
    data_ens_qm =  {}
    data_ens_qm_tmp = {}
    for stat_name in stat_name_list:
        data_ens_qm_tmp[stat_name] = {}
    
    for exp in ["historical", "rcp85"]:#, "rcp26", "rcp85"]:
        run_list = run_dict[exp]
        sm_out_dir = out_dir+"/sm/sm_20240128_Tlr_53_qm"+qm_exp
    
        print(exp)
        for stat_name in stat_name_list:
            data = post_tools.get_simu_ens( exp, res, sm_out_dir, sm_par_dir, reg_list, var_list, stat_name)
            data_ens_qm_tmp[stat_name] = post_tools.merge_data_runs( data_ens_qm_tmp[stat_name], data)
        
    # Merge historical - rcp85
    run = "ens"
    data_ens_qm[run] = {"all":{}}
    
    
    for reg in reg_list:
        data_ens_qm[run]["all"][reg] = {}
        for var in var_list:
            data_ens_qm[run]["all"][reg][var] = {}
            for stat_name in stat_name_list: 
                data_ens_qm[run]["all"][reg][var][stat_name] = {}
    
                var_hist = data_ens_qm_tmp[stat_name][run]["historical"][reg][var]
                var_rcp = data_ens_qm_tmp[stat_name][run]["rcp85"][reg][var]
                var_concat = xr.concat([var_hist, var_rcp], dim="time").sortby("time")
                            
                # Identify duplicate time values
                time_values = var_concat["time"].values
                unique_indices = np.unique(time_values, return_index=True)[1]
                
                # Select the first value for each duplicate
                var_concat = var_concat.isel(time=unique_indices) # only for MOHC-HadGEM2-ES__ICTP-RegCM4-6 which has december 2005 in both historical AND rcp85
                
                var_concat_clean = post_tools.clean_uncomplete_hyear(var_concat)
                data_ens_qm["ens"]["all"][reg][var][stat_name] = var_concat_clean
        
        data_ens_qm[run]["all"][reg]["spre"] = {}
        for stat_name in stat_name_list: 
            data_ens_qm[run]["all"][reg]["spre"][stat_name] = data_ens_qm[run]["all"][reg]["prec"][stat_name].copy(deep=True).rename({"prec":"spre"})
            data_ens_qm[run]["all"][reg]["spre"][stat_name]["spre"] = data_ens_qm[run]["all"][reg]["prec"][stat_name].prec - data_ens_qm[run]["all"][reg]["rpre"][stat_name].rpre
        
    del(data_ens_qm_tmp)
    
    # Open Parc contour
    contour_parc = {}            
    for reg in reg_list:
        x_parc, y_parc = post_tools.open_parc_shp(reg)
        contour_parc[reg] = (x_parc , y_parc)

    #################################################### 
            ## PLOT DSWE = f(DTAIR) ##
    #################################################### 
    
    # ALL YEAR - CALCULATED LOCALLY CALCULATED ON 2023-03-05
    dT_2050 = {'AIGUE': {'rcp26': 0.804, 'rcp45': 1.095, 'rcp85': 1.588},
               'GUADA': {'rcp26': 0.798, 'rcp45': 1.095, 'rcp85': 1.567},
               'ORDES': {'rcp26': 0.779, 'rcp45': 1.059, 'rcp85': 1.521},
               'PICOS': {'rcp26': 0.564, 'rcp45': 0.765, 'rcp85': 1.115},
               'SIERR': {'rcp26': 0.786, 'rcp45': 1.094, 'rcp85': 1.578}}
    
    data_2050 = {}
    central_year_ref = 2005
    period_year_ref = (1990, 2020)
    for reg in reg_list:
        print(reg)
        data_2050[dT_stat][reg] = {"spre":{}, "prec":{}, "prec":{}, "ssub":{}, "smlt":{} }
        tair = data_ens_qm[run]["all"][reg]["tair"]["mean"]
        
        tair_mean_30yr =  tair.mean(dim=("lat","lon")).rolling(time=30*365, center=True).mean()
        
        tair_ref = tair_mean_30yr.where( tair_mean_30yr.hyear == central_year_ref, drop=True ).tair[0].values
        dtair_mean_30yr = tair_mean_30yr - tair_ref 
        
        for var_name in data_2050[dT_stat][reg].keys():
            print(var_name)
            var = data_ens_qm[run]["all"][reg][var_name]["mean"]
    
            data_2050[dT_stat][reg][var_name]["ref"] = var.where( (var.hyear> period_year_ref[0]) & (var.hyear < period_year_ref[1]), drop = True)
        
            for exp in dT_2050[reg].keys():
                print(exp)
                first_date_above_dT = np.where( dtair_mean_30yr.to_array() > dT_2050[reg][exp] )[1][0]
                if tair_mean_30yr.time[first_date_above_dT].dohy> 182:
                    first_hyear_above_dT = tair_mean_30yr.hyear[first_date_above_dT].data.flatten()[0] + 1
                else:
                    first_hyear_above_dT = tair_mean_30yr.hyear[first_date_above_dT].data.flatten()[0]
                period_year_rcp = (first_hyear_above_dT-15, first_hyear_above_dT+15)
        
                data_2050[dT_stat][reg][var_name][exp] = var.where( (var.hyear>= period_year_rcp[0]) & (var.hyear < period_year_rcp[1]), drop = True)
    
    # PLOT FOR SNOW PRECIP
    months_to_select = [12,1,2,3,4,5]
    for reg in reg_list:
        print(reg)
        (x_parc, y_parc) = contour_parc[reg]
        
        in_HS = "/home/hola/datos_seis/nacional_parques/data/COP30/250m/limite_"+reg+"_COP30_250m_UTM_DEM_HS.tif"
        HS = xr.open_dataset( in_HS )
        tt = data_2050[dT_stat][reg]["spre"]["ref"]
        VAR_0 = tt.sel(time=tt['time.month'].isin( months_to_select )).sum(dim="time").spre/30
    
        for exp in ["rcp26", "rcp45", "rcp85"]: #(SWE, dSWE) in enumerate([(SWE_rcp26, dSWE_rcp26), (SWE_rcp45, dSWE_rcp45), (SWE_rcp85, dSWE_rcp85)]):
            tt = data_2050[dT_stat][reg]["spre"][exp]
            VAR = tt.sel(time=tt['time.month'].isin( months_to_select )).sum(dim="time").spre/30
            dVAR = 100*(VAR - VAR_0)/VAR_0
            print(exp)
            fig = plt.figure( figsize=( 9, 4) )
            
            ax1 = plt.subplot(131)
            data2im = np.flipud(VAR_0.to_numpy())
#            data2im = np.ma.masked_where(data2im < min_VAR_mask, data2im)    
            data2im2mask = data2im.copy() 
            im1 = ax1.imshow( HS.band_data.squeeze(), cmap='Greys_r', vmin=0, vmax=255, extent = post_tools.get_xr_im_extent(HS), origin="upper", interpolation="nearest", alpha=0.8)
            im2 = ax1.imshow( data2im, cmap='viridis', vmin = 0 , vmax = 2, extent = post_tools.get_xr_im_extent(HS), origin="upper", interpolation="nearest")
            ax1.set_title('Reference period (1990-2020)',fontsize=10, horizontalalignment="center")
        
            ax2 = plt.subplot(132)
            data2im = np.flipud(VAR.to_numpy())
#            data2im = np.ma.masked_where(data2im < min_VAR_mask, data2im)    
            im1 = ax2.imshow( HS.band_data.squeeze(), cmap='Greys_r', vmin=0, vmax=255, extent = post_tools.get_xr_im_extent(HS), origin="upper", interpolation="nearest", alpha=0.8)
            im3 = ax2.imshow( data2im, cmap='viridis', vmin = 0 , vmax = 2, extent = post_tools.get_xr_im_extent(HS), origin="upper", interpolation="nearest")
            ax2.set_title('2050',fontsize=10, horizontalalignment="center")
        
            ax3 = plt.subplot(133)
            data2im = np.flipud(dVAR.to_numpy())
#            data2im = np.ma.masked_where(data2im2mask < min_VAR_mask, data2im)    
            im1 = ax3.imshow( HS.band_data.squeeze(), cmap='Greys_r', vmin=0, vmax=255, extent = post_tools.get_xr_im_extent(HS), origin="upper", interpolation="nearest", alpha=0.8)
            im4 = ax3.imshow( data2im, cmap='RdYlBu', vmin = -100, vmax = 100, extent = post_tools.get_xr_im_extent(HS), origin="upper", interpolation="nearest")
            ax3.set_title('2050 minus reference period',fontsize=10, horizontalalignment="center")
        
            for ax in fig.get_axes():
                ax.set_xticks([])
                ax.set_yticks([])
                ax.plot(x_parc, y_parc, color=plt_dict[reg]["col"], lw=0.8) #color="#f6cd61", lw=0.5 )
        
            scalebar = ScaleBar(0.001, "km", length_fraction=0.25, width_fraction=0.03, fixed_value=10, font_properties={"size":9})
            plt.gca().add_artist(scalebar)
            
            fig.subplots_adjust(bottom=0.1) #, right=0.8)  # Adjust as needed
            cbar_ax = fig.add_axes([0.15, 0.25, 0.3, 0.02])
            cb = fig.colorbar(im2, cax=cbar_ax, orientation='horizontal')
            cb.ax.set_title('Snow precip (m.w.e.)',fontsize=10, horizontalalignment="center")
        
            cbar_ax = fig.add_axes([0.50, 0.25, 0.3, 0.02])
            cb = fig.colorbar(im4, cax=cbar_ax, orientation='horizontal')
            cb.ax.set_title('Snow precip change (%)',fontsize=10, horizontalalignment="center")
            
            plt.tight_layout()    
            
            plt.savefig(out_folder+"/sprec_dsprec_2050_"+exp+"_"+reg+"_winspr_report_new.png")
            plt.savefig(out_folder+"/sprec_dsprec_2050_"+exp+"_"+reg+"_winspr_report_new.svg")
            plt.close()
            
    # PLOT FOR TOTAL PRECIP
    months_to_select = [12,1,2,3,4,5]
    for reg in reg_list:
        print(reg)
        (x_parc, y_parc) = contour_parc[reg]
        
        in_HS = "/home/hola/datos_seis/nacional_parques/data/COP30/250m/limite_"+reg+"_COP30_250m_UTM_DEM_HS.tif"
        HS = xr.open_dataset( in_HS )
        tt = data_2050[dT_stat][reg]["prec"]["ref"]
        VAR_0 = tt.sel(time=tt['time.month'].isin( months_to_select )).sum(dim="time").prec/30
    
        for exp in ["rcp26", "rcp45", "rcp85"]: #(SWE, dSWE) in enumerate([(SWE_rcp26, dSWE_rcp26), (SWE_rcp45, dSWE_rcp45), (SWE_rcp85, dSWE_rcp85)]):
            tt = data_2050[dT_stat][reg]["prec"][exp]
            VAR = tt.sel(time=tt['time.month'].isin( months_to_select )).sum(dim="time").prec/30
            dVAR = 100*(VAR - VAR_0)/VAR_0
            print(exp)
            fig = plt.figure( figsize=( 9, 4) )
            
            ax1 = plt.subplot(131)
            data2im = np.flipud(VAR_0.to_numpy())
#            data2im = np.ma.masked_where(data2im < min_VAR_mask, data2im)    
            data2im2mask = data2im.copy() 
            im1 = ax1.imshow( HS.band_data.squeeze(), cmap='Greys_r', vmin=0, vmax=255, extent = post_tools.get_xr_im_extent(HS), origin="upper", interpolation="nearest", alpha=0.8)
            im2 = ax1.imshow( data2im, cmap='viridis', vmin = 0 , vmax = 2, extent = post_tools.get_xr_im_extent(HS), origin="upper", interpolation="nearest")
            ax1.set_title('Reference period (1990-2020)',fontsize=10, horizontalalignment="center")
        
            ax2 = plt.subplot(132)
            data2im = np.flipud(VAR.to_numpy())
#            data2im = np.ma.masked_where(data2im < min_VAR_mask, data2im)    
            im1 = ax2.imshow( HS.band_data.squeeze(), cmap='Greys_r', vmin=0, vmax=255, extent = post_tools.get_xr_im_extent(HS), origin="upper", interpolation="nearest", alpha=0.8)
            im3 = ax2.imshow( data2im, cmap='viridis', vmin = 0 , vmax = 2, extent = post_tools.get_xr_im_extent(HS), origin="upper", interpolation="nearest")
            ax2.set_title('2050',fontsize=10, horizontalalignment="center")
        
            ax3 = plt.subplot(133)
            data2im = np.flipud(dVAR.to_numpy())
#            data2im = np.ma.masked_where(data2im2mask < min_VAR_mask, data2im)    
            im1 = ax3.imshow( HS.band_data.squeeze(), cmap='Greys_r', vmin=0, vmax=255, extent = post_tools.get_xr_im_extent(HS), origin="upper", interpolation="nearest", alpha=0.8)
            im4 = ax3.imshow( data2im, cmap='RdYlBu', vmin = -100, vmax = 100, extent = post_tools.get_xr_im_extent(HS), origin="upper", interpolation="nearest")
            ax3.set_title('2050 minus reference period',fontsize=10, horizontalalignment="center")
        
            for ax in fig.get_axes():
                ax.set_xticks([])
                ax.set_yticks([])
                ax.plot(x_parc, y_parc, color=plt_dict[reg]["col"], lw=0.8) #color="#f6cd61", lw=0.5 )
        
            scalebar = ScaleBar(0.001, "km", length_fraction=0.25, width_fraction=0.03, fixed_value=10, font_properties={"size":9})
            plt.gca().add_artist(scalebar)
            
            fig.subplots_adjust(bottom=0.1) #, right=0.8)  # Adjust as needed
            cbar_ax = fig.add_axes([0.15, 0.25, 0.3, 0.02])
            cb = fig.colorbar(im2, cax=cbar_ax, orientation='horizontal')
            cb.ax.set_title('Total precip (m.w.e.)',fontsize=10, horizontalalignment="center")
        
            cbar_ax = fig.add_axes([0.50, 0.25, 0.3, 0.02])
            cb = fig.colorbar(im4, cax=cbar_ax, orientation='horizontal')
            cb.ax.set_title('Total precip change (%)',fontsize=10, horizontalalignment="center")
            
            plt.tight_layout()    
            
            plt.savefig(out_folder+"/prec_dprec_2050_"+exp+"_"+reg+"_winspr_report_new.png")
            plt.savefig(out_folder+"/prec_dprec_2050_"+exp+"_"+reg+"_winspr_report_new.svg")
            plt.close()

    # PLOT FOR SNOW MELT
    months_to_select = [12,1,2,3,4,5]
    for reg in reg_list:
        print(reg)
        (x_parc, y_parc) = contour_parc[reg]
        
        in_HS = "/home/hola/datos_seis/nacional_parques/data/COP30/250m/limite_"+reg+"_COP30_250m_UTM_DEM_HS.tif"
        HS = xr.open_dataset( in_HS )
        tt = data_2050[dT_stat][reg]["prec"]["ref"]
        VAR_0 = tt.sel(time=tt['time.month'].isin( months_to_select )).sum(dim="time").prec/30
    
        for exp in ["rcp26", "rcp45", "rcp85"]: #(SWE, dSWE) in enumerate([(SWE_rcp26, dSWE_rcp26), (SWE_rcp45, dSWE_rcp45), (SWE_rcp85, dSWE_rcp85)]):
            tt = data_2050[dT_stat][reg]["prec"][exp]
            VAR = tt.sel(time=tt['time.month'].isin( months_to_select )).sum(dim="time").prec/30
            dVAR = 100*(VAR - VAR_0)/VAR_0
            print(exp)
            fig = plt.figure( figsize=( 9, 4) )
            
            ax1 = plt.subplot(131)
            data2im = np.flipud(VAR_0.to_numpy())
#            data2im = np.ma.masked_where(data2im < min_VAR_mask, data2im)    
            data2im2mask = data2im.copy() 
            im1 = ax1.imshow( HS.band_data.squeeze(), cmap='Greys_r', vmin=0, vmax=255, extent = post_tools.get_xr_im_extent(HS), origin="upper", interpolation="nearest", alpha=0.8)
            im2 = ax1.imshow( data2im, cmap='viridis', vmin = 0 , vmax = 2, extent = post_tools.get_xr_im_extent(HS), origin="upper", interpolation="nearest")
            ax1.set_title('Reference period (1990-2020)',fontsize=10, horizontalalignment="center")
        
            ax2 = plt.subplot(132)
            data2im = np.flipud(VAR.to_numpy())
#            data2im = np.ma.masked_where(data2im < min_VAR_mask, data2im)    
            im1 = ax2.imshow( HS.band_data.squeeze(), cmap='Greys_r', vmin=0, vmax=255, extent = post_tools.get_xr_im_extent(HS), origin="upper", interpolation="nearest", alpha=0.8)
            im3 = ax2.imshow( data2im, cmap='viridis', vmin = 0 , vmax = 2, extent = post_tools.get_xr_im_extent(HS), origin="upper", interpolation="nearest")
            ax2.set_title('2050',fontsize=10, horizontalalignment="center")
        
            ax3 = plt.subplot(133)
            data2im = np.flipud(dVAR.to_numpy())
#            data2im = np.ma.masked_where(data2im2mask < min_VAR_mask, data2im)    
            im1 = ax3.imshow( HS.band_data.squeeze(), cmap='Greys_r', vmin=0, vmax=255, extent = post_tools.get_xr_im_extent(HS), origin="upper", interpolation="nearest", alpha=0.8)
            im4 = ax3.imshow( data2im, cmap='RdYlBu', vmin = -100, vmax = 100, extent = post_tools.get_xr_im_extent(HS), origin="upper", interpolation="nearest")
            ax3.set_title('2050 minus reference period',fontsize=10, horizontalalignment="center")
        
            for ax in fig.get_axes():
                ax.set_xticks([])
                ax.set_yticks([])
                ax.plot(x_parc, y_parc, color=plt_dict[reg]["col"], lw=0.8) #color="#f6cd61", lw=0.5 )
        
            scalebar = ScaleBar(0.001, "km", length_fraction=0.25, width_fraction=0.03, fixed_value=10, font_properties={"size":9})
            plt.gca().add_artist(scalebar)
            
            fig.subplots_adjust(bottom=0.1) #, right=0.8)  # Adjust as needed
            cbar_ax = fig.add_axes([0.15, 0.25, 0.3, 0.02])
            cb = fig.colorbar(im2, cax=cbar_ax, orientation='horizontal')
            cb.ax.set_title('Total precip (m.w.e.)',fontsize=10, horizontalalignment="center")
        
            cbar_ax = fig.add_axes([0.50, 0.25, 0.3, 0.02])
            cb = fig.colorbar(im4, cax=cbar_ax, orientation='horizontal')
            cb.ax.set_title('Total precip change (%)',fontsize=10, horizontalalignment="center")
            
            plt.tight_layout()    
            
            plt.savefig(out_folder+"/prec_dprec_2050_"+exp+"_"+reg+"_winspr_report_new.png")
            plt.savefig(out_folder+"/prec_dprec_2050_"+exp+"_"+reg+"_winspr_report_new.svg")
            plt.close()
                        

for reg in reg_list:
    print(reg)

    plt.figure(figsize=(8,4))

    ############
    # TEMPERATURE
    # plot ens mean
    tt = data_ens_qm["ens"]["all"][reg]["prec"]["mean"]
    ts0 = tt.sel(time=tt['time.month'].isin( months_to_select_winter_spring )).mean(dim=("lat","lon")).groupby("hyear").sum()
#    ts1 = ts0.to_pandas().rolling(30, center=True).mean() 
#    ts = ts0 - ts1.loc[2005] # ANOMALY COMPARED TO REF PERIOD
    
    plt.plot(ts0.hyear, ts0.prec, lw=2, ms=6, mew=0, color="b")
    
    tt = data_ens_qm["ens"]["all"][reg]["spre"]["mean"]
    ts0 = tt.sel(time=tt['time.month'].isin( months_to_select_winter_spring )).mean(dim=("lat","lon")).groupby("hyear").sum()
#    ts1 = ts0.to_pandas().rolling(30, center=True).mean() 
#    ts = ts0 - ts1.loc[2005] # ANOMALY COMPARED TO REF PERIOD
    
    plt.plot(ts0.hyear, ts0.spre, lw=2, ms=6, mew=0, color="r")
    
    tt = data_ens_qm["ens"]["all"][reg]["ssub"]["mean"]
    ts0 = tt.sel(time=tt['time.month'].isin( months_to_select_winter_spring )).mean(dim=("lat","lon")).groupby("hyear").sum()
#    ts1 = ts0.to_pandas().rolling(30, center=True).mean() 
#    ts = ts0 - ts1.loc[2005] # ANOMALY COMPARED TO REF PERIOD
    
    plt.plot(ts0.hyear, ts0.ssub, lw=2, ms=6, mew=0, color="g")
    
    
    tt = data_ens_qm["ens"]["all"][reg]["smlt"]["mean"]
    ts0 = tt.sel(time=tt['time.month'].isin( months_to_select_winter_spring )).mean(dim=("lat","lon")).groupby("hyear").sum()
#    ts1 = ts0.to_pandas().rolling(30, center=True).mean() 
#    ts = ts0 - ts1.loc[2005] # ANOMALY COMPARED TO REF PERIOD
    plt.plot(ts0.hyear, ts0.smlt, lw=2, ms=6, mew=0, color="c")    
    
    tt = data_ens_qm["ens"]["all"][reg]["var5"]["mean"]
    ts0 = tt.sel(time=tt['time.month'].isin( months_to_select_winter_spring )).mean(dim=("lat","lon")).groupby("hyear").sum()
    plt.plot(ts0.hyear, ts0.var5, lw=2, ms=6, mew=0, color=[0.2, 0.2, 0.2])
    
    tt = data_ens_qm["ens"]["all"][reg]["var6"]["mean"]
    ts0 = tt.sel(time=tt['time.month'].isin( months_to_select_winter_spring )).mean(dim=("lat","lon")).groupby("hyear").sum()
    plt.plot(ts0.hyear, ts0.var6, lw=2, ms=6, mew=0, color=[0.6, 0.6, 0.6])

    
#    plt.fill_between(ts_max.index, ts_min.tair, ts_max.tair, color=plt_dict["SIMU"]["col"], alpha=0.5)
    
    plt.hlines(0, 1970, 2100, color="k", lw=0.3)
    #plt.axis([1970, 2060, -1, 2])
    plt.title("Mean yearly precipitation (m w.e.)")
    plt.ylabel("Precipitation (m w.e.)")
    plt.grid(True, lw=0.4)
    
            
    plt.savefig(out_folder+"/prec_ssub_winspr_"+reg+"_qm_report_new.png")
    plt.savefig(out_folder+"/prec_ssub_winspr_"+reg+"_qm_report_new.svg")
    plt.close()
    
    
    








#################################################### 
        ## PLOT SWED, TEMP = f(time) 30 yr rolling mean ## 
#################################################### 

out_stat = {}
for reg in reg_list:
    print(reg)

    plt.figure(figsize=(8,4))

    ############
    # TEMPERATURE
    # plot ens mean
    plt.subplot(211)
    tt = data_ens_qm["ens"]["all"][reg]["tair"]["mean"]
    ts0 = tt.mean(dim=("lat","lon")).groupby("hyear").mean()
    ts0 = ts0.to_pandas().rolling(30, center=True).mean() 
    ts = ts0 - ts0.loc[2005] # ANOMALY COMPARED TO REF PERIOD

        
#    tt = data_ens_qm["ens"]["all"][reg]["tair"]["mean_mstd"]
#    tt = tt.mean(dim=("lat","lon")).groupby("hyear").mean()
#    ts_min = tt.to_pandas().rolling(30, center=True).mean() 
#    ts_min = ts_min - ts0.loc[2005] # ANOMALY COMPARED TO REF PERIOD
            
#    tt = data_ens_qm["ens"]["all"][reg]["tair"]["mean_pstd"]
#    tt = tt.mean(dim=("lat","lon")).groupby("hyear").mean()
#    ts_max = tt.to_pandas().rolling(30, center=True).mean() 
#    ts_max = ts_max - ts0.loc[2005] # ANOMALY COMPARED TO REF PERIOD
    
    plt.plot(ts.index, ts.tair, lw=2, ms=6, mew=0, color=plt_dict["SIMU"]["col"])
#    plt.fill_between(ts_max.index, ts_min.tair, ts_max.tair, color=plt_dict["SIMU"]["col"], alpha=0.5)
    
    plt.hlines(0, 1970, 2100, color="k", lw=0.3)
    plt.axis([1970, 2060, -1, 2])
    plt.title("Mean yearly temperature anomaly (ºC)")
    plt.ylabel("Temperature anomaly (ºC)")
    plt.grid(True, lw=0.4)
    
        
    ############
    # SWED
    # plot ens mean
    plt.subplot(212)
    tt = data_ens_qm["ens"]["all"][reg]["swed"]["mean"]
    tt = tt.mean(dim=("lat","lon")).groupby("hyear").mean()
    ts0 = tt.to_pandas().rolling(30, center=True).mean() 
    ts = 100*(ts0 / ts0.loc[2005].swed) # ANOMALY COMPARED TO REF PERIOD

    tt = data_ens_qm["ens"]["all"][reg]["swed"]["min"]
    tt = tt.mean(dim=("lat","lon")).groupby("hyear").mean()
    ts_min = tt.to_pandas().rolling(30, center=True).mean() 
    ts_min = 100*(ts_min / ts0.loc[2005].swed) # ANOMALY COMPARED TO REF PERIOD


    tt = data_ens_qm["ens"]["all"][reg]["swed"]["max"]
    tt = tt.mean(dim=("lat","lon")).groupby("hyear").mean()
    ts_max = tt.to_pandas().rolling(30, center=True).mean() 
    ts_max = 100*(ts_max / ts0.loc[2005].swed) # ANOMALY COMPARED TO REF PERIOD


    
    plt.plot(ts.index, ts.swed, lw=2, ms=6, mew=0, color=plt_dict["SIMU"]["col"])
    #plt.fill_between(ts_max.index, ts_min.swed, ts_max.swed, color=plt_dict["SIMU"]["col"], alpha=0.5)
   
    plt.axis([1970, 2060, 0, 200])
    plt.title("Yearly mean snow mass (1990-2020 as ref. 100)")
    plt.ylabel("Snow mass")
    plt.grid(True, lw=0.4)
    
    plt.tight_layout()
    
    plt.savefig("/home/hola/datos_seis/port_transfert/temp_swed_"+reg+"_qm_report_new.png")
    plt.savefig("/home/hola/datos_seis/port_transfert/temp_swed_"+reg+"_qm_report_new.svg")
    plt.close()






    

