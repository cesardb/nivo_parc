#!/bin/sh

### Preparation of the Cordex data
conda activate parc
chmod 777 /home/hola/project/nacional_parques/script/nivo_parc/toolbox/*

# Site array
declare -a site_arr=("TEIDE")

# Init folders
#dataFolder="/home/hola/datos_seis/nacional_parques/data/cordex"
outFolder0="/home/hola/datos_seis/nacional_parques/prod/sm/sm_Tlr_53_corr"
cordexFolder0="/home/hola/datos_seis/nacional_parques/prod/cordex"
workFolder="/home/hola/project/nacional_parques/sm_Tlr_53"

mkdir -p ${workFolder}"/par_corr" ${outFolder0}"/log" #  ${workFolder}"/met_prod"
CordexToolbox="/home/hola/project/nacional_parques/script/nivo_parc/toolbox"

run_name="historical" # historical rcp85 rcp26
source $CordexToolbox/exp_list_${run_name}_TEIDE.txt

for exp_name in "${exp_arr[@]}"
do

for site_name in "${site_arr[@]}"
do
echo $exp_name "-" $site_name
# prepare .par
outFolder1=${outFolder0}/${exp_name}/${run_name}/${site_name}"/"
inMET=$(ls ${cordexFolder0}"/"${exp_name}"/"${run_name}"/"${site_name}"/dat_corr/allvar_"*".dat")
res_simu="250m"
${CordexToolbox}/sm_par_write.py -inVEG "/home/hola/datos_seis/nacional_parques/data/land_cover/CORINE/"${res_simu}"/U2012_CLC2006_V2020_20u1_"${res_simu}"_"${site_name}"_sm_label.asc" -inDEM "/home/hola/datos_seis/nacional_parques/data/COP30/"${res_simu}"/limite_"${site_name}"_COP30_"${res_simu}"_UTM_DEM.asc" -inMET $inMET -outPAR ${workFolder}"/par_corr/"${exp_name}"_"${run_name}"_"${site_name}".par"  -outFolder ${outFolder1}
mkdir -p ${outFolder1}"/wo_assim" ${outFolder1}"/wi_assim" ${outFolder1}"/fig"

# run snowmodel
cd ${workFolder}

nohup ./snowmodel  ${workFolder}"/par_corr/"${exp_name}"_"${run_name}"_"${site_name}".par" > ${outFolder0}"/log/log_"${exp_name}"_"${run_name}"_"${site_name}".txt" &

done
done


######################
# post-treatment
sm_out_dir="/home/hola/datos_seis/nacional_parques/prod/sm/sm_Tlr_53_corr"
sm_par_dir="/home/hola/project/nacional_parques/sm_Tlr_53/par_corr"
python ${CordexToolbox}/post_treatment_gdat_to_nc.py -sm_out_dir $sm_out_dir -sm_par_dir $sm_par_dir -exp rcp85 -is_teide 1  # historical rcp85

# !!! convert gdat to nc compress !!! 

ctl2nccomp(){
while [[ "$#" > 1 ]]; do case $1 in
    -inFILE) inFILE="$2";;
    *) break;;
  esac; shift; shift
done

NAME=`echo "$inFILE" | cut -d'.' -f1`;
EXT=`echo "$inFILE" | cut -d'.' -f2`;

if [ "$EXT" = "ctl" ]
then
cdo -f nc4 -z zip_9 import_binary $inFILE ${NAME}.nc
#ncks -4 -L 9 ${NAME}_0.nc ${NAME}.nc
elif [ "$EXT" = "gdat" ]
then
cdo -f nc4 -z zip_9 import_binary ${NAME}.ctl ${NAME}.nc
fi

[ -f ${NAME}.nc ] && rm ${NAME}.gdat 
}

export -f ctl2nccomp

find $sm_out_dir/*/*/*/* -type f -name "*.gdat" > inFILE_list.txt
parallel --jobs 4 ctl2nccomp -inFILE {} :::: inFILE_list.txt
rm inFILE_list.txt

######################
# create ensemble netcdf
parallelTxtFolder="/home/hola/project/nacional_parques/script/parallelTxt"

declare -a run_arr=("historical" "rcp85")
declare -a var_arr=("snod" "swed" "tair" "prec") # "smlt" "ssub"  
declare -a site_arr=("TEIDE")

sm_out_dir="/home/hola/datos_seis/nacional_parques/prod/sm/sm_Tlr_53_corr"

for run_name in ${run_arr[@]}
do
source $CordexToolbox/exp_list_${run_name}_TEIDE.txt

for site_name in ${site_arr[@]}
do
for var_name in ${var_arr[@]}
do
out_dir=${sm_out_dir}/ens/${run_name}/${site_name}
mkdir -p ${out_dir}
echo cdo ensmean ${sm_out_dir}/*/${run_name}/${site_name}/wo_assim/${var_name}.nc ${out_dir}/${var_name}_mean.nc >> ${parallelTxtFolder}/tmp_make_ens_mean.txt 
#echo cdo ensmedian ${sm_out_dir}/*/${run_name}/${site_name}/wo_assim/${var_name}.nc ${out_dir}/${var_name}_median.nc >> ${parallelTxtFolder}/tmp_make_ens_median.txt 
echo cdo ensmin ${sm_out_dir}/*/${run_name}/${site_name}/wo_assim/${var_name}.nc ${out_dir}/${var_name}_min.nc >> ${parallelTxtFolder}/tmp_make_ens_min.txt 
echo cdo ensmax ${sm_out_dir}/*/${run_name}/${site_name}/wo_assim/${var_name}.nc ${out_dir}/${var_name}_max.nc >> ${parallelTxtFolder}/tmp_make_ens_max.txt 
echo cdo ensstd ${sm_out_dir}/*/${run_name}/${site_name}/wo_assim/${var_name}.nc ${out_dir}/${var_name}_std.nc >> ${parallelTxtFolder}/tmp_make_ens_std.txt 

done
done
done

parallel --jobs 4 :::: ${parallelTxtFolder}/tmp_make_ens_mean.txt 
parallel --jobs 4 :::: ${parallelTxtFolder}/tmp_make_ens_median.txt 
parallel --jobs 4 :::: ${parallelTxtFolder}/tmp_make_ens_min.txt 
parallel --jobs 4 :::: ${parallelTxtFolder}/tmp_make_ens_max.txt 
parallel --jobs 4 :::: ${parallelTxtFolder}/tmp_make_ens_std.txt 
