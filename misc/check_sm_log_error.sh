
# cd to log folder
for log_file in log*
do

x=$(tail -n 10 $log_file)
run_finished=false
for ff in ${x[@]}
do
if [ "$ff" = "Finished" ]; then
run_finished=true
fi
done

if $run_finished
then
  echo -e "Snowmodel run ended correctly\n"

else
  echo $log_file
  gcm=$(echo $log_file | cut -d'_' -f2)
  rcm=$(echo $log_file | cut -d'_' -f4)
  run=$(echo $log_file | cut -d'_' -f5)
  reg=$(echo $log_file | cut -d'_' -f6 | sed "s/.txt//g")
  echo -e "Wrong end " $gcm - $rcm - $reg - $run "\n"
  tt=$(grep output_path_wo_assim $log_file)
  path_to_delete=$(echo $tt | cut -d'=' -f2)
  echo "rm $path_to_delete/*" >> tmp_rm.txt
  echo ${gcm}__${rcm},${reg}  >> to_run_sm.txt
fi

done


