
import xarray as xr
import rioxarray
from pyproj import CRS
import numpy as np
import subprocess

subprocess.run('for year in {1950..2020}; do   ncks -d time,"${year}-01-01 00:00:00","${year}-12-31 23:59:59" /home/hola/datos_seis/nacional_parques/data/STEAD_SPREAD/SPREAD_pen_pcp_utm30.nc /home/hola/datos_seis/nacional_parques/prod/STEAD_SPREAD/SPREAD_pen_pcp_utm30_${year}.nc; done')

# Define the target CRS (latitude and longitude)
target_crs = CRS.from_epsg("4326")
source_crs = CRS.from_epsg("32630")

for yy in np.arange(1950,2013):
    print(yy)
    ds = xr.open_dataset("/home/hola/datos_seis/nacional_parques/prod/STEAD_SPREAD/SPREAD_pen_pcp_utm30_"+str(yy)+".nc", decode_times=False) #, chunks={'time': 'auto', 'lat': 'auto', 'lon': 'auto'})
    ds["pcp"] = ds.pcp.transpose("time","Y","X")
    ds.rio.write_crs(source_crs, inplace=True)
    ds_reprojected = ds.rio.reproject(target_crs, chunks={'time': 'auto', 'lat': 'auto', 'lon': 'auto'})
    del ds_reprojected["pcp"].attrs["grid_mapping"]
    ds_reprojected = ds_reprojected.rename({'x': 'lon', 'y':'lat'})
    ds_reprojected.to_netcdf("/home/hola/datos_seis/nacional_parques/prod/STEAD_SPREAD/SPREAD_pen_pcp_latlon_"+str(yy)+".nc")

subprocess.run('cdo remapbil,"/home/hola/datos_seis/nacional_parques/data/Iberia/Iberia01_v1.0_DD_010reg_aa3d_pr.nc" "/home/hola/datos_seis/nacional_parques/prod/STEAD_SPREAD/SPREAD_pen_pcp_latlon_1950_2012.nc" "/home/hola/datos_seis/nacional_parques/prod/STEAD_SPREAD/SPREAD_pen_pcp_latlon_1950_2012_011.nc"')
