import pickle as pkl
QM_folder = "/home/hola/datos_seis/nacional_parques/prod/pkl/QM_SS"

# Temperature
for reg in reg_list:
    print("\n" + reg)
    out_list = []
    in_QM_list = glob.glob( QM_folder + "/tas_*_"+reg+"_qm.p" )
    for in_QM in in_QM_list:
        print("_".join(in_QM.split("_")[5:9]))
        with open(in_QM, 'rb') as p:
            QM = pkl.load(p)
            
        for ii,kk in enumerate(QM.keys()):
            out_list.append( QM[kk]["ds"]["af"].data )
            
        out_arr = np.array(out_list)
        print( np.mean( out_arr , axis=(0,2)))
        print( np.min(np.mean( out_arr , axis=(0,2))).round(2), np.max(np.mean( out_arr , axis=(0,2))).round(2))


# Precipitation
for reg in reg_list:
    print("\n" + reg)
    out_list = []
    in_QM_list = glob.glob( QM_folder + "/pr_*_"+reg+"_qm.p" )
    for in_QM in in_QM_list:
        print("_".join(in_QM.split("_")[5:9]))
        with open(in_QM, 'rb') as p:
            QM = pkl.load(p)
            
        for ii,kk in enumerate(QM.keys()):
            tt = QM[kk]["ds"]["af"].data
            tt[ np.isnan(tt) == 1] = 0
            tt[ tt > 100] = 0
            out_list.append( tt )
            
        out_arr = np.array(out_list)
        
        tt_arr = out_arr
        print( 1/np.nanmean( tt_arr , axis=(0,1)))
        print( (1/np.min(np.nanmean( tt_arr , axis=(0,1)))).round(2), (1/np.max(np.nanmean( tt_arr , axis=(0,1)))).round(2))
        
        #tt_arr = out_arr[:,:,:10]
        #print( np.nanmean( tt_arr , axis=(0,1)))
        #print( (1/np.min(np.nanmean( tt_arr , axis=(0,1)))).round(2), (1/np.max(np.nanmean( tt_arr , axis=(0,1)))).round(2))
        
        #tt_arr = out_arr[:,:,10::]
        #print( np.nanmean( tt_arr , axis=(0,1)))
        #print( (1/np.min(np.nanmean( tt_arr , axis=(0,1)))).round(2), (1/np.max(np.nanmean( tt_arr , axis=(0,1)))).round(2))
        
            
