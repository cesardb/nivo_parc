#!/home/hola/miniconda3/envs/parc/bin/python
import argparse
import xarray as xr
import os
import pandas as pd
import numpy as np
import pyproj
import fiona
from fiona.crs import from_epsg
from shapely.geometry import Point, mapping, shape
import argparse

def get_position_netcdf_meteo(inNC, outFolder, inSHP, dist_buffer):
    UTM_dict ={"SIERR":"epsg:32630","PICOS":"epsg:32630","ORDES":"epsg:32631","GUADA":"epsg:32630","AIGUE":"epsg:32631"}
    inOrogrot="/home/hola/datos_seis/nacional_parques/data/cordex/orog/orog_EUR-11_ICHEC-EC-EARTH_rcp26_r0i0p0_CLMcom-CCLM4-8-17_v1_fx.nc"
    inOrogunrot="/home/hola/datos_seis/nacional_parques/data/cordex/orog/orog_EUR-11_CNRM-CERFACS-CNRM-CM5_historical_r1i1p1_CNRM-ALADIN63_v2_fx.nc"
    
    # find output name
    outName = inNC.split("/")[-1].replace(".nc","")
    siteName = inNC.split("/")[-3]
    
    if siteName not in UTM_dict.keys():
        print("WARNING PATH MAY HAVE CHANGE SITE NAME NOT PROPERLY IDENTIFIED")
        
        
    # open NC file
    ds = xr.open_dataset(inNC)

    lat_grid = ds.lat.to_numpy()
    lon_grid = ds.lon.to_numpy()  
        
    # open SHP of parc boundary
    SHP = fiona.open(inSHP)
    print(SHP.schema)
    first = SHP.next()
    parc_limit_poly = shape(first['geometry']) # or shp_geom = shape(first) with PyShp

        
    # open orography
    if "rlat" in ds.dims:
        orog =  xr.open_dataset(inOrogrot)
        x_coord = ds.rlon.to_numpy()
        y_coord = ds.rlat.to_numpy()
        
    else:
        orog =  xr.open_dataset(inOrogunrot)
        x_coord = ds.x.to_numpy()
        y_coord = ds.y.to_numpy()
        
        
    x_shp = []
    y_shp = []
    ix_shp = []
    iy_shp = []
    orog_shp = []
    list_station = []
    is_select = []
    
    inProj = pyproj.Proj('EPSG:4326')
    #inProj=pyproj.CRS.from_epsg(4326)
    outProj = pyproj.Proj(UTM_dict[siteName])    
    #outProj=pyproj.CRS.from_epsg(32631)

    for ix,x in enumerate(x_coord):
        for iy,y in enumerate(y_coord):
 # to adapt to each parc

            xy_utm = np.array(pyproj.transform(inProj, outProj, lat_grid[iy,ix], lon_grid[iy,ix])).transpose()
            #transformer = pyproj.Transformer.from_crs( inProj, outProj)
            #xy_utm = np.array(transformer.transform(lon_grid[iy,ix], lat_grid[iy,ix])).transpose().astype(int)
            dist2parc = parc_limit_poly.distance(Point(xy_utm[0],xy_utm[1]))
            
            if "rlat" in ds.dims:
                xZ = np.where(orog.rlon==x)[0][0]
                yZ = np.where(orog.rlat==y)[0][0]
                Z_val = orog["orog"][yZ,xZ]
            else:
                xZ = np.where(orog.x==x)[0][0]
                yZ = np.where(orog.y==y)[0][0]
                Z_val = orog["orog"][yZ,xZ]
                
            x_shp.append(xy_utm[0])
            y_shp.append(xy_utm[1])
            ix_shp.append(ix)
            iy_shp.append(iy)
            orog_shp.append(int(Z_val))
            
            if dist2parc < dist_buffer:
                is_select.append(1)
               
            else:                
                is_select.append(1)
                
 
    ds = None
    
    shpName  = outFolder+"/"+outName+"_points.shp"
    with fiona.open(shpName, 'w',crs=from_epsg(UTM_dict[siteName].replace("epsg:","")),driver='ESRI Shapefile', schema={'geometry': 'Point','properties':{"iy":"int","ix":"int","orog":"int","is_select":"int"}}) as shpout:
        for x2write, y2write, ix2write, iy2write, orog2write, is_select2write in zip(x_shp, y_shp, ix_shp, iy_shp, orog_shp, is_select):
            point = Point(x2write, y2write)
            shpout.write({'geometry': mapping(point),"properties":{"ix":int(ix2write),"iy":int(iy2write), "orog":orog2write, "is_select":is_select2write}})
        

def esgf_netcdf2shp(inNC, outFolder):
  
    # find output name
    outName = inNC.split("/")[-1].replace(".nc","")
        
    # open NC file
    with xr.open_dataset(inNC) as ds:
        
        lon_shp = []
        lat_shp = []
        rlon_shp = []
        rlat_shp = []
        orog_shp = []
                    
        # open orography
        if "rlat" in ds.dims:
            x_coord = ds.rlon.values
            y_coord = ds.rlat.values
            rlat = ds.rlat.values
            rlon = ds.rlon.values

        else:
            x_coord = ds.x.values
            y_coord = ds.y.values
            rlat = y_coord*0+1
            rlon = x_coord*0+1
        
        lon =  ds.lon.values
        lat =  ds.lat.values
        
        if "orog" in ds.variables:
            orog = ds.orog.values
            
        for ix, x in enumerate(x_coord):
            for iy, y in enumerate(y_coord):
                lon_shp.append(lon[iy, ix])
                lat_shp.append(lat[iy, ix])      
                rlon_shp.append(rlon[ix])
                rlat_shp.append(rlat[iy])         
                
                if "orog" in ds.variables:
                    orog_shp.append(int(orog[iy, ix]))

    shpName  = outFolder+"/"+outName+"_points.shp"
    with fiona.open(shpName, 'w',crs=from_epsg("4326"),driver='ESRI Shapefile', schema={'geometry': 'Point','properties':{"rlon":"float","rlat":"float","orog":"int"}}) as shpout:
        for lon2write, lat2write,rlon2write, rlat2write, orog2write in zip(lon_shp, lat_shp, rlon_shp, rlat_shp, orog_shp):
            point = Point(lon2write, lat2write)
            shpout.write({'geometry': mapping(point),"properties":{"rlon":rlon2write,"rlat":rlat2write, "orog":orog2write}})
        
orog_folder="/media/cesardb/Datos/nacional_parques/data/orog"
esgf_netcdf2shp(orog_folder+"/orog_EUR-11_CNRM-CERFACS-CNRM-CM5_historical_r1i1p1_CNRM-ALADIN63_v2_fx.nc", orog_folder)
esgf_netcdf2shp(orog_folder+"/orog_EUR-11_ICHEC-EC-EARTH_rcp26_r0i0p0_CLMcom-CCLM4-8-17_v1_fx.nc", orog_folder)
esgf_netcdf2shp(orog_folder+"/orog_EUR-11_ICHEC-EC-EARTH_rcp85_r12i1p1_ICTP-RegCM4-6_v1_fx.nc", orog_folder)
esgf_netcdf2shp(orog_folder+"/orog_EUR-11_ICHEC-EC-EARTH_historical_r12i1p1_ICTP-RegCM4-6_v1_fx.nc", orog_folder)
esgf_netcdf2shp(orog_folder+"/orog_EUR-11_ICHEC-EC-EARTH_historical_r12i1p1_ICTP-RegCM4-6_v1_fx.nc", orog_folder)
esgf_netcdf2shp(orog_folder+"/orog_EUR-11_NCC-NorESM1-M_rcp85_r1i1p1_CNRM-ALADIN63_v1_fx.nc", orog_folder)

dd_folder="/home/hola/datos_seis/nacional_parques/data/cordex/EUR11/MPI-M-MPI-ESM-LR__ICTP-RegCM4-6/rcp26/"
ll=["/AIGUE/pr/pr_EUR-11_MPI-M-MPI-ESM-LR_rcp26_r1i1p1_ICTP-RegCM4-6_v1_3hr_210001010130-210012302230.nc",
    "/GUADA/pr/pr_EUR-11_MPI-M-MPI-ESM-LR_rcp26_r1i1p1_ICTP-RegCM4-6_v1_3hr_210001010130-210012302230.nc",
    "/ORDES/pr/pr_EUR-11_MPI-M-MPI-ESM-LR_rcp26_r1i1p1_ICTP-RegCM4-6_v1_3hr_210001010130-210012302230.nc",
    "/PICOS/pr/pr_EUR-11_MPI-M-MPI-ESM-LR_rcp26_r1i1p1_ICTP-RegCM4-6_v1_3hr_210001010130-210012302230.nc",
    "/SIERR/pr/pr_EUR-11_MPI-M-MPI-ESM-LR_rcp26_r1i1p1_ICTP-RegCM4-6_v1_3hr_210001010130-210012302230.nc"]

for region in ["AIGUE", "GUADA", "ORDES", "PICOS", "SIERR"]:
     esgf_netcdf2shp(dd_folder+"/"+region+"/pr/pr_EUR-11_MPI-M-MPI-ESM-LR_rcp26_r1i1p1_ICTP-RegCM4-6_v1_3hr_210001010130-210012302230.nc" , dd_folder+"/"+region+"/")
     
     
inNC=dd_folder+nc
outFolder=dd_folder 
inSHP=inSHP 
dist_buffer=15000

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description = "Extract relevant informations from a Cordex file name")
    parser.add_argument("-inNC", dest = "inNC")
    parser.add_argument("-outFolder", dest = "outFolder")
    parser.add_argument("-inSHP", dest = "inSHP")
    parser.add_argument("-dist_buffer", dest = "dist_buffer", type = float)
    args = parser.parse_args()
    
    myprep_ncmet2micromet( args.inNC, args.outFolder, args.inSHP, args.dist_buffer)
