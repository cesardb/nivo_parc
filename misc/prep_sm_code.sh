# enable long file name
cd /home/hola/project/nacional_parques/sm_2024_01_28_4pn/code
for ff in $(ls ./*{.f,.inc})
do
sed -i 's/character\*80/character\*400/g' $ff
sed -i 's/=\ 80\ /=\ 400\ /g' $ff
done

sed -i "s/character\*100/character\*400/g" readparam_code.f

sed -i "s/4.4,5.9,7.1,7.8,8.1,8.2/5.3,5.3,5.3,5.3,5.3,5.3/g" micromet_code.f
sed -i "s/8.1,8.1,7.7,6.8,5.5,4.7/5.3,5.3,5.3,5.3,5.3,5.3/g" micromet_code.f



     
# change in readparam_code.f, 
# ~L117
#      character*240 input_string
#      character*200 c_param
#      character*200 c_value

# ~L146
#        read (40,'(a440)',end=99) input_string

# ~L1900
#c Check to see whether we have found any paths/fnames that are
#c   longer than 80 characters.
#          if (i_value_chars.gt.80) then