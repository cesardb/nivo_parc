# Taking the mean of the min and max files
cdo ensmean /home/hola/datos_seis/nacional_parques/data/STEAD_SPREAD/tmax_pen.nc /home/hola/datos_seis/nacional_parques/data/STEAD_SPREAD/tmin_pen.nc /home/hola/datos_seis/nacional_parques/prod/STEAD_SPREAD/tmean_pen.nc

# Reprojecting to Iberia grid (0.1º lat,lon; ~11 km of EuroCORDEX)
cdo remapbil,"/home/hola/datos_seis/nacional_parques/data/Iberia/Iberia01_v1.0_DD_010reg_aa3d_pr.nc" "/home/hola/datos_seis/nacional_parques/prod/STEAD_SPREAD/tmean_pen.nc.nc" "/home/hola/datos_seis/nacional_parques/prod/STEAD_SPREAD/tmean_pen_011.nc"

# Compression (something along that)
cdo -z zip copy "/home/hola/datos_seis/nacional_parques/prod/STEAD_SPREAD/tmean_pen_011.nc" "/home/hola/datos_seis/nacional_parques/prod/STEAD_SPREAD/tmean_pen_comp_011.nc"