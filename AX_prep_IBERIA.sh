#!/bin/sh

### Preparation of the Cordex data
conda activate parc
chmod 777 /home/hola/project/nacional_parques/script/nivo_parc/toolbox/*

# Site array
declare -a site_arr=("AIGUE" "GUADA" "ORDES" "PICOS" "SIERR") # "TEIDE")

res_simu="250m"

# Scenarios array
var_name="hurs"
# Init folders
dataCordexFolder="/home/hola/datos_seis/nacional_parques/data/cordex/EUR11"
prodFolder0="/home/hola/datos_seis/nacional_parques/prod/cordex"
CordexToolbox="/home/hola/project/nacional_parques/script/nivo_parc/toolbox"

inIberia_orog="/home/hola/datos_seis/nacional_parques/data/Iberia/Iberia01_v1.0_fix_010reg_orog.nc"
inIberia_pr="/home/hola/datos_seis/nacional_parques/data/Iberia/Iberia01_v1.0_DD_010reg_aa3d_pr.nc"
inIberia_tas="/home/hola/datos_seis/nacional_parques/data/Iberia/Iberia01_v1.0_DD_010reg_aa3d_tas.nc"


run_name="historical" # historical rcp85 rcp26
source $CordexToolbox/exp_list_${run_name}.txt


# for each historical scenario
# for each region
# regrid Iberia


for exp_name in "${exp_arr[@]}"
do

for site_name in "${site_arr[@]}"
do

echo $exp_name" - "$site_name" - "$var_name
dataFolder1=${dataCordexFolder}/${exp_name}/${run_name}/${site_name}/${var_name}
workFolder1=${prodFolder0}/${exp_name}/${run_name}/${site_name}/${var_name}
outIberiaFolder=${prodFolder0}/${exp_name}/${run_name}/${site_name}/iberia
echo mkdir -p ${outIberiaFolder}

inCordex=$(ls $dataFolder1/*_1990*)
cdo -remapbil,$inCordex $inIberia_orog ${outIberiaFolder}/orog_Iberia.nc
cdo -remapbil,$inCordex $inIberia_pr ${outIberiaFolder}/pr_Iberia.nc
cdo -remapbil,$inCordex $inIberia_tas ${outIberiaFolder}/tas_Iberia.nc

echo $inCordex

done
done
