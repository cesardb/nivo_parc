#!/usr/bin/python3
import argparse

####
# Extract relevant informations from a Cordex file name
####


def myget_Cordex_name_with_prefix_suffix(inNC, prefix=None, suffix=None, str2rem=None):
    
    if prefix is None:
        prefix=""
    if suffix is None:
        suffix=""

    outNCName = prefix+inNC.split("/")[-1].replace(".nc","")+suffix+".nc"
    
    if str2rem is not None:
        outNCName = outNCName.replace(str2rem,"")
    print(outNCName)
    
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description = "Extract relevant informations from a Cordex file name")
    parser.add_argument("-inNC", dest = "inNC")
    parser.add_argument("-prefix", dest = "prefix")
    parser.add_argument("-suffix", dest = "suffix")
    parser.add_argument("-str2rem", dest = "str2rem")
    args = parser.parse_args()
    
    myget_Cordex_name_with_prefix_suffix( args.inNC, args.prefix, args.suffix, args.str2rem)
    

