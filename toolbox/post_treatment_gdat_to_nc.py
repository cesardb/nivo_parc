from multiprocessing import Pool
import argparse
import itertools

# edit of sys.path required due to a weird bug/feature of python3.6 https://github.com/conda/conda/issues/6018
import sys
sys.path.append('/home/hola/project/nacional_parques/script')
from nivo_parc.toolbox import post_tools


def post_treatment_gdat_to_nc(sm_out_dir, sm_par_dir, exp, is_teide=0):

    if is_teide:
        run_dict = post_tools.read_run_list("/home/hola/project/nacional_parques/script/nivo_parc/toolbox/exp_list_EXP_TEIDE.txt")
    else:
        run_dict = post_tools.read_run_list("/home/hola/project/nacional_parques/script/nivo_parc/toolbox/exp_list_EXP.txt")

    run_list = run_dict[exp]
    
    if is_teide:
        reg_list = ["TEIDE"] 
        var_list = ["snod", "prec", "relh", "rpre", "smlt", "ssub", "tair", "qlin", "roff", "sden", "swed", "wdir", "wspd", "qsin", "albd", "var5", "var6"]
    
        res = int(250)      
    
        arg_list = [{"exp":xarg_list[0],
                     "run":xarg_list[1],
                     "reg":xarg_list[2],
                     "var":xarg_list[3],
                     "res":res,
                     "sm_out_dir":sm_out_dir,
                     "sm_par_dir":sm_par_dir} 
                     for xarg_list in list(itertools.product([exp], run_list, reg_list, var_list))]
    
    
        # write ctl file and dohy, hyear pkl file
        with Pool(40) as p:
            f_out = p.map(post_tools.f_prep_dohy_hyear, arg_list)        
    else:
        reg_list = ["AIGUE", "GUADA", "ORDES", "PICOS", "SIERR"] 
        var_list = ["snod", "prec", "relh", "rpre", "smlt", "ssub", "tair", "qlin", "roff", "sden", "swed", "wdir", "wspd", "qsin", "albd", "var5", "var6"]
    
        res = int(250)      
    
        arg_list = [{"exp":xarg_list[0],
                     "run":xarg_list[1],
                     "reg":xarg_list[2],
                     "var":xarg_list[3],
                     "res":res,
                     "sm_out_dir":sm_out_dir,
                     "sm_par_dir":sm_par_dir} 
                     for xarg_list in list(itertools.product([exp], run_list, reg_list, var_list))]
    
    
        # write ctl file and dohy, hyear pkl file
        with Pool(40) as p:
            f_out = p.map(post_tools.f_prep_dohy_hyear, arg_list)
        
        

    
if __name__ == "__main__": 
    parser = argparse.ArgumentParser(description = "")
    parser.add_argument("-sm_out_dir", dest = "sm_out_dir")
    parser.add_argument("-sm_par_dir", dest = "sm_par_dir")
    parser.add_argument("-exp", dest = "exp")
    parser.add_argument("-is_teide", dest = "is_teide", default=0)
    args = parser.parse_args()
    
    post_treatment_gdat_to_nc( args.sm_out_dir, args.sm_par_dir, args.exp, args.is_teide)
            
            
            
            
