import xarray as xr
import matplotlib as mpl
mpl.rcParams.update({'text.usetex': False,"svg.fonttype": 'none'})
import matplotlib.pyplot as plt
import numpy as np
from xclim import sdba
import pickle as pkl
import os
import argparse
import glob
import pandas as pd
from pyproj import Proj, transform
import fiona

#nCordex = "/home/hola/datos_seis/nacional_parques/prod/cordex/CNRM-CERFACS-CNRM-CM5__CNRM-ALADIN63/historical/AIGUE/pr/pr_EUR-11_CNRM-CERFACS-CNRM-CM5_historical_r1i1p1_CNRM-ALADIN63_v2_3hr_195101010300_end.nc"
#inCordex = "/home/hola/datos_seis/nacional_parques/prod/cordex/CNRM-CERFACS-CNRM-CM5__ICTP-RegCM4-6/historical/AIGUE/pr/pr_EUR-11_CNRM-CERFACS-CNRM-CM5_historical_r1i1p1_ICTP-RegCM4-6_v2_3hr_197001010300_end.nc"
#inIberia = "/home/hola/datos_seis/nacional_parques/data/Iberia/Iberia01_v1.0_DD_010reg_aa3d_pr.nc"
#inRefClim = "/home/hola/datos_seis/nacional_parques/prod/STEAD_SPREAD/SPREAD_pen_pcp_latlon_1950_2012_011.nc"
#out_QM_folder = "/home/hola/datos_seis/nacional_parques/prod/pkl/QM"
#calc_read = "calc_apply"
#method = "interp_point"
#daymean_monthsum = "monthsum"
#remove_drizzle_day=True
#out_fig_folder="/home/hola/datos_seis/port_transfert/"

def get_coord(xr_data):
    if "rlat" in xr_data.dims: 
        x_coord, y_coord = xr_data.rlon.to_numpy(), xr_data.rlat.to_numpy()         
        x_name, y_name = "rlon", "rlat"      
    else: 
        x_coord, y_coord = xr_data.x.to_numpy(), xr_data.y.to_numpy()
        x_name, y_name = "x", "y"
    return x_coord, y_coord, x_name, y_name
        
def read_prep_cordex_refclim(inCordex, inRefClim, daymean_monthsum, remove_drizzle_day):
    daily_min_precip_sum = 1 # kg m-2
    daily_min_precip_rate = daily_min_precip_sum / (24. * 60. * 60.) # kg m-2 s-1
    day_in_sec = 24 * 60 * 60
    hr3_in_sec = 3 * 60 * 60

    print("Cordex preparation", end="\n")    
    Cordex = xr.open_dataset(inCordex) # read Cordex file
    
    ### Various small fix
    if type(Cordex.time.values[0]) == np.float64:
        print("!! Warning: time dtype conversion")
        tt = Cordex.time.to_numpy()
        hh = np.array( ( tt - np.floor( tt ) ) * 24 ).astype( int ).astype( str )
        ttt = [t.split(".")[0]+"-"+h for (t, h) in zip(tt.astype(str), hh)]
        Cordex["time"] = pd.to_datetime(ttt, format='%Y%m%d-%H')
        
    #Cordex = Cordex.convert_calendar("gregorian", align_on="year", missing=np.nan)
    Cordex.lon.values[ Cordex.lon.values > 180 ] = Cordex.lon.values[ Cordex.lon.values > 180 ]-360 # ensure that lon coord is [-180; 180]
    ###
    
    if remove_drizzle_day: # not perfect because the daily sum might be > min but here we filter on the 3hr res
        Cordex["pr"] = Cordex["pr"].where( Cordex["pr"] >= daily_min_precip_rate, np.nan)
        
    # Resample Cordex data
    if daymean_monthsum == "daymean": # daily mean
        Cordex4qm = Cordex.resample( time='D' ).mean( keep_attrs=True ) 
        Cordex4qm["pr"] = Cordex4qm.pr.assign_attrs(units="kg m-2 s-1")
    elif daymean_monthsum == "monthsum": # monthly sum    
        Cordex4qm = Cordex.resample( time='M' ).sum( keep_attrs=True ) * hr3_in_sec
        Cordex4qm["pr"] = Cordex4qm.pr.assign_attrs(units="kg m-2")
    else:
        return
    Cordex4qm0 = Cordex4qm.copy(True)# kept without nan from the commomn period extraction below
    
    # Load the RefClim data
    print("RefClim preparation", end="\n")
    RefClim = xr.open_dataset(inRefClim)

    ### Various small fix
    if RefClim.time.dt.calendar != Cordex4qm.time.dt.calendar:
        RefClim = RefClim.convert_calendar(Cordex4qm.time.dt.calendar, align_on="year", missing=np.nan)
        # Unknown warning: /home/hola/miniconda3/envs/parc/lib/python3.10/site-packages/xarray/core/accessor_dt.py:72: FutureWarning: Index.ravel returning ndarray is deprecated; in a future version this will return a view on self.values_as_series = pd.Series(values.ravel(), copy=False)
    
    if "pcp" in RefClim.variables: # i.e. SPREAD dataset
        RefClim = RefClim.rename(pcp="pr")
        RefClim["pr"] = RefClim.pr / (10*day_in_sec) # convert from precip cumul to flux
        RefClim["pr"] = RefClim.pr.assign_attrs(units="kg m-2 s-1")
    else:
        RefClim["pr"] = RefClim.pr / day_in_sec # convert from precip cumul to flux
        RefClim["pr"] = RefClim.pr.assign_attrs(units="kg m-2 s-1")
                
    # Resample RefClim data
    RefClim = RefClim.interp({"lat":Cordex.lat, "lon":Cordex.lon}, method="linear")
    if remove_drizzle_day: # not perfect because Cordex is filtered on the 3hr res
        RefClim["pr"] = RefClim["pr"].where( RefClim["pr"] >= daily_min_precip_rate, np.nan)
    
    mask_nodata_RefClim_xy = ~np.isnan(RefClim.pr.mean(axis=0)) # mask where there are data (e.g. no data on the sea and France)

    RefClim4qm = RefClim.copy( deep=True) 
    
    if daymean_monthsum == "monthsum":    
        RefClim4qm = RefClim4qm.resample( time='M' ).sum( keep_attrs=True ) * day_in_sec
        RefClim4qm["pr"] = RefClim4qm.pr.assign_attrs(units="kg m-2")

    RefClim4qm = apply_mask_xy_to_dset_xyt(mask_nodata_RefClim_xy.data, RefClim4qm)
    RefClim = apply_mask_xy_to_dset_xyt(mask_nodata_RefClim_xy.data, RefClim)


    # mask Cordex and RefClim to keep common time period
    min_common_date = max( Cordex4qm.time[0] , RefClim4qm.time[0])
    max_common_date = min( Cordex4qm.time[-1], RefClim4qm.time[-1]) 
    
    Cordex4qm = Cordex4qm.where( Cordex4qm.time > min_common_date, np.nan)
    Cordex4qm = Cordex4qm.where( Cordex4qm.time < max_common_date, np.nan)
    RefClim4qm = RefClim4qm.where( RefClim4qm.time > min_common_date, np.nan)
    RefClim4qm = RefClim4qm.where( RefClim4qm.time < max_common_date, np.nan)
    
    return Cordex, RefClim, Cordex4qm, Cordex4qm0, RefClim4qm, mask_nodata_RefClim_xy,min_common_date, max_common_date

def QM_correction_calc_pr_point_interp(RefClim4qm_pnt, Cordex4qm_pnt):
    
    QM = sdba.EmpiricalQuantileMapping.train(
        RefClim4qm_pnt, Cordex4qm_pnt , nquantiles=20, group="time.month", kind="*"
        )
   
    return QM
                
def QM_correction_calc_pr_region_mean(RefClim4qm, Cordex4qm, mask_nodata_RefClim_xy):
    x_coord, y_coord, x_name, y_name = get_coord(Cordex4qm)

    Cordex4qm_masked = apply_mask_xy_to_dset_xyt(mask_nodata_RefClim_xy.data, Cordex4qm)
    Cordex4qm_mean = Cordex4qm_masked.mean(dim=(x_name, y_name))

    RefClim_region_mean = RefClim4qm.mean(dim=("x","y"))
    
    RefClim_region_mean["pr"] = RefClim_region_mean.pr.assign_attrs(units=Cordex4qm.pr.units)
    Cordex4qm_mean["pr"] = Cordex4qm_mean.pr.assign_attrs(units=Cordex4qm.pr.units)
    
    QM = sdba.EmpiricalQuantileMapping.train(
        RefClim_region_mean.pr, Cordex4qm_mean.pr, nquantiles=20, group="time.month", kind="*"
        )
    return QM    

def QM_correction_calc_pr(RefClim4qm, Cordex4qm, mask_nodata_RefClim_xy, method):
    x_coord, y_coord, x_name, y_name = get_coord(Cordex4qm)

    QM_dict = {}
    inan = 0
    #n_tot = int(Cordex4qm.pr.size / Cordex4qm.time.size)
    
    print("QM correction calculation", end="\n")
    
    if method == "interp_point":
        for ix, ttx in enumerate(x_coord):
            print("X "+str(ix)+"/"+str(len(x_coord)), end="\n")
            for iy, tty in enumerate(y_coord): # for each cordex point
                print("Y "+str(iy)+"/"+str(len(y_coord)), end="\r")
                
                # RefClim4qm = RefClim.interp(lat = Cordex4qm.lat[iy,ix], lon = Cordex4qm.lon[iy,ix], method="linear")
                RefClim4qm_pnt = RefClim4qm.pr[:, iy, ix]
                Cordex4qm_pnt = Cordex4qm.pr[:, iy, ix]
    
                if mask_nodata_RefClim_xy[iy, ix]: # mask of no data  RefClim4qm_pnt.std().values > 0: # if RefClim data available (not over France)
                    QM = QM_correction_calc_pr_point_interp(RefClim4qm_pnt, Cordex4qm_pnt)
                    QM_dict[ (ix, iy) ] = {"QM":QM, "ds":QM.ds}
                else:
                    if inan == 0:
                        QM_mean = QM_correction_calc_pr_region_mean(RefClim4qm, Cordex4qm, mask_nodata_RefClim_xy)
                    QM_dict[ (ix, iy) ] = {"QM":QM_mean, "ds":QM_mean.ds}
                    inan+=1
                    
    elif method == "region_mean":
        QM_mean = QM_correction_calc_pr_region_mean(RefClim4qm, Cordex4qm, mask_nodata_RefClim_xy)
        for ix, ttx in enumerate(x_coord):
            print("X "+str(ix)+"/"+str(len(x_coord)), end="\n")
            for iy, tty in enumerate(y_coord): # for each cordex point
                print("Y "+str(iy)+"/"+str(len(y_coord)), end="\r")
                QM_dict[ (ix, iy) ] = {"QM":QM_mean, "ds":QM_mean.ds}

    return QM_dict

def QM_correction_apply_pr(Cordex, Cordex4qm, RefClim4qm, mask_nodata_RefClim_xy, QM_dict, daymean_monthsum):

    # Correct Cordex
    Cordex_corr = Cordex.copy(deep=True)
    
    x_coord, y_coord, x_name, y_name = get_coord(Cordex)
 
    RefClim_region_mean = RefClim4qm.mean(dim=("x","y")).pr

    #n_tot = int(Cordex.pr.size/Cordex.time.size)
    if daymean_monthsum == "daymean":
        range_lim = [0,0.0006]
        var_name = "Daily precip rate (kg m-2 s-1)"
    if daymean_monthsum == "monthsum":
        range_lim = [0,1000]
        var_name = "Monthly precip cumul (kg m-2)"
    
    print("QM correction application", end="\n")
    for ix,ttx in enumerate(x_coord):
        print("X "+str(ix)+"/"+str(len(x_coord)), end="\n")
        for iy,tty in enumerate(y_coord): # for each cordex point
            print("Y "+str(iy)+"/"+str(len(y_coord)), end="\n")
            QM = QM_dict[ (ix, iy) ]["QM"]
            QM.ds = QM_dict[ (ix, iy) ]["ds"]
            
            Cordex4qm_corr = QM.adjust(Cordex4qm.pr[:, iy, ix], interp="linear")
            
            corr_factor_qm_res = Cordex4qm_corr / Cordex4qm.pr[:,iy,ix]
            corr_factor_qm_res[ np.isnan(corr_factor_qm_res) == True ] = 1 # otherwise problem arise when 0 precipitation
            corr_factor_hourly  = corr_factor_qm_res.resample(time='3H',closed="right").ffill().interp(time=Cordex.time).ffill(dim="time")
            Cordex_corr["pr"][:, iy, ix] = Cordex_corr["pr"][:, iy, ix] * corr_factor_hourly

            plt.figure(figsize=(6,6))
            if mask_nodata_RefClim_xy[iy,ix]:
                RefClim4qm_pnt = RefClim4qm.pr[:, iy, ix]
                lstyle="dotted"
            else:
                RefClim4qm_pnt = RefClim_region_mean.copy(deep=True)
                lstyle="dashed"
                
            tmp_cordex0 = Cordex4qm.pr[:, iy, ix]
            tmp_cordex1 = Cordex4qm_corr

            plt.subplot(221)
            h0 = plt.hist(tmp_cordex0.values.flatten(), bins=100, range=range_lim, histtype="step", color="r", label="sim.", cumulative=True)
            h1 = plt.hist(tmp_cordex1.values.flatten(), bins=100, range=range_lim, histtype="step", color="b", label="sim.+corr.", cumulative=True)
            h2 = plt.hist(RefClim4qm_pnt.values.flatten(), bins=100, range=range_lim, histtype="step", color="g", label="ref.", ls=lstyle, lw=2, cumulative=True)
            plt.xlabel( var_name )
            plt.xlim( range_lim )
            plt.title( "Cumulative histogram" )
            
            plt.subplot(222)
            plt.hist(tmp_cordex0.values.flatten(), bins=100, range=range_lim, histtype="step", color="r", label="sim.", cumulative=True, density=True)
            plt.hist(tmp_cordex1.values.flatten(), bins=100, range=range_lim, histtype="step", color="b", label="sim.+corr.", cumulative=True, density=True)
            plt.hist(RefClim4qm_pnt.values.flatten(), bins=100, range=range_lim, histtype="step", color="g", label="ref.", ls=lstyle, lw=2, cumulative=True, density=True)
            plt.xlim( range_lim)
            plt.ylim( [0, 1] )
            plt.xlabel( var_name )
            plt.title("Cumulative histogram\n normalized")
            
            plt.subplot(223)
            q = QM.ds.quantiles.values
            plt.plot(100*q, np.nanquantile(tmp_cordex0.values.flatten(),q=q), color="r")
            plt.plot(100*q, np.nanquantile(tmp_cordex1.values.flatten(),q=q), color="b")
            plt.plot(100*q, np.nanquantile(RefClim4qm_pnt.values.flatten(),q=q), ls=lstyle, lw=2, color="g")
            plt.xlabel("Percentile")
            plt.ylabel( var_name )
            plt.title("Percentile")
            
            plt.subplot(224)
            h0 = np.histogram(tmp_cordex0.values.flatten(), bins=100, range=range_lim)
            hsum = h0[0]*h0[1][:-1]+((h0[1][1]-h0[1][0])/2)
            plt.plot(h0[1][:-1], hsum.cumsum(), color="r", label="sim.")
            
            h0 = np.histogram(tmp_cordex1.values.flatten(), bins=100, range=range_lim)
            hsum = h0[0]*h0[1][:-1]+((h0[1][1]-h0[1][0])/2)
            plt.plot(h0[1][:-1], hsum.cumsum(), color="b", label="sim.+corr.")

            h0 = np.histogram(RefClim4qm_pnt.values.flatten(), bins=100, range=range_lim)
            hsum = h0[0]*h0[1][:-1]+((h0[1][1]-h0[1][0])/2)
            if mask_nodata_RefClim_xy[iy,ix]:
                plt.plot(h0[1][:-1], hsum.cumsum(), color="g", ls=lstyle, lw=2, label="ref.")
            else:
                plt.plot(h0[1][:-1], hsum.cumsum(), color="g", ls=lstyle, lw=2, label="ref. (region mean)")
            
            plt.legend()
            plt.xlabel(var_name)
            plt.ylabel("Cumulative precip")
            plt.title("Cumulative precip")
                        
            plt.tight_layout()
            plt.savefig("/home/hola/datos_seis/port_transfert/plot_quantile_"+str(ix)+"_"+str(iy)+".png")
            plt.close()
    
    # Convert the nan to zero otherwise snowmodel sad
    Cordex_corr["pr"] = Cordex_corr["pr"].where(~np.isnan(Cordex_corr.pr),0)

    return Cordex_corr
                      
def apply_mask_xy_to_dset_xyt(mask_xy, dset_xyt):
    # Creating a mask dataset with the same coordinates as RefClim4qm
    mask_dataset = xr.Dataset({'mask': (('y', 'x'), mask_xy)},
                              coords={'y': dset_xyt['y'], 'x': dset_xyt['x']})
    
    # Broadcasting the mask to the time dimension
    mask_dataset_broadcasted = mask_dataset.expand_dims(time=dset_xyt['time'])
    
    # Applying the mask to RefClim4qm
    dset_xyt = dset_xyt.where(mask_dataset_broadcasted['mask'] == 1)
    
    return dset_xyt

def QM_correction_pr(inRefClim, inCordex, out_QM_folder, out_fig_folder, out_NC_folder, calc_read, method, daymean_monthsum, remove_drizzle_day):
    # main function
    reg = inCordex.split("/")[-3]
    exp = inCordex.split("/")[-5]
    
    Cordex, RefClim, Cordex4qm, Cordex4qm0, RefClim4qm, mask_nodata_RefClim_xy, min_common_date, max_common_date = read_prep_cordex_refclim(inCordex, inRefClim, daymean_monthsum, remove_drizzle_day)
    dt_common = ( max_common_date - min_common_date).dt.days / 365

    # read time units hours or days 
    Cordex4units = xr.open_dataset(inCordex, decode_times=False)
    time_units = Cordex4units.time.units
    Cordex4units.close()
    
    if calc_read == "calc_apply":
        print("Calculating quantile mapping file.")
        QM_dict = QM_correction_calc_pr(RefClim4qm, Cordex4qm, mask_nodata_RefClim_xy, method)
        
        # Save QM_dict to apply to projections (RCPs)
        if not os.path.exists(out_QM_folder):
            os.makedirs(out_QM_folder)
        
        with open(out_QM_folder+"/"+inCordex.split("/")[-1].replace(".nc","")+"_"+reg+"_qm.p", 'wb') as p:
            pkl.dump(QM_dict, p)
        
    elif calc_read == "read_apply":
        print("Reading quantile mapping file.")
        tt = inCordex.split("/")[-1].split("_")
        in_QM = glob.glob(out_QM_folder+"/"+"_".join([xx if "rcp" not in xx else "historical" for xx in tt][0:-3])+"*_"+reg+"_qm.p")[0]
        with open(in_QM, 'rb') as p:
            QM_dict = pkl.load(p)
    
    Cordex_corr = QM_correction_apply_pr(Cordex, Cordex4qm0, RefClim4qm, mask_nodata_RefClim_xy, QM_dict, daymean_monthsum)

    # Save corrected variable
    out_NC_folder_full = "/".join(inCordex.split("/")[:-2])+"/"+out_NC_folder+"/"
    if not os.path.exists(out_NC_folder_full):
        os.makedirs(out_NC_folder_full)
        
    Cordex_corr.to_netcdf(out_NC_folder_full+"/"+inCordex.split("/")[-1], encoding={'time':{'units':time_units}}) # 'days since '+str(Cordex.time[0].dt.strftime("%Y-%m-%d %H:%M:%S").values)}})

    #########################
    ##### Figure ############
    #########################

    # make fig
    xx = Cordex.pr.values.flatten() # where(Cordex.pr>min_precip)
    yy = Cordex_corr.pr.values.flatten() # .where( Cordex_corr.pr>min_precip)
    plt.plot(xx, yy , ".", ms=2, alpha=0.1)
    xl = list(plt.gca().get_xlim())
    plt.plot(xl,xl,lw=1,color="k")
    xl[0]=0
    plt.axis("square")
    plt.axis( xl+xl )
    plt.grid()
    plt.xlabel("Precip before quantile-mapping") 
    plt.ylabel("Precip after quantile-mapping") 
    plt.savefig(out_fig_folder+"/scatter_qm_"+inCordex.split("/")[-1].split("3hr")[0]+reg+".png")
    plt.savefig(out_fig_folder+"/scatter_qm_"+inCordex.split("/")[-1].split("3hr")[0]+reg+".svg")
    

    # map viz of the QM correction    
    dt_3h = 3*60*60
    dt_24h = 24*60*60
    dCordex = Cordex_corr-Cordex
    
    # load parc contour
    in_shp_parc = glob.glob("/home/hola/datos_seis/nacional_parques/data/contour/limite_"+reg+"*.shp")[0]
    shape = fiona.open(in_shp_parc)
    contour= shape.next()
    if reg=="PICOS":
        lon = np.array(contour["geometry"]["coordinates"][0]).squeeze()[:,0] # hay dos huecitos en Picos...
        lat = np.array(contour["geometry"]["coordinates"][0]).squeeze()[:,1]
    else:
        lon = np.array(contour["geometry"]["coordinates"]).squeeze()[:,0]
        lat = np.array(contour["geometry"]["coordinates"]).squeeze()[:,1]
    lon_parc, lat_parc = transform(Proj(shape.crs), Proj(init='EPSG:4326'), lon, lat)
    
    # select by season
    for months_to_select, season_name in zip([[12,1,2], [3,4,5], [6,7,8], [9,10,11], [1,2,3,4,5,6,7,8,9,10,11,12]]
                                             ,["winter","spring", "summer", "autumn", "allyear"]):
        
        tt = Cordex.sel(time=slice(min_common_date,max_common_date))
        data1 = tt.sel({"time":tt.time.dt.month.isin(months_to_select)}).sum(dim="time").pr * dt_3h / dt_common.values
        
        tt = RefClim.sel(time=slice(min_common_date,max_common_date))
        data2 = tt.sel({"time":tt.time.dt.month.isin(months_to_select)}).pr.sum(dim="time").where(mask_nodata_RefClim_xy) * dt_24h / dt_common.values
        
        tt = Cordex_corr.sel(time=slice(min_common_date,max_common_date))
        data3 = tt.sel({"time":tt.time.dt.month.isin(months_to_select)}).sum(dim="time").pr * dt_3h / dt_common.values
        
        tt = dCordex.sel(time=slice(min_common_date,max_common_date))
        data4 = tt.sel({"time":tt.time.dt.month.isin(months_to_select)}).sum(dim="time").pr * dt_3h / dt_common.values
        
        data5 = data1-data2
        data6 = data3-data2
        
        if season_name in ["winter","spring", "summer", "autumn"]:
            vmin, vmax = 0, 1000
            dvmin, dvmax = -200, 200
        else:
            vmin, vmax = 0, 3000
            dvmin, dvmax = -750, 750
    
        # bounding box coordinate
        ymin_cordex, ymax_cordex, xmin_cordex, xmax_cordex = np.min(Cordex.lat), np.max(Cordex.lat), np.min(Cordex.lon), np.max(Cordex.lon)
        ymin_refclim, ymax_refclim, xmin_refclim, xmax_refclim = np.min(RefClim.lat), np.max(RefClim.lat), np.min(RefClim.lon), np.max(RefClim.lon)
        
        # Create a 2x2 grid of subplots
        fig, axes = plt.subplots(nrows=2, ncols=3)
        
        ax1 = axes[0,0] 
        im1 = ax1.imshow(data1, cmap='viridis', vmin=vmin, vmax=vmax, extent = (xmin_cordex, xmax_cordex, ymin_cordex, ymax_cordex), origin="lower")
        ax1.plot(lon_parc, lat_parc)
        ax1.set_xticklabels("")
        cs = ax1.contour(data1.lon.data, data1.lat.data, data1 ,origin="lower", levels=np.arange(0, 5000, 200) ,colors="k",linewidths=0.5)
        ax1.set_xticklabels("")
        ax1.set_yticklabels("")
        ax1.clabel(cs, fontsize="smaller" )
        ax1.set_title("Cordex")
        
        ax2 = axes[0,1]
        ax2.imshow( data2, cmap='viridis', vmin=vmin, vmax=vmax, extent = (xmin_refclim, xmax_refclim, ymin_refclim, ymax_refclim), origin="lower")
        ax2.axis([xmin_cordex, xmax_cordex, ymin_cordex, ymax_cordex])
        ax2.plot(lon_parc, lat_parc)
        ax2.set_xticklabels("")
        ax2.set_yticklabels("")
        cs = ax2.contour(data2.lon.data, data2.lat.data, data2 ,origin="lower", levels=np.arange(0, 5000, 200) ,colors="k",linewidths=0.5)
        ax2.set_xticklabels("")
        ax2.set_yticklabels("")
        ax2.clabel(cs, fontsize="smaller" )
        ax2.set_title("RefClim")
             
        ax3 = axes[1,0] 
        im3 = ax3.imshow(data3, cmap='viridis', vmin=vmin, vmax=vmax, extent = (xmin_cordex, xmax_cordex, ymin_cordex, ymax_cordex), origin="lower")
        ax3.plot(lon_parc, lat_parc)
        cs = ax3.contour(data3.lon.data, data3.lat.data, data3 ,origin="lower", levels=np.arange(0, 5000, 200) ,colors="k",linewidths=0.5)
        ax3.set_xticklabels("")
        ax3.set_yticklabels("")
        ax3.clabel(cs, fontsize="smaller" )
        ax3.set_title("Cordex + QM corr.")
        
        ax4 = axes[1,1] 
        im4 = ax4.imshow(data4, cmap='RdYlBu', vmin=dvmin, vmax=dvmax, extent = (xmin_cordex, xmax_cordex, ymin_cordex, ymax_cordex), origin="lower")
        ax4.plot(lon_parc, lat_parc)
        ax4.set_xticklabels("")
        ax4.set_yticklabels("")
        ax4.set_title("QM corr.")
        
        ax5 = axes[0,2]
        ax5.imshow( data5, cmap='RdYlBu', vmin=dvmin, vmax=dvmax, extent = (xmin_refclim, xmax_refclim, ymin_refclim, ymax_refclim), origin="lower")
        ax5.axis([xmin_cordex, xmax_cordex, ymin_cordex, ymax_cordex])
        ax5.plot(lon_parc, lat_parc)
        ax5.set_xticklabels("")
        ax5.set_yticklabels("")
        #cs = ax5.contour(data2.lon.data, data2.lat.data, data2 ,origin="lower", levels=np.arange(0, 5000, 200) ,colors="k",linewidths=0.5)
        #ax2.clabel(cs, fontsize="smaller" )
        ax5.set_title("Cordex - RefClim")
        
        ax6 = axes[1,2]
        ax6.imshow( data6, cmap='RdYlBu', vmin=dvmin, vmax=dvmax, extent = (xmin_refclim, xmax_refclim, ymin_refclim, ymax_refclim), origin="lower")
        ax6.axis([xmin_cordex, xmax_cordex, ymin_cordex, ymax_cordex])
        ax6.plot(lon_parc, lat_parc)
        ax6.set_xticklabels("")
        ax6.set_yticklabels("")
        #cs = ax5.contour(data2.lon.data, data2.lat.data, data2 ,origin="lower", levels=np.arange(0, 5000, 200) ,colors="k",linewidths=0.5)
        #ax2.clabel(cs, fontsize="smaller" )
        ax6.set_title("Cordex + QM_corr - RefClim")
                
        plt.tight_layout()
        
        fig.subplots_adjust(bottom=0.3)
        cbar_ax = fig.add_axes([0.25, 0.15, 0.15, 0.04])
        fig.colorbar(im1, cax=cbar_ax, orientation='horizontal', label='Mean '+season_name+' precipitation (mm)')
        
        cbar_ax = fig.add_axes([0.6, 0.15, 0.15, 0.04])
        fig.colorbar(im4, cax=cbar_ax, orientation='horizontal', label='Precipitation difference (mm)')
    
        plt.savefig(out_fig_folder+"/map_qm_"+inCordex.split("/")[-1].split("3hr")[0]+reg+"_"+season_name+".png")
        plt.savefig(out_fig_folder+"/map_qm_"+inCordex.split("/")[-1].split("3hr")[0]+reg+"_"+season_name+".svg")
        plt.close()
    
        # Create a 2x2 grid of subplots
        data5 = data1-data2
        data6 = data3-data2
        
        plt.figure()        
        plt.axis("square")
        plt.hlines(0, -2000, 2000, "k", lw=0.5)
        plt.vlines(0, -2000, 2000, "k", lw=0.5)
        
        plt.plot(data5.values.flatten(), data6.values.flatten(), ".", zorder=2)
        plt.plot([-2000, 2000], [-2000, 2000], "k", lw=0.5)
        plt.plot([-2000, 2000], [2000, -2000], "k", lw=0.5)        
        plt.axis([-1000, 1000, -1000, 1000])

        plt.title(season_name+" cumulative precip - CORDEX minus RefClim")
        plt.xlabel("before corr.")
        plt.ylabel("after corr.")
        plt.grid(True)
        plt.savefig(out_fig_folder+"/scatter_yearly_diff_qm_"+inCordex.split("/")[-1].split("3hr")[0]+reg+"_"+season_name+".png")
        plt.savefig(out_fig_folder+"/scatter_yearly_diff_qm_"+inCordex.split("/")[-1].split("3hr")[0]+reg+"_"+season_name+".svg")
        plt.close()

        data5 = data1/data2
        data6 = data3/data2
        
        plt.figure()        
        plt.axis("square")
        plt.hlines(0, -4, 4, "k", lw=0.5)
        plt.vlines(0, -4, 4, "k", lw=0.5)
        
        plt.plot(data5.values.flatten(), data6.values.flatten(), ".", zorder=2)
        #plt.plot([-4, 4], [-4, 4], "k", lw=0.5)
        #plt.plot([-4, 4], [4, -4], "k", lw=0.5)     
        plt.plot([-4, 4], [1, 1], "k", lw=0.5)        
        plt.axis([0, 3, 0, 3])

        plt.title(season_name+" cumulative precip - CORDEX minus RefClim")
        plt.xlabel("before corr.")
        plt.ylabel("after corr.")
        plt.grid(True)
        plt.savefig(out_fig_folder+"/scatter_yearly_ratio_qm_"+inCordex.split("/")[-1].split("3hr")[0]+reg+"_"+season_name+".png")
        plt.savefig(out_fig_folder+"/scatter_yearly_ratio_qm_"+inCordex.split("/")[-1].split("3hr")[0]+reg+"_"+season_name+".svg")
        plt.close()
        
        

    x_coord, y_coord, x_name, y_name = get_coord(Cordex)
                
    plt.figure(figsize=[10,10])
    colors = plt.cm.viridis(np.linspace(0,1,20))
    
    for ix,ttx in enumerate(x_coord):
        for iy,tty in enumerate(y_coord[::-1]): # for each cordex point
            plt.subplot(len(y_coord), len(x_coord), ix+1+len(x_coord)*iy)
    
            for ii,qm in enumerate(QM_dict[(ix,len(y_coord)-1-iy)]["ds"]["af"].T):
                if np.mod(ii,5)==0:
                    plt.plot(qm, color=colors[ii], label=str(int(qm.quantiles.values*100))+"th perc." )
                else:
                    plt.plot(qm, color=colors[ii])
            #plt.legend()
            plt.hlines(1,0,11, "r", lw=1.5)
            plt.grid(True, lw=0.5)
            if iy < len(y_coord)-1:
                plt.tick_params(
                    axis='x',          # changes apply to the x-axis
                    labelbottom=False)                  
            
            if ix>0:
                plt.xticks([0,3,6,9], ["Jan", "Apr", "Jul", "Oct"])
                plt.tick_params(
                    axis='y',          # changes apply to the x-axis
                    labelleft=False)       
            
            if (iy == len(y_coord)-1) & (ix == 0):     
                plt.xticks([0,3,6,9], ["Jan", "Apr", "Jul", "Oct"])
                
            plt.axis([0,11,0,3])
        
    plt.tight_layout()
    plt.savefig(out_fig_folder+"/plot_qm_factor_"+inCordex.split("/")[-1].split("3hr")[0]+reg+".png")
    plt.savefig(out_fig_folder+"/plot_qm_factor_"+inCordex.split("/")[-1].split("3hr")[0]+reg+".svg")


if __name__ == "__main__": 
    parser = argparse.ArgumentParser(description = "Calculate and apply quantile-mapping correction from RefClim reanalysis and Cordex data.")
    parser.add_argument("-inRefClim", dest = "inRefClim")
    parser.add_argument("-inCordex", dest = "inCordex")
    parser.add_argument("-out_QM_folder", dest = "out_QM_folder")
    parser.add_argument("-out_fig_folder", dest = "out_fig_folder")
    parser.add_argument("-out_NC_folder", dest = "out_NC_folder")
    parser.add_argument("-calc_read", dest = "calc_read")
    parser.add_argument("-method", dest = "method")
    parser.add_argument("-daymean_monthsum", dest = "daymean_monthsum")
    parser.add_argument("-remove_drizzle_day", dest = "remove_drizzle_day")
    args = parser.parse_args()
    
    QM_correction_pr( args.inRefClim, args.inCordex, args.out_QM_folder,  args.out_fig_folder, args.out_NC_folder, args.calc_read, args.method, args.daymean_monthsum, args.remove_drizzle_day)
    
    
