#!/home/cesardb/anaconda3/envs/parc/bin/python
import rasterio
import glob as glob
import numpy as np
import argparse


def convert_land_cover_map_CORINE2SnowModel(inLCM, outLCM, CORINE2SM):

    LabelTable = np.genfromtxt(CORINE2SM,dtype=int,delimiter=",")
  
    with rasterio.open(inLCM) as srcLCM:
        inRST = srcLCM.read()
        out_meta = srcLCM.meta.copy()
        outRST = np.copy(inRST)
        
        for CORINE_label, SM_label in zip(LabelTable[:,0],LabelTable[:,1]):
            outRST[inRST==CORINE_label] = SM_label
        
        outRST = np.squeeze(outRST)
        
        with rasterio.open(fp=outLCM, mode='w',**out_meta) as dst:
            dst.write(outRST, 1) # the numer one is the number of bands

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description = "Extract relevant informations from a Cordex file name")
    parser.add_argument("-inLCM", dest = "inLCM")
    parser.add_argument("-outLCM", dest = "outLCM")
    parser.add_argument("-CORINE2SM", dest = "CORINE2SM")
    args = parser.parse_args()
    
    convert_land_cover_map_CORINE2SnowModel( args.inLCM, args.outLCM, args.CORINE2SM)
    

