#!/usr/bin/python3
import glob
import argparse
import os

####
# Extract relevant informations from a Cordex file name
####


def myget_Cordex_full_period_name(inNCFolder, inTimeStep):
    
    NCList = glob.glob( inNCFolder+"/*.nc")
    
    tList=[]
    if len(NCList)>0:
        for File in NCList:
            tList.append(int(File.split("_"+inTimeStep+"_")[1].split("-")[0]))
            try:
                tList.append(int(File.split("_"+inTimeStep+"_")[1].split("-")[1].split(".nc")[0]))
                suffix = ".nc"
            except ValueError:
                tList.append(int(File.split("_"+inTimeStep+"_")[1].split("-")[1].split("_")[0]))
                suffix = "_"+File.split("_"+inTimeStep+"_")[1].split("-")[1].split("_")[1]
        FileBasename = os.path.basename(NCList[0]).split("_"+inTimeStep+"_")[0]+"_"+inTimeStep+"_"
        outNCName = FileBasename+str(min(tList))+"-"+str(max(tList))+suffix
    else:
        outNCName=""
    print(outNCName)

    
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description = "Extract relevant informations from a Cordex file name")
    parser.add_argument("-inNCFolder", dest = "inNCFolder")
    parser.add_argument("-inTimeStep", dest = "inTimeStep")
    args = parser.parse_args()
    
    myget_Cordex_full_period_name( args.inNCFolder, args.inTimeStep)
    

