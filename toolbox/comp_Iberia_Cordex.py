
import xarray as xr
import glob 
import matplotlib as mpl
mpl.rcParams.update({'text.usetex': False,"svg.fonttype": 'none'})
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from xclim import sdba


def QM_hist_pr(inRef, inCordex, out_QM_folder):
    
    inCordex="/home/hola/datos_seis/nacional_parques/prod/cordex/CNRM-CERFACS-CNRM-CM5__CNRM-ALADIN63/historical/AIGUE/pr/pr_EUR-11_CNRM-CERFACS-CNRM-CM5_historical_r1i1p1_CNRM-ALADIN63_v2_3hr_195101010300_end.nc"
    inIberia="/home/hola/datos_seis/nacional_parques/data/Iberia/Iberia01_v1.0_DD_010reg_aa3d_pr.nc"
    
    # Load the netCDF files
    Iberia = xr.open_dataset(inIberia)
    day_in_sec = 24*60*60
    Iberia["pr"] = Iberia.pr/day_in_sec # convert from precip cumul to flux
    Iberia["pr"] = Iberia.pr.assign_attrs(units="kg m-2 s-1")
    
    Cordex = xr.open_dataset(inCordex)
    Cordex_daily = Cordex.resample(time='D').mean(keep_attrs=True)
    Cordex_daily = Cordex_daily.convert_calendar("gregorian", align_on="year", missing=np.nan)
    Cordex_daily["pr"] = Cordex_daily.pr.assign_attrs(units="kg m-2 s-1")
    
    QM_dict = {}
    inan = 0
    for ix,x in enumerate(Cordex_daily.x.values):
        for iy,y in enumerate(Cordex_daily.y.values): # for each cordex point
            Iberia_int = Iberia.interp(lat = Cordex_daily.lat[iy,ix], lon = Cordex_daily.lon[iy,ix], method="linear")
            if ~np.isnan(Iberia_int.pr.mean().values): # if Iberia data available (not over France)
                print(ix,iy)
                QM = sdba.EmpiricalQuantileMapping.train(
                    Iberia_int.pr, Cordex_daily.pr[:,iy,ix], nquantiles=20, group="time.month", kind="*"
                    )
                QM_dict[(ix,iy)] = QM
    
            else:
                if inan == 0:
                    # Define the latitude and longitude bounds
                    lat_min, lat_max = Cordex.lat.min().values, Cordex.lat.max().values
                    lon_min, lon_max = Cordex.lon.min().values, Cordex.lon.max().values
                    
                    # Select the data within the specified latitude and longitude bounds
                    Iberia_region_mean = Iberia.sel(lat=slice(lat_min, lat_max), lon=slice(lon_min, lon_max)).mean(dim=("lat","lon"))
                    Cordex_daily_mean = Cordex.mean(dim=("x","y"))
                    
                    Iberia_region_mean["pr"] = Iberia_region_mean.pr.assign_attrs(units="kg m-2 s-1")
                    Cordex_daily_mean["pr"] = Cordex_daily_mean.pr.assign_attrs(units="kg m-2 s-1")
                    
                    QM_mean = sdba.EmpiricalQuantileMapping.train(
                        Iberia_region_mean.pr, Cordex_daily_mean.pr, nquantiles=20, group="time.month", kind="*"
                        )
                QM_dict[(ix,iy)] = QM_mean
                inan+=1
    
    
    # Correct Cordex
    Cordex_corr = Cordex.copy(deep=True)
    for ix,x in enumerate(Cordex.x.values):
        for iy,y in enumerate(Cordex.y.values): # for each cordex point
            QM = QM_dict[(ix,iy)]
            
            Cordex_daily_corr = QM.adjust(Cordex_daily.pr[:,iy,ix], interp="linear")
            corr_factor_daily = Cordex_daily_corr/Cordex_daily.pr[:,iy,ix]
            corr_factor_hourly  = corr_factor_daily.resample(time='3H',closed="right").ffill().interp(time=Cordex.time).ffill(dim="time")
            Cordex_corr["pr"][:,iy,ix] = Cordex_corr["pr"][:,iy,ix]*corr_factor_hourly
            
            
    # Save QM_dict to apply to projections (RCPs)
    import pickle as pkl
    
    # Save corrected variable
    Cordex_corr.to_netcdf()


plt.close("all")
plt.plot(np.quantile(Iberia_int.pr.values.flatten(),[0.05,0.25,0.50,0.75,0.95]),np.quantile(Cordex_daily.pr[:,iy,ix].values.flatten(),[0.05,0.25,0.50,0.75,0.95]),"r")
plt.plot(np.quantile(Iberia_int.pr.values.flatten(),[0.05,0.25,0.50,0.75,0.95]),np.quantile(Cordex_daily_corr.values.flatten(),[0.05,0.25,0.50,0.75,0.95]), "g")
plt.axis("square")
plt.plot([0,0.0004],[0,0.0004],"k")
plt.savefig("/home/hola/datos_seis/port_transfert/ttt.png")


def groupby_multicoords(da, fields): 
    # https://stackoverflow.com/questions/60380993/groupby-multiple-coords-along-a-single-dimension-in-xarray
    common_dim = da.coords[fields[0]].dims[0]
    tups_arr = np.empty(len(da[common_dim]), dtype=object)
    tups_arr[:] = list(zip(*(da[f].values for f in fields)))
    return da.assign_coords(grouping_zip=xr.DataArray(tups_arr, dims=common_dim)).groupby('grouping_zip')


plt_dict = {
    "historical":{"col":"#4a4e4d", "mrk":"v"},
    "rcp26":{"col":"#f6cd61", "mrk":"^"},
    "rcp85":{"col":"#fe8a71", "mrk":">"},
    "AIGUE":{"col":"#ee4035","mrk":"H"},
    "GUADA":{"col":"#f37736","mrk":"X"},
    "ORDES":{"col":"#ffe800","mrk":"o"},
    "PICOS":{"col":"#7bc043","mrk":"d"},
    "SIERR":{"col":"#0392cf","mrk":"s"},
    "DJF":{"col":"#0057e7","mrk":"*"},
    "MAM":{"col":"#008744","mrk":"s"},
    "JJA":{"col":"#d62d20","mrk":"."},
    "SON":{"col":"#ffa700","mrk":"^"},
    }


sm_fig_out_dir = "/home/hola/datos_seis/nacional_parques/fig/sm/sm_Tlr_53"


run_dict = {
    "historical": ["CNRM-CERFACS-CNRM-CM5__CNRM-ALADIN63", 
                   "CNRM-CERFACS-CNRM-CM5__ICTP-RegCM4-6", 
                   "ICHEC-EC-EARTH__ICTP-RegCM4-6", 
                   "MOHC-HadGEM2-ES__CNRM-ALADIN63", 
                   "MOHC-HadGEM2-ES__ICTP-RegCM4-6", 
                   "MPI-M-MPI-ESM-LR__CNRM-ALADIN63", 
                   "MPI-M-MPI-ESM-LR__ICTP-RegCM4-6", 
                   "NCC-NorESM1-M__CNRM-ALADIN63", 
                   "NCC-NorESM1-M__ICTP-RegCM4-6"],
    "rcp26":      ["NCC-NorESM1-M__ICTP-RegCM4-6"],
                   #"MOHC-HadGEM2-ES__ICTP-RegCM4-6",
                   #"MPI-M-MPI-ESM-LR__ICTP-RegCM4-6"],
    "rcp85":      ["NCC-NorESM1-M__ICTP-RegCM4-6",
                   "NCC-NorESM1-M__CNRM-ALADIN63",
                   "MPI-M-MPI-ESM-LR__ICTP-RegCM4-6",
                   "MPI-M-MPI-ESM-LR__CNRM-ALADIN63",
                   "CNRM-CERFACS-CNRM-CM5__ICTP-RegCM4-6",
                   "MOHC-HadGEM2-ES__CNRM-ALADIN63",
                   "MOHC-HadGEM2-ES__ICTP-RegCM4-6",
                   "ICHEC-EC-EARTH__ICTP-RegCM4-6"]
    }

out_dir_dict = {"historical":"/home/hola/datos_seis/nacional_parques/prod",
                "rcp26":"/home/hola/datos_seis/nacional_parques/prod",
                "rcp85":"/home/hola/datos_seis/nacional_parques/prod"}


reg_list = ["AIGUE", "GUADA", "ORDES", "PICOS", "SIERR"] 

for reg in reg_list:
    inIberia = "/home/hola/datos_seis/nacional_parques/data/Iberia/pr_Iberia_"+reg+".nc"
    for run in run_dict["historical"]:
        print(reg+" - "+run)
        # For one Cordex member
        inCordex = glob.glob("/home/hola/datos_seis/nacional_parques/prod/cordex/"+run+"/historical/"+reg+"/pr/pr_*_end.nc")[0]
    
        # Load the netCDF files
        Iberia = xr.open_dataset(inIberia)
        #Iberia = Iberia.assign( hyear = ("time", [yy-1 if mm<9 else yy for yy,mm in zip(Iberia["time"].dt.year, Iberia["time"].dt.month)  ]))
        Iberia = Iberia.assign_coords( season = ("time", Iberia.time.dt.season.values.squeeze()))
        Iberia = Iberia.assign_coords( hyear = ("time", [yy-1 if mm<9 else yy for yy,mm in zip(Iberia["time"].dt.year, Iberia["time"].dt.month)  ]))
        
        Cordex = xr.open_dataset(inCordex)
        Cordex = Cordex.assign_coords( hyear = ("time", [yy-1 if mm<9 else yy for yy,mm in zip(Cordex["time"].dt.year, Cordex["time"].dt.month)  ]))
        Cordex = Cordex.assign_coords( season = ("time", Cordex.time.dt.season.values.squeeze()))
        
        # bug if non standard calendar
        #if type(Cordex.indexes["time"]) is xr.coding.cftimeindex.CFTimeIndex:
        #    tt_dt = Cordex.indexes["time"].to_datetimeindex(unsafe=True)
        #    Cordex["time"] = tt_dt
        #time_step = Cordex.time.dt.date[1].time-Cordex.time.dt.date[0].time
        #time_step_sec = time_step.values.astype('timedelta64[s]').astype(int)
        time_step_sec = 10800
        Cordex["pr"]=Cordex.pr*time_step_sec # convert from precip flux to cumul
        
        plt.figure()
        for ix,x in enumerate(Cordex.x.values):
            for iy,y in enumerate(Cordex.y.values):
                Iberia_int = Iberia.interp(lat = Cordex.lat[iy,ix], lon = Cordex.lon[iy,ix])
                if ~np.isnan(Iberia_int.pr.mean().values):
                    ### Iberia
                    Iberia_int_gp_hyear_season = groupby_multicoords( Iberia_int, ["hyear", "season"])
                    Iberia_int_hyear_season_mean = Iberia_int_gp_hyear_season.sum()
                    
                    hyear_list, season_list, pr_list = [], [], []
                    for ii,gp in enumerate(Iberia_int_hyear_season_mean.grouping_zip):
                        hyear_list.append(gp.grouping_zip.values.flatten()[0][0])
                        season_list.append(gp.grouping_zip.values.flatten()[0][1])
                        pr_list.append(Iberia_int_hyear_season_mean.pr.values[ii])
            
                    Iberia_df = pd.DataFrame({"hyear":hyear_list, "season":season_list, "pr":pr_list, "ix":ix, "iy":iy})     
                 
                 
                    ### Cordex
                    tt = Cordex.pr[:,iy,ix]
                    Cordex_int_gp_hyear_season = groupby_multicoords( tt, ["hyear", "season"])
                    Cordex_int_hyear_season_mean = Cordex_int_gp_hyear_season.sum()
                
                    hyear_list, season_list, pr_list = [], [], []
                    for ii,gp in enumerate(Cordex_int_hyear_season_mean):
                        hyear_list.append(gp.grouping_zip.values.flatten()[0][0])
                        season_list.append(gp.grouping_zip.values.flatten()[0][1])
                        pr_list.append(gp.values.flatten()[0])
            
                    Cordex_df = pd.DataFrame({"hyear":hyear_list, "season":season_list, "pr":pr_list})          
                    
                    for ll in Iberia_df.iterrows():
                        hyear = ll[1].hyear
                        season = ll[1].season
                        cc = Cordex_df[(Cordex_df.season==season) & (Cordex_df.hyear==hyear)]
                        if len(cc) > 0:
                            plt.plot(ll[1].pr, cc.pr.values, color=plt_dict[season]["col"], marker=plt_dict[season]["mrk"], ms=6, mew=0, ls="", label=season)
    
        plt.ylabel("Simu. cumulative precipitation (mm)")
        plt.xlabel("Iberia cumulative  precipitation (mm)")
        plt.axis("square")
        plt.plot([0, 2000], [0, 2000],"k", lw=0.6)
        plt.axis((0, 2000, 0, 2000))
        plt.savefig(sm_fig_out_dir+"/pr_"+reg+"_"+run+"_Iberia.png")
        plt.close()
        Iberia.close()
        Cordex.close()
    
# Quantile mapping
from xclim import sdba
inIberia="/home/hola/datos_seis/nacional_parques/data/Iberia/Iberia01_v1.0_DD_010reg_aa3d_pr.nc"


# convert daily to monthly
cdo monavg /home/hola/datos_seis/nacional_parques/data/Iberia/Iberia01_v1.0_DD_010reg_aa3d_pr.nc /home/hola/datos_seis/nacional_parques/data/Iberia/Iberia01_v1.0_DD_010reg_aa3d_monthly_pr.nc

for reg in reg_list:
    for run in run_dict["historical"]:
        print(reg+" - "+run)
        # For one Cordex member
        inCordex = glob.glob("/home/hola/datos_seis/nacional_parques/prod/cordex/"+run+"/historical/"+reg+"/pr/pr_*_end.nc")[0]
    
        # Load the netCDF files
        Iberia = xr.open_dataset(inIberia)
 #       #Iberia = Iberia.assign( hyear = ("time", [yy-1 if mm<9 else yy for yy,mm in zip(Iberia["time"].dt.year, Iberia["time"].dt.month)  ]))
#        Iberia = Iberia.assign_coords( season = ("time", Iberia.time.dt.season.values.squeeze()))
#        Iberia = Iberia.assign_coords( hyear = ("time", [yy-1 if mm<9 else yy for yy,mm in zip(Iberia["time"].dt.year, Iberia["time"].dt.month)  ]))
        
        Cordex = xr.open_dataset(inCordex)
#        Cordex = Cordex.assign_coords( hyear = ("time", [yy-1 if mm<9 else yy for yy,mm in zip(Cordex["time"].dt.year, Cordex["time"].dt.month)  ]))
#        Cordex = Cordex.assign_coords( season = ("time", Cordex.time.dt.season.values.squeeze()))
        

        time_step_sec = 10800
#        Cordex["pr"]=Cordex.pr*time_step_sec # convert from precip flux to cumul
        
        for ix,x in enumerate(Cordex.x.values):
            for iy,y in enumerate(Cordex.y.values):
                Iberia_int = Iberia.interp(lat = Cordex.lat[iy,ix], lon = Cordex.lon[iy,ix])
                if ~np.isnan(Iberia_int.pr.mean().values):
                    print(ix,iy)
                    QM = sdba.EmpiricalQuantileMapping.train(
                        Cordex.pr[:,iy,ix], Iberia_int.pr, nquantiles=15, group="time", kind="+"
                        )
                    scen = QM.adjust(Iberia_int.pr, extrapolation="constant", interp="nearest")
                    

            
QM = sdba.EmpiricalQuantileMapping.train(
                        Iberia.pr, Cordex.pr, nquantiles=15, group="time", kind="+"
                        )

QM = sdba.EmpiricalQuantileMapping.train(
                        Cordex.pr[:,iy,ix], Iberia_int.pr, nquantiles=15, group="time", kind="+"
                        )
                    
scen = QM.adjust(Iberia.pr, extrapolation="constant", interp="nearest")


for reg in reg_list:
    inIberia = "/home/hola/datos_seis/nacional_parques/data/Iberia/tas_Iberia_"+reg+".nc"
    for run in run_dict["historical"]:
        print(reg+" - "+run)
        # For one Cordex member
        inCordex = glob.glob("/home/hola/datos_seis/nacional_parques/prod/cordex/"+run+"/historical/"+reg+"/tas/tas_*_end.nc")[0]
    
        # Load the netCDF files
        Iberia = xr.open_dataset(inIberia)
        #Iberia = Iberia.assign( hyear = ("time", [yy-1 if mm<9 else yy for yy,mm in zip(Iberia["time"].dt.year, Iberia["time"].dt.month)  ]))
        Iberia = Iberia.assign_coords( season = ("time", Iberia.time.dt.season.values.squeeze()))
        Iberia = Iberia.assign_coords( hyear = ("time", [yy-1 if mm<9 else yy for yy,mm in zip(Iberia["time"].dt.year, Iberia["time"].dt.month)  ]))
        
        Cordex = xr.open_dataset(inCordex)
        Cordex = Cordex.assign_coords( hyear = ("time", [yy-1 if mm<9 else yy for yy,mm in zip(Cordex["time"].dt.year, Cordex["time"].dt.month)  ]))
        Cordex = Cordex.assign_coords( season = ("time", Cordex.time.dt.season.values.squeeze()))
        
        Cordex["tas"]=Cordex.tas-273.15 #
        
        plt.figure()
        for ix,x in enumerate(Cordex.x.values):
            for iy,y in enumerate(Cordex.y.values):
                Iberia_int = Iberia.interp(lat = Cordex.lat[iy,ix], lon = Cordex.lon[iy,ix])
                if ~np.isnan(Iberia_int.tas.mean().values):
                    ### Iberia
                    Iberia_int_gp_hyear_season = groupby_multicoords( Iberia_int, ["hyear", "season"])
                    Iberia_int_hyear_season_mean = Iberia_int_gp_hyear_season.mean()
                    
                    hyear_list, season_list, tas_list = [], [], []
                    for ii,gp in enumerate(Iberia_int_hyear_season_mean.grouping_zip):
                        hyear_list.append(gp.grouping_zip.values.flatten()[0][0])
                        season_list.append(gp.grouping_zip.values.flatten()[0][1])
                        tas_list.append(Iberia_int_hyear_season_mean.tas.values[ii])
            
                    Iberia_df = pd.DataFrame({"hyear":hyear_list, "season":season_list, "tas":tas_list})     
                 
                 
                    ### Cordex
                    tt = Cordex.tas[:,iy,ix]
                    Cordex_int_gp_hyear_season = groupby_multicoords( tt, ["hyear", "season"])
                    Cordex_int_hyear_season_mean = Cordex_int_gp_hyear_season.mean()
                
                    hyear_list, season_list, tas_list = [], [], []
                    for ii,gp in enumerate(Cordex_int_hyear_season_mean):
                        hyear_list.append(gp.grouping_zip.values.flatten()[0][0])
                        season_list.append(gp.grouping_zip.values.flatten()[0][1])
                        tas_list.append(gp.values.flatten()[0])
            
                    Cordex_df = pd.DataFrame({"hyear":hyear_list, "season":season_list, "tas":tas_list})          
                    
                    for ll in Iberia_df.iterrows():
                        hyear = ll[1].hyear
                        season = ll[1].season
                        cc = Cordex_df[(Cordex_df.season==season) & (Cordex_df.hyear==hyear)]
                        if len(cc) > 0:
                            plt.plot(ll[1].tas, cc.tas.values, color=plt_dict[season]["col"], marker=plt_dict[season]["mrk"], ms=6, mew=0, ls="", label=season)
    
        plt.ylabel("Simu. mean temperature (º)")
        plt.xlabel("Iberia mean temperature (º)")
        plt.axis("square")
        plt.plot([-10, 30], [-10, 30],"k", lw=0.6)
        plt.axis((-10, 30, -10, 30))
        plt.savefig(sm_fig_out_dir+"/tas_"+reg+"_"+run+"_Iberia.png")
        plt.close()
        Iberia.close()
        Cordex.close()
    