import xarray as xr
import matplotlib as mpl
mpl.rcParams.update({'text.usetex': False,"svg.fonttype": 'none'})
import matplotlib.pyplot as plt
import numpy as np
from xclim import sdba
import pickle as pkl
import os
import argparse
import glob
import pandas as pd
from pyproj import Proj, transform
import fiona

#inCordex="/home/hola/datos_seis/nacional_parques/prod/cordex/CNRM-CERFACS-CNRM-CM5__CNRM-ALADIN63/historical/AIGUE/pr/pr_EUR-11_CNRM-CERFACS-CNRM-CM5_historical_r1i1p1_CNRM-ALADIN63_v2_3hr_195101010300_end.nc"
#inIberia = "/home/hola/datos_seis/nacional_parques/data/Iberia/Iberia01_v1.0_DD_010reg_aa3d_tas.nc"
#out_QM_folder = "/home/hola/datos_seis/nacional_parques/prod/pkl/QM"
#inCordex = "/home/hola/datos_seis/nacional_parques/prod/cordex/CNRM-CERFACS-CNRM-CM5__CNRM-ALADIN63/historical/AIGUE/tas/tas_EUR-11_CNRM-CERFACS-CNRM-CM5_historical_r1i1p1_CNRM-ALADIN63_v2_3hr_195101010300_end.nc"
#calc_read = "calc_apply"

#inCordex="/home/hola/datos_seis/nacional_parques/prod/cordex/CNRM-CERFACS-CNRM-CM5__CNRM-ALADIN63/historical/AIGUE/pr/pr_EUR-11_CNRM-CERFACS-CNRM-CM5_historical_r1i1p1_CNRM-ALADIN63_v2_3hr_195101010300_end.nc"
#inRefClim = "/home/hola/datos_seis/nacional_parques/prod/STEAD_SPREAD/tmean_pen_comp_011.nc"
#out_QM_folder = "/home/hola/datos_seis/nacional_parques/prod/pkl/QM_SS"
#inCordex = "/home/hola/datos_seis/nacional_parques/prod/cordex/CNRM-CERFACS-CNRM-CM5__CNRM-ALADIN63/historical/AIGUE/tas/tas_EUR-11_CNRM-CERFACS-CNRM-CM5_historical_r1i1p1_CNRM-ALADIN63_v2_3hr_195101010300_end.nc"
#calc_read = "calc_apply"
#method = "interp_point"

def get_coord(xr_data):
    if "rlat" in xr_data.dims: 
        x_coord, y_coord = xr_data.rlon.to_numpy(), xr_data.rlat.to_numpy()         
        x_name, y_name = "rlon", "rlat"      
    else: 
        x_coord, y_coord = xr_data.x.to_numpy(), xr_data.y.to_numpy()
        x_name, y_name = "x", "y"
    return x_coord, y_coord, x_name, y_name

def QM_correction_calc_tas_point_interp(ix, iy, RefClim_int, Cordex_daily):
    print(ix,iy)
    tmp_cordex = Cordex_daily.tas[:,iy,ix]

    QM = sdba.EmpiricalQuantileMapping.train(
        RefClim_int, tmp_cordex , nquantiles=20, group="time.month", kind="+"
        )
    return QM   

def QM_correction_calc_tas_region_mean(RefClim, Cordex_daily, mask_nodata_RefClim_xy):
    x_coord, y_coord, x_name, y_name = get_coord(Cordex_daily)
    
    # Select the data within the specified latitude and longitude bounds
    RefClim_region_mean = RefClim.mean(dim=("x","y"))
    
    Cordex_daily_mean_masked = apply_mask_xy_to_dset_xyt(mask_nodata_RefClim_xy.data, Cordex_daily)
    Cordex_daily_mean = Cordex_daily_mean_masked.mean(dim=(x_name, y_name))
    
    RefClim_region_mean["tas"] = RefClim_region_mean.tas.assign_attrs(units="K")
    Cordex_daily_mean["tas"] = Cordex_daily_mean.tas.assign_attrs(units="K")
    
    QM = sdba.EmpiricalQuantileMapping.train(
        RefClim_region_mean.tas, Cordex_daily_mean.tas, nquantiles=20, group="time.month", kind="+"
        )
    return QM    
    
def QM_correction_calc_tas(RefClim, Cordex_daily, mask_nodata_RefClim_xy, method):
    x_coord, y_coord, x_name, y_name = get_coord(Cordex_daily)
                
    QM_dict = {}
    inan = 0
    n_tot = int(Cordex_daily.tas.size/Cordex_daily.time.size)
    
    print("QM correction calculation", end="\n")
    
    if method == "interp_point":
        for ix,ttx in enumerate(x_coord):
            print("X "+str(ix)+"/"+str(len(x_coord)), end="\n")
            for iy,tty in enumerate(y_coord): # for each cordex point
                print("Y "+str(iy)+"/"+str(len(y_coord)), end="\r")
    
                #RefClim_int = RefClim.interp(lat = Cordex_daily.lat[iy,ix], lon = Cordex_daily.lon[iy,ix], method="linear")
                RefClim_int = RefClim.tas[:, iy, ix]

                if mask_nodata_RefClim_xy[iy, ix]: #if ~np.isnan(RefClim_int.tas.mean().values): # if RefClim data available (not over France)
                    QM = QM_correction_calc_tas_point_interp(ix, iy, RefClim_int, Cordex_daily)
                    QM_dict[(ix,iy)] = {"QM":QM, "ds":QM.ds}
        
                else:
                    if inan == 0:
                        QM_mean = QM_correction_calc_tas_region_mean(RefClim, Cordex_daily, mask_nodata_RefClim_xy)
                    QM_dict[(ix,iy)] = {"QM":QM_mean, "ds":QM_mean.ds}
                    inan+=1
                    
    elif method == "region_mean":
        QM_mean = QM_correction_calc_tas_region_mean(RefClim, Cordex_daily, mask_nodata_RefClim_xy)
        for ix,ttx in enumerate(x_coord):
            print("X "+str(ix)+"/"+str(len(x_coord)), end="\n")
            for iy,tty in enumerate(y_coord): # for each cordex point
                print("Y "+str(iy)+"/"+str(len(y_coord)), end="\r")
                QM_dict[(ix,iy)] = {"QM":QM_mean, "ds":QM_mean.ds}
                
    return QM_dict

def QM_correction_apply_tas(Cordex, Cordex_daily, QM_dict):

    # Correct Cordex
    Cordex_corr = Cordex.copy(deep=True)
    x_coord, y_coord, x_name, y_name = get_coord(Cordex)

    n_tot = int(Cordex.tas.size/Cordex.time.size)
    
    print("QM correction application", end="\n")
    for ix,ttx in enumerate(x_coord):
        print("X "+str(ix)+"/"+str(len(x_coord)), end="\n")
        for iy,tty in enumerate(y_coord): # for each cordex point
            print("Y "+str(iy)+"/"+str(len(y_coord)), end="\n")
            QM = QM_dict[(ix,iy)]["QM"]
            QM.ds = QM_dict[(ix,iy)]["ds"]
            
            Cordex_daily_corr = QM.adjust(Cordex_daily.tas[:,iy,ix], interp="linear")
            
            corr_factor_daily = Cordex_daily_corr/Cordex_daily.tas[:,iy,ix]
            corr_factor_hourly  = corr_factor_daily.resample(time='3H',closed="right").ffill().interp(time=Cordex.time).ffill(dim="time")
            Cordex_corr["tas"][:,iy,ix] = Cordex_corr["tas"][:,iy,ix]*corr_factor_hourly
    
    return Cordex_corr
                      
def apply_mask_xy_to_dset_xyt(mask_xy, dset_xyt):
    x_coord, y_coord, x_name, y_name = get_coord(dset_xyt)
    # Creating a mask dataset with the same coordinates as RefClim4qm
    mask_dataset = xr.Dataset({'mask': ((y_name, x_name), mask_xy)},
                              coords={y_name: dset_xyt[y_name], x_name: dset_xyt[x_name]})
    
    # Broadcasting the mask to the time dimension
    mask_dataset_broadcasted = mask_dataset.expand_dims(time=dset_xyt['time'])
    
    # Applying the mask to RefClim4qm
    dset_xyt = dset_xyt.where(mask_dataset_broadcasted['mask'] == 1)
    
    return dset_xyt
    

def read_prep_cordex_refclim(inCordex, inRefClim):
    
    print("Cordex preparation", end="\n")    
    Cordex = xr.open_dataset(inCordex)
    if type(Cordex.time.values[0]) == np.float64:
        tt = Cordex.time.to_numpy()
        hh = np.array((tt-np.floor(tt))*24).astype(int).astype(str)
        ttt = [t.split(".")[0]+"-"+h for (t, h) in zip(tt.astype(str), hh)]
        Cordex["time"] = pd.to_datetime(ttt, format='%Y%m%d-%H')
    
    #Cordex = Cordex.convert_calendar("gregorian", align_on="year", missing=np.nan)
    Cordex.lon.values[Cordex.lon.values>180] = Cordex.lon.values[Cordex.lon.values>180]-360
    Cordex_daily = Cordex.resample(time='D').mean(keep_attrs=True)
    Cordex_daily0 = Cordex_daily.copy(True)  # kept without nan from the commomn period extraction below
    
    if "height" in Cordex.coords: # bug occurs with CNRM__CNRM, rcp85...
        Cordex_daily = Cordex_daily.drop_vars("height")
        
    # Load the netCDF files    
    print("RefClim preparation", end="\n")
    RefClim = xr.open_dataset(inRefClim)
    if "Time" in RefClim.coords:
        RefClim = RefClim.rename(Time="time")
        
    if "tx" in RefClim.variables:
        RefClim = RefClim.rename(tx="tas")
    
    if RefClim.time.dt.calendar != Cordex_daily.time.dt.calendar:
        RefClim = RefClim.convert_calendar(Cordex_daily.time.dt.calendar, align_on="year", missing=np.nan)
        # Unknown warning: /home/hola/miniconda3/envs/parc/lib/python3.10/site-packages/xarray/core/accessor_dt.py:72: FutureWarning: Index.ravel returning ndarray is deprecated; in a future version this will return a view on self.values_as_series = pd.Series(values.ravel(), copy=False)

    RefClim = RefClim.interp({"lat":Cordex.lat, "lon":Cordex.lon}, method="linear")
    mask_nodata_RefClim_xy = ~np.isnan(RefClim.tas.mean(axis=0)) # mask where there are data (e.g. no data on the sea and France)
    RefClim = apply_mask_xy_to_dset_xyt(mask_nodata_RefClim_xy.data, RefClim)
    
    # mask Cordex and RefClim to keep common time period
    Cordex_daily = Cordex_daily.where(Cordex_daily.time > RefClim.time[0], np.nan)
    Cordex_daily = Cordex_daily.where(Cordex_daily.time < RefClim.time[-1], np.nan)
    RefClim = RefClim.where(RefClim.time > Cordex_daily.time[0], np.nan)
    RefClim = RefClim.where(RefClim.time < Cordex_daily.time[-1], np.nan)
    
    if np.nanmin(RefClim["tas"]) < 100:
        RefClim["tas"] = RefClim.tas+273.15 # convert from deg Celsius to K
        RefClim["tas"] = RefClim.tas.assign_attrs(units="K")
            
    return Cordex, RefClim, Cordex_daily, Cordex_daily0, mask_nodata_RefClim_xy


def QM_correction_tas(inRefClim, inCordex, out_QM_folder, out_fig_folder, out_NC_folder, calc_read, method):
    # main function
    reg = inCordex.split("/")[-3]
    
    Cordex, RefClim, Cordex_daily, Cordex_daily0, mask_nodata_RefClim_xy = read_prep_cordex_refclim(inCordex, inRefClim)
    
    # read time units hours or days 
    Cordex4units = xr.open_dataset(inCordex, decode_times=False)
    time_units=Cordex4units.time.units
    Cordex4units.close()    
    
    if calc_read == "calc_apply":
        print("Calculating quantile mapping file.")
        QM_dict = QM_correction_calc_tas(RefClim, Cordex_daily, mask_nodata_RefClim_xy, method)
        
        # Save QM_dict to apply to projections (RCPs)
        if not os.path.exists(out_QM_folder):
            os.makedirs(out_QM_folder)
        
        with open(out_QM_folder+"/"+inCordex.split("/")[-1].replace(".nc","")+"_"+reg+"_qm.p", 'wb') as p:
            pkl.dump(QM_dict, p)
        
    elif calc_read == "read_apply":
        print("Reading quantile mapping file.")
        tt =inCordex.split("/")[-1].split("_")
        in_QM = glob.glob(out_QM_folder+"/"+"_".join([xx if "rcp" not in xx else "historical" for xx in tt][0:-3])+"*_"+reg+"_qm.p")[0]
        with open(in_QM, 'rb') as p:
            QM_dict = pkl.load(p)
    
    Cordex_corr = QM_correction_apply_tas(Cordex, Cordex_daily0, QM_dict)
        
    # Save corrected variable
    out_NC_folder_full = "/".join(inCordex.split("/")[:-2])+"/"+out_NC_folder+"/"
    if not os.path.exists(out_NC_folder_full):
        os.makedirs(out_NC_folder_full)
        
    Cordex_corr.to_netcdf(out_NC_folder_full+"/"+inCordex.split("/")[-1], encoding={'time':{'units':time_units}}) # 'days since '+str(Cordex.time[0].dt.strftime("%Y-%m-%d %H:%M:%S").values)}})
    
    #########################
    ##### Figure ############
    #########################
    
    # scatter plot
    #plt.figure()
    #xx = Cordex.tas.to_numpy().flatten()
    #yy = Cordex_corr.tas.to_numpy().flatten() 
    #plt.plot(xx, yy , ".", ms=2, alpha=0.1)
    #plt.plot([230, 320], [230, 320],lw=1,color="k")
    #plt.axis("square")
    #plt.axis( [230, 320, 230, 320] )
    #plt.grid()
    #plt.xlabel("Temperature before quantile-mapping") 
    #plt.ylabel("Temperature after quantile-mapping") 
    #plt.savefig(out_fig_folder+"/scatter_qm_"+inCordex.split("/")[-1].split("3hr")[0]+reg+".png")
    #plt.savefig(out_fig_folder+"/scatter_qm_"+inCordex.split("/")[-1].split("3hr")[0]+reg+".svg")
    
    # Hist2D
    plt.figure()
    xx = Cordex.tas.to_numpy().flatten()
    yy = Cordex_corr.tas.to_numpy().flatten()
    plt.hist2d(xx, yy, bins=90, range=[[230, 320], [230, 320]], cmap='viridis')
    plt.colorbar(label="Frequency")
    plt.plot([230, 320], [230, 320], lw=1, color="k")
    plt.axis("square")
    plt.axis([230, 320, 230, 320])
    plt.grid()
    plt.xlabel("Temperature before quantile-mapping")
    plt.ylabel("Temperature after quantile-mapping")
    plt.savefig(out_fig_folder + "/hist2d_qm_" + inCordex.split("/")[-1].split("3hr")[0] + reg + ".png")
    plt.savefig(out_fig_folder + "/hist2d_qm_" + inCordex.split("/")[-1].split("3hr")[0] + reg + ".svg")

    # map viz of the QM correction    
    dCordex = Cordex_corr-Cordex
    
    # load parc contour
    in_shp_parc = glob.glob("/home/hola/datos_seis/nacional_parques/data/contour/limite_"+reg+"*.shp")[0]
    shape = fiona.open(in_shp_parc)
    contour= shape.next()
    if reg=="PICOS":
        lon = np.array(contour["geometry"]["coordinates"][0]).squeeze()[:,0] # hay dos huecitos en Picos...
        lat = np.array(contour["geometry"]["coordinates"][0]).squeeze()[:,1]
    else:
        lon = np.array(contour["geometry"]["coordinates"]).squeeze()[:,0]
        lat = np.array(contour["geometry"]["coordinates"]).squeeze()[:,1]
    lon_parc, lat_parc = transform(Proj(shape.crs), Proj(init='EPSG:4326'), lon, lat)
    
    # Create some sample data
    data1 = Cordex.mean(dim="time").tas
    data2 = np.nanmean(RefClim.tas.values, axis=0) #.where(mask_nodata_RefClim_xy) #RefClim.tas.mean(dim="time")
    data3 = Cordex_corr.mean(dim="time").tas
    data4 = dCordex.mean(dim="time").tas
    data5 = data1-data2
    data6 = data3-data2
    # bounding box coordinate
    ymin_cordex, ymax_cordex, xmin_cordex, xmax_cordex = np.min(Cordex.lat.values), np.max(Cordex.lat.values), np.min(Cordex.lon.values), np.max(Cordex.lon.values)
    ymin_refclim, ymax_refclim, xmin_refclim, xmax_refclim = np.min(RefClim.lat.values), np.max(RefClim.lat.values), np.min(RefClim.lon.values), np.max(RefClim.lon.values)
    
    # Create a 2x2 grid of subplots
    fig, axes = plt.subplots(nrows=2, ncols=3)
    
    ax1 = axes[0,0] 
    im1 = ax1.imshow(data1, cmap='viridis', vmin=270, vmax=300, extent = (xmin_cordex, xmax_cordex, ymin_cordex, ymax_cordex), origin="lower")
    ax1.plot(lon_parc, lat_parc)
    ax1.set_xticklabels("")
    ax1.set_yticklabels("")
    ax1.set_title("Cdx")
    
    ax2 = axes[0,1]
    ax2.imshow(     data2, cmap='viridis', vmin=270, vmax=300, extent = (xmin_refclim, xmax_refclim, ymin_refclim, ymax_refclim), origin="lower")
    ax2.axis([xmin_cordex, xmax_cordex, ymin_cordex, ymax_cordex])
    ax2.plot(lon_parc, lat_parc)
    ax2.set_xticklabels("")
    ax2.set_yticklabels("")
    ax2.set_title("RefClim")
    
    ax3 = axes[0,2]
    ax3.imshow( data5, cmap='RdYlBu_r', vmin=-10, vmax=10, extent = (xmin_refclim, xmax_refclim, ymin_refclim, ymax_refclim), origin="lower")
    ax3.axis([xmin_cordex, xmax_cordex, ymin_cordex, ymax_cordex])
    ax3.plot(lon_parc, lat_parc)
    ax3.set_xticklabels("")
    ax3.set_yticklabels("")
    ax3.set_title("Cdx - RefClim")
    
    ax4 = axes[1,0] 
    im4 = ax4.imshow(data3, cmap='viridis', vmin=270, vmax=300, extent = (xmin_cordex, xmax_cordex, ymin_cordex, ymax_cordex), origin="lower")
    ax4.plot(lon_parc, lat_parc)
    ax4.set_xticklabels("")
    ax4.set_yticklabels("")
    ax4.set_title("Cdx + QM corr.")
    
    ax5 = axes[1,1] 
    im5 = ax5.imshow(data4, cmap='RdYlBu_r', vmin=-10, vmax=10, extent = (xmin_cordex, xmax_cordex, ymin_cordex, ymax_cordex), origin="lower")
    ax5.plot(lon_parc, lat_parc)
    ax5.set_xticklabels("")
    ax5.set_yticklabels("")
    ax5.set_title("QM corr.")

    ax6 = axes[1,2]
    ax6.imshow( data6, cmap='RdYlBu_r', vmin=-10, vmax=10, extent = (xmin_refclim, xmax_refclim, ymin_refclim, ymax_refclim), origin="lower")
    ax6.axis([xmin_cordex, xmax_cordex, ymin_cordex, ymax_cordex])
    ax6.plot(lon_parc, lat_parc)
    ax6.set_xticklabels("")
    ax6.set_yticklabels("")
    ax6.set_title("Cdx+QM corr.-RefClim")
        
    plt.tight_layout()
    
    fig.subplots_adjust(bottom=0.3)
    cbar_ax = fig.add_axes([0.25, 0.15, 0.15, 0.04])
    fig.colorbar(im1, cax=cbar_ax, orientation='horizontal', label='Mean temperature (K)')
    
    cbar_ax = fig.add_axes([0.6, 0.15, 0.15, 0.04])
    fig.colorbar(im5, cax=cbar_ax, orientation='horizontal', label='Temperature difference (K)')

    plt.savefig(out_fig_folder+"/map_qm_"+inCordex.split("/")[-1].split("3hr")[0]+reg+".png")
    plt.savefig(out_fig_folder+"/map_qm_"+inCordex.split("/")[-1].split("3hr")[0]+reg+".svg")

    
if __name__ == "__main__": 
    parser = argparse.ArgumentParser(description = "Calculate and apply quantile-mapping correction from a reference reanalysis (Iberia, STEAD, SPREAD)and Cordex data.")
    parser.add_argument("-inRefClim", dest = "inRefClim")
    parser.add_argument("-inCordex", dest = "inCordex")
    parser.add_argument("-out_QM_folder", dest = "out_QM_folder")
    parser.add_argument("-out_fig_folder", dest = "out_fig_folder")
    parser.add_argument("-out_NC_folder", dest = "out_NC_folder")
    parser.add_argument("-calc_read", dest = "calc_read")
    parser.add_argument("-method", dest = "method")
    args = parser.parse_args()
    
    QM_correction_tas( args.inRefClim, args.inCordex, args.out_QM_folder, args.out_fig_folder, args.out_NC_folder, args.calc_read, args.method)
    