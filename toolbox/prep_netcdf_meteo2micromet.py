#!/home/hola/miniconda3/envs/parc/bin/python
import argparse
import xarray as xr
import rioxarray as rxr
import os
import pandas as pd
import numpy as np
import pyproj
from pyproj import Transformer
import fiona
from fiona.crs import from_epsg
from shapely import wkt
from shapely.geometry import Point, Polygon, LineString, mapping, shape
import glob
import argparse
import datetime

####
# Convert NC file with all appropriate variables for sm in gdat readable by sm. One gdat per point of the inNC.
####
# inNC="/home/hola/datos_seis/nacional_parques/prod/cordex/CNRM-CERFACS-CNRM-CM5__CNRM-ALADIN63/historical/AIGUE/allvar/allvar_EUR-11_CNRM-CERFACS-CNRM-CM5_historical_r1i1p1_CNRM-ALADIN63_v2_3hr_195101010300_end.nc"
# outFolder="/home/hola/datos_seis/nacional_parques/prod/cordex/CNRM-CERFACS-CNRM-CM5__CNRM-ALADIN63/historical/AIGUE/dat/"
# inSHP="/home/hola/datos_seis/nacional_parques/data/contour/limite_AIGUE.shp"
# dist_buffer=15000
# resDEM="250m"
# # calendar_type '360_day'
# inNC="/home/hola/datos_seis/nacional_parques/prod/cordex/MOHC-HadGEM2-ES__ICTP-RegCM4-6/historical/AIGUE/allvar/allvar_EUR-11_MOHC-HadGEM2-ES_historical_r1i1p1_ICTP-RegCM4-6_v1_3hr_197006010300_end.nc"
# # calendar_type  "365_day"
# inNC="/home/hola/datos_seis/nacional_parques/prod/cordex/NCC-NorESM1-M__ICTP-RegCM4-6/historical/AIGUE/allvar/allvar_EUR-11_NCC-NorESM1-M_historical_r1i1p1_ICTP-RegCM4-6_v1_3hr_197001010300_end.nc"
#
# inNC="/home/hola/datos_seis/nacional_parques/prod/cordex/CNRM-CERFACS-CNRM-CM5__CNRM-ALADIN63/rcp85/AIGUE/allvar/allvar_EUR-11_CNRM-CERFACS-CNRM-CM5_rcp85_r1i1p1_CNRM-ALADIN63_v2_3hr_200601010300_end.nc"
# outFolder="/home/hola/datos_seis/nacional_parques/prod/cordex/CNRM-CERFACS-CNRM-CM5__CNRM-ALADIN63/rcp85/AIGUE/dat/"
# resDEM="250m"
# dist_buffer=15000

def prep_netcdf_meteo2micromet(inNC, outFolder, resDEM, dist_buffer, EUR_AFR="EUR"):
    UTM_dict ={"SIERR": "epsg:32630", "PICOS": "epsg:32630", "ORDES": "epsg:32631", "GUADA": "epsg:32630", "AIGUE": "epsg:32631", "TEIDE": "epsg:32628"}
    
    # find output name
    outName = inNC.split("/")[-1].replace(".nc", "")
    siteName = inNC.split("/")[-3]
    if siteName not in UTM_dict.keys(): 
        print("WARNING PATH MAY HAVE CHANGE SITE NAME NOT PROPERLY IDENTIFIED")
        
    expName = inNC.split("/")[-5]
    if EUR_AFR == "EUR":
        inOrog = glob.glob("/home/hola/datos_seis/nacional_parques/data/cordex/EUR11/"+expName+"/orog/orog*")[0]
    elif EUR_AFR == "AFR":
        inOrog = glob.glob("/home/hola/datos_seis/nacional_parques/data/cordex/AFR22/"+expName+"/orog/orog*")[0]

    inDEM = glob.glob("/home/hola/datos_seis/nacional_parques/data/COP30/"+resDEM+"/limite*"+siteName+"*DEM.tif")[0]
            
    # open NC file
    ds0 = xr.open_dataset(inNC)
    stream = os.popen("ncdump -h "+inNC+" | grep time:calendar")
    out = stream.read()
    calendar_type = out.split("\"")[1]
    # check if gregorian calendar, if not interpolation required for snowmodel
    if calendar_type not in ("gregorian", "proleptic_gregorian"):
        # calendar type is then "360_day" (12x30 days) or "365_day" (no leap-year):
        ds_cal = ds0.convert_calendar("gregorian", align_on="year", missing=np.nan)
        wh_null_date = np.where(ds_cal.tas[:,0,0].isnull()==True)[0]
        for var in ds_cal.var():
            for ii,null_date in enumerate(wh_null_date):
                if ii < 8: # !! depends on the time step...here 3h*8=1 day
                    ds_cal[var][null_date] = ds_cal[var][null_date+8]
                elif ii > len(ds_cal.time)-8:
                    ds_cal[var][null_date] = ds_cal[var][null_date-8]
                else:
                    ds_cal[var][null_date] = (ds_cal[var][null_date+8]+ds_cal[var][null_date-8])/2.
             
            # plot to check the interpolation scheme. all seems ok.       
            # plt.plot(ds_cal.time,ds_cal.tas[:,0,0])
            # plt.plot(ds_cal.time[wh_null_date],ds_cal.tas[wh_null_date,0,0],"r.")
            # plt.plot(ds_cal.time[wh_null_date-8],ds_cal.tas[wh_null_date-8,0,0],"b.")
            # plt.plot(ds_cal.time[wh_null_date+8],ds_cal.tas[wh_null_date+8,0,0],"b.")
            # for xx in np.arange(6000,7000,50):
            #     plt.xlim((xx,xx+50))
            #     plt.savefig(fig_out_dir+"/interp_"+var+"_"+str(xx)+".png")
            # plt.close()
        ds = ds_cal.copy()
        ds_cal.close()
    else:
        ds = ds0.copy()        
    ds0.close()
    
    # open orography
    DEM = rxr.open_rasterio(inDEM)
    
    
    
    n_time = len(ds.time)
    time_step = ds.time.dt.date[1].time-ds.time.dt.date[0].time
    time_step_sec = time_step.values.astype('timedelta64[s]').astype(int)
    
    
    # different type of calendar to manage (in historical for now: time:calendar = 
    # "gregorian" => works
    # "proleptic_gregorian" equivalent to "gregorian" but allows to go before reference day 0 => works
    # "360_day" 12*30 days  => dont work
    # "noleap" 365 days all years => dont work

    # open 
    # prepare output
    dict_out = {}
    dict_out["year"] = ds.time.dt.year.to_numpy() # stay away from time_bnds : shifts everything
    dict_out["month"] = ds.time.dt.month.to_numpy()
    dict_out["day"] = ds.time.dt.day.to_numpy()
    dict_out["hour"] = ds.time.dt.hour.to_numpy()
    
    lat_grid = ds.lat.to_numpy()
    lon_grid = ds.lon.to_numpy()   
 
    tas_grid = ds.tas.to_numpy()
    hurs_grid = ds.hurs.to_numpy()
    pr_grid  = ds.pr.to_numpy()
    # calculate the wind direction and intensity
    tt = np.sqrt((np.square(ds.uas))+(np.square(ds.vas)))
    wint_grid = tt.to_numpy()
    tt = (360/(2*np.pi))*np.arctan2(ds.uas, ds.vas)
    wdir_grid = tt.to_numpy()
    
    # open orography
    orog =  xr.open_dataset(inOrog)

    if "rlat" in ds.dims: 
        x_coord = ds.rlon.to_numpy()
        y_coord = ds.rlat.to_numpy()        
    else: 
        x_coord = ds.x.to_numpy()
        y_coord = ds.y.to_numpy()
        
    x_shp = []
    y_shp = []
    ix_shp = []
    iy_shp = []
    orog_shp = []
    list_station = []

    transformer_latlon2utm = Transformer.from_crs("epsg:4326", UTM_dict[siteName])
    
    #(lon_min, lon_max, lat_min, lat_max) = (np.min(lon_grid), np.max(lon_grid), np.min(lat_grid), np.max(lat_grid))
    #(ulx_utm, uly_utm) = transformer_latlon2utm.transform(lat_max,lon_min)
    #(lrx_utm, lry_utm) = transformer_latlon2utm.transform(lat_min,lon_max)
    (ulx_utm, lrx_utm, uly_utm, lry_utm) = (np.min(DEM.x), np.max(DEM.x), np.max(DEM.y), np.min(DEM.y))

    #poly = wkt.loads("POLYGON(("+str(ulx_utm)+" "+str(uly_utm)+","+str(lrx_utm)+" "+str(uly_utm)+","+str(lrx_utm)+" "+str(lry_utm)+","+str(ulx_utm)+" "+str(lry_utm)+"))")
    ROI_bb = Polygon(LineString([Point(ulx_utm, uly_utm), Point(lrx_utm, uly_utm), Point(lrx_utm, lry_utm), Point(ulx_utm, lry_utm)]))
    
    for ix, x in enumerate(x_coord): 
        for iy, y in enumerate(y_coord): 
            (x_utm, y_utm) = transformer_latlon2utm.transform(lat_grid[iy, ix], lon_grid[iy, ix])
            #xy_utm = np.array(pyproj.transform(inProj, outProj, lat_grid[iy, ix], lon_grid[iy, ix])).transpose()
            #transformer = pyproj.Transformer.from_crs( inProj, outProj)
            #xy_utm = np.array(transformer.transform(lon_grid[iy, ix], lat_grid[iy, ix])).transpose().astype(int)
            dist2parc = ROI_bb.distance(Point(x_utm, y_utm))
            print(dist2parc)
            
            if "rlat" in ds.dims: 
                xZ = np.where(orog.rlon==x)[0][0]
                yZ = np.where(orog.rlat==y)[0][0]
                Z_val = max(orog["orog"][yZ, xZ].to_numpy().astype(int),0) # CNRM-ALADIN63 orography has negative values over sea, bugs in sm
                
            else: 
                xZ = np.where(orog.x==x)[0][0]
                yZ = np.where(orog.y==y)[0][0]
                Z_val = max(orog["orog"][yZ, xZ].to_numpy().astype(int),0) # CNRM-ALADIN63 orography has negative values over sea, bugs in sm

            if dist2parc < dist_buffer: 
                print("Point selected: ")
                print(x_utm,y_utm)
                dict_out["ID"] = int(str(ix)+str(iy))
                dict_out["X"] = np.round(x_utm)*np.ones(n_time)
                dict_out["Y"] = np.round(y_utm)*np.ones(n_time)
                dict_out["Z"] =  Z_val*np.ones(n_time).astype(int)
                dict_out["TAS"] = tas_grid[: , iy, ix].round(4)
                dict_out["HURS"] = hurs_grid[: , iy, ix].round(4)
                dict_out["WINT"] = wint_grid[: , iy, ix].round(4)
                dict_out["WDIR"] = wdir_grid[: , iy, ix].round(4)
                pr_grid[-1 , iy, ix] = pr_grid[-2 , iy, ix] # due to different time axis between pr and other var. 1e36 values are written for the last time step while ncks -A the files
                # !!! convert precip to mm PER TIMESTEP !!!
                # deduce dt from the met timestep
                dict_out["PR"] = np.round(pr_grid[: , iy, ix]*time_step_sec, 6)
                
                csv_file_out = outFolder+"/"+siteName+"_"+str(ix)+"_"+str(iy)+".dat"
                list_station.append(csv_file_out)
                df2write = pd.DataFrame.from_dict(dict_out)
                df2write.to_csv(csv_file_out, index=False, sep=" ", header=None)
                
                x_shp.append(x_utm)
                y_shp.append(y_utm)
                ix_shp.append(ix)
                iy_shp.append(iy)
                orog_shp.append(int(Z_val))
            else: 
                print("Point excluded: ")
                print( "lat: " + str( np.round(lat_grid[iy, ix], 3))
                       + " ; lon: " + str( np.round(lon_grid[iy, ix], 3))
                       + " ; x_utm: " + str( np.round(x_utm , 3))
                       + " ; y_utm: " + str( np.round(y_utm, 3)))
    
    # create a text file with only the number of station writen
    csv_file_out = outFolder+"/"+siteName+"_station_number.dat"
    np2write = len(list_station)*np.ones(n_time).astype(int)
    np.savetxt(csv_file_out, np2write, delimiter=" ", fmt="%d")
    list_station.insert(0, csv_file_out)
    # format files for micromet to read them
    
    cmd2exec="paste -d \'\n\' "+" ".join(list_station)+" >"+outFolder+"/"+outName+".dat"
    os.system(cmd2exec)    
    
    ds.close()
    DEM.close()
    
    shpName  = outFolder+"/"+outName+"_points.shp"
    with fiona.open(shpName, 'w', crs=from_epsg(UTM_dict[siteName].replace("epsg:", "")), driver='ESRI Shapefile', schema={'geometry': 'Point', 'properties': {"iy": "int", "ix": "int", "orog": "int"}}) as shpout: 
        for x2write, y2write, ix2write, iy2write, orog2write in zip(x_shp, y_shp, ix_shp, iy_shp, orog_shp): 
            point = Point(x2write, y2write)
            shpout.write({'geometry': mapping(point), "properties": {"ix": int(ix2write), "iy": int(iy2write), "orog": orog2write}})
    
    ########################################
    #!! ADD A DELETE TMP FILE AND FOLDER !!#
    ########################################
    
    
if __name__ == "__main__": 
    parser = argparse.ArgumentParser(description = "Extract Cordex netcdf at a set distance from the ROI. Write to MicroMet readable file.")
    parser.add_argument("-inNC", dest = "inNC")
    parser.add_argument("-outFolder", dest = "outFolder")
    parser.add_argument("-resDEM", dest = "resDEM")
    parser.add_argument("-dist_buffer", dest = "dist_buffer", type = float)
    parser.add_argument("-EUR_AFR", dest = "EUR_AFR")
    args = parser.parse_args()
    
    prep_netcdf_meteo2micromet( args.inNC, args.outFolder, args.resDEM, args.dist_buffer, args.EUR_AFR)
            
            
            
            
