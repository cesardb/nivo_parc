#!/bin/sh

### Preparation of the Cordex data
conda activate parc

IB_folder="/home/cesardb/Datos/nacional_parques/data/Iberia"
out_folder="/home/cesardb/Datos/nacional_parques/product/Iberia"

#######################################
# "AIGUE":[0.50,1.49, 42.28,43.05]
region="AIGUE" 
lat_bound=42.28,43.05
lon_bound=0.50,1.49
inNC=${IB_folder}/Iberia01_v1.0_DD_010reg_aa3d_pr.nc
ncks -d lat,${lat_bound} -d lon,${lon_bound} ${inNC} -O ${out_folder}/pr_Iberia_${region}.nc

inNC=${IB_folder}/Iberia01_v1.0_DD_010reg_aa3d_tas.nc
ncks -d lat,${lat_bound} -d lon,${lon_bound} ${inNC} -O ${out_folder}/tas_Iberia_${region}.nc

inNC=${IB_folder}/Iberia01_v1.0_fix_010reg_orog.nc
ncks -d lat,${lat_bound} -d lon,${lon_bound} ${inNC} -O ${out_folder}/orog_Iberia_${region}.nc

gdalwarp -s_srs EPSG:4326 -t_srs EPSG:32630 ${out_folder}/orog_Iberia_${region}.nc ${out_folder}/orog_Iberia_${region}_UTM30N.nc
gdal_translate -of GTiff NETCDF:"${out_folder}/orog_Iberia_${region}_UTM30N.nc":Band1 ${out_folder}/orog_Iberia_${region}_UTM30N.tif

#######################################
# "GUADA":[-4.50,-3.40, 40.46,41.31] 

region="GUADA" 
lat_bound=40.46,41.31
lon_bound=-4.50,-3.40
inNC=${IB_folder}/Iberia01_v1.0_DD_010reg_aa3d_pr.nc
ncks -d lat,${lat_bound} -d lon,${lon_bound} ${inNC} -O ${out_folder}/pr_Iberia_${region}.nc

inNC=${IB_folder}/Iberia01_v1.0_DD_010reg_aa3d_tas.nc
ncks -d lat,${lat_bound} -d lon,${lon_bound} ${inNC} -O ${out_folder}/tas_Iberia_${region}.nc

inNC=${IB_folder}/Iberia01_v1.0_fix_010reg_orog.nc
ncks -d lat,${lat_bound} -d lon,${lon_bound} ${inNC} -O ${out_folder}/orog_Iberia_${region}.nc

gdalwarp -s_srs EPSG:4326 -t_srs EPSG:32630 ${out_folder}/orog_Iberia_${region}.nc ${out_folder}/orog_Iberia_${region}_UTM30N.nc
gdal_translate -of GTiff NETCDF:"${out_folder}/orog_Iberia_${region}_UTM30N.nc":Band1 ${out_folder}/orog_Iberia_${region}_UTM30N.tif

#######################################
# "ORDES":[-0.40,0.52, 42.25,43.0]

region="ORDES" 
lat_bound=42.25,43.0
lon_bound=-0.40,0.52
inNC=${IB_folder}/Iberia01_v1.0_DD_010reg_aa3d_pr.nc
ncks -d lat,${lat_bound} -d lon,${lon_bound} ${inNC} -O ${out_folder}/pr_Iberia_${region}.nc

inNC=${IB_folder}/Iberia01_v1.0_DD_010reg_aa3d_tas.nc
ncks -d lat,${lat_bound} -d lon,${lon_bound} ${inNC} -O ${out_folder}/tas_Iberia_${region}.nc

inNC=${IB_folder}/Iberia01_v1.0_fix_010reg_orog.nc
ncks -d lat,${lat_bound} -d lon,${lon_bound} ${inNC} -O ${out_folder}/orog_Iberia_${region}.nc

gdalwarp -s_srs EPSG:4326 -t_srs EPSG:32630 ${out_folder}/orog_Iberia_${region}.nc ${out_folder}/orog_Iberia_${region}_UTM30N.nc
gdal_translate -of GTiff NETCDF:"${out_folder}/orog_Iberia_${region}_UTM30N.nc":Band1 ${out_folder}/orog_Iberia_${region}_UTM30N.tif

#######################################
# "PICOS":[-5.43,-4.26, 42.78,43.68]

region="PICOS" 
lat_bound=42.78,43.68
lon_bound=-5.43,-4.26
inNC=${IB_folder}/Iberia01_v1.0_DD_010reg_aa3d_pr.nc
ncks -d lat,${lat_bound} -d lon,${lon_bound} ${inNC} -O ${out_folder}/pr_Iberia_${region}.nc

inNC=${IB_folder}/Iberia01_v1.0_DD_010reg_aa3d_tas.nc
ncks -d lat,${lat_bound} -d lon,${lon_bound} ${inNC} -O ${out_folder}/tas_Iberia_${region}.nc

inNC=${IB_folder}/Iberia01_v1.0_fix_010reg_orog.nc
ncks -d lat,${lat_bound} -d lon,${lon_bound} ${inNC} -O ${out_folder}/orog_Iberia_${region}.nc

gdalwarp -s_srs EPSG:4326 -t_srs EPSG:32630 ${out_folder}/orog_Iberia_${region}.nc ${out_folder}/orog_Iberia_${region}_UTM30N.nc
gdal_translate -of GTiff NETCDF:"${out_folder}/orog_Iberia_${region}_UTM30N.nc":Band1 ${out_folder}/orog_Iberia_${region}_UTM30N.tif

#######################################
# "SIERR":[-3.98,-2.31, 36.59,37.54]

region="SIERR" 
lat_bound=36.59,37.54
lon_bound=-3.98,-2.31
inNC=${IB_folder}/Iberia01_v1.0_DD_010reg_aa3d_pr.nc
ncks -d lat,${lat_bound} -d lon,${lon_bound} ${inNC} -O ${out_folder}/pr_Iberia_${region}.nc

inNC=${IB_folder}/Iberia01_v1.0_DD_010reg_aa3d_tas.nc
ncks -d lat,${lat_bound} -d lon,${lon_bound} ${inNC} -O ${out_folder}/tas_Iberia_${region}.nc

inNC=${IB_folder}/Iberia01_v1.0_fix_010reg_orog.nc
ncks -d lat,${lat_bound} -d lon,${lon_bound} ${inNC} -O ${out_folder}/orog_Iberia_${region}.nc

gdalwarp -s_srs EPSG:4326 -t_srs EPSG:32630 ${out_folder}/orog_Iberia_${region}.nc ${out_folder}/orog_Iberia_${region}_UTM30N.nc
gdal_translate -of GTiff NETCDF:"${out_folder}/orog_Iberia_${region}_UTM30N.nc":Band1 ${out_folder}/orog_Iberia_${region}_UTM30N.tif


