import xarray as xr
import numpy as np
import copy
import requests
#from bs4 import BeautifulSoup
import netCDF4 # very important : avoids some "curl error details: "
import os
import glob
import numpy as np
import matplotlib.pyplot as plt
from selenium import webdriver
from selenium.webdriver import FirefoxOptions

def read_ctx_hit(hit, inctx):
    """ Reads a ctx search results defined by hit."""
    try:
        result = inctx.search()[hit]
        data_egsf = check_esgf_result(result)     
        return data_egsf
    except:
        # sometimes hit_count does not match with the real size of ctx.search()
        pass

def check_esgf_result(result):
    """ Identify the result (gcm name, rcm name...) and lists all associated url to download."""
    gcm = result.dataset_id.split(".")[4]
    rcm = result.dataset_id.split(".")[7]
    ens = result.dataset_id.split(".")[6]
    exp = result.dataset_id.split(".")[5]
    var = result.dataset_id.split(".")[10]
    
    # signifies that this combination of gcm, rcm...var was found
    dict_data_esgf_avail = {gcm:{rcm:{ens:{exp:{var:{"isvalid":1}}}}}}	
    url2write = []

    # list all the urls available for download
    files = result.file_context().search()
    for file in files:
        url2write.append(file.opendap_url)
    dict_data_esgf_avail[gcm][rcm][ens][exp][var]["url"] = url2write
    
    return dict_data_esgf_avail
    
def merge_pool_output_read_ctx(pool_out_list, dict_avail):
    """ Merge all the output of the function passed to pool. """
    for pool_out in pool_out_list:
        if pool_out is not None:
            gcm = list(pool_out.keys())[0] 
            rcm = list(pool_out[gcm].keys())[0] 
            ens = list(pool_out[gcm][rcm].keys())[0] 
            exp = list(pool_out[gcm][rcm][ens].keys())[0] 
            var = list(pool_out[gcm][rcm][ens][exp].keys())[0] 
            
            if gcm not in dict_avail.keys():
                dict_avail[gcm] = {}
                
            if rcm not in dict_avail[gcm].keys():
                dict_avail[gcm][rcm] = {}	
                
            if ens not in dict_avail[gcm][rcm].keys():
                dict_avail[gcm][rcm][ens] = {}	
                
            if exp not in dict_avail[gcm][rcm][ens].keys():
                dict_avail[gcm][rcm][ens][exp] = {}	
                
            if var not in dict_avail[gcm][rcm][ens][exp].keys():
                dict_avail[gcm][rcm][ens][exp][var] = {"isvalid":1}
                dict_avail[gcm][rcm][ens][exp][var]["url"] = pool_out[gcm][rcm][ens][exp][var]["url"]
            else:
                dict_avail[gcm][rcm][ens][exp][var]["url"].append(pool_out[gcm][rcm][ens][exp][var]["url"])
    return dict_avail

def clean_esgf_data(dict_avail, var_list, exp_list):
    """ keeps only data found that have all the wanted variables and experiences (rcp)"""
    tmp_dict = copy.deepcopy(dict_avail)

    for gcm in dict_avail.keys():        
        for rcm in dict_avail[gcm].keys():
            for ens in dict_avail[gcm][rcm].keys():        
                for exp in dict_avail[gcm][rcm][ens].keys():
                    check_n_var = 0
                    for var in dict_avail[gcm][rcm][ens][exp].keys():
                        check_n_var+= dict_avail[gcm][rcm][ens][exp][var]["isvalid"]
                    if check_n_var<len(var_list):
                        print(check_n_var)
                        tmp_dict[gcm][rcm][ens].pop(exp, None)
    
    out_dict_avail = copy.deepcopy(tmp_dict)
    for gcm in tmp_dict.keys():        
        for rcm in tmp_dict[gcm].keys():
            for ens in tmp_dict[gcm][rcm].keys():        
                if len(tmp_dict[gcm][rcm][ens].keys())<len(exp_list):
                    out_dict_avail[gcm][rcm].pop(ens, None)
            if len(out_dict_avail[gcm][rcm]) == 0:
                out_dict_avail[gcm].pop(rcm,None)
            if len(out_dict_avail[gcm]) == 0:
                out_dict_avail.pop(gcm,None)
                    
    return out_dict_avail

def get_url_list(data_avail, var_list, exp_list):
    """ Keeps only data found that have all the wanted variables and experiences (rcp)"""
    url_list = []
    for exper in exp_list:
        for var in var_list:
            for gcm in data_avail.keys():
                for rcm in data_avail[gcm].keys():
                    for ens in  data_avail[gcm][rcm].keys():
                        print(exper+" - "+var+" - "+gcm+" - "+rcm+" - "+ens)
                        
                        if  type(data_avail[gcm][rcm][ens][exper][var]["url"][0]) is list:
                            flat_url_list = [x for xs in data_avail[gcm][rcm][ens][exper][var]["url"] for x in xs  ]
                            flat_url_list = [x for x in flat_url_list if len(x)>0]
                        else:
                            flat_url_list = [x for x in data_avail[gcm][rcm][ens][exper][var]["url"] ]
                            flat_url_list = [x for x in flat_url_list if len(x)>0]
                        url_list+= flat_url_list
    return url_list

def check_status_node_esgf():
    """ Checks the status of the download nodes on the esgf website."""
    node_status = {}
    status_conv = {"check-circle":"UP", "close-circle":"DOWN"}
    URL = "https://esgf-node.llnl.gov/status/"
    opts = FirefoxOptions()
    opts.add_argument("--headless")
    browser = webdriver.Firefox(options=opts)
    browser.get(URL)
    page_node_status = browser.page_source
    browser.close
    page_node_status_short = page_node_status.split("tbody")[-2]
    node_status_list = page_node_status_short.split("data-row-key")[1::]
    
    for node_status_str in node_status_list:
        node_name = node_status_str.split(" ")[0].replace("=", "").replace("\"","")
        node_status_updown = status_conv[node_status_str.split(" ")[10].split("\"")[1]]
        node_status[node_name] = node_status_updown
    # OLD WAY BEFORE 2024 AN
    # page = requests.get(URL)
    # soup = BeautifulSoup(page.content, "html.parser")
    #
    # for tr in soup.find_all('tr')[5:5+48]:
    #     tds = tr.find_all('td')
    #     #print( " Node: %s, Status: %s" % ( tds[1].text, tds[2].text))
    #     node_status[tds[1].text] = tds[2].text.replace("\n","")
    #
    # node_status["esgf1.dkrz.de"]="UP" # mail from Katarina Berger saying that this node is falsly always shown as down...
    return node_status





def extract_esgf_data_ROI_netcdf(url, ROI_boundingbox, base_out_dir, node_status):         
    """ Opens url and extracts the ROI to a netcdf writen in base_out_dir if the node_status is up."""               
    print("Treating "+url)
    out_name = url.split('/')[-1]
    variable = url.split("3hr")[1].split("/")[1]
    gcm = url.split("/")[-1].split("_")[2]
    rcm = url.split("/")[-1].split("_")[5]
    exper = url.split("/")[-1].split("_")[3]

    base_out_dir0 = base_out_dir+"/"+gcm+"__"+rcm+"/"+exper+"/"
    for region in ROI_boundingbox.keys():
        out_dir = base_out_dir+"/"+gcm+"__"+rcm+"/"+exper+"/"+region+"/"+variable+"/"
        if not os.path.exists(out_dir):
            os.makedirs(out_dir)
                            
    node_download = url.split("//")[1].split("/")[0]
    if node_status[node_download] == "DOWN":
        print(node_download+" is down. Skipping download.")
    elif node_status[node_download] == "UP":
        try:
            # check if files exist yet
            file_count = 0
            for region in ROI_boundingbox.keys():
                out_dir = base_out_dir0+"/"+region+"/"+variable+"/"
                if os.path.exists(out_dir + out_name):
                    file_count+= 1

            if file_count==len(ROI_boundingbox.keys()):
                print("Files exist for all region")
            else:
                with xr.open_dataset(url, decode_times = False) as ds:
                    if variable not in ds.variables:
                        print(variable+" not found in "+out_name)
                    else:
                        dataset = ds[variable] # meilleur de [[variable, "lat", "lon"]] parcequ'on perd des downloads au check coord
                        for region in ROI_boundingbox.keys():
                            out_dir = base_out_dir0+"/"+region+"/"+variable+"/"
                            if not os.path.exists(out_dir + out_name):
                                # file does not exist
                                if "rlat" in ds.coords:
                                    bb = np.where((ds.rlon > ROI_boundingbox[region]["rot"][0]) & (ds.rlon < ROI_boundingbox[region]["rot"][1]) & (ds.rlat > ROI_boundingbox[region]["rot"][2]) & (ds.rlat < ROI_boundingbox[region]["rot"][3]))
    
                                else:
                                    bb = np.where((ds.lon > ROI_boundingbox[region]["unrot"][0]) & (ds.lon < ROI_boundingbox[region]["unrot"][1]) & (ds.lat > ROI_boundingbox[region]["unrot"][2]) & (ds.lat < ROI_boundingbox[region]["unrot"][3]))
                                    
                                da2 = dataset[:, np.min(bb[0]):np.max(bb[0]), np.min(bb[1]):np.max(bb[1])]
    
                                check_coord = ("lat" in da2.coords and "lon" in da2.coords) + ("rlat" in da2.coords and "rlon" in da2.coords)
                                if check_coord: # !! des fois lat,lon sont dans les variables, pas dans les coordonnees
                                    da2.to_netcdf(out_dir + out_name, encoding={variable:{"zlib":True}}) # lossless compression @sgascoin mail
                                else:
                                    print("Something went wrong, skipping download - "+region+" - "+out_name)                        
                            else:
                                print("File already exist, skipping download - "+region+" - "+out_name) 
                #ds.close()
        except IOError:
            print("Server likely down. Skipping.")
            
            
            
            
def extract_esgf_data_EUR_orog_netcdf(url, base_out_dir, node_status):         
    """ Opens url and extracts the ROI to a netcdf writen in base_out_dir if the node_status is up."""               
    print("Treating "+url)
    out_name = url.split('/')[-1]
    variable = "orog"
    gcm = url.split("/")[-1].split("_")[2]
    rcm = url.split("/")[-1].split("_")[5]
    exper = url.split("/")[-1].split("_")[3]

    base_out_dir0 = base_out_dir+"/"+gcm+"__"+rcm+"/"
    if os.path.exists(base_out_dir0):
        out_dir = base_out_dir0+"/"+variable+"/"
        if not os.path.exists(out_dir):
            os.makedirs(out_dir)
                                
        node_download = url.split("//")[1].split("/")[0]
        if node_status[node_download] == "DOWN":
            print(node_download+" is down. Skipping download.")
        elif node_status[node_download] == "UP":
            try:
                if os.path.exists(out_dir + out_name):                
                    print("File already exist, skipping download - "+out_name) 
                else:
                    ds = xr.open_dataset(url, decode_times = False) 
                    if variable not in ds.variables:
                        print(variable+" not found in "+out_name)
                    else:
                        dataset = ds[variable]
                        check_coord = ("lat" in ds.coords and "lon" in ds.coords) + ("rlat" in ds.coords and "rlon" in ds.coords)
                        if check_coord:
                            dataset.to_netcdf(out_dir + out_name, encoding={variable:{"zlib":True}}) # lossless compression @sgascoin mail
                        else:
                            print("Something went wrong, skipping download - "+out_name)  
                    ds.close()
            except IOError:
                print("Server likely down. Skipping.")          
                
                
def get_download_progression(data_folder = "/media/hola/datos_seis/nacional_parques/data/cordex/EUR11", region_list = ["AIGUE", "GUADA", "ORDES", "PICOS", "SIERR"], 
    exp_list = ['historical', 'rcp26', 'rcp45', 'rcp85'], var_list = ["tas", "pr", "uas", "vas", "hurs"], figFolder = "/home/hola/datos_seis/nacional_parques/fig"):         
    """ Checks how the progression of CORDEX data is going. Makes a figure."""
    path_list = glob.glob(data_folder+"/*/")
    n_run = len(path_list)
    n_var = len(var_list)
    gcm_list = [path.split("/")[-2].split("__")[0] for path in path_list]
    rcm_list = [path.split("/")[-2].split("__")[1] for path in path_list]

    out={}
    for exp in exp_list:
        out[exp] = {}
        for region in region_list:
            out[exp][region] = {}
            out[exp][region]["status_im"] = np.zeros((1+n_run*n_var, 3000))

    run_name = []
    for ipath,path in enumerate(path_list):
        run_name.append(path.split("/")[-2].replace("CNRM", "").replace("IPSL", ""))
        for exp in exp_list:
            for ivar,var in enumerate(var_list):
                for region in region_list:
                    nc_list = glob.glob(path+"/"+exp+"/"+region+"/"+var+"/*.nc")
                    for nc in nc_list:
                        if ivar==0:
                            out[exp][region]["status_im"][ivar+n_var*ipath,int(nc.split("/")[-1].split("_")[-1][0:4])]=2
                        else:
                            out[exp][region]["status_im"][ivar+n_var*ipath,int(nc.split("/")[-1].split("_")[-1][0:4])]=1
                        
    for exp in exp_list:
        plt.figure()
        
        for i_region, region in enumerate(region_list):
            plt.subplot(int(np.ceil(len(region_list)/2)),2,i_region+1)
            plt.imshow(out[exp][region]["status_im"])
            plt.xlim(1940,2100)
            plt.title(exp+" - "+region)

        #plt.subplot(len(region_list)+1,1,len(region_list)+1)
        #for yy, tt in enumerate(run_name):
        #    plt.text(0, yy, tt, fontsize=8)

        plt.tight_layout()
        plt.savefig(figFolder+"/status_"+exp+".png")
        plt.close()                 
                    
                
