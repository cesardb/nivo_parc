#!/usr/bin/python3
import glob
import argparse
import os

####
# Extract relevant informations from a Cordex file name
####


def myget_Cordex_basename(inNCName, inTimeStep):
    outNCName = os.path.basename(inNCName).split("_"+inTimeStep+"_")[0]+"_"+inTimeStep+"_"
    print(outNCName)

    
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description = "Extract relevant informations from a Cordex file name")
    parser.add_argument("-inNCName", dest = "inNCName")
    parser.add_argument("-inTimeStep", dest = "inTimeStep")
    args = parser.parse_args()
    
    myget_Cordex_basename( args.inNCName, args.inTimeStep)
    

