ctl2nccomp(){
while [[ "$#" > 1 ]]; do case $1 in
    -inFILE) inFILE="$2";;
    *) break;;
  esac; shift; shift
done

NAME=`echo "$inFILE" | cut -d'.' -f1`;
EXT=`echo "$inFILE" | cut -d'.' -f2`;

if [ "$EXT" = "ctl" ]
then
cdo -f nc4 -z zip_9 import_binary $inFILE ${NAME}.nc
#ncks -4 -L 9 ${NAME}_0.nc ${NAME}.nc
elif [ "$EXT" = "gdat" ]
then
cdo -f nc4 -z zip_9 import_binary ${NAME}.ctl ${NAME}.nc
fi

[ -f ${NAME}.nc ] && rm ${NAME}.gdat 
}

export -f ctl2nccomp

find /home/hola/datos2/nacional_parques/prod/sm/sm_Tlr_53_qm/*/*/*/* -type f -name "*.gdat" > inFILE_list.txt
parallel --jobs 10 ctl2nccomp -inFILE {} :::: inFILE_list.txt
rm inFILE_list.txt


find /home/hola/datos2/nacional_parques/prod/sm/sm_Tlr_53_qm/{"NCC-NorESM1-M__CNRM-ALADIN63","MPI-M-MPI-ESM-LR__ICTP-RegCM4-6","ICHEC-EC-EARTH__ICTP-RegCM4-6"}/*/ORDES/* -type f -name "*.ctl" > inFILE_list.txt
parallel --jobs 10 ctl2nccomp -inFILE {} :::: inFILE_list.txt
rm inFILE_list.txt
