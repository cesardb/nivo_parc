import os
import glob
import numpy as np
import pandas as pd
from sklearn.linear_model import LinearRegression
import pickle
from xgrads import open_CtlDataset # replace with xarray
import matplotlib as mpl
mpl.rcParams.update({'text.usetex': False,"svg.fonttype": 'none'})
import matplotlib.pyplot as plt
import datetime as dt
import xarray as xr
import calendar 
import itertools
from multiprocessing import Pool
import fiona
import operator

plt_dict = {
    "historical":{"col":"#4a4e4d", "mrk":"v"},
    "rcp26":{"col":"#f6cd61", "mrk":"^"},
    "rcp85":{"col":"#fe8a71", "mrk":">"},
    "AIGUE":{"col":"#ee4035","mrk":"H"},
    "GUADA":{"col":"#f37736","mrk":"X"},
    "ORDES":{"col":"#ffe800","mrk":"o"},
    "PICOS":{"col":"#7bc043","mrk":"d"},
    "SIERR":{"col":"#0392cf","mrk":"s"},
    "DJF":{"col":"#0057e7","mrk":"*"},
    "MAM":{"col":"#008744","mrk":"s"},
    "JJA":{"col":"#d62d20","mrk":"."},
    "SON":{"col":"#ffa700","mrk":"^"},
    }

def groupby_multicoords(da, fields): 
    # https://stackoverflow.com/questions/60380993/groupby-multiple-coords-along-a-single-dimension-in-xarray
    common_dim = da.coords[fields[0]].dims[0]
    tups_arr = np.empty(len(da[common_dim]), dtype=object)
    tups_arr[:] = list(zip(*(da[f].values for f in fields)))
    return da.assign_coords(grouping_zip=xr.DataArray(tups_arr, dims=common_dim)).groupby('grouping_zip')

def write_ctl(in_gdat, in_par_folder="/home/hola/project/nacional_parques/sm/par"):
    """ Write the .ctl file further used to read the .gdat."""
    var_def = {
        "prec" : "prec  0  0 water-equivalent precipitation (m/time_step)",
        "rpre" : "rpre  0  0 liquid precipitation, rain (m/time_step)",
        "sden" : "sden  0  0 snow density (kg/m3)",
        "snod" : "snod  0  0 snow depth (m)",
        "spre" : "spre  0  0 solid precipitation, snowfall (m/time_step)",
        "swed" : "swed  0  0 snow-water-equivalent depth (m)",
        "tair" : "tair  0  0 air temperature (deg C)",
        "ssub" : "ssub  0 0 static-surface sublimation (m)",
        "roff" : "roff  0 0 runoff from base of snowpack (m/time_step)"
        }
    
    month_dic = {
        "1" : "jan", "2" : "feb", "3" : "mar", "4" : "apr",
        "5" : "may", "6" : "jun", "7" : "jul", "8" : "aug",
        "9" : "sep", "10" : "oct", "11" : "nov", "12" : "dec"
        }
    
    out_folder = os.path.dirname(in_gdat)

    var = in_gdat.split("/")[-1].replace(".gdat","")
    reg = in_gdat.split("/")[-3]
    exp = in_gdat.split("/")[-4]
    gcm = in_gdat.split("/")[-5].split("__")[0]
    rcm = in_gdat.split("/")[-5].split("__")[1]
    
    in_par = glob.glob(in_par_folder+"/"+gcm+"__"+rcm+"_"+exp+"_"+reg+".par")[0]
    out_ctl = out_folder+"/"+var+".ctl"
    
    with open(in_par) as f:
        lines = f.readlines()
    
    for line in lines:
        if "nx =" in line:
            nx = line.split("nx = ")[1].replace("\n","")
            
        elif "ny =" in line:
            ny = line.split("ny = ")[1].replace("\n","")
            
        if "xmn =" in line:
            x_mn = line.split("xmn = ")[1].replace("\n","")
            
        elif "ymn =" in line:
            y_mn = line.split("ymn = ")[1].replace("\n","")
            
        elif "deltax =" in line:
            delta_x = line.split("deltax = ")[1].replace("\n","")
        
        elif "deltay =" in line:
            delta_y = line.split("deltay = ")[1].replace("\n","")
                
        elif "iyear_init =" in line:
            year0 = line.split("iyear_init = ")[1].replace("\n","")

        elif "imonth_init =" in line:
            month0 = line.split("imonth_init = ")[1].replace("\n","")

        elif "iday_init =" in line:
            day0 = line.split("iday_init = ")[1].replace("\n","")

        elif "xhour_init =" in line:
            hour0 = line.split("xhour_init = ")[1].replace("\n","")

        elif "max_iter =" in line:
            max_iter = line.split("max_iter = ")[1].replace("\n","")
            
        elif "print_inc =" in line:
            print_inc = line.split("print_inc = ")[1].replace("\n","")
    

    time_step = "1dy"
    date0 = hour0.zfill(2)+"Z"+day0.zfill(2)+month_dic[month0]+year0
    n_time = str(int(float(max_iter)/float(print_inc)))

    lines = []
    lines.append("DSET "+in_gdat+"\n")
    lines.append("TITLE SnowModel single-variable output file"+"\n")
    lines.append("UNDEF    -9999.0"+"\n")
    lines.append("XDEF       "+nx+" LINEAR     "+str(x_mn)+"        "+str(delta_x)+"\n")
    lines.append("YDEF       "+ny+" LINEAR     "+str(y_mn)+"        "+str(delta_y)+"\n")
    lines.append("ZDEF         1 LINEAR 1 1"+"\n")
    lines.append("TDEF    "+str(n_time)+" LINEAR "+date0+"  "+str(time_step)+"\n")  # 12Z01dec1949  1dy
    lines.append("VARS     1"+"\n")
    if var in var_def.keys():
        lines.append(var_def[var]+"\n")
    else:
        lines.append(var+" 0 0 unknown var\n")
    lines.append("ENDVARS")
    
    with open(out_ctl, "w") as f:
        f.writelines(lines)
                 
def f_prep_dohy_hyear(args):         
    in_gdat = args["sm_out_dir"]+"/"+args["run"]+"/"+args["exp"]+"/"+args["reg"]+"/wo_assim/"+args["var"]+".gdat"
    if os.path.isfile(in_gdat) is False:
        return
    else:
        print(in_gdat)
        write_ctl(in_gdat, in_par_folder=args["sm_par_dir"])
    
        if not glob.glob(args["sm_out_dir"]+"/"+args["run"]+"/"+args["exp"]+"/"+args["reg"]+"/wo_assim/dohy_hyear.pkl"):
            args["var"] = in_gdat.split("/")[-1].replace(".gdat","")
            in_ctl = args["sm_out_dir"]+"/"+args["run"]+"/"+args["exp"]+"/"+args["reg"]+"/wo_assim/"+args["var"]+".ctl"
            dset = open_CtlDataset(in_ctl) # replace with xarray xr.open_dataset(inNCcomp)
        
            # add a day of hydrological year variable
            dohy = []
            for dd in dset.time: # tested with map : it is 17% slower 
               if dd.dt.date.item()<dt.date(year=dd.dt.year.item(),month=9,day=1):
                    dohy.append(dd.dt.dayofyear.item()+121)
               else:
                   if calendar.isleap(dd.dt.year.item()) is False:
                       dohy.append(dd.dt.dayofyear.item()-244)
                   else:
                       dohy.append(dd.dt.dayofyear.item()-245)
        
            hyear = [ dd.dt.year.item() if (dd.dt.month>=9) else dd.dt.year.item()-1 for dd in dset.time]
            with open(args["sm_out_dir"]+"/"+args["run"]+"/"+args["exp"]+"/"+args["reg"]+"/wo_assim/dohy_hyear.pkl", "wb") as f:
                pickle.dump({"dohy":dohy,"hyear":hyear}, f)  
                
def f_prep_dohy_hyear_ens(args):         
    in_nc = args["sm_out_dir"]+"/ens/"+args["exp"]+"/"+args["reg"]+"/"+args["var"]+"_"+args["stat_name"]+".nc"
    if os.path.isfile(in_nc) is False:
        print("Not found : "+in_nc)
        return
    else:
        print(in_nc)
        if not glob.glob(args["sm_out_dir"]+"/ens/"+args["exp"]+"/"+args["reg"]+"/dohy_hyear.pkl"):
            with xr.open_dataset(in_nc) as dset:
                # add a day of hydrological year variable
                dohy = []
                for dd in dset.time: # tested with map : it is 17% slower 
                   if dd.dt.date.item()<dt.date(year=dd.dt.year.item(),month=9,day=1):
                        dohy.append(dd.dt.dayofyear.item()+121)
                   else:
                       if calendar.isleap(dd.dt.year.item()) is False:
                           dohy.append(dd.dt.dayofyear.item()-244)
                       else:
                           dohy.append(dd.dt.dayofyear.item()-245)
            
                hyear = [ dd.dt.year.item() if (dd.dt.month>=9) else dd.dt.year.item()-1 for dd in dset.time]
                with open(args["sm_out_dir"]+"/ens/"+args["exp"]+"/"+args["reg"]+"/dohy_hyear.pkl", "wb") as f:
                    pickle.dump({"dohy":dohy,"hyear":hyear}, f)   

def f_load_dset_from_nc(args):
    print("Treating "+args["exp"]+ " - "+args["run"]+" - "+args["reg"])
    DEM = np.loadtxt("/home/hola/datos_seis/nacional_parques/data/COP30/"+str(args["res"])+"m/limite_"+args["reg"]+"_COP30_"+str(args["res"])+"m_UTM_DEM.asc", skiprows=6)
    SLP = np.loadtxt("/home/hola/datos_seis/nacional_parques/data/COP30/"+str(args["res"])+"m/limite_"+args["reg"]+"_COP30_"+str(args["res"])+"m_UTM_SLP.asc", skiprows=6)
    ASP = np.loadtxt("/home/hola/datos_seis/nacional_parques/data/COP30/"+str(args["res"])+"m/limite_"+args["reg"]+"_COP30_"+str(args["res"])+"m_UTM_ASP.asc", skiprows=6)
    MSK = np.loadtxt("/home/hola/datos_seis/nacional_parques/data/COP30/"+str(args["res"])+"m/limite_"+args["reg"]+"_COP30_"+str(args["res"])+"m_UTM_MSK.asc", skiprows=6)

    in_nc_name = args["sm_out_dir"]+"/"+args["run"]+"/"+args["exp"]+"/"+args["reg"]+"/wo_assim/"+args["var"]+".nc"
    if not glob.glob(in_nc_name):
        print("No file found in "+in_nc_name)
        dset = {}

    else:
        in_nc = glob.glob(in_nc_name)[0]

        try:
            dset = xr.open_dataset(in_nc, chunks={"time":5000})
            with open(args["sm_out_dir"]+"/"+args["run"]+"/"+args["exp"]+"/"+args["reg"]+"/wo_assim/dohy_hyear.pkl",'rb') as f:
                time_coord = pickle.load(f)
            
            # add coordinates
            dset = dset.assign_coords(dohy=("time", np.array(time_coord["dohy"]))) # day of hydrological year (0=1st september) 
            dset = dset.assign_coords(hyear=("time", np.array(time_coord["hyear"]))) # hydrological year (sept_yyyy-dec_yyyy=yyyy, january_yyyy+1-august_yyyy+1=yyyy)
            dset = dset.assign_coords(DEM=(["lat","lon"], np.flipud( DEM ))) # elevation
            dset = dset.assign_coords(ASP=(["lat","lon"], np.flipud( ASP ))) # aspect
            dset = dset.assign_coords(SLP=(["lat","lon"], np.flipud( SLP ))) # slope
            dset = dset.assign_coords(MSK=(["lat","lon"], np.flipud( MSK ))) # PN mask
        except:
            print("! Error ! Check "+in_nc+" and associated dohy_hyear.pkl")
            dset = {}
            
    return(dset)

def f_load_dset_from_nc_ens(args):
    print("Treating "+args["exp"]+ " - ensemble - "+args["reg"])
    DEM = np.loadtxt("/home/hola/datos_seis/nacional_parques/data/COP30/"+str(args["res"])+"m/limite_"+args["reg"]+"_COP30_"+str(args["res"])+"m_UTM_DEM.asc", skiprows=6)
    SLP = np.loadtxt("/home/hola/datos_seis/nacional_parques/data/COP30/"+str(args["res"])+"m/limite_"+args["reg"]+"_COP30_"+str(args["res"])+"m_UTM_SLP.asc", skiprows=6)
    ASP = np.loadtxt("/home/hola/datos_seis/nacional_parques/data/COP30/"+str(args["res"])+"m/limite_"+args["reg"]+"_COP30_"+str(args["res"])+"m_UTM_ASP.asc", skiprows=6)
    MSK = np.loadtxt("/home/hola/datos_seis/nacional_parques/data/COP30/"+str(args["res"])+"m/limite_"+args["reg"]+"_COP30_"+str(args["res"])+"m_UTM_MSK.asc", skiprows=6)
    
    in_nc_name = args["sm_out_dir"]+"/ens/"+args["exp"]+"/"+args["reg"]+"/"+args["var"]+"_"+args["stat_name"]+".nc"

    if not glob.glob( in_nc_name ):
        print("No file found in "+in_nc_name )
        dset = {}

    else:
        in_nc = glob.glob( in_nc_name )[0]
        f_prep_dohy_hyear_ens( args )
        try:
            dset = xr.open_dataset( in_nc, chunks={"time":5000})
            with open( args["sm_out_dir"]+"/ens/"+args["exp"]+"/"+args["reg"]+"/dohy_hyear.pkl", 'rb') as f:
                time_coord = pickle.load(f)
            
            # add coordinates
            dset = dset.assign_coords(dohy=("time", np.array(time_coord["dohy"]))) # day of hydrological year (0=1st september) 
            dset = dset.assign_coords(hyear=("time", np.array(time_coord["hyear"]))) # hydrological year (sept_yyyy-dec_yyyy=yyyy, january_yyyy+1-august_yyyy+1=yyyy)
            dset = dset.assign_coords(DEM=(["lat","lon"], np.flipud( DEM ))) # elevation
            dset = dset.assign_coords(ASP=(["lat","lon"], np.flipud( ASP ))) # aspect
            dset = dset.assign_coords(SLP=(["lat","lon"], np.flipud( SLP ))) # slope
            dset = dset.assign_coords(MSK=(["lat","lon"], np.flipud( MSK ))) # PN mask
        except:
            print("! Error ! Check "+in_nc+" and associated dohy_hyear.pkl")
            dset = {}
            
    return(dset)

def get_simu_single_exp(exp, res, sm_out_dir, sm_par_dir, in_run_list, in_reg_list, in_var_list):
    arg_list = [{"exp":xarg_list[0],
                 "run":xarg_list[1],
                 "reg":xarg_list[2],
                 "var":xarg_list[3],
                 "res":res,
                 "sm_out_dir":sm_out_dir,
                 "sm_par_dir":sm_par_dir} 
                 for xarg_list in list(itertools.product([exp], in_run_list, in_reg_list, in_var_list))]
    
    # load sm output and save in pkl file
    with Pool(40) as p:
        f_out = p.map(f_load_dset_from_nc, arg_list)
        
    data_all = {}    
    for run in in_run_list:
        data_all[run] = {}
        data_all[run][exp] = {}
        for reg in in_reg_list:  
            data_all[run][exp][reg] = {}
                
    for i,args in enumerate(arg_list):
        data_all[args["run"]][args["exp"]][args["reg"]][args["var"]]= f_out[i]
    
    return data_all

def get_simu_ens(exp, res, sm_out_dir, sm_par_dir, in_reg_list, in_var_list, stat_name):
    arg_list = [{"exp":xarg_list[0],
                 "reg":xarg_list[1],
                 "var":xarg_list[2],
                 "res":res,
                 "sm_out_dir":sm_out_dir,
                 "sm_par_dir":sm_par_dir,
                 "stat_name" :stat_name} 
                 for xarg_list in list(itertools.product([exp], in_reg_list, in_var_list))]
    
    # load sm output and save in pkl file
    with Pool(40) as p:
        f_out = p.map(f_load_dset_from_nc_ens, arg_list)
        
    data_all = {}    
    data_all["ens"] = {}
    data_all["ens"][exp] = {}
    for reg in in_reg_list:  
        data_all["ens"][exp][reg] = {}
                
    for i,args in enumerate(arg_list):
        data_all["ens"][args["exp"]][args["reg"]][args["var"]] = f_out[i]
    
    return data_all

def merge_data_runs(data_all, data):
    for kk in data_all.keys():
        if kk in data.keys():
            for kkk in data[kk].keys():
                data_all[kk][kkk] = data[kk][kkk]
            
    for kk in data.keys():
        if kk not in data_all.keys():
            data_all[kk] = data[kk]
            
    return data_all 
    
def read_asc(inASC):
    data = np.genfromtxt(inASC, skip_header=6, dtype=int)
    metadata = np.genfromtxt(inASC, max_rows=6, dtype=float)
    (x_ll, y_ll, nx, ny, dxy) = metadata[(2, 3, 0, 1, 4), 1]
    xx = np.arange(nx)*dxy+x_ll
    yy = np.arange(ny)*dxy+y_ll
    return(data, xx, yy)

def get_name_in_ctl(in_ctl):
    reg = in_ctl.split("/")[-3]
    exp = in_ctl.split("/")[-4]
    run = in_ctl.split("/")[-5]
    return exp, run, reg

def get_var(dset, var_name, period):
    if period:
        var = dset[var_name].sel(time = slice(period[0],period[1]))
    else:
        var = dset[var_name]
    return var

def read_run_list(in_file_template):
    out_dict = {}
    for exp in ["historical", "rcp26", "rcp85"]:
        in_file = in_file_template.replace("EXP", exp)
        with open(in_file, 'r') as file:
            data = file.read().replace('\n', '')
        
        out_dict[exp] = data.split("(")[1].replace(")","").replace("\"","").split(" ")
        
    return out_dict

def clean_uncomplete_hyear(var):
    # First, convert time to pandas DatetimeIndex
    time_index = pd.to_datetime(var['time'].values)
    
    # Find the first occurrence of September 1st
    sep_1st_idx = np.where((time_index.month == 9) & (time_index.day == 1))[0][0]

    # Find the last occurrence of August 31st
    aug_31st_idx = np.where((time_index.month == 8) & (time_index.day == 31))[0][-1]
     
    # Select values within these boundaries
    var_cleaned = var.isel(time=slice(sep_1st_idx, aug_31st_idx+1))
    return var_cleaned

def open_parc_shp(reg):
    in_shp_parc = glob.glob("/home/hola/datos_seis/nacional_parques/data/contour/limite_"+reg+"*.shp")[0]
    shape = fiona.open(in_shp_parc)
    contour = shape.next()
    if reg == "PICOS":
        x = np.array(contour["geometry"]["coordinates"][0]).squeeze()[:,0] # hay dos huecitos en Picos...
        y = np.array(contour["geometry"]["coordinates"][0]).squeeze()[:,1]
    else:
        x = np.array(contour["geometry"]["coordinates"]).squeeze()[:,0]
        y = np.array(contour["geometry"]["coordinates"]).squeeze()[:,1]
    
    return x, y
        
def calc_SMOD(arr):
    if np.sum( arr > 0 ) > 10:
        r = max((list(y) for (x, y) in itertools.groupby(enumerate(arr), operator.itemgetter(1)) if x == 1), key=len)
        return r[-1][0]
    else:
        return np.nan

def calc_SOD(arr):
    if np.sum( arr > 0 ) > 10:
        r = max((list(y) for (x, y) in itertools.groupby(enumerate(arr), operator.itemgetter(1)) if x == 1), key=len)
        return r[0][0]
    else:
        return np.nan
        
def get_xr_im_extent(xr_data):
    if "x" in xr_data.coords:
        half_dx = (xr_data.x[1]-xr_data.x[0])/2
        half_dy = (xr_data.y[1]-xr_data.y[0])/2
        ymin, ymax, xmin, xmax = np.min(xr_data.y)-half_dy, np.max(xr_data.y)+half_dy, np.min(xr_data.x)-half_dx, np.max(xr_data.x)+half_dx

    elif "lat" in xr_data.coords:
        half_dx = (xr_data.lon[1]-xr_data.lon[0])/2
        half_dy = (xr_data.lat[1]-xr_data.lat[0])/2    
        ymin, ymax, xmin, xmax = np.min(xr_data.lat)-half_dy, np.max(xr_data.lat)+half_dy, np.min(xr_data.lon)-half_dx, np.max(xr_data.lon)+half_dx
    return (xmin, xmax, ymin, ymax)

def plot_prec_valid_station(dset, var_name, in_ctl, fig_out_dir, period = None, in_pkl_station = None):
    exp, run, reg = get_name_in_ctl(in_ctl)
    var = get_var(dset, var_name, period)
    
    with open(in_pkl_station, "rb") as f:
        station_data_pr_region = pickle.load(f)   
    
    x2interp,y2interp=[],[]
    for station in station_data_pr_region[reg]["ycum_pr"].columns:
        y2interp.append(station_data_pr_region[reg]["station_coord"].loc[station]["y"])
        x2interp.append(station_data_pr_region[reg]["station_coord"].loc[station]["x"])

    x = xr.DataArray(x2interp, dims="points")
    y = xr.DataArray(y2interp, dims="points")    
        
    dset_prec_sum = var.groupby("hyear").sum().interp(
            {"lat":y
            ,"lon":x},method="nearest")
    
    x_stat, y_stat = [],[]
    plt.figure()
    for station, xxx_simu in zip(station_data_pr_region[reg]["ycum_pr"].columns, dset_prec_sum.prec.values.T):
        xx_simu = pd.Series(xxx_simu, dset_prec_sum.hyear.values)
        xx_obs = station_data_pr_region[reg]["ycum_pr"][station]
        for yy in set(xx_obs.index).intersection(xx_simu.index):
            x_obs = xx_obs[yy]
            x_simu = xx_simu[yy] 
            if (x_obs>0) & (x_simu>0):
                plt.plot(x_obs,1000*x_simu,".k",ms=0.5)       
                x_stat.append(x_obs)
                y_stat.append(1000*x_simu) 

    if len(x_stat)>0:
        reglin = LinearRegression().fit(np.array(x_stat).reshape(-1,1), np.array(y_stat).reshape(-1,1))
        x_pred = np.array([0,5000])
        y_pred = reglin.intercept_ + reglin.coef_ * x_pred
        plt.plot(x_pred, y_pred.squeeze(),"r")

    
    plt.plot([0,5000],[0,5000],"k")        
    plt.xlabel("Obs. Yearly precipitation (mm)")
    plt.ylabel("Simu. Yearly precipitation (mm)")
    plt.axis("square")
    plt.axis((0,5000,0,5000))
    plt.savefig(fig_out_dir+"/"+var_name+"_"+reg+"_ycum_stat.png")
    plt.close()
    
    plt.figure()
    plt.hexbin(x_stat,y_stat,extent=(0,5000,0,5000), mincnt=1)
    plt.plot([0,5000],[0,5000],"k")        
    plt.xlabel("Obs. Yearly precipitation (mm)")
    plt.ylabel("Simu. Yearly precipitation (mm)")
    plt.axis("square")    
    plt.axis((0,5000,0,5000))
    plt.savefig(fig_out_dir+"/"+var_name+"_"+reg+"_ycum_stat_hexa.png")
    plt.savefig(fig_out_dir+"/"+var_name+"_"+reg+"_ycum_stat_hexa.svg")
    plt.close()
    
def plot_tair_prec_valid_ERA5_qm(dset_noqm, dset_qm, in_ctl, fig_out_dir, period = None, in_ERA5 = None):
    exp, run, reg = get_name_in_ctl(in_ctl)

    ERA5 = pd.read_csv(in_ERA5)
    
    # Air temperature NO QM
    var_name="tair"
    var = get_var(dset_noqm, var_name, period)
    var["season"] = var.time.dt.season

    var_mean = var.mean(dim=["lat","lon"]) #.groupby('hyear') #, pd.Grouper(key='season')])
    var_gp_hyear_season = groupby_multicoords( var_mean, ["hyear", "season"])
    
    var_hyear_season_mean = var_gp_hyear_season.mean().tair.values
    plt.subplot(221)
    for (hyear,season),mean_temp_SM in zip(list(var_gp_hyear_season.groups.keys()), var_hyear_season_mean):
        if  len(ERA5[(ERA5.hyear==hyear) & (ERA5.season==season)].mean_temperature) > 0:    
            print(hyear,season) 
            mean_temp_ERA5 = ERA5[(ERA5.hyear==hyear) & (ERA5.season==season)].mean_temperature.values[0]
            plt.plot(mean_temp_ERA5, mean_temp_SM, color=plt_dict[season]["col"], marker=plt_dict[season]["mrk"], ms=6, mew=0, ls="", label=season)
            
    # Eliminate duplicates in legend
    handles, labels = plt.gca().get_legend_handles_labels()
    handle_list, label_list = [], []
    for handle, label in zip(handles, labels):
        if label not in label_list:
            handle_list.append(handle)
            label_list.append(label)
    plt.legend(handle_list, label_list)
    
    plt.plot([-10, 30], [-10,30], "k", lw=0.5)
    plt.vlines(0,-10,30,color="k",lw=0.2)
    plt.hlines(0,-10,30,color="k",lw=0.2)
    plt.xlabel("ERA5 mean temperature (º)")
    plt.ylabel("Simu. mean temperature (º)")
    
    plt.axis("square")
    plt.axis((-10, 30, -10, 30))
    
    # Precip NO QM
    var_name="prec"
    var = get_var(dset_noqm, var_name, period)
    var["season"] = var.time.dt.season

    var_mean = var.mean(dim=["lat","lon"]) #.groupby('hyear') #, pd.Grouper(key='season')])
    var_gp_hyear_season = groupby_multicoords( var_mean, ["hyear", "season"])
    
    var_hyear_season_sum = var_gp_hyear_season.sum().prec.values
    plt.subplot(222)
    for (hyear,season),cum_prec_SM in zip(list(var_gp_hyear_season.groups.keys()), var_hyear_season_sum):
        if  len(ERA5[(ERA5.hyear==hyear) & (ERA5.season==season)].cum_prec) > 0:    
            print(hyear,season) 
            cum_prec_ERA5 = ERA5[(ERA5.hyear==hyear) & (ERA5.season==season)].cum_prec.values[0]
            plt.plot(cum_prec_ERA5, cum_prec_SM*1000 , color=plt_dict[season]["col"], marker=plt_dict[season]["mrk"], ms=6, mew=0) # convert SM from m to mm

    plt.plot([0,1500], [0,1500], "k", lw=0.5)
    
    plt.xlabel("ERA5. cumulative precipitation (mm)")
    plt.ylabel("Simu. cumulative precipitation (mm)")
    
    plt.axis("square")
    plt.axis((0,1500,0,1500))
    plt.grid(True)
    
    # Air temperature QM
    var_name="tair"
    var = get_var(dset_qm, var_name, period)
    var["season"] = var.time.dt.season

    var_mean = var.mean(dim=["lat","lon"]) #.groupby('hyear') #, pd.Grouper(key='season')])
    var_gp_hyear_season = groupby_multicoords( var_mean, ["hyear", "season"])
    
    var_hyear_season_mean = var_gp_hyear_season.mean().tair.values
    plt.subplot(223)
    for (hyear,season),mean_temp_SM in zip(list(var_gp_hyear_season.groups.keys()), var_hyear_season_mean):
        if  len(ERA5[(ERA5.hyear==hyear) & (ERA5.season==season)].mean_temperature) > 0:    
            print(hyear,season) 
            mean_temp_ERA5 = ERA5[(ERA5.hyear==hyear) & (ERA5.season==season)].mean_temperature.values[0]
            plt.plot(mean_temp_ERA5, mean_temp_SM, color=plt_dict[season]["col"], marker=plt_dict[season]["mrk"], ms=6, mew=0, ls="", label=season)
            
    # Eliminate duplicates in legend
    handles, labels = plt.gca().get_legend_handles_labels()
    handle_list, label_list = [], []
    for handle, label in zip(handles, labels):
        if label not in label_list:
            handle_list.append(handle)
            label_list.append(label)
    plt.legend(handle_list, label_list)
    
    plt.plot([-10, 30], [-10,30], "k", lw=0.5)
    plt.vlines(0,-10,30,color="k",lw=0.2)
    plt.hlines(0,-10,30,color="k",lw=0.2)
    plt.xlabel("ERA5 mean temperature (º)")
    plt.ylabel("Simu. mean temperature (º)")
    
    plt.axis("square")
    plt.axis((-10, 30, -10, 30))
    
    # Precip QM
    var_name="prec"
    var = get_var(dset_qm, var_name, period)
    var["season"] = var.time.dt.season

    var_mean = var.mean(dim=["lat","lon"]) #.groupby('hyear') #, pd.Grouper(key='season')])
    var_gp_hyear_season = groupby_multicoords( var_mean, ["hyear", "season"])
    
    var_hyear_season_sum = var_gp_hyear_season.sum().prec.values
    plt.subplot(224)
    for (hyear,season),cum_prec_SM in zip(list(var_gp_hyear_season.groups.keys()), var_hyear_season_sum):
        if  len(ERA5[(ERA5.hyear==hyear) & (ERA5.season==season)].cum_prec) > 0:    
            print(hyear,season) 
            cum_prec_ERA5 = ERA5[(ERA5.hyear==hyear) & (ERA5.season==season)].cum_prec.values[0]
            plt.plot(cum_prec_ERA5, cum_prec_SM*1000 , color=plt_dict[season]["col"], marker=plt_dict[season]["mrk"], ms=6, mew=0) # convert SM from m to mm

    plt.plot([0,1500], [0,1500], "k", lw=0.5)
    
    plt.xlabel("ERA5. cumulative precipitation (mm)")
    plt.ylabel("Simu. cumulative precipitation (mm)")
    
    plt.axis("square")
    plt.axis((0,1500,0,1500))
    plt.grid(True)
    
    plt.tight_layout()
    plt.savefig(fig_out_dir+"/tair_prec_"+reg+"_ERA5_simu.svg")
    plt.savefig(fig_out_dir+"/tair_prec_"+reg+"_ERA5_simu.png")
    plt.close()
    
    
def get_group_values( data_grouped, n_min ):
    boxplot_data, bin2plot = [], []
    for name, group in data_grouped:
        gg_val = group.values
        gg_val = gg_val[~np.isnan(gg_val)]
        if len(gg_val) > n_min:
            boxplot_data.append(gg_val)
        else:
            boxplot_data.append([])
        bin2plot.append(name.mid)
    return boxplot_data, bin2plot
