#!/home/hola/miniconda3/envs/parc/bin/python

import rasterio
import pyproj
from pyproj import Transformer
import argparse
import numpy as np

def my_sm_par_write(inDEM, inVEG, inMET, outPAR, outFolder):

    UTM_dict ={"SIERR": "epsg: 32630", "PICOS": "epsg: 32630", "ORDES": "epsg: 32631", "GUADA": "epsg: 32630", "AIGUE": "epsg: 32631", "TEIDE": "epsg: 32628"}
    siteName = inDEM.split("_")[-5]
    outFolder_wo = outFolder +"/wo_assim/"
    outFolder_wi = outFolder +"/wi_assim/"
    
    # From inDEM
    #rasterio.open("/home/hola/datos_seis/parc/data/COP30/100m/limite_AIGUE_COP30_100m_UTM_DEM.asc")
    with rasterio.open(inDEM) as DEM:
        nx = DEM.width
        ny = DEM.height
        delta_x = abs(DEM.get_transform()[1])
        delta_y = abs(DEM.get_transform()[-1])
        x_min = DEM.bounds.left
        y_min = DEM.bounds.bottom
        
        #inProj = pyproj.Proj(init=UTM_dict[siteName])
        #outProj = pyproj.Proj(init='epsg:4326')
        #lat_lon = np.array(pyproj.transform(inProj, outProj,  )).transpose()

        transformer_utm2latlon = Transformer.from_crs(UTM_dict[siteName], "epsg:4326")
        (lat, lon) = transformer_utm2latlon.transform(x_min+(nx*delta_x)/2, y_min+(ny*delta_y)/2) # !!! (lat,lon) = f(x,y) ...super counterintuitive

        xlat = np.round(lat,3)
        
    # From inMET
    #MET = open("/home/hola/project/nacional_parques/sm/met/stations/mk_micromet_input_file/SnowModel_test_met.dat","r")
    with open(inMET,'r') as MET:
        lines = MET.readlines()
        l0 = lines[0].replace("\n","").split(" ")
        l0 = [x for x in l0 if x]
        n_station = int(l0[0]) 

        l_t_init = lines[1].split(" ")
        l_t_init = [x for x in l_t_init if x]
        iyear_init = l_t_init[0]
        imonth_init = l_t_init[1]
        iday_init = l_t_init[2]
        xhour_init = l_t_init[3]

        # max_iter = int(np.size(lines)/(n_station+1)) => MUSEUM OF BAD CODING "Takes for ever and even crashes quite often..."
        max_iter = int(len(lines)/(n_station+1))

        l_t_next = lines[1+n_station+1].split(" ")
        iday_next = l_t_next[2]
        xhour_next = l_t_next[3]
        dt=60*60*(int(xhour_next)-int(xhour_init))+60*60*24*(int(iday_next)-int(iday_init))

    with open(outPAR,'w') as PAR:
        PAR.write("nx = "+str(nx)+"\n")
        PAR.write("ny = "+str(ny)+"\n")
        PAR.write("deltax = "+str(delta_x)+"\n")
        PAR.write("deltay = "+str(delta_y)+"\n")
        PAR.write("xmn = "+str(x_min)+"\n")
        PAR.write("ymn = "+str(y_min)+"\n")
        PAR.write("dt = "+str(dt)+"\n")
        PAR.write("iyear_init = "+str(iyear_init)+"\n")
        PAR.write("imonth_init = "+str(imonth_init)+"\n")
        PAR.write("iday_init = "+str(iday_init)+"\n")
        PAR.write("xhour_init = "+str(xhour_init)+"\n")
        PAR.write("max_iter = "+str(max_iter)+"\n")
        PAR.write("met_input_fname = "+inMET+"\n")
        PAR.write("ascii_topoveg = 1.0"+"\n")
        PAR.write("topoveg_fname = xxxxxxxxxx"+"\n")
        PAR.write("topo_ascii_fname = "+inDEM+"\n")
        PAR.write("veg_ascii_fname = "+inVEG+"\n")
        PAR.write("xlat = "+str(xlat)+"\n")
       
        PAR.write("output_path_wo_assim = "+outFolder_wo+"\n")
        PAR.write("output_path_wi_assim = "+outFolder_wi+"\n")
        
        PAR.write("isingle_stn_flag = 0"+"\n")
        PAR.write("igrads_metfile = 0"+"\n")
        PAR.write("undef = -9999.0"+"\n")
        PAR.write("veg_shd_25 = 0.10"+"\n")
        PAR.write("veg_shd_26 = 0.10"+"\n")
        PAR.write("veg_shd_27 = 0.10"+"\n")
        PAR.write("veg_shd_28 = 0.10"+"\n")
        PAR.write("veg_shd_29 = 0.10"+"\n")
        PAR.write("veg_shd_30 = 0.10"+"\n")
        PAR.write("const_veg_flag = 0.0"+"\n")
        PAR.write("iveg_ht_flag = 0"+"\n")
        PAR.write("lat_solar_flag = 0"+"\n")
        PAR.write("UTC_flag = 0.0"+"\n")
        PAR.write("run_micromet = 1.0"+"\n")
        PAR.write("run_enbal = 1.0"+"\n")
        PAR.write("run_snowpack = 1.0"+"\n")
        PAR.write("run_snowtran = 1.0"+"\n")
        PAR.write("irun_data_assim = 0"+"\n")
        PAR.write("ihrestart_flag = -2"+"\n")
        PAR.write("i_dataassim_loop = 1"+"\n")
        PAR.write("ihrestart_inc = 0"+"\n")
        PAR.write("i_tair_flag = 1"+"\n")
        PAR.write("i_rh_flag = 1"+"\n")
        PAR.write("i_wind_flag = 1"+"\n")
        PAR.write("i_solar_flag = 1"+"\n")
        PAR.write("i_longwave_flag = 1"+"\n")
        PAR.write("i_prec_flag = 1"+"\n")
        PAR.write("ifill = 1"+"\n")
        PAR.write("iobsint = 0"+"\n")
        PAR.write("dn = 1.0"+"\n")
        PAR.write("barnes_lg_domain = 1.0"+"\n")
        PAR.write("n_stns_used = 9"+"\n")
        PAR.write("snowmodel_line_flag = 0.0"+"\n")
        PAR.write("check_met_data = 0.0"+"\n") # dont check the met data to save time (=0)
        PAR.write("curve_len_scale = 300.0"+"\n")
        PAR.write("slopewt = 0.58"+"\n")
        PAR.write("curvewt = 0.42"+"\n")
        PAR.write("curve_lg_scale_flag = 0.0"+"\n")
        PAR.write("windspd_min = 0.1"+"\n")
        PAR.write("lapse_rate_user_flag = 1"+"\n") # user defined T lapse-rate (=1)
        PAR.write("iprecip_lapse_rate_user_flag = 0"+"\n")
        PAR.write("iprecip_scheme = 1"+"\n")
        PAR.write("snowfall_frac = 3.0"+"\n")
        PAR.write("wind_lapse_rate = 0.0"+"\n")
        PAR.write("calc_subcanopy_met = 1.0"+"\n")
        PAR.write("gap_frac = 0.2"+"\n")
        PAR.write("cloud_frac_factor = 1.0"+"\n")
        PAR.write("use_shortwave_obs = 0.0"+"\n")
        PAR.write("use_longwave_obs = 0.0"+"\n")
        PAR.write("use_sfc_pressure_obs = 0.0"+"\n")
        PAR.write("cf_precip_flag = 0.0"+"\n")
        PAR.write("Utau_t_flag = 0.0"+"\n")
        PAR.write("Utau_t_const = 0.25"+"\n")
        PAR.write("subgrid_flag = 0.0"+"\n")
        PAR.write("tabler_dir = -270.0"+"\n")
        PAR.write("slope_adjust = 1.0"+"\n")
        PAR.write("twolayer_flag = 1.0"+"\n")
        PAR.write("bc_flag = 0.0"+"\n")
        PAR.write("ht_windobs = 10.0"+"\n")
        PAR.write("ht_rhobs = 10.0"+"\n")
        PAR.write("ro_snow = 300.0"+"\n")
        PAR.write("snow_d_init_const = 0.0"+"\n")
        PAR.write("topoflag = 1.0"+"\n")
        PAR.write("icond_flag = 1"+"\n")
        PAR.write("albedo_snow_forest = 0.45"+"\n")
        PAR.write("albedo_snow_clearing = 0.60"+"\n")
        PAR.write("albedo_glacier = 0.40"+"\n")
        PAR.write("sfc_sublim_flag = 1.0"+"\n")
        PAR.write("multilayer_snowpack = 1"+"\n")
        PAR.write("tsls_threshold = 24.0"+"\n")
        PAR.write("max_layers = 6"+"\n")        
        PAR.write("nz_max = 7"+"\n")
        PAR.write("dz_snow_min = 0.001"+"\n")
        PAR.write("izero_snow_date = 090112"+"\n")
        PAR.write("seaice_run = 0.0"+"\n")
        PAR.write("print_micromet = 0.0"+"\n")
        PAR.write("micromet_output_fname = outputs/micromet.gdat"+"\n")
        PAR.write("print_snowtran = 0.0"+"\n")
        PAR.write("snowtran_output_fname = outputs/snowtran.gdat"+"\n")
        PAR.write("Tabler_1_flag = 0.0"+"\n")
        PAR.write("tabler_sfc_path_name = outputs/"+"\n")
        PAR.write("Tabler_2_flag = 0.0"+"\n")
        PAR.write("print_enbal = 0.0"+"\n")
        PAR.write("enbal_output_fname = outputs/enbal.gdat"+"\n")
        PAR.write("print_snowpack = 0.0"+"\n")
        PAR.write("snowpack_output_fname = outputs/snowpack.gdat"+"\n")
        PAR.write("print_multilayer = 0.0"+"\n")
        PAR.write("multilayer_output_fname = outputs/multilayer.gdat"+"\n")
        PAR.write("print_user = 1.0"+"\n")
        PAR.write("print_inc = 8.0"+"\n")
        PAR.write("print_var_01 = y"+"\n") # air temperature (degrees C)
        PAR.write("print_var_02 = y"+"\n") # relative humidity (%)
        PAR.write("print_var_03 = y"+"\n") # wind speed (m/s)
        PAR.write("print_var_04 = y"+"\n") # incoming solar radiation reaching the surface (W/m2)
        PAR.write("print_var_05 = y"+"\n") # incoming longwave radiation reaching the surface (W/m2)
        PAR.write("print_var_06 = n"+"\n") # emitted longwave radiation (W/m2)
        PAR.write("print_var_07 = n"+"\n") # albedo (0-1)
        PAR.write("print_var_08 = y"+"\n") # wind direction (0-360, true N)
        PAR.write("print_var_09 = y"+"\n") # water-equivalent precipitation (m/time_step)
        PAR.write("print_var_10 = y"+"\n") # liquid precipitation (m/time_step)
        PAR.write("print_var_11 = n"+"\n") # solid precipitation (m/time_step)
        PAR.write("print_var_12 = y"+"\n") # snow-water-equivalent melt (m)
        PAR.write("print_var_13 = y"+"\n") # static-surface sublimation (m)
        PAR.write("print_var_14 = n"+"\n") # runoff from base of snowpack (m/time_step)
        PAR.write("print_var_15 = n"+"\n") # snow-water-equivalent melt from glacier ice (m)
        PAR.write("print_var_16 = y"+"\n") # snow depth (m)
        PAR.write("print_var_17 = y"+"\n") # snow density (kg/m3) (if snod is zero, sden is undef)
        PAR.write("print_var_18 = y"+"\n") # snow-water-equivalent depth (m)
        PAR.write("print_var_19 = n"+"\n") # summed snow precipitation during simulation year (m)
        PAR.write("print_var_20 = n"+"\n") # summed snow-water-equivalent melt during simulation year (m)
        PAR.write("print_var_21 = n"+"\n") # cloud fraction (0-1) (averaged over the write period)
        PAR.write("print_var_22 = n"+"\n")
        PAR.write("print_var_23 = n"+"\n")
        PAR.write("print_var_24 = n"+"\n")
        PAR.write("print_var_25 = n"+"\n")
        PAR.write("print_var_26 = y"+"\n") # Qcs canopy sublimation - manually defined in output_user
        PAR.write("print_var_27 = y"+"\n") # wbal_qsubl wind sublimation - manually defined in output_user
        PAR.write("print_var_28 = n"+"\n")
        PAR.write("print_var_29 = n"+"\n")
        PAR.write("print_var_30 = n"+"\n")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description = "Prepare .par")
    parser.add_argument("-inDEM", dest = "inDEM")
    parser.add_argument("-inVEG", dest = "inVEG")
    parser.add_argument("-inMET", dest = "inMET")
    parser.add_argument("-outPAR", dest = "outPAR")
    parser.add_argument("-outFolder", dest = "outFolder")
    args = parser.parse_args()
    
    my_sm_par_write(args.inDEM, args.inVEG, args.inMET, args.outPAR, args.outFolder)    

