
### Preparation of the Cordex data
###
### Esto lo hago en local para utilizar OTB...
### 


######################################
######### DEM Preparation ############
######################################
#conda activate parc
#chmod 777 /home/hola/project/nacional_parques/script/nivo-parc/toolbox/*

# Site array
declare -a site_arr=("AIGUE" "GUADA" "ORDES" "PICOS" "SIERR") # "TEIDE")
declare -a res_arr=("30m" "50m" "100m" "150m" "200m" "250m" "300m" "400m" "500m")
OTB="/home/cesardb/App/OTB-7.4.0-Linux64/bin"

source /home/cesardb/Documents/script/myDEMtoolbox.sh

DEMFolder=""
tmpFolder="/tmp/tmpMjoh18Aa93/"
mkdir -p $tmpFolder

for res in ${res_arr[@]}
do
parallel ${OTB}/otbcli_BandMath -il {} -out $tmpFolder/{/} -exp \"\(im1b1==-32767\) \&\& \(im1b1<0\) ? 0 : im1b1\" ::: $(ls /media/cesardb/Datos/nacional_parques/product/contour/COP30/${res}/*DEM.tif)
parallel ${OTB}/otbcli_BandMath -il {} -out $tmpFolder/{/} -exp \"\(im1b1==-32767\) ? 0 : im1b1\" ::: $(ls /media/cesardb/Datos/nacional_parques/product/contour/COP30/${res}/*DEM.tif)

parallel gdal_translate -of AAIGrid $tmpFolder/{/} {.}.asc ::: $(ls /media/cesardb/Datos/nacional_parques/product/contour/COP30/${res}/*DEM.tif)
parallel gdal_translate -ot UInt16 -of AAIGrid {} {.}.asc  ::: $(ls /media/cesardb/Datos/nacional_parques/product/contour/COP30/${res}/*DEM.tif)
parallel gdal_translate -ot UInt16 -of AAIGrid {} {.}.asc  ::: $(ls /media/cesardb/Datos/nacional_parques/product/contour/COP30/${res}/*ASP.tif)
parallel gdal_translate -ot UInt16 -of AAIGrid {} {.}.asc  ::: $(ls /media/cesardb/Datos/nacional_parques/product/contour/COP30/${res}/*SLP.tif)

done

# PREP MASK LOCAL
#
#in_DEM="/media/cesardb/Datos/nacional_parques/product/contour/COP30/250m/limite_AIGUE_COP30_250m_UTM_DEM_UTM31.tif"
#out_mask="/media/cesardb/Datos/nacional_parques/product/contour/COP30/250m/limite_AIGUE_COP30_250m_UTM_MASK_UTM31.tif"
#in_shp="/media/cesardb/Datos/nacional_parques/product/contour/shp/limite_AIGUE_UTM31N.shp"
#cp ${in_DEM} ${out_mask}
#gdal_rasterize -burn 1 ${in_shp} ${out_mask}
#gdal_rasterize -i -burn 0 ${in_shp} ${out_mask}
#gdal_translate -ot UInt16 -a_nodata 9999 -of AAIGrid  ${out_mask} "/media/cesardb/Datos/nacional_parques/product/contour/COP30/250m/limite_AIGUE_COP30_250m_UTM_MSK.asc"
#
#in_DEM="/media/cesardb/Datos/nacional_parques/product/contour/COP30/250m/limite_ORDES_COP30_250m_UTM_DEM_UTM31.tif"
#out_mask="/media/cesardb/Datos/nacional_parques/product/contour/COP30/250m/limite_ORDES_COP30_250m_UTM_MASK_UTM31.tif"
#in_shp="/media/cesardb/Datos/nacional_parques/product/contour/shp/limite_ORDES_UTM31N.shp"
#cp ${in_DEM} ${out_mask}
#gdal_rasterize -burn 1 ${in_shp} ${out_mask}
#gdal_rasterize -i -burn 0 ${in_shp} ${out_mask}
#gdal_translate -ot UInt16 -a_nodata 9999 -of AAIGrid  ${out_mask} "/media/cesardb/Datos/nacional_parques/product/contour/COP30/250m/limite_ORDES_COP30_250m_UTM_MSK.asc"
#
#in_DEM="/media/cesardb/Datos/nacional_parques/product/contour/COP30/250m/limite_PICOS_COP30_250m_UTM_DEM_UTM30.tif"
#out_mask="/media/cesardb/Datos/nacional_parques/product/contour/COP30/250m/limite_PICOS_COP30_250m_UTM_MASK_UTM30.tif"
#in_shp="/media/cesardb/Datos/nacional_parques/product/contour/shp/limite_PICOS_UTM30N.shp"
#cp ${in_DEM} ${out_mask}
#gdal_rasterize -burn 1 ${in_shp} ${out_mask}
#gdal_rasterize -i -burn 0 ${in_shp} ${out_mask}
#gdal_translate -ot UInt16 -a_nodata 9999 -of AAIGrid  ${out_mask} "/media/cesardb/Datos/nacional_parques/product/contour/COP30/250m/limite_PICOS_COP30_250m_UTM_MSK.asc"
#
#in_DEM="/media/cesardb/Datos/nacional_parques/product/contour/COP30/250m/limite_GUADA_COP30_250m_UTM_DEM_UTM30.tif"
#out_mask="/media/cesardb/Datos/nacional_parques/product/contour/COP30/250m/limite_GUADA_COP30_250m_UTM_MASK_UTM30.tif"
#in_shp="/media/cesardb/Datos/nacional_parques/product/contour/shp/limite_GUADA_UTM30N.shp"
#cp ${in_DEM} ${out_mask}
#gdal_rasterize -burn 1 ${in_shp} ${out_mask}
#gdal_rasterize -i -burn 0 ${in_shp} ${out_mask}
#gdal_translate -ot UInt16 -a_nodata 9999 -of AAIGrid  ${out_mask} "/media/cesardb/Datos/nacional_parques/product/contour/COP30/250m/limite_GUADA_COP30_250m_UTM_MSK.asc"
#
#in_DEM="/media/cesardb/Datos/nacional_parques/product/contour/COP30/250m/limite_SIERR_COP30_250m_UTM_DEM_UTM30.tif"
#out_mask="/media/cesardb/Datos/nacional_parques/product/contour/COP30/250m/limite_SIERR_COP30_250m_UTM_MASK_UTM30.tif"
#in_shp="/media/cesardb/Datos/nacional_parques/product/contour/shp/limite_SIERR_UTM30N.shp"
#cp ${in_DEM} ${out_mask}
#gdal_rasterize -burn 1 ${in_shp} ${out_mask}
#gdal_rasterize -i -burn 0 ${in_shp} ${out_mask}
#gdal_translate -ot UInt16 -a_nodata 9999 -of AAIGrid  ${out_mask} "/media/cesardb/Datos/nacional_parques/product/contour/COP30/250m/limite_SIERR_COP30_250m_UTM_MSK.asc"

# PREP MASK CALCULADOR

in_DEM="/home/hola/datos_seis/nacional_parques/data/COP30/250m/limite_AIGUE_COP30_250m_UTM_DEM.tif"
out_mask="/home/hola/datos_seis/nacional_parques/data/COP30/250m/limite_AIGUE_COP30_250m_UTM_MSK.tif"
in_shp="/home/hola/datos_seis/nacional_parques/data/contour/limite_AIGUE_UTM31N.shp"
cp ${in_DEM} ${out_mask}
gdal_rasterize -burn 1 ${in_shp} ${out_mask}
gdal_rasterize -i -burn 0 ${in_shp} ${out_mask}
gdal_translate -ot UInt16 -a_nodata 9999 -of AAIGrid  ${out_mask} "/home/hola/datos_seis/nacional_parques/data/COP30/250m/limite_AIGUE_COP30_250m_UTM_MSK.asc"

in_DEM="/home/hola/datos_seis/nacional_parques/data/COP30/250m/limite_ORDES_COP30_250m_UTM_DEM.tif"
out_mask="/home/hola/datos_seis/nacional_parques/data/COP30/250m/limite_ORDES_COP30_250m_UTM_MSK.tif"
in_shp="/home/hola/datos_seis/nacional_parques/data/contour/limite_ORDES_UTM31N.shp"
cp ${in_DEM} ${out_mask}
gdal_rasterize -burn 1 ${in_shp} ${out_mask}
gdal_rasterize -i -burn 0 ${in_shp} ${out_mask}
gdal_translate -ot UInt16 -a_nodata 9999 -of AAIGrid  ${out_mask} "/home/hola/datos_seis/nacional_parques/data/COP30/250m/limite_ORDES_COP30_250m_UTM_MSK.asc"

in_DEM="/home/hola/datos_seis/nacional_parques/data/COP30/250m/limite_PICOS_COP30_250m_UTM_DEM.tif"
out_mask="/home/hola/datos_seis/nacional_parques/data/COP30/250m/limite_PICOS_COP30_250m_UTM_MSK.tif"
in_shp="/home/hola/datos_seis/nacional_parques/data/contour/limite_PICOS_UTM30N.shp"
cp ${in_DEM} ${out_mask}
gdal_rasterize -burn 1 ${in_shp} ${out_mask}
gdal_rasterize -i -burn 0 ${in_shp} ${out_mask}
gdal_translate -ot UInt16 -a_nodata 9999 -of AAIGrid  ${out_mask} "/home/hola/datos_seis/nacional_parques/data/COP30/250m/limite_PICOS_COP30_250m_UTM_MSK.asc"

in_DEM="/home/hola/datos_seis/nacional_parques/data/COP30/250m/limite_GUADA_COP30_250m_UTM_DEM.tif"
out_mask="/home/hola/datos_seis/nacional_parques/data/COP30/250m/limite_GUADA_COP30_250m_UTM_MSK.tif"
in_shp="/home/hola/datos_seis/nacional_parques/data/contour/limite_GUADA_UTM30N.shp"
cp ${in_DEM} ${out_mask}
gdal_rasterize -burn 1 ${in_shp} ${out_mask}
gdal_rasterize -i -burn 0 ${in_shp} ${out_mask}
gdal_translate -ot UInt16 -a_nodata 9999 -of AAIGrid  ${out_mask} "/home/hola/datos_seis/nacional_parques/data/COP30/250m/limite_GUADA_COP30_250m_UTM_MSK.asc"

in_DEM="/home/hola/datos_seis/nacional_parques/data/COP30/250m/limite_SIERR_COP30_250m_UTM_DEM.tif"
out_mask="/home/hola/datos_seis/nacional_parques/data/COP30/250m/limite_SIERR_COP30_250m_UTM_MSK.tif"
in_shp="/home/hola/datos_seis/nacional_parques/data/contour/limite_SIERR_UTM30N.shp"
cp ${in_DEM} ${out_mask}
gdal_rasterize -burn 1 ${in_shp} ${out_mask}
gdal_rasterize -i -burn 0 ${in_shp} ${out_mask}
gdal_translate -ot UInt16 -a_nodata 9999 -of AAIGrid  ${out_mask} "/home/hola/datos_seis/nacional_parques/data/COP30/250m/limite_SIERR_COP30_250m_UTM_MSK.asc"

in_DEM="/home/hola/datos_seis/nacional_parques/data/COP30/250m/limite_TEIDE_COP30_250m_UTM_DEM.tif"
out_mask="/home/hola/datos_seis/nacional_parques/data/COP30/250m/limite_TEIDE_COP30_250m_UTM_MSK.tif"
in_shp="/home/hola/datos_seis/nacional_parques/data/contour/limite_TEIDE_UTM28N.shp"
cp ${in_DEM} ${out_mask}
gdal_rasterize -burn 1 ${in_shp} ${out_mask}
gdal_rasterize -i -burn 0 ${in_shp} ${out_mask}
gdal_translate -ot UInt16 -a_nodata 9999 -of AAIGrid  ${out_mask} "/home/hola/datos_seis/nacional_parques/data/COP30/250m/limite_TEIDE_COP30_250m_UTM_MSK.asc"



# PREP DEM FOR MODIS DATA

in_MODIS="/media/cesardb/Datos/nacional_parques/data/MODIS/map/AIGUE_2000_2010.tif"
in_DEM="/media/cesardb/Datos/nacional_parques/product/contour/COP30/30m/limite_AIGUE_COP30_30m_UTM_DEM_UTM31.tif"
mySuperImpose --DEMref ${in_MODIS} --DEMmob ${in_DEM}  --noData -9999 --out "/media/cesardb/Datos/nacional_parques/data/MODIS/map/AIGUE_MODIS_DEM.tif" --int cubic --outSRS EPSG:32631

in_MODIS="/media/cesardb/Datos/nacional_parques/data/MODIS/map/ORDES_2000_2010.tif"
in_DEM="/media/cesardb/Datos/nacional_parques/product/contour/COP30/30m/limite_ORDES_COP30_30m_UTM_DEM_UTM31.tif"
mySuperImpose --DEMref ${in_MODIS} --DEMmob ${in_DEM}  --noData -9999 --out "/media/cesardb/Datos/nacional_parques/data/MODIS/map/ORDES_MODIS_DEM.tif" --int cubic --outSRS EPSG:32631

in_MODIS="/media/cesardb/Datos/nacional_parques/data/MODIS/map/PICOS_2000_2010.tif"
in_DEM="/media/cesardb/Datos/nacional_parques/product/contour/COP30/30m/limite_PICOS_COP30_30m_UTM_DEM_UTM30.tif"
mySuperImpose --DEMref ${in_MODIS} --DEMmob ${in_DEM}  --noData -9999 --out "/media/cesardb/Datos/nacional_parques/data/MODIS/map/PICOS_MODIS_DEM.tif" --int cubic --outSRS EPSG:32630

in_MODIS="/media/cesardb/Datos/nacional_parques/data/MODIS/map/GUADA_2000_2010.tif"
in_DEM="/media/cesardb/Datos/nacional_parques/product/contour/COP30/30m/limite_GUADA_COP30_30m_UTM_DEM_UTM30.tif"
mySuperImpose --DEMref ${in_MODIS} --DEMmob ${in_DEM}  --noData -9999 --out "/media/cesardb/Datos/nacional_parques/data/MODIS/map/GUADA_MODIS_DEM.tif" --int cubic --outSRS EPSG:32630

in_MODIS="/media/cesardb/Datos/nacional_parques/data/MODIS/map/SIERR_2000_2010.tif"
in_DEM="/media/cesardb/Datos/nacional_parques/product/contour/COP30/30m/limite_SIERR_COP30_30m_UTM_DEM_UTM30.tif"
mySuperImpose --DEMref ${in_MODIS} --DEMmob ${in_DEM}  --noData -9999 --out "/media/cesardb/Datos/nacional_parques/data/MODIS/map/SIERR_MODIS_DEM.tif" --int cubic --outSRS EPSG:32630

in_MODIS="/media/cesardb/Datos/nacional_parques/data/MODIS/map/TEIDE_2000_2010.tif"
in_DEM="/media/cesardb/Datos/nacional_parques/product/contour/COP30/30m/limite_TEIDE_COP30_30m_UTM_DEM_UTM28.tif"
mySuperImpose --DEMref ${in_MODIS} --DEMmob ${in_DEM}  --noData -9999 --out "/media/cesardb/Datos/nacional_parques/data/MODIS/map/TEIDE_MODIS_DEM.tif" --int cubic --outSRS EPSG:32628

parallel gdal_translate -ot UInt16 -of AAIGrid {} {.}.asc  ::: $(ls /media/cesardb/Datos/nacional_parques/data/MODIS/map/*_MODIS_DEM.tif)


######################################
######### Land cover Preparation #####
######################################

source /home/cesardb/Documents/script/myDEMtoolbox.sh

# if using CORINE land cover map

dataFolder="/home/cesardb/Datos/nacional_parques/data/land_cover/CORINE"
outFolder="/home/cesardb/Datos/nacional_parques/product/land_cover/CORINE"

out_res="250m"
inCORINE=${dataFolder}"/DATA/U2012_CLC2006_V2020_20u1.tif"
inDEM=${outFolder}"/../../contour/COP30/"$out_res"/limite_ORDES_COP30_"${out_res}"_UTM_DEM_UTM31.tif"
mySuperImpose --DEMref ${inDEM} --DEMmob ${inCORINE} --noData -9999 --out ${outFolder}"/raw/U2012_CLC2006_V2020_20u1_"${out_res}"_ORDES.tif" --int near --outSRS EPSG:32631
# mySuperImposeOTB need because previous step actually did not set the origin identical to the ref...
mySuperImposeOTB --DEMref ${inDEM}  --DEMmob ${outFolder}"/raw/U2012_CLC2006_V2020_20u1_"${out_res}"_ORDES.tif"  --out ${outFolder}"/raw/U2012_CLC2006_V2020_20u1_"${out_res}"_ORDES.tif"  --int "nn" --OTB  $OTB --nd -9999 
$OTB/otbcli_BandMath -il ${inDEM} ${outFolder}"/raw/U2012_CLC2006_V2020_20u1_"${out_res}"_ORDES.tif" -out ${outFolder}"/raw/U2012_CLC2006_V2020_20u1_"${out_res}"_ORDES.tif" -exp " im1b1<=0 ? -9999:im2b1"

inDEM=${outFolder}"/../../contour/COP30/"$out_res"/limite_AIGUE_COP30_"${out_res}"_UTM_DEM_UTM31.tif"
mySuperImpose --DEMref ${inDEM} --DEMmob ${inCORINE} --noData -9999 --out ${outFolder}"/raw/U2012_CLC2006_V2020_20u1_"${out_res}"_AIGUE.tif" --int near --outSRS EPSG:32631
# mySuperImposeOTB need because previous step actually did not set the origin identical to the ref...
mySuperImposeOTB --DEMref ${inDEM}  --DEMmob ${outFolder}"/raw/U2012_CLC2006_V2020_20u1_"${out_res}"_AIGUE.tif"  --out ${outFolder}"/raw/U2012_CLC2006_V2020_20u1_"${out_res}"_AIGUE.tif"  --int "nn" --OTB  $OTB --nd -9999 
$OTB/otbcli_BandMath -il ${inDEM} ${outFolder}"/raw/U2012_CLC2006_V2020_20u1_"${out_res}"_AIGUE.tif" -out ${outFolder}"/raw/U2012_CLC2006_V2020_20u1_"${out_res}"_AIGUE.tif" -exp " im1b1<=0 ? -9999:im2b1"

inDEM=${outFolder}"/../../contour/COP30/"$out_res"/limite_GUADA_COP30_"${out_res}"_UTM_DEM_UTM30.tif"
mySuperImpose --DEMref ${inDEM} --DEMmob ${inCORINE} --noData -9999 --out ${outFolder}"/raw/U2012_CLC2006_V2020_20u1_"${out_res}"_GUADA.tif" --int near --outSRS EPSG:32630
# mySuperImposeOTB need because previous step actually did not set the origin identical to the ref...
mySuperImposeOTB --DEMref ${inDEM}  --DEMmob ${outFolder}"/raw/U2012_CLC2006_V2020_20u1_"${out_res}"_GUADA.tif"  --out ${outFolder}"/raw/U2012_CLC2006_V2020_20u1_"${out_res}"_GUADA.tif"  --int "nn" --OTB  $OTB --nd -9999 
$OTB/otbcli_BandMath -il ${inDEM} ${outFolder}"/raw/U2012_CLC2006_V2020_20u1_"${out_res}"_GUADA.tif" -out ${outFolder}"/raw/U2012_CLC2006_V2020_20u1_"${out_res}"_GUADA.tif" -exp " im1b1<=0 ? -9999:im2b1"

inDEM=${outFolder}"/../../contour/COP30/"$out_res"/limite_PICOS_COP30_"${out_res}"_UTM_DEM_UTM30.tif"
mySuperImpose --DEMref ${inDEM} --DEMmob ${inCORINE} --noData -9999 --out ${outFolder}"/raw/U2012_CLC2006_V2020_20u1_"${out_res}"_PICOS.tif" --int near --outSRS EPSG:32630
# mySuperImposeOTB need because previous step actually did not set the origin identical to the ref...
mySuperImposeOTB --DEMref ${inDEM}  --DEMmob ${outFolder}"/raw/U2012_CLC2006_V2020_20u1_"${out_res}"_PICOS.tif"  --out ${outFolder}"/raw/U2012_CLC2006_V2020_20u1_"${out_res}"_PICOS.tif"  --int "nn" --OTB  $OTB --nd -9999 
$OTB/otbcli_BandMath -il ${inDEM} ${outFolder}"/raw/U2012_CLC2006_V2020_20u1_"${out_res}"_PICOS.tif" -out ${outFolder}"/raw/U2012_CLC2006_V2020_20u1_"${out_res}"_PICOS.tif" -exp " im1b1<=0 ? -9999:im2b1"

inDEM=${outFolder}"/../../contour/COP30/"$out_res"/limite_SIERR_COP30_"${out_res}"_UTM_DEM_UTM30.tif"
mySuperImpose --DEMref ${inDEM} --DEMmob ${inCORINE} --noData -9999 --out ${outFolder}"/raw/U2012_CLC2006_V2020_20u1_"${out_res}"_SIERR.tif" --int near --outSRS EPSG:32630
# mySuperImposeOTB need because previous step actually did not set the origin identical to the ref...
mySuperImposeOTB --DEMref ${inDEM}  --DEMmob ${outFolder}"/raw/U2012_CLC2006_V2020_20u1_"${out_res}"_SIERR.tif"  --out ${outFolder}"/raw/U2012_CLC2006_V2020_20u1_"${out_res}"_SIERR.tif"  --int "nn" --OTB  $OTB --nd -9999 
$OTB/otbcli_BandMath -il ${inDEM} ${outFolder}"/raw/U2012_CLC2006_V2020_20u1_"${out_res}"_SIERR.tif" -out ${outFolder}"/raw/U2012_CLC2006_V2020_20u1_"${out_res}"_SIERR.tif" -exp " im1b1<=0 ? -9999:im2b1"

## special for TEIDE, set everything to bare rock (=31 CORINE, =18 SM)
#inDEM=${outFolder}"/../../contour/COP30/"$out_res"/limite_TEIDE_COP30_"${out_res}"_UTM_DEM.tif"
#gdal_calc.py -A $inDEM   --outfile=${outFolder}"/raw/U2012_CLC2006_V2020_20u1_"${out_res}"_TEIDE.tif" --calc="31"

inDEM=${outFolder}"/../../contour/COP30/"$out_res"/limite_TEIDE_COP30_"${out_res}"_UTM_DEM_UTM28.tif"
mySuperImpose --DEMref ${inDEM} --DEMmob ${inCORINE} --noData -9999 --out ${outFolder}"/raw/U2012_CLC2006_V2020_20u1_"${out_res}"_TEIDE.tif" --int near --outSRS EPSG:32628
# mySuperImposeOTB need because previous step actually did not set the origin identical to the ref...
mySuperImposeOTB --DEMref ${inDEM}  --DEMmob ${outFolder}"/raw/U2012_CLC2006_V2020_20u1_"${out_res}"_TEIDE.tif"  --out ${outFolder}"/raw/U2012_CLC2006_V2020_20u1_"${out_res}"_TEIDE.tif"  --int "nn" --OTB  $OTB --nd -9999 
$OTB/otbcli_BandMath -il ${inDEM} ${outFolder}"/raw/U2012_CLC2006_V2020_20u1_"${out_res}"_TEIDE.tif" -out ${outFolder}"/raw/U2012_CLC2006_V2020_20u1_"${out_res}"_TEIDE.tif" -exp " im1b1<=0 ? -9999:im2b1"

# http://localhost:8888/notebooks/Datos/nacional_parques/script/Prepare_Land_Cover_Map.ipynb

CordexToolbox="/home/cesardb/Datos/nacional_parques/script/nivo_parc/toolbox"
for inCLC in $(ls ${outFolder}"/raw/"*".tif")
do
bn=$(basename -s .tif ${inCLC})
${CordexToolbox}/convert_land_cover_map_CORINE2SnowModel.py -inLCM ${inCLC}  -outLCM ${outFolder}"/sm_label/"${bn}"_sm_label.tif" -CORINE2SM "/home/cesardb/Datos/nacional_parques/data/land_cover/CORINE/CORINE_legende.csv"
done


for inCLC in $(ls ${outFolder}"/sm_label/"*)
do
echo $inCLC
bn=$(basename -s .tif $inCLC)
gdal_translate -of AAIGrid ${inCLC} ${outFolder}"/asc2/"${bn}".asc"
done


# sm reads .asc vege and topo (!no data!)
# point at the right files in snowmodel.par
# also set ascii_topoveg
# veg_ascii_fname topo_ascii_fname

# snowmodel.par
# met_input_fname = met/stations/mk_micromet_input_file/test.dat => file generated in met/stations/mk_micromet_input_file/
# change fnamein and nfiles mk_micromet_input_file.f 
# carefull with the time steps also (in snowmodel.par)

# if using SIOSE land cover map
dataFolder="/home/cesardb/Datos/nacional_parques/data/land_cover/SIOSE"
outFolder="/home/cesardb/Datos/nacional_parques/product/land_cover/SIOSE"

ogr2ogr ${outFolder}/SIOSE_Aragon_2005.shp ${dataFolder}/SIOSE_Aragon_2005.gpkg 
cp ${outFolder}/../../contour/COP30/30m/limite_ORDES_COP30_30m_UTM_DEM.tif ${outFolder}/SIOSE_Aragon_2005.tif
gdal_calc.py --NoDataValue -9999 -A ${outFolder}/SIOSE_Aragon_2005.tif --outfile ${outFolder}/SIOSE_Aragon_2005.tif --calc="-9999"
gdal_rasterize -l T_POLIGONOS -a CODIIGE ${dataFolder}/SIOSE_Aragon_2005.gpkg ${outFolder}/SIOSE_Aragon_2005.tif 
gdal_translate -a_nodata -9999 ${outFolder}/SIOSE_Aragon_2005.tif  ${outFolder}/SIOSE_Aragon_2005_nd.tif 
python convert to SM land cover classes


