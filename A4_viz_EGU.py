

import pickle
import blosc
from nivo_parc.toolbox import post_tools
import itertools
from multiprocessing import Pool
import pandas as pd
import datetime as dt

import matplotlib as mpl
mpl.rcParams.update({'text.usetex': False,"svg.fonttype": 'none'})
import matplotlib.pyplot as plt

# fixed parameters
cordex_domain = "EUR-11"  # CORDEX domain [STR] eg "EUR-44"
project = 'CORDEX' # eg CMIP6
time_frequency = "3hr" #'day'  # eg 3hr
sm_par_dir = "/home/hola/project/nacional_parques/sm_Tlr_53/par"
sm_fig_out_dir = "/home/hola/datos_seis/nacional_parques/fig/sm/sm_Tlr_53"
run_dict = post_tools.read_run_list("/home/hola/project/nacional_parques/script/nivo_parc/toolbox/exp_list_EXP.txt")


plt_dict = {
    "historical":{"col":"#4a4e4d", "mrk":"v"},
    "rcp26":{"col":"#f6cd61", "mrk":"^"},
    "rcp85":{"col":"#fe8a71", "mrk":">"},
    "AIGUE":{"col":"#ee4035","mrk":"H"},
    "GUADA":{"col":"#f37736","mrk":"X"},
    "ORDES":{"col":"#ffe800","mrk":"o"},
    "PICOS":{"col":"#7bc043","mrk":"d"},
    "SIERR":{"col":"#0392cf","mrk":"s"},
    "DJF":{"col":"#0057e7","mrk":"*"},
    "MAM":{"col":"#008744","mrk":"s"},
    "JJA":{"col":"#d62d20","mrk":"."},
    "SON":{"col":"#ffa700","mrk":"^"},
    }

# rcp26
#run_list = 
# rcp85


                   
run_dict = {
    "historical": ["CNRM-CERFACS-CNRM-CM5__CNRM-ALADIN63",
                   "MPI-M-MPI-ESM-LR__CNRM-ALADIN63",  
                   "MOHC-HadGEM2-ES__CNRM-ALADIN63", 
                   "NCC-NorESM1-M__CNRM-ALADIN63", 
                   "MOHC-HadGEM2-ES__ICTP-RegCM4-6", 
                   "MPI-M-MPI-ESM-LR__ICTP-RegCM4-6", 
                   "CNRM-CERFACS-CNRM-CM5__ICTP-RegCM4-6", 
                   "ICHEC-EC-EARTH__ICTP-RegCM4-6", 
                   "NCC-NorESM1-M__ICTP-RegCM4-6"],
    "rcp26":      ["NCC-NorESM1-M__ICTP-RegCM4-6"],
                #   "MOHC-HadGEM2-ES__ICTP-RegCM4-6",
                 #  "MPI-M-MPI-ESM-LR__ICTP-RegCM4-6"],
    "rcp85":      ["NCC-NorESM1-M__CNRM-ALADIN63",
                   "MPI-M-MPI-ESM-LR__CNRM-ALADIN63",
                   "MOHC-HadGEM2-ES__CNRM-ALADIN63",
                   "NCC-NorESM1-M__ICTP-RegCM4-6",
                   "MPI-M-MPI-ESM-LR__ICTP-RegCM4-6",
                   "CNRM-CERFACS-CNRM-CM5__ICTP-RegCM4-6",
                   "MOHC-HadGEM2-ES__ICTP-RegCM4-6",
                   "ICHEC-EC-EARTH__ICTP-RegCM4-6"]
    }

out_dir_dict = {"historical":"/home/hola/datos_seis/nacional_parques/prod",
                "rcp26":"/home/hola/datos_seis/nacional_parques/prod",
                "rcp85":"/home/hola/datos_seis/nacional_parques/prod"}

reg_list = ["AIGUE", "GUADA", "ORDES", "PICOS", "SIERR"] 
var_list = ["prec", "rpre", "tair", "roff", "snod", "swed"] #["prec", "relh", "rpre", "smlt", "ssub", "tair", "qlin", "roff", "sden", "snod", "swed", "wdir"]



res=int(250)      
pixel_size = res*res/1e6

def get_simu_single_exp(exp, in_run_list, in_reg_list, in_var_list):
    arg_list = [{"exp":xarg_list[0],
                 "run":xarg_list[1],
                 "reg":xarg_list[2],
                 "var":xarg_list[3],
                 "res":res,
                 "sm_out_dir":sm_out_dir,
                 "sm_par_dir":sm_par_dir} 
                 for xarg_list in list(itertools.product([exp], in_run_list, in_reg_list, in_var_list))]
    
    
    # load sm output and save in pkl file
    with Pool(40) as p:
        f_out = p.map(post_tools.f_load_dset_from_nc, arg_list)
        
    data_all = {}    
    for run in in_run_list:
        data_all[run] = {}
        data_all[run][exp] = {}
        for reg in in_reg_list:  
            data_all[run][exp][reg] = {}
                
    for i,args in enumerate(arg_list):
        data_all[args["run"]][args["exp"]][args["reg"]][args["var"]]= f_out[i]
    
    return data_all

def merge_data_runs(data_all, data):
    for kk in data_all.keys():
        if kk in data.keys():
            for kkk in data[kk].keys():
                data_all[kk][kkk] = data[kk][kkk]
            
    for kk in data.keys():
        if kk not in data_all.keys():
            data_all[kk] = data[kk]
            
    return data_all 
    

data_all_run =  {}
for exp in ["historical"]: #, "rcp26", "rcp85"]:
    run_list = run_dict[exp]
    out_dir = out_dir_dict[exp]
    sm_out_dir = out_dir+"/sm/sm_Tlr_53_qm"
    print(exp)
    print(run_list)
    print(out_dir)
    data = get_simu_single_exp(exp, run_list, reg_list, var_list)
    data_all_run = merge_data_runs(data_all_run, data)


########################## 
## PLOT SWE = f(time)  ##
########################## 
for reg in reg_list:
    for run in data_all_run.keys():
        print(run+"-"+reg)
        for exp in data_all_run[run].keys():
            tt = data_all_run[run][exp]["AIGUE"]["swed"].mean(dim=("lat","lon")).groupby("hyear").mean()
            plt.plot(tt.hyear, tt.swed, label=run, color=plt_dict[exp]["col"], lw=0.2)
    plt.ylabel("Mean annual temperature (º)")
    plt.grid(True, lw=0.2)
    plt.savefig("/home/hola/datos_seis/port_transfert/swed_t_"+reg+"_"+name_fig_ext+".png")
    plt.savefig("/home/hola/datos_seis/port_transfert/swed_t_"+reg+"_"+name_fig_ext+".svg")
    plt.close()




########################## 
## PLOT TAIR = f(time)  ##
########################## 
for reg in reg_list:
    for run in data_all_run.keys():
        for exp in data_all_run[run].keys():
            tt = data_all_run[run][exp]["AIGUE"]["tair"].mean(dim=("lat","lon")).groupby("hyear").mean()
            plt.plot(tt.hyear, tt.tair, label=run, color=plt_dict[exp]["col"])
            
            
            data_all_run[run][exp]["AIGUE"]["tair"].sel(time=slice("1975-01-01","2005-01-01")).mean()
            
    plt.ylabel("Mean annual temperature (º)")
    plt.grid(True, lw=0.2)
    plt.savefig("/home/hola/datos_seis/port_transfert/tair_t_"+reg+"_"+name_fig_ext+".png")
    plt.savefig("/home/hola/datos_seis/port_transfert/tair_t_"+reg+"_"+name_fig_ext+".svg")
    plt.close()


########################## 
## PLOT DSWE = f(DTAIR) ##
########################## 

data2plt = {}
for reg in reg_list:
    data2plt[reg] = {}
    for run in data_all_run.keys():
        data2plt[reg][run] = {}
        print(reg+" - "+run)
        if len(data_all_run[run].keys())>1:
            kk_list = list(data_all_run[run].keys())
            kk_list .remove("historical")
            
            T_ref = data_all_run[run]["historical"][reg]["tair"].sel(time=slice("1975-01-01","2005-01-01")).mean().tair.values
            SWE_ref = data_all_run[run]["historical"][reg]["swed"].sel(time=slice("1975-01-01","2005-01-01")).mean().swed.values

            
            for exp in kk_list:
                dT_list = []
                dSWE_list = []
        
                T = data_all_run[run][exp][reg]["tair"].sel(time=slice("2006-01-01","2036-01-01")).mean().tair.values
                SWE = data_all_run[run][exp][reg]["swed"].sel(time=slice("2006-01-01","2036-01-01")).mean().swed.values
            
                dT_list.append(T-T_ref)
                dSWE_list.append(SWE-SWE_ref)
                
                T = data_all_run[run][exp][reg]["tair"].sel(time=slice("2037-01-01","2066-01-01")).mean().tair.values
                SWE = data_all_run[run][exp][reg]["swed"].sel(time=slice("2037-01-01","2066-01-01")).mean().swed.values
                
                dT_list.append(T-T_ref)
                dSWE_list.append(SWE-SWE_ref)
                            
                T = data_all_run[run][exp][reg]["tair"].sel(time=slice("2067-01-01","2096-01-01")).mean().tair.values
                SWE = data_all_run[run][exp][reg]["swed"].sel(time=slice("2067-01-01","2096-01-01")).mean().swed.values
                
                dT_list.append(T-T_ref)
                dSWE_list.append(SWE-SWE_ref)
                
                data2plt[reg][run][exp] = {"dT":dT_list, "dSWE":dSWE_list}
                
                

plt.figure()
for reg in reg_list:
    for exp in data2plt[reg].keys():
        plt.plot(data2plt[reg][exp]["dT"], data2plt[reg][exp]["dSWE"], marker=".", color=plt_dict[reg]["col"], ms=12, mew=0, lw=0.8)
        
plt.ylabel("dSWE (m)")
plt.xlabel("dT (ºC)")
plt.axis((0,4,-0.1,0))
plt.grid(True, lw=0.5)

plt.savefig("/home/hola/datos_seis/port_transfert/dtair_dswed_t_"+run+"_"+name_fig_ext+".png")
plt.savefig("/home/hola/datos_seis/port_transfert/dtair_dswed_t_"+run+"_"+name_fig_ext+".svg")


plt.ylabel("dSWE (m)")
plt.xlabel("dT (ºC)")
plt.axis((0,6.5,-0.45,0))
plt.grid(True, lw=0.5)
plt.savefig("/home/hola/datos_seis/port_transfert/dtair_dswed_t_"+name_fig_ext+".png")
plt.savefig("/home/hola/datos_seis/port_transfert/dtair_dswed_t_"+name_fig_ext+".svg")

plt.axis((0,6.5,-0.20,0))
plt.grid(True, lw=0.5)
plt.savefig("/home/hola/datos_seis/port_transfert/dtair_dswed_t_zoom_"+name_fig_ext+".png")
plt.savefig("/home/hola/datos_seis/port_transfert/dtair_dswed_t_zoom_"+name_fig_ext+".svg")

########################## 
## PLOT SWE = f(period)  ##
########################## 


for reg in reg_list:
    SWE_dict={"historical":[],"rcp26":[],"rcp85":[]}
    dT_list = []
    for run in data_all_run.keys():
        print(reg+" - "+run)
        if len(data_all_run[run].keys())>1:
            kk_list = list(data_all_run[run].keys())
            kk_list .remove("historical")
            
            #T_ref = data_all_run[run]["historical"][reg]["tair"].sel(time=slice("1975-01-01","2005-01-01")) #.mean().tair.values
            SWE_ref = data_all_run[run]["historical"][reg]["swed"].sel(time=slice("1975-01-01","2005-01-01"))#.mean().swed.values
            T_ref   = data_all_run[run]["historical"][reg]["tair"].sel(time=slice("1975-01-01","2005-01-01")).mean().tair.values
            
            SWE_dict["historical"].append(SWE_ref.mean(dim="time").swed.values)
            
            for exp in ["rcp85"]:
                SWE = data_all_run[run][exp][reg]["swed"].sel(time=slice("2060-01-01","2089-01-01"))#.mean().swed.values
                SWE_dict[exp].append(SWE.mean(dim="time").swed.values)
                T = data_all_run[run][exp][reg]["tair"].sel(time=slice("2060-01-01","2089-01-01")).mean().tair.values
                dT_list.append(T-T_ref)

    SWE_ref = np.stack(SWE_dict["historical"]).mean(axis=0)
    SWE_fut = np.stack(SWE_dict["rcp85"]).mean(axis=0)
    dSWE = SWE_fut-SWE_ref
    
    plt.figure()
    plt.suptitle(reg+" "+str(np.mean(dT_list).round(2))+" 1975-2005 2060-2089")
    plt.subplot(311)
    plt.imshow(SWE_ref, cmap="Blues_r", vmin=0, vmax=2, origin="lower")
    plt.xticks([])
    plt.yticks([])

    plt.subplot(312)
    plt.imshow(SWE_fut, cmap="Blues_r", vmin=0, vmax=2, origin="lower")
    plt.xticks([])
    plt.yticks([])  
    
    plt.subplot(313)
    plt.imshow(dSWE, cmap="RdYlBu", vmin=-1, vmax=1, origin="lower")
    plt.xticks([])
    plt.yticks([])

    plt.savefig("/home/hola/datos_seis/port_transfert/dswed_map_"+reg+"_"+name_fig_ext+".png")
    plt.savefig("/home/hola/datos_seis/port_transfert/dswed_map_"+reg+"_"+name_fig_ext+".svg")

# for the legend
plt.figure()
plt.subplot(211)
plt.imshow(SWE_fut, cmap="Blues_r", vmin=0, vmax=2, origin="lower")
plt.colorbar() 

plt.subplot(212)
plt.imshow(dSWE, cmap="RdYlBu", vmin=-1, vmax=1, origin="lower")
plt.colorbar()

plt.savefig("/home/hola/datos_seis/port_transfert/dswed_map_legend.svg")


########################## 
## PLOT SCA = f(time)  ##
########################## 


for reg in reg_list:
    print(reg)
    plt.figure(figsize=(8,3))
    for run in data_all_run.keys():
        for exp in data_all_run[run].keys():
            snod = data_all_run[run][exp][reg]["snod"]
            if len(snod)>0:
                sca = (snod>0.2).astype(int)
                tt = sca.sum(dim=("lat","lon")).groupby("hyear").mean()*250*250/1e6
            
                plt.plot(tt.hyear, tt.snod, lw=0.2, color=plt_dict[exp]["col"],marker=".", ms=6, mew=0)
    
    in_SCA_MODIS="/home/hola/datos_seis/nacional_parques/data/MODIS/Snow_Cover_Area_MODIS_"+reg+".csv"
    SCA = pd.read_csv(in_SCA_MODIS,names=["time","sca"],header=0)
    time_dt = [dt.datetime.strptime(dd, "%Y_%m_%d") for dd in SCA.time]
    SCA["time"]=time_dt
    SCA["hyear"] = [yy if mm<12 else yy+1 for yy,mm in zip(SCA.time.dt.year, SCA.time.dt.month) ]
    tt = SCA.groupby("hyear").mean()
    plt.plot(tt.index, tt.sca, lw=2, color="r")
    plt.ylim((0,plt.gca().get_ylim()[1]))
    plt.ylabel("Yearly mean SCA")
    plt.savefig("/home/hola/datos_seis/port_transfert/sca_"+reg+"_"+name_fig_ext+".png")
    plt.savefig("/home/hola/datos_seis/port_transfert/sca_"+reg+"_"+name_fig_ext+".svg")
    plt.close()




########################## 
## PLOT SNOD = f(dohy)  ##
########################## 

for reg in reg_list:
    for run in data_all_run.keys():
        plt.figure()
        print(reg+" - "+run)
        if len(data_all_run[run].keys())>1:
            # historical
            ddd = data_all_run[run]["historical"][reg]["snod"].sel(time=slice("1975-01-01","2005-01-01")).chunk(dict(time=-1))
            dd = ddd.mean(dim=("lat","lon")).groupby(ddd.time.dohy)
            q1 = dd.quantile(0.25).snod.values # [np.quantile(xx[1]["snod"].values,0.25) for xx in dd]
            q3 = dd.quantile(0.75).snod.values # [np.quantile(xx[1]["snod"].values,0.75) for xx in dd]  
            med = dd.quantile(0.50).snod.values #[np.median(xx[1]["snod"].values) for xx in dd]
            plt.fill_between(dd.groups.keys(), q1, q3, color=[0.6,0.6,0.9], alpha=0.5,label="First-third quartile")
            plt.plot(dd.groups.keys(), med, lw=0.5, label="median")
            
            # historical
            ddd = data_all_run[run]["rcp85"][reg]["snod"].sel(time=slice("2060-01-01","2089-01-01")).chunk(dict(time=-1))
            dd = ddd.mean(dim=("lat","lon")).groupby(ddd.time.dohy)
            q1 = dd.quantile(0.25).snod.values # [np.quantile(xx[1]["snod"].values,0.25) for xx in dd]
            q3 = dd.quantile(0.75).snod.values # [np.quantile(xx[1]["snod"].values,0.75) for xx in dd]  
            med = dd.quantile(0.50).snod.values #[np.median(xx[1]["snod"].values) for xx in dd]
            plt.fill_between(dd.groups.keys(), q1, q3, color=[0.9,0.3,0.3], alpha=0.5,label="First-third quartile")
            plt.plot(dd.groups.keys(), med, lw=0.5, label="median")
            
            plt.ylabel("Mean snow depth (m)")
            plt.xlabel("Day of hydrological year (1st sept.=0)")
            plt.axis((0,365,0,plt.gca().get_ylim()[1]))
            
        
        plt.savefig("/home/hola/datos_seis/port_transfert/snod_"+run+"_"+reg+"_spatialmean_tsdohy_"+name_fig_ext+".png")
        plt.savefig("/home/hola/datos_seis/port_transfert/snod_"+run+"_"+reg+"_spatialmean_tsdohy_"+name_fig_ext+".svg")
        plt.close()
            