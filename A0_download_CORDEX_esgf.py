from pyesgf.logon import LogonManager
import os.path
from pyesgf.search import SearchConnection
from multiprocessing import Pool
import time
import pickle
import numpy as np    
import netCDF4 # very important : avoids some "curl error details: "
import sys
import glob
import random

# edit of sys.path required due to a weird bug/feature of python3.6 https://github.com/conda/conda/issues/6018
sys.path.append('/home/hola/project/nacional_parques/script')
from nivo_parc.toolbox import download_tools

             
# fixed parameters
cordex_domain = "AFR-22" #"EUR-11"  # CORDEX domain [STR] eg "EUR-44" "AFR-22"
openid = "https://esg-dn1.nsc.liu.se/esgf-idp/openid/cesardb"
project = 'CORDEX' # eg CMIP6
time_frequency = "3hr" #'day'  # eg 3hr
out_dir0 = "/home/hola/datos_seis/nacional_parques/data/cordex/"+cordex_domain.replace("-","")

if not os.path.exists(out_dir0):
    os.makedirs(out_dir0)

# variables
var_list = [ "hurs", "tas", "uas", "vas", "pr"] #, "rsds", "rlds"]
#var_list = [ "huss", "tas", "uas", "vas", "pr", "ps"]
exp_list = ['rcp85','historical'] # 
exp_list = ['rcp26', 'rcp85', 'historical']

#region_bb = {"AIGUE":{"rot":[-12.7550, -12.4250, -7, -6.40], "unrot":[0.60, 1.26, 42.37, 42.77]}, 
#"GUADA":{"rot":[-16.8250, -16.27499, -7.64499, -7.20499], "unrot":[-4.32, -3.54, 40.60, 41.18]}, 
#"ORDES":{"rot":[-13.41499, -13.08500, -6.76499, -6.32499], "unrot":[-0.30, 0.33, 42.37, 42.84]}, 
#"PICOS":{"rot":[-16.82500, -16.27499, -5.2249, -4.78499], "unrot":[-5.3, -4.42, 42.91, 43.47]}, 
#"SIERR":{"rot":[-17.48500, -16.60499, -11.49499, -10.94499], "unrot":[-3.73, -2.47, 36.80, 37.35]}}

region_bb = {"AIGUE":{"rot":[-12.864, -12.07, -6.875, -6.30], "unrot":[0.50, 1.49, 42.28, 43.05]}, 
             "GUADA":{"rot":[-16.9, -16.16, -7.76, -6.98], "unrot":[-4.50, -3.40, 40.46, 41.31]}, 
             "ORDES":{"rot":[-13.53, -12.67, -6.88, -6.0], "unrot":[-0.40, 0.52, 42.25, 43.0]},
             "PICOS":{"rot":[-16.94, -16.16, -5.34, -4.56], "unrot":[-5.43, -4.26, 42.78, 43.68]}, 
             "SIERR":{"rot":[-17.60, -11.38, -11.72, -10.83], "unrot":[-3.98, -2.31, 36.59, 37.54]}}


region_bb = {"TEIDE":{"unrot":[-17.50, -15.90, 27.7, 28.7]}}

# logon manager
lm = LogonManager()
if not lm.is_logged_on():
        lm.logon(hostname = 'esgf-data.dkrz.de', interactive = True, bootstrap = True)
        #lm.logon(hostname = 'esg-dn1.nsc.liu.se', interactive = True, bootstrap = True)

lm.is_logged_on()

####################################################
# Explore the ESGF database to extract the list 
# of models run with wanted variables and periods
conn = SearchConnection('https://esgf-data.dkrz.de/esg-search', distrib = True)
esgf_data_avail = {}
for exp in exp_list:
    print("experiment: " + exp)
    for var in var_list:
        print("Variable: " + var)
        ctx = conn.new_context(
            project = 'CORDEX', 
            domain = cordex_domain, 
            experiment = exp, 
            time_frequency = time_frequency, 
            variable = var, 
#            institute = "GERICS",
#            driving_model = "ICHEC-EC-EARTH",
            facets = "project, domain, experiment, time_frequency, variable"
            ) #"*" "model"

        print("Found hits: " + str(ctx.hit_count))

        pool = Pool(100)
        pool_out = pool.starmap(download_tools.read_ctx_hit, [(hit, ctx) for hit in range(0, ctx.hit_count)])
        esgf_data_avail = download_tools.merge_pool_output_read_ctx(pool_out, esgf_data_avail)
        pool.close()
    
        
# Only keeps runs for which all variables are available            
esgf_data_avail_filtered = download_tools.clean_esgf_data(esgf_data_avail, var_list, exp_list)   

# Prints the number of valid () gcm-rcm         
run_count = 0
for gcm in esgf_data_avail_filtered.keys():
    for rcm in esgf_data_avail_filtered[gcm]:
        print(gcm+" - "+rcm)
        run_count+= 1
        if len(esgf_data_avail_filtered[gcm][rcm].keys())>1:
            print("!! There are several versions of this combination of gcm-rcm!!")
print(str(run_count)+" combination of gcm-rcm found.")


# save search results in a pickle
outName = "request_esgf_py"
for exp in exp_list:
    outName+= "_"+exp
    
# loading precedent search
with open(out_dir0+"/"+outName+".pkl", "rb") as handle:
    esgf_data_avail_filtered = pickle.load(handle)    
    
# save search results in a pickle
with open(out_dir0+"/"+outName+".pkl", "wb") as handle:
    pickle.dump( esgf_data_avail_filtered, handle, protocol = pickle.HIGHEST_PROTOCOL)

####################################################
# Downloads the model runs of the list   

# get a flat list of all the url to download
url_list = download_tools.get_url_list(esgf_data_avail_filtered, var_list, exp_list)


# write all url in a txt file
outName = "request_esgf_py"
for exp in exp_list:
    outName+= "_"+exp
    
with open (out_dir0+"/"+outName+".txt", 'w') as in_files:
    for url in url_list: in_files.write(url+'\n')
    

# check the status (working or not) of the esgf download nodes   
#node_status = download_tools.check_status_node_esgf()
node_status = check_status_node_esgf()

# in parallel
pool = Pool(5)
mp_out = pool.starmap(download_tools.extract_esgf_data_ROI_netcdf, [(url, region_bb, out_dir0, node_status) for url in url_list ])
pool.close()



# without parallel
for ii, url in enumerate(url_list):
    if np.mod(ii, 100) == 0:
        node_status = download_tools.check_status_node_esgf()
    print("\n"+url+"\n")
    try:
        download_tools.extract_esgf_data_ROI_netcdf(url, region_bb, out_dir0, node_status) 
    except IOError:
        print("Error")
 

	
# only for one gcm-rcm

short_url_list = []
for url in url_list:
    if len(url.split("CNRM-CERFACS-CNRM-CM5/rcp26/r1i1p1/CNRM-ALADIN63/v2/3hr/uas"))>1:
        #CNRM-CERFACS-CNRM-CM5/rcp26/r1i1p1/CNRM-ALADIN63/v2/3hr/pr/v20190702/pr_EUR-11_CNRM-CERFACS-CNRM-CM5_rcp26_r1i1p1_CNRM-ALADIN63_v2_3hr_208201010130-208212312230.nc

        print(url)
        short_url_list.append(url)
    


node_status = download_tools.check_status_node_esgf()

# in parallel
pool = Pool(20)
mp_out = pool.starmap(download_tools.extract_esgf_data_ROI_netcdf, [(url, region_bb, out_dir0, node_status) for url in short_url_list ])
pool.close()


for url in short_url_list:
    print("\n"+url+"\n")
    try:
        extract_esgf_data_ROI_netcdf(url, region_bb, out_dir0, node_status) 
    except IOError:
        print("Error")
        
short_url_list = []
for url in url_list:
    if len(url.split("CNRM/MPI-M-MPI-ESM-LR/historical/r1i1p1/CNRM-ALADIN63/v1/3hr"))>1:
        short_url_list.append(url)

for url in short_url_list:
    print("\n"+url+"\n")
    try:
        extract_esgf_data_ROI_netcdf(url, region_bb, out_dir0, node_status) 
    except IOError:
        print("Error")
        

# find small file, most likely corrupted

for exp in ["NCC-NorESM1-M__CNRM-ALADIN63", "CNRM-CERFACS-CNRM-CM5__CNRM-ALADIN63", "MOHC-HadGEM2-ES__CNRM-ALADIN63", "MPI-M-MPI-ESM-LR__CNRM-ALADIN63"]:
    folder = out_dir0+"/"+exp+"/"

for folder in glob.glob(out_dir0+"/*/"):
    gcm = folder.split("/")[-2].split("__")[0]
    rcm = folder.split("/")[-2].split("__")[1]
    for exp in exp_list:
        for var in var_list:
            for region in region_bb.keys():
                out_dir1 = folder+"/"+exp+"/"+region+"/"+var
                #print(out_dir1)
                file_list = glob.glob(out_dir1+"/*")
                for filename in file_list:
                    get_size = os.path.getsize(filename)
                    if get_size < 4000: # sometimes 500 works but there are several at ~3000
                        print(filename)
                        os.remove(filename)
# find missing files

file_list_missing = []
y0 = 2005 # 1951 # will not detect if first years are missing (1949-1950)
y1 = 2100 # 2005 # idem with 2006


# for all subfolders                      
for folder in glob.glob(out_dir0+"/*/"):
    gcm = folder.split("/")[-2].split("__")[0]
    rcm = folder.split("/")[-2].split("__")[1]
    for exp in exp_list:
        for var in var_list:
            for yy in np.arange(y0, y1+1):
                num_check = 0
                for region in region_bb.keys():
                    out_dir1 = folder+"/"+exp+"/"+region+"/"+var
                    file_list = glob.glob(out_dir1+"/*")
                    if len(file_list)>0:
                        if "010300" in file_list[0]:
                            if len(glob.glob(out_dir1+"/*"+str(yy)+"*"+str(yy+1)+"*")) == 0:
                                num_check+= 1       
                                print(out_dir1+"/*"+str(yy)+"*"+str(yy+1)+"*") 
                        else:                        
                            if len(glob.glob(out_dir1+"/*"+str(yy)+"*")) == 0:
                                print(out_dir1+"/*"+str(yy)+"*")
                                num_check+= 1      
                if num_check>0:
                    file_list_missing.append([gcm, rcm, var, str(yy)])                                

# for specific exp
for exp in ["NCC-NorESM1-M__CNRM-ALADIN63", "CNRM-CERFACS-CNRM-CM5__CNRM-ALADIN63", "MOHC-HadGEM2-ES__CNRM-ALADIN63", "MPI-M-MPI-ESM-LR__CNRM-ALADIN63"]:
    folder = out_dir0+"/"+exp+"/"
    gcm = folder.split("/")[-2].split("__")[0]
    rcm = folder.split("/")[-2].split("__")[1]
    for exp in exp_list:
        for var in var_list:
            for yy in np.arange(y0, y1+1):
                num_check = 0
                for region in region_bb.keys():
                    out_dir1 = folder+"/"+exp+"/"+region+"/"+var
                    #print(out_dir1)
                    file_list = glob.glob(out_dir1+"/*")
                    if len(file_list)>0:
                        if "010300" in file_list[0]:
                            if len(glob.glob(out_dir1+"/*"+str(yy)+"*"+str(yy+1)+"*")) == 0:
                                num_check+= 1       
                        else:                        
                            if len(glob.glob(out_dir1+"/*"+str(yy)+"*")) == 0:
                                num_check+= 1       
                if num_check>0:
                    file_list_missing.append([gcm, rcm, var, str(yy)])   
                                
url2download = []
for url in url_list:
    for gonemissing in file_list_missing:
        if (gonemissing[0] in url) and (gonemissing[1] in url) and (gonemissing[2] in url) and (gonemissing[3] in url):
            url2download.append(url)       
            
# without parallel
for ii, url in enumerate(reversed(url2download)):
    if np.mod(ii, 100) == 0:
        node_status = download_tools.check_status_node_esgf()
    print("\n"+url+"\n")
    try:
        download_tools.extract_esgf_data_ROI_netcdf(url, region_bb, out_dir0, node_status) 
    except IOError:
        print("Error")

### parallel
pool = Pool(10)
mp_out = pool.starmap(download_tools.extract_esgf_data_ROI_netcdf, [(url, region_bb, out_dir0, node_status) for url in url2download ])
pool.close()

# find and download orography file
ctx = conn.new_context(
    project = 'CORDEX', 
    domain = cordex_domain, 
    variable = "orog", 
    facets = "project, domain, variable"
    ) #"*" "model"

pool = Pool(100)
pool_out = pool.starmap(download_tools.read_ctx_hit, [(hit, ctx) for hit in range(0, ctx.hit_count)])
esgf_data_avail = download_tools.merge_pool_output_read_ctx(pool_out, esgf_data_avail)
pool.close()

exp_list=["rcp45","rcp26","rcp85","historical" ]

url_list = []
var="orog"
data_avail=esgf_data_avail

for gcm in data_avail.keys():
    for rcm in data_avail[gcm].keys():
        for ens in  data_avail[gcm][rcm].keys():
            for exper in data_avail[gcm][rcm][ens].keys():
                    print(exper+" - "+var+" - "+gcm+" - "+rcm+" - "+ens)
                    if len(data_avail[gcm][rcm][ens][exper][var]["url"])>0:
                        for url in data_avail[gcm][rcm][ens][exper][var]["url"]:
                            if len(url)>0:
                                url_list.append(url)
                            

# without parallel
import xarray as xr
        
download_tools.get_download_progression()




# DOWNLOAD GCM OUTPUT FOR GLOBAL T CHANGE

var_list = [ "tas"] #, "rsds", "rlds"]
exp_list = ['rcp85','historical'] # 'rcp26', 'rcp45', 'rcp85']
time_frequency = "day" #'day'  # eg 3hr

# logon manager
lm = LogonManager()
if not lm.is_logged_on():
        lm.logon(hostname = 'esgf-data.dkrz.de', interactive = True, bootstrap = True)
        #lm.logon(hostname = 'esg-dn1.nsc.liu.se', interactive = True, bootstrap = True)

lm.is_logged_on()

####################################################
# Explore the ESGF database to extract the list 
# of models run with wanted variables and periods

conn = SearchConnection('https://esgf-data.dkrz.de/esg-search', distrib = True)
esgf_data_avail = {}
for exp in exp_list:
    print("experiment: " + exp)
    for var in var_list:
        print("Variable: " + var)
        ctx = conn.new_context(
            project = 'CMIP5', 
            #domain = cordex_domain, 
            experiment = exp, 
            time_frequency = time_frequency, 
            variable = var, 
#            institute = "GERICS",
#            driving_model = "ICHEC-EC-EARTH",
            facets = "project, domain, experiment, time_frequency, variable"
            ) #"*" "model"

        print("Found hits: " + str(ctx.hit_count))

        pool = Pool(100)
        pool_out = pool.starmap(download_tools.read_ctx_hit, [(hit, ctx) for hit in range(0, ctx.hit_count)])
        esgf_data_avail = download_tools.merge_pool_output_read_ctx(pool_out, esgf_data_avail)
        pool.close()
    
run_list=["CNRM-CERFACS-CNRM-CM5__ICTP-RegCM4-6" "ICHEC-EC-EARTH__ICTP-RegCM4-6" "MOHC-HadGEM2-ES__CNRM-ALADIN63" "MOHC-HadGEM2-ES__ICTP-RegCM4-6" "MPI-M-MPI-ESM-LR__CNRM-ALADIN63" "MPI-M-MPI-ESM-LR__ICTP-RegCM4-6" "NCC-NorESM1-M__CNRM-ALADIN63" "NCC-NorESM1-M__ICTP-RegCM4-6"]

url_list = []
for exp in exp_list:
    for kk in esgf_data_avail["historical"]["3hr"]["atmos"]["3hr"].keys():
        if esgf_data_avail["historical"]["3hr"]["atmos"]["3hr"][kk]["url"][0] is not None:
            if esgf_data_avail["historical"]["3hr"]["atmos"]["3hr"][kk]["url"][0][0] is not None:
                url_list.append(esgf_data_avail["historical"]["3hr"]["atmos"]["3hr"][kk]["url"])
        
url_list = sum(url_list, [])

url2download = []
for url in url_list:
    if "CNRM" in url:
        if "tas" in url:
            print(url)
            url2download.append(url)
        

with xr.open_dataset(url, decode_times = False) as ds:
    dataset = ds["tas"] # meilleur de [[variable, "lat", "lon"]] parcequ'on perd des downloads au check coord
    dataset.to_netcdf( "/home/hola/datos_seis/nacional_parques/data/cmip5/test.nc" , encoding={variable:{"zlib":True}}) # lossless compression @sgascoin mail
                      
        