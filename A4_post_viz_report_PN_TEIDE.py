import os
import glob
import sys
from xgrads import CtlDescriptor # replace with xarray
from xgrads import open_CtlDataset # replace with xarray
import datetime as dt
import calendar 
import numpy as np    
import matplotlib as mpl
mpl.rcParams.update({'text.usetex': False,"svg.fonttype": 'none'})
import pickle
import itertools
from multiprocessing import Pool
import xarray as xr
import blosc
import fiona
import rasterio.features
#import rasterio as rio
import rioxarray as rioxr
import xarray as xr
from matplotlib_scalebar.scalebar import ScaleBar

import matplotlib.pyplot as plt
plt.rcParams.update({'font.size': 10, 'font.family' : 'Arial'})
import pandas as pd

# edit of sys.path required due to a weird bug/feature of python3.6 https://github.com/conda/conda/issues/6018
sys.path.append('/home/hola/project/nacional_parques/script')
from nivo_parc.toolbox import post_tools


            
# fixed parameters
#cordex_domain = "EUR-11"  # CORDEX domain [STR] eg "EUR-44"
project = 'CORDEX' # eg CMIP6
time_frequency = "3hr" #'day'  # eg 3hr
res = int(250)      
pixel_size = res*res/1e6

run_dict = post_tools.read_run_list("/home/hola/project/nacional_parques/script/nivo_parc/toolbox/exp_list_EXP_TEIDE.txt")
out_dir = "/home/hola/datos_seis/nacional_parques/prod"

# variables
exp_list = ['historical', "rcp85"] #,'historical' 'rcp26', 'rcp45', 'rcp85']
reg_list = ["TEIDE"] 
#var_list = ["prec", "relh", "rpre", "smlt", "ssub", "tair", "qlin", "roff", "sden", "snod", "swed", "wdir"]
#var_list = ["prec", "relh", "rpre", "smlt", "ssub", "tair", "qlin", "roff", "sden", "swed", "wdir", "wspd", "qsin", "albd"]
var_list = ["snod",  "tair", "prec"] # "swed",


plt_dict = {
    "historical":{"col":"#4a4e4d", "mrk":"v"},
    "rcp26":{"col":"#f6cd61", "mrk":"^"},
    "rcp85":{"col":"#fe8a71", "mrk":">"},
    "AIGUE":{"col":"#ee4035","mrk":"H"},
    "GUADA":{"col":"#f37736","mrk":"X"},
    "ORDES":{"col":"#ffe800","mrk":"o"},
    "PICOS":{"col":"#7bc043","mrk":"d"},
    "SIERR":{"col":"#0392cf","mrk":"s"},
    "TEIDE":{"col":"#8874a3","mrk":"s"},
    "DJF":{"col":"#0057e7","mrk":"*"},
    "MAM":{"col":"#008744","mrk":"s"},
    "JJA":{"col":"#d62d20","mrk":"."},
    "SON":{"col":"#ffa700","mrk":"^"},
    "MODIS":{"col":"#283655"},
    "SIMU": {"col":"#35a79c"}
    }

in2cm = 1/2.54

common_years_hist = list( np.arange(2000, 2004)) # years with MODIS observations and simu (reanalysis forcings)
common_years_all = list( np.arange(2000, 2023)) # years with MODIS observations and simu (reanalysis + clim proj)

################################
########    LOAD DATA   ########
################################
# Load data for each run
data_all_run_qm =  {}
data_all_run_qm_tmp = {}

sm_par_dir = "/home/hola/project/nacional_parques/sm_Tlr_53/par"

for exp in ["historical", "rcp85"]: #, "rcp26", "rcp85"]:
    run_list = run_dict[exp]
    sm_out_dir = out_dir+"/sm/sm_Tlr_53"
    print(exp)
    print(run_list)
    print(out_dir)
    data = post_tools.get_simu_single_exp(exp, res, sm_out_dir, sm_par_dir, run_list, reg_list, var_list)
    data_all_run_qm_tmp = post_tools.merge_data_runs(data_all_run_qm_tmp, data)
    
# FIND T_INIT AND FINAL FOR ALL RUNS
for run in data_all_run_qm_tmp.keys():
    reg = "TEIDE"
    var = "tair"
    
    var_hist = data_all_run_qm_tmp[run]["historical"][reg][var]
    var_rcp = data_all_run_qm_tmp[run]["rcp85"][reg][var]
    
    print(run+" - "+str(var_hist.time[0].dt.strftime('%Y.%m.%d').values )+" - START")
    print(run+" - "+str(var_hist.time[-1].dt.strftime('%Y.%m.%d').values)+" - END")
    print(run+" - "+str(var_rcp.time[0].dt.strftime('%Y.%m.%d').values  )+" - START")
    print(run+" - "+str(var_rcp.time[-1].dt.strftime('%Y.%m.%d').values  )+" - END")
    print("")
    
# Merge historical - rcp85
for run in data_all_run_qm_tmp.keys():
    data_all_run_qm[run] = {"all":{}}
    for reg in reg_list:
        data_all_run_qm[run]["all"][reg] = {}
        for var in var_list:
            
            var_hist = data_all_run_qm_tmp[run]["historical"][reg][var]
            var_rcp = data_all_run_qm_tmp[run]["rcp85"][reg][var]
            var_concat = xr.concat([var_hist, var_rcp], dim="time").sortby("time")
                        
            # Identify duplicate time values
            time_values = var_concat["time"].values
            unique_indices = np.unique(time_values, return_index=True)[1]
            
            # Select the first value for each duplicate
            var_concat = var_concat.isel(time=unique_indices) # only for MOHC-HadGEM2-ES__ICTP-RegCM4-6 which has december 2005 in both historical AND rcp85
            
            var_concat_clean = post_tools.clean_uncomplete_hyear(var_concat)
            data_all_run_qm[run]["all"][reg][var] = var_concat_clean
del(data_all_run_qm_tmp)

# Calculate snod
# for exp in ["all"]: #["historical", "rcp85"]: 
#     for reg in reg_list:
#         print(reg)
#         for run in data_all_run_qm.keys():
#             swe = data_all_run_qm[run][exp][reg]["swed"]
#             rho = data_all_run_qm[run][exp][reg]["sden"]
#             data_all_run_qm[run][exp][reg]["snod"] = swe.copy(deep=True).rename({"swed":"snod"})
#             mask = (swe['swed'] == 0).astype(int)
#             data_all_run_qm[run][exp][reg]["snod"]["snod"] = 1000*swe.swed/rho.sden
#             data_all_run_qm[run][exp][reg]["snod"]["snod"].fillna(0)
               
# Calculate SCA
snod_threshold = 0.1
for exp in ["all"]: #["historical", "rcp85"]: 
    for reg in reg_list:
        print(reg)
        for run in data_all_run_qm.keys():
            snod = data_all_run_qm[run][exp][reg]["snod"]
            if len(snod)>0:
                data_all_run_qm[run][exp][reg]["sca"] = snod.copy(deep=True).rename({"snod":"sca"})
            
                # Create the binary mask using Xarray operations
                mask = (snod['snod'] > snod_threshold).astype(int)
                data_all_run_qm[run][exp][reg]["sca"]["sca"] = mask
            else:
                data_all_run_qm[run][exp][reg]["sca"] = {}
        
# Calculate SMOD
for exp in ["all"]: #["historical", "rcp85"]: 
    for reg in reg_list:
        for run in data_all_run_qm.keys():
            print(reg+" - "+run)
            sca = data_all_run_qm[run][exp][reg]["sca"]
    
            SMOD_list = []
            sca_gp = sca.groupby("hyear")        
            for sca_hy in sca_gp:
                SMOD_list.append( xr.apply_ufunc(
                    post_tools.calc_SMOD, 
                    sca_hy[1].chunk(dict(time=-1)), 
                    input_core_dims=[['time']], 
                    vectorize=True,
                    dask="parallelized"
                ))
            data_all_run_qm[run][exp][reg]["smod"] = xr.concat(SMOD_list, pd.Index(sca_gp.groups.keys(), name='hyear')).rename({"sca":"smod"})

# Open MODIS data
data_MODIS = {}
for reg in reg_list:
    print(reg)
    data_MODIS[reg] = {}

    MODIS_list = []

    for in_MODIS_im, in_MODIS_date in zip(sorted(glob.glob( "/home/hola/datos_seis/nacional_parques/data/MODIS/"+reg+"*.tif")), sorted(glob.glob( "/home/hola/datos_seis/nacional_parques/data/MODIS/"+reg+"*.csv"))):
        MODIS_tmp = rioxr.open_rasterio(in_MODIS_im)
        MODIS_date_raw = np.genfromtxt(in_MODIS_date, dtype = str, delimiter = ",", skip_header=1)
        MODIS_date = [dt.datetime.strptime("".join(ll.split('_')[1:4]),"%Y%m%d") for ll in MODIS_date_raw]
        MODIS_out = xr.DataArray( data = MODIS_tmp.data, dims = ('time', 'y', 'x'), coords = {'time': MODIS_date, 'y': MODIS_tmp.y, 'x': MODIS_tmp.x})
        
        DEM = np.loadtxt("/home/hola/datos_seis/nacional_parques/data/MODIS/"+reg+"_MODIS_DEM.asc", skiprows=6)
        MODIS_out = MODIS_out.assign_coords(DEM=(["y","x"], DEM)) # elevation


        MODIS_list.append( MODIS_out )
        
    MODIS = xr.concat(MODIS_list, dim='time')
    var_concat_clean = post_tools.clean_uncomplete_hyear(var_concat)

    hyear = [ dd.dt.year.item() if (dd.dt.month>=9) else dd.dt.year.item()-1 for dd in MODIS.time]
    MODIS = MODIS.assign_coords(hyear=("time", np.array(hyear))) # day of hydrological year (0=1st september) 
            
    MODIS_sca = (MODIS>50).astype(int)

    data_MODIS[reg]["scf"] = MODIS
    data_MODIS[reg]["sca"] = MODIS_sca
    
    SMOD_list = []
    sca_gp = MODIS_sca.groupby("hyear")        
    for sca_hy in sca_gp:
        SMOD_list.append( xr.apply_ufunc(
            post_tools.calc_SMOD, 
            sca_hy[1].chunk(dict(time=-1)), 
            input_core_dims=[['time']], 
            vectorize=True,
            dask="parallelized"
        ))
    data_MODIS[reg]["smod"] = xr.concat(SMOD_list, pd.Index(sca_gp.groups.keys(), name='hyear')) #.rename({"sca":"smod"})



# Load data for ensemble mean
var_list = ["snod", "tair", "prec"] #, "smlt", "ssub"] # ,"rpre" "swed",  
stat_name_list = [ "mean", "std"] # "min", "max",

data_ens_qm =  {}
data_ens_qm_tmp = {}
for stat_name in stat_name_list:
    data_ens_qm_tmp[stat_name] = {}
sm_par_dir = "/home/hola/project/nacional_parques/sm_Tlr_53/par"

for exp in ["historical", "rcp85"]:#, "rcp26", "rcp85"]:
    run_list = run_dict[exp]
    sm_out_dir = out_dir+"/sm/sm_Tlr_53_corr"
    print(exp)
    for stat_name in stat_name_list:
        data = post_tools.get_simu_ens( exp, res, sm_out_dir, sm_par_dir, reg_list, var_list, stat_name)
        data_ens_qm_tmp[stat_name] = post_tools.merge_data_runs( data_ens_qm_tmp[stat_name], data)
    
# Merge historical - rcp85
run = "ens"
data_ens_qm[run] = {"all":{}}


for reg in reg_list:
    data_ens_qm[run]["all"][reg] = {}
    for var in var_list:
        data_ens_qm[run]["all"][reg][var] = {}
        for stat_name in stat_name_list: 
            data_ens_qm[run]["all"][reg][var][stat_name] = {}

            var_hist = data_ens_qm_tmp[stat_name][run]["historical"][reg][var]
            var_rcp = data_ens_qm_tmp[stat_name][run]["rcp85"][reg][var]
            var_concat = xr.concat([var_hist, var_rcp], dim="time").sortby("time")
                        
            # Identify duplicate time values
            time_values = var_concat["time"].values
            unique_indices = np.unique(time_values, return_index=True)[1]
            
            # Select the first value for each duplicate
            var_concat = var_concat.isel(time=unique_indices) # only for MOHC-HadGEM2-ES__ICTP-RegCM4-6 which has december 2005 in both historical AND rcp85
            
            var_concat_clean = post_tools.clean_uncomplete_hyear(var_concat)
            data_ens_qm["ens"]["all"][reg][var][stat_name] = var_concat_clean
del(data_ens_qm_tmp)

# Make mean-std and mean+std
for reg in reg_list:
    for var in var_list:
        if var in ["swed","ssub", "smlt"]: #  "snod", "prec", 
            tt = data_ens_qm["ens"]["all"][reg][var]["mean"] - data_ens_qm["ens"]["all"][reg][var]["std"]
            data_ens_qm["ens"]["all"][reg][var]["mean_mstd"] = tt.where( tt>0, 0)
        else:
            data_ens_qm["ens"]["all"][reg][var]["mean_mstd"] = data_ens_qm["ens"]["all"][reg][var]["mean"] - data_ens_qm["ens"]["all"][reg][var]["std"]
        data_ens_qm["ens"]["all"][reg][var]["mean_pstd"] = data_ens_qm["ens"]["all"][reg][var]["mean"] + data_ens_qm["ens"]["all"][reg][var]["std"]

# Calculate SCA
snod_threshold = 0.1
for exp in ["all"]: #["historical", "rcp85"]: 
    for reg in reg_list:
        print(reg)
        for run in data_ens_qm.keys():
            data_ens_qm[run][exp][reg]["sca"] = { "min":{}, "max":{}, "mean":{}, "mean_mstd":{}, "mean_pstd":{}}

            for stat_name in ["mean", "mean_mstd", "mean_pstd"]: # "min", "max", 

                snod = data_ens_qm[run][exp][reg]["snod"][stat_name]
                if len(snod)>0:
                    data_ens_qm[run][exp][reg]["sca"][stat_name] = snod.copy(deep=True).rename({"snod":"sca"})
                
                    # Create the binary mask using Xarray operations
                    mask = (snod['snod'] > snod_threshold).astype(int)
                    data_ens_qm[run][exp][reg]["sca"][stat_name]["sca"] = mask
                else:
                    data_ens_qm[run][exp][reg]["sca"][stat_name] = {}
        
# Calculate SMOD
for exp in ["all"]: #["historical", "rcp85"]: 
    for reg in reg_list:
        for run in data_ens_qm.keys():
            print(reg+" - "+run)

            data_ens_qm[run][exp][reg]["smod"] = { "min":{}, "max":{}, "mean":{}, "mean_mstd":{}, "mean_pstd":{}}
            for stat_name in ["mean", "mean_mstd", "mean_pstd"]: #"min", "max",  
                sca = data_ens_qm[run][exp][reg]["sca"][stat_name]
                
                SMOD_list = []
                sca_gp = sca.groupby("hyear")        
                for sca_hy in sca_gp:
                    SMOD_list.append( xr.apply_ufunc(
                        post_tools.calc_SMOD, 
                        sca_hy[1].chunk(dict(time=-1)), 
                        input_core_dims=[['time']], 
                        vectorize=True,
                        dask="parallelized"
                    ))
                data_ens_qm[run][exp][reg]["smod"][stat_name] = xr.concat(SMOD_list, pd.Index(sca_gp.groups.keys(), name='hyear')).rename({"sca":"smod"})


# Open Parc contour
contour_parc = {}
for reg in reg_list:
    x_parc, y_parc = post_tools.open_parc_shp(reg)
    contour_parc[reg] = (x_parc , y_parc)

################################################### 
################################################### 

########################## 
# all run plot SCA
gcm_rcm_list = list(data_all_run_qm.keys())
out_stat = {}
for reg in reg_list:
    print(reg)
    
    # Read MODIS data
    in_SCA_MODIS="/home/hola/datos_seis/nacional_parques/data/MODIS/Snow_Cover_Area_MODIS_"+reg+".csv"
    SCA = pd.read_csv(in_SCA_MODIS,names=["time","sca"],header=0)
    time_dt = [dt.datetime.strptime(dd, "%Y_%m_%d") for dd in SCA.time]
    SCA["time"]=time_dt
    SCA["hyear"] = [yy if mm<12 else yy+1 for yy,mm in zip(SCA.time.dt.year, SCA.time.dt.month) ]
    ts_MODIS = SCA.groupby("hyear").mean()
    
    plt.figure(figsize=(8,3))
    # plot each run
    # for exp in ["all"]:
    #     for run in data_all_run_qm.keys():
    #         # qm 
    #         sca = data_all_run_qm[run][exp][reg]["sca"]
    #         if len(sca)>0:
    #             ts_sca = sca.sum(dim=("lat","lon")).groupby("hyear").mean()*250*250/1e6
    #             plt.plot(ts_sca.hyear, ts_sca.sca, lw=0.2, marker=".", ms=6, mew=0, color=[0.1,0.1,0.9])
    
    # plot ens mean
    sca = data_ens_qm["ens"]["all"][reg]["sca"]["mean"]
    ts_sca = sca.sum(dim=("lat","lon")).groupby("hyear").mean()*250*250/1e6
    plt.plot(ts_sca.hyear, ts_sca.sca, lw=2, ms=6, mew=0, color=plt_dict["SIMU"]["col"]) #[0.1,0.1,0.9])

    sca = data_ens_qm["ens"]["all"][reg]["sca"]["mean_pstd"]
    ts_sca_plus = sca.sum(dim=("lat","lon")).groupby("hyear").mean()*250*250/1e6
    #plt.plot(ts_sca_plus.hyear, ts_sca_plus.sca, lw=1, ms=6, mew=0, color="k" )#[0.1,0.1,0.9])
    
    sca = data_ens_qm["ens"]["all"][reg]["sca"]["mean_mstd"]
    ts_sca_minus = sca.sum(dim=("lat","lon")).groupby("hyear").mean()*250*250/1e6
    #plt.plot(ts_sca_minus.hyear, ts_sca_minus.sca, lw=1, ms=6, mew=0, color="g") #[0.1,0.1,0.9])
    plt.fill_between(ts_sca_plus.hyear, ts_sca_minus.sca, ts_sca_plus.sca, color=plt_dict["SIMU"]["col"], alpha=0.5)
    # plot MODIS
    plt.plot(ts_MODIS.index, ts_MODIS.sca, lw=2, color=plt_dict["MODIS"]["col"])
    
    plt.ylim((0, plt.gca().get_ylim()[1]))
    plt.ylabel("Yearly mean SCA (km2)")
    plt.grid(True, lw=0.4)
    
    plt.savefig("/home/hola/datos_seis/port_transfert/sca_"+reg+"_qm_report.png")
    plt.savefig("/home/hola/datos_seis/port_transfert/sca_"+reg+"_qm_report.svg")
    plt.close()
    
    # Calculate statistics - mean bias, rmse
    out_stat[reg] = {}
    MODIS_filtered = ts_MODIS[ts_MODIS.index.isin(common_years_hist)]
    SIMU_filtered = ts_sca.where(ts_sca.hyear.isin(common_years_hist), drop = True)
    
    out_stat[reg]["mean_bias_hist"] = (SIMU_filtered-MODIS_filtered).mean().round(2)["sca"].values.item()
    out_stat[reg]["rmse_hist"] = np.sqrt(np.square(np.subtract(MODIS_filtered.sca, SIMU_filtered.sca)).mean()).round(2)

    MODIS_filtered = ts_MODIS[ts_MODIS.index.isin(common_years_all)]
    SIMU_filtered = ts_sca.where(ts_sca.hyear.isin(common_years_all), drop = True)
    
    out_stat[reg]["mean_bias_all"] = (SIMU_filtered-MODIS_filtered).mean().round(2)["sca"].values.item()
    out_stat[reg]["rmse_all"] = np.sqrt(np.square(np.subtract(MODIS_filtered.sca, SIMU_filtered.sca)).mean()).round(2)

########################## 
# all run map - VS SCA MODIS
gcm_rcm_list = list(data_all_run_qm.keys())

exp = "all" 
for reg in reg_list:
    print(reg)

    (x_parc, y_parc) = contour_parc[reg]
    # Open Parc hillshade
    
    in_HS = "/home/hola/datos_seis/nacional_parques/data/COP30/250m/limite_"+reg+"_COP30_250m_UTM_DEM_HS.tif"
    HS = xr.open_dataset( in_HS )

    # load simu data
    var_data = 100*data_ens_qm["ens"]["all"][reg]["sca"]["mean"].sel(time=slice("2000-09-01","2004-08-31")).sum(dim={"time"})/(4*365)
    data2im = np.flipud(var_data.to_array().squeeze())
    data2im = np.ma.masked_where( data2im < 5, data2im)     
    
#     # Process SM outpur
#     xr_list = [data_all_run_qm[gcm_rcm][exp][reg]["sca"].sel(time=slice("2000-09-01","2004-08-31")) for gcm_rcm in gcm_rcm_list]
#     var_data = xr.concat(xr_list, pd.Index(gcm_rcm_list, name='gcm_rcm')).chunk(dict(gcm_rcm=-1))
#     import dask
#     from multiprocessing.pool import ThreadPool 
#
#     with dask.config.set(schedular='threads', pool=ThreadPool(1)):
#         var_data = var_data.unify_chunks().chunk({"lat":10, "lon":10})
#         var_data_mean = var_data.mean(dim="gcm_rcm").sum(dim={"time"})/(4*365) # 
# #    var_data_mean = var_data_mean.where(var_data_mean>0, np.nan)
#         data2im = np.flipud(var_data_mean.sca.to_numpy())
#         data2im = np.ma.masked_where(data2im==0, data2im)    
    
    data2im_MODIS = 100*data_MODIS[reg]["sca"].sel(time=slice("2000-09-01","2004-08-31")).sum(dim={"time"})/(4*365)
    data2im_MODIS = np.ma.masked_where(data2im_MODIS < 5, data2im_MODIS)    

    # Plot
    fig = plt.figure(figsize=(8,3))
    ax1 = plt.subplot(121)

    im1 = ax1.imshow( HS.band_data.squeeze(), cmap='Greys_r', vmin=0, vmax=255, extent = post_tools.get_xr_im_extent( HS ), origin="upper", interpolation="nearest", alpha=0.8)
    im2 = ax1.imshow( data2im, cmap='viridis', vmin = 0 , vmax = 100, extent = post_tools.get_xr_im_extent( var_data), origin="upper", interpolation="nearest")
    ax1.plot(x_parc, y_parc, color="#f6cd61", lw=0.5 )
    ax1.set_title("Simulation")
    
    ax2 = plt.subplot(122)
    im1 = ax2.imshow(HS.band_data.squeeze(), cmap = 'Greys_r', vmin=0, vmax=255, extent=post_tools.get_xr_im_extent( HS ), origin = "upper", interpolation = "nearest", alpha = 0.8)
    im3 = ax2.imshow( data2im_MODIS, cmap='viridis', vmin=0 , vmax=100, extent=post_tools.get_xr_im_extent( data_MODIS[reg]["sca"] ), origin = "upper", interpolation = "nearest")
    ax2.plot(x_parc, y_parc, color="#f6cd61", lw=0.5 )
    ax1.set_title("Observation")
    
    for ax in fig.get_axes():
        ax.set_xticks([])
        ax.set_yticks([])
    
    scalebar = ScaleBar(0.001, "km", length_fraction=0.25, width_fraction=0.03, fixed_value=10, font_properties={"size":9})
    plt.gca().add_artist(scalebar)
    
    fig.subplots_adjust(right=0.8)
    cbar_ax = fig.add_axes([0.90, 0.3, 0.02, 0.3])
    cb = fig.colorbar(im2, cax=cbar_ax)
    cb.set_ticks([0, 25, 50, 75, 100], labels=[0, 25, 50, 75, 100], fontsize=10)
    cb.ax.set_title('Snow probability\n2000-2003 (%)',fontsize=10, horizontalalignment="center")

    plt.savefig("/home/hola/datos_seis/port_transfert/sca_"+reg+"_qm_map_report.png")
    plt.savefig("/home/hola/datos_seis/port_transfert/sca_"+reg+"_qm_map_report.svg")
    plt.close()
    
########################## 
# all mean run - VS SCA = f(ELEVATION)

gcm_rcm_list = list(data_all_run_qm.keys())

exp = "all" 
for reg in reg_list:
    print(reg)

    (x_parc, y_parc) = contour_parc[reg]
    # Open Parc hillshade
    
    in_HS = "/home/hola/datos_seis/nacional_parques/data/COP30/250m/limite_"+reg+"_COP30_250m_UTM_DEM_HS.tif"
    HS = xr.open_dataset( in_HS )

    # load simu data
    var_data_mean = data_ens_qm["ens"]["all"][reg]["sca"]["mean"].sel(time=slice("2000-09-01","2004-08-31")).sum(dim={"time"})/(4*365)

    # # Process SM outpur
    # xr_list = [data_all_run_qm[gcm_rcm][exp][reg]["sca"].sel(time=slice("2000-09-01","2004-08-31")) for gcm_rcm in gcm_rcm_list]
    # var_data = xr.concat(xr_list, pd.Index(gcm_rcm_list, name='gcm_rcm')).chunk(dict(gcm_rcm=-1))
    # import dask
    # from multiprocessing.pool import ThreadPool 
    #
    # with dask.config.set(schedular='threads', pool=ThreadPool(1)):
    #     var_data = var_data.unify_chunks().chunk({"lat":10, "lon":10})
    #     var_data_mean = var_data.mean(dim="gcm_rcm").sum(dim={"time"})/(4*365) # 

    bin_edges = np.arange(300, 3900, 300)
    
    # SIMU
    data_grouped = var_data_mean.groupby_bins( 'DEM', bin_edges)
    
    boxplot_data, bin2plot = [], []
    for name, group in data_grouped:
        boxplot_data.append(group.sca.values)
        bin2plot.append(name.mid)
        
    # Create the boxplot
    fig, ax = plt.subplots()
    bp_simu = ax.boxplot(boxplot_data, positions=np.array(bin2plot)+35, widths=55, patch_artist=True, whis=1e9,
                boxprops = { "facecolor":plt_dict["SIMU"]["col"], "color":plt_dict["SIMU"]["col"]},
                capprops = { "color":plt_dict["SIMU"]["col"]},
                whiskerprops = { "color":plt_dict["SIMU"]["col"]},
                medianprops = { "color":"w"},
                )

    # MODIS
    data2im_MODIS = data_MODIS[reg]["sca"].sel(time=slice("2000-09-01","2004-08-31")).sum(dim={"time"})/(4*365)

    data_grouped = data2im_MODIS.groupby_bins( 'DEM', bin_edges)
    
    boxplot_data, bin2plot = [], []
    for name, group in data_grouped:
        boxplot_data.append(group.values[group.values>0.05])
        bin2plot.append(name.mid)
        
    # Create the boxplot
    bp_MODIS = ax.boxplot(boxplot_data, positions=np.array(bin2plot)-35, widths=55, patch_artist=True, whis=1e9,
                boxprops = { "facecolor":plt_dict["MODIS"]["col"], "color":plt_dict["MODIS"]["col"]},
                capprops = { "color":plt_dict["MODIS"]["col"]},
                whiskerprops = { "color":plt_dict["MODIS"]["col"]},
                medianprops = { "color":"w"},
                )


    # Customize the appearance of the boxplot
    ax.set_xlabel('Elevation (m)')
    ax.set_ylabel('Snow probability')
    ax.set_title('Snow probability per elevation - '+reg)
    ax.set_xticks(bin_edges)
    ax.set_xticklabels([str(int(x)) for x in bin_edges])
    ax.legend([bp_simu["boxes"][0], bp_MODIS["boxes"][0]], ['Simu.', 'Obs. MODIS'], loc='upper right')
    ax.axis([600 ,3300, 0, 1])
    ax.grid(lw=0.4)    
    
    plt.savefig("/home/hola/datos_seis/port_transfert/sca_"+reg+"_elevation_report.png")
    plt.savefig("/home/hola/datos_seis/port_transfert/sca_"+reg+"_elevation_report.svg")
    plt.close()
    
    
########################## 
# all run map - VS SMOD MODIS
gcm_rcm_list = list(data_all_run_qm.keys())

exp = "all" 
for reg in reg_list:
    print(reg)
    (x_parc, y_parc) = contour_parc[reg]
    
    # Open Parc hillshade
    
    in_HS = "/home/hola/datos_seis/nacional_parques/data/COP30/250m/limite_"+reg+"_COP30_250m_UTM_DEM_HS.tif"
    HS = xr.open_dataset( in_HS )
    
    # load simu data
    var_data = data_ens_qm["ens"]["all"][reg]["smod"]["mean"].sel(hyear=slice("2000","2003")).mean( dim = {"hyear"} )
    data2im = np.flipud(var_data.to_array().squeeze())
    data2im = np.ma.masked_where(data2im<0.05, data2im)   
#     # Process SM outpur
#     xr_list = [data_all_run_qm[gcm_rcm][exp][reg]["smod"].sel(hyear=slice("2000","2004")) for gcm_rcm in gcm_rcm_list]
#     var_data = xr.concat(xr_list, pd.Index(gcm_rcm_list, name='gcm_rcm')).chunk(dict(gcm_rcm=-1))
#     import dask
#     from multiprocessing.pool import ThreadPool 
#
#     with dask.config.set(schedular='threads', pool=ThreadPool(1)):
#         var_data = var_data.unify_chunks().chunk( {"lat":10, "lon":10} )
#         var_data_mean = var_data.mean( dim="gcm_rcm" ).mean( dim = {"hyear"} )
#
# #    var_data_mean = var_data_mean.where(var_data_mean>0, np.nan)
#         data2im = np.flipud(var_data_mean.smod.to_numpy())
#         data2im = np.ma.masked_where( data2im < 0, data2im )    
    
    data2im_MODIS = data_MODIS[reg]["smod"].sel(hyear=slice("2000","2003")).mean(dim={"hyear"})
    data2im_MODIS = np.ma.masked_where(data2im_MODIS < 0, data2im_MODIS)    
    ymin_MODIS, ymax_MODIS, xmin_MODIS, xmax_MODIS = np.min(data_MODIS[reg]["smod"].y), np.max(data_MODIS[reg]["smod"].y), np.min(data_MODIS[reg]["smod"].x), np.max(data_MODIS[reg]["smod"].x)

    # Plot
    fig = plt.figure(figsize=(8,3))
    ax1 = plt.subplot(121)

    im1 = ax1.imshow( HS.band_data.squeeze(), cmap='Greys_r', vmin=0, vmax=255, extent = post_tools.get_xr_im_extent( HS ), origin="upper", interpolation="nearest", alpha=0.8)
    im2 = ax1.imshow( data2im, cmap='viridis', vmin = 0 , vmax = 365, extent = post_tools.get_xr_im_extent( var_data), origin="upper", interpolation="nearest")
    ax1.plot(x_parc, y_parc, color="#f6cd61", lw=0.5 )
    ax1.set_xticklabels("")
    ax1.set_yticklabels("")
    ax1.set_title("Simulation")
    
    ax2 = plt.subplot(122)
    im1 = ax2.imshow(HS.band_data.squeeze(), cmap='Greys_r', vmin=0, vmax=255, extent = post_tools.get_xr_im_extent( HS ), origin="upper", interpolation="nearest", alpha=0.8)
    im3 = ax2.imshow( data2im_MODIS, cmap='viridis', vmin = 0 , vmax = 365, extent = post_tools.get_xr_im_extent( data_MODIS[reg]["smod"] ), origin="upper", interpolation="nearest")
    ax2.plot(x_parc, y_parc, color="#f6cd61", lw=0.5 )
    ax2.set_title("Observation")

    for ax in fig.get_axes():
        ax.set_xticks([])
        ax.set_yticks([])
        
    scalebar = ScaleBar(0.001, "km", length_fraction=0.25, width_fraction=0.03, fixed_value=10, font_properties={"size":9})
    plt.gca().add_artist(scalebar)
    
    fig.subplots_adjust(right=0.8)
    cbar_ax = fig.add_axes([0.90, 0.3, 0.02, 0.3])
    cb = fig.colorbar(im2, cax=cbar_ax, label="day of year")
    cb.ax.set_title('Snow melt-out date\n2000-2003',fontsize=10, horizontalalignment="center")
    cb.set_ticks([ 0, 100, 200, 300], labels=[ 0, 100, 200, 300])

    
    cbar_axt = cbar_ax.twinx()
    cbt = fig.colorbar(im2, cax=cbar_axt, label='date')
    cbt.set_ticks([ 0, 90, 180, 270, 365], labels=[ "sep", "dec", "mar", "jun", "sep"])

    cbt.ax.yaxis.set_ticks_position('left')
    cbt.ax.yaxis.set_label_position('left')
    
    cb.ax.yaxis.set_ticks_position('right')
    cb.ax.yaxis.set_label_position('right')
    
    plt.savefig("/home/hola/datos_seis/port_transfert/smod_"+reg+"_qm_map_report.png")
    plt.savefig("/home/hola/datos_seis/port_transfert/smod_"+reg+"_qm_map_report.svg")
    plt.close()
    
    
    
  
########################## 
# all mean run - VS SMOD = f(ELEVATION)

gcm_rcm_list = list(data_all_run_qm.keys())

exp = "all" 
for reg in reg_list:
    print(reg)

    (x_parc, y_parc) = contour_parc[reg]
    # Open Parc hillshade
    
    in_HS = "/home/hola/datos_seis/nacional_parques/data/COP30/250m/limite_"+reg+"_COP30_250m_UTM_DEM_HS.tif"
    HS = xr.open_dataset( in_HS )

    # load simu data

    var_data_mean = data_ens_qm["ens"]["all"][reg]["smod"]["mean"].sel(hyear=slice("2000","2003")).mean( dim = {"hyear"} )

    # # Process SM outpur
    # xr_list = [data_all_run_qm[gcm_rcm][exp][reg]["smod"].sel(hyear=slice("2000","2003")) for gcm_rcm in gcm_rcm_list]
    # var_data = xr.concat(xr_list, pd.Index(gcm_rcm_list, name='gcm_rcm')).chunk(dict(gcm_rcm=-1))
    # import dask
    # from multiprocessing.pool import ThreadPool 
    #
    # with dask.config.set(schedular='threads', pool=ThreadPool(1)):
    #     var_data = var_data.unify_chunks().chunk({"lat":10, "lon":10})
    #     var_data_mean = var_data.mean(dim="gcm_rcm").mean(dim={"hyear"}) # 

    bin_edges = np.arange(600, 4000, 300)
    
    # SIMU
    data_grouped = var_data_mean.groupby_bins( 'DEM', bin_edges)
    
    boxplot_data, bin2plot = [], []
    for name, group in data_grouped:
        boxplot_data.append(group.smod.values)
        bin2plot.append(name.mid)
        
    # Create the boxplot
    fig, ax = plt.subplots()
    bp_simu = ax.boxplot(boxplot_data, positions=np.array(bin2plot)+35, widths=55, patch_artist=True, whis=1e9,
                boxprops = { "facecolor":plt_dict["SIMU"]["col"], "color":plt_dict["SIMU"]["col"]},
                capprops = { "color":plt_dict["SIMU"]["col"]},
                whiskerprops = { "color":plt_dict["SIMU"]["col"]},
                medianprops = { "color":"w"},
                )

    # MODIS
    data2im_MODIS = data_MODIS[reg]["smod"].sel(hyear=slice("2000","2003")).mean(dim={"hyear"})
    data_grouped = data2im_MODIS.groupby_bins( 'DEM', bin_edges)
    
    boxplot_data, bin2plot = [], []
    for name, group in data_grouped:
        boxplot_data.append(group.values)
        bin2plot.append(name.mid)
        
    # Create the boxplot
    bp_MODIS = ax.boxplot(boxplot_data, positions=np.array(bin2plot)-35, widths=55, patch_artist=True, whis=1e9,
                boxprops = { "facecolor":plt_dict["MODIS"]["col"], "color":plt_dict["MODIS"]["col"]},
                capprops = { "color":plt_dict["MODIS"]["col"]},
                whiskerprops = { "color":plt_dict["MODIS"]["col"]},
                medianprops = { "color":"w"},
                )


    # Customize the appearance of the boxplot
    ax.set_xlabel('Elevation (m)')
    ax.set_ylabel('Snow probability')
    ax.set_title('Snow probability per elevation - '+reg)
    ax.set_xticks(bin_edges)
    ax.set_xticklabels([str(int(x)) for x in bin_edges])
    ax.legend([bp_simu["boxes"][0], bp_MODIS["boxes"][0]], ['Simu.', 'Obs. MODIS'], loc='upper right')
    ax.axis([600 ,3300, 0, 365])
    ax.grid(lw=0.4)    
    
    plt.savefig("/home/hola/datos_seis/port_transfert/smod_"+reg+"_elevation_report.png")
    plt.savefig("/home/hola/datos_seis/port_transfert/smod_"+reg+"_elevation_report.svg")
    plt.close()
    
    
    
#################################################### 
         ## PLOT SCA = f(dohy)  ENSEMBLE ##
#################################################### 

run="ens"
exp="all"
for reg in reg_list:
    in_SCA_MODIS="/home/hola/datos_seis/nacional_parques/data/MODIS/Snow_Cover_Area_MODIS_"+reg+".csv"
    SCA = pd.read_csv( in_SCA_MODIS, names=["time","sca"], header=0)
    time_dt = [dt.datetime.strptime(dd, "%Y_%m_%d") for dd in SCA.time]
    SCA["time"] = time_dt
    SCA["hyear"] = [yy if mm>=9 else yy-1 for yy,mm in zip(SCA.time.dt.year, SCA.time.dt.month) ]

    SCA = SCA.set_index(SCA.time, drop = True).to_xarray()

    MODIS_filtered = SCA.where(SCA.hyear.isin(common_years_hist), drop = True)
    
    plt.figure(figsize=(8,3))

    print(reg+" - "+run)
    
    sca = data_ens_qm["ens"]["all"][reg]["sca"]["mean"].sel(time=slice("2000-09-01","2004-08-31"))
    ts_sca = sca.sum(dim=("lat","lon"))*250*250/1e6

    plt.plot(ts_sca.time, ts_sca.sca, color=plt_dict["SIMU"]["col"], lw=2)
    plt.ylabel("Snow cover area (km2)")
    
    sca = data_ens_qm["ens"]["all"][reg]["sca"]["mean_mstd"].sel(time=slice("2000-09-01","2004-08-31"))
    ts_sca_minus = sca.sum(dim=("lat","lon"))*250*250/1e6
    
    sca = data_ens_qm["ens"]["all"][reg]["sca"]["mean_pstd"].sel(time=slice("2000-09-01","2004-08-31"))
    ts_sca_plus = sca.sum(dim=("lat","lon"))*250*250/1e6

    plt.fill_between(ts_sca_plus.time, ts_sca_minus.sca, ts_sca_plus.sca, color=plt_dict["SIMU"]["col"], alpha=0.5)
    
    plt.grid(True, lw=0.3)

    plt.plot(MODIS_filtered.time, MODIS_filtered.sca, plt_dict["MODIS"]["col"], lw=2)
    plt.ylim([0, plt.gca().get_ylim()[1]])
        
    plt.savefig("/home/hola/datos_seis/port_transfert/snod_"+reg+"_spatialmean_tsdohy_report.png")
    plt.savefig("/home/hola/datos_seis/port_transfert/snod_"+reg+"_spatialmean_tsdohy_report.svg")
    plt.close()
        
#################################################### 
        ## PLOT DSWE = f(DTAIR) ##
#################################################### 

#data_ens_qm["ens"]["all"][reg][var] = var_concat_clean
# put that in a parallel function for all var
delta_data = {}
run = "ens"
exp = "all"

months_to_select = [12, 1, 2, 3, 4, 5] # list of winter and spring month

# # ALL YEAR - CALCULATED LOCALLY
# dT_2050 = {'AIGUE': {'rcp26': 1.552, 'rcp45': 1.876, 'rcp85': 2.405},
#            'GUADA': {'rcp26': 1.535, 'rcp45': 1.862, 'rcp85': 2.371},
#            'ORDES': {'rcp26': 1.616, 'rcp45': 1.948, 'rcp85': 2.477},
#            'PICOS': {'rcp26': 1.115, 'rcp45': 1.333, 'rcp85': 1.698},
#            'SIERR': {'rcp26': 1.482, 'rcp45': 1.821, 'rcp85': 2.343}}
# WINTER+SPRING MONTHS - CALCULATED LOCALLY
dT_2050 = {'AIGUE': {'rcp26': 1.306, 'rcp45': 1.523, 'rcp85': 2.014},
           'GUADA': {'rcp26': 1.316, 'rcp45': 1.52, 'rcp85': 1.991},
           'ORDES': {'rcp26': 1.365, 'rcp45': 1.587, 'rcp85': 2.058},
           'PICOS': {'rcp26': 1.088, 'rcp45': 1.254, 'rcp85': 1.634},
           'SIERR': {'rcp26': 1.282, 'rcp45': 1.484, 'rcp85': 2.005},
           'TEIDE': {'rcp26': 1.453, 'rcp45': 1.643, 'rcp85': 2.188}}

for reg in reg_list:
    delta_data[reg] = {}
    for var in ["mean", "mean_pstd", "mean_mstd"]:
        delta_data[reg][var] = {}
        print(reg+" - "+run)
    
        tt = data_ens_qm[run]["all"][reg]["tair"][var].sel(time=slice("1974-09-01","2004-08-31"))
        tair_ref = tt.sel(time=tt['time.month'].isin( months_to_select )).mean().tair.values
    
        tt = data_ens_qm[run]["all"][reg]["swed"][var].sel(time=slice("1974-09-01","2004-08-31"))
        SWE_ref = tt.sel(time=tt['time.month'].isin( months_to_select )).mean().swed.values
        
        tt = data_ens_qm[run]["all"][reg]["prec"][var].sel(time=slice("1974-09-01","2004-08-31"))
        prec_ref = tt.sel(time=tt['time.month'].isin( months_to_select )).mean().prec.values
    
        for exp in ["all"]:
            dtair_list = [0] # create point for ref, i.e. 0 change
            
            dSWE_list = [0]
            dSWE_rel_list = [100]
    
            dprec_list = [0]
            dprec_rel_list = [100]
            
            for (time_min, time_max) in [("2005-09-01","2035-08-31"), ("2035-09-01","2065-08-31"), ("2065-09-01","2095-08-31")]:
                tt = data_ens_qm[run]["all"][reg]["tair"][var].sel(time=slice(time_min, time_max))
                tair = tt.sel(time=tt['time.month'].isin( months_to_select )).mean().tair.values
            
                tt = data_ens_qm[run]["all"][reg]["swed"][var].sel(time=slice(time_min, time_max))
                SWE = tt.sel(time=tt['time.month'].isin( months_to_select )).mean().swed.values
                
                tt = data_ens_qm[run]["all"][reg]["prec"][var].sel(time=slice(time_min, time_max))
                prec = tt.sel(time=tt['time.month'].isin( months_to_select )).mean().prec.values
    
                
                dtair_list.append(tair-tair_ref)
                dSWE_list.append(SWE-SWE_ref)
                dSWE_rel_list.append(int(100*SWE/SWE_ref))
                dprec_list.append(prec-prec_ref)
                dprec_rel_list.append(int(100*(prec)/prec_ref))
    
            delta_data[reg][var][exp] = {"dtair":dtair_list, "dSWE":dSWE_list, "dSWE_rel":dSWE_rel_list, "dprec":dprec_list, "dprec_rel":dprec_rel_list}
            
# DSWE = f( DTAIR)
var_name = "mean"
plt.figure()
for reg in reg_list:
    plt.plot(delta_data[reg][var_name][exp]["dtair"], delta_data[reg][var_name][exp]["dSWE"], marker=".", color=plt_dict[reg]["col"], ms=12, mew=0, lw=0.8, label=reg)

plt.ylabel("dSWE (m)")
plt.xlabel("dT (ºC)")
plt.xlim([0, 5])
plt.ylim([0.01, -0.21])
plt.grid(True, lw=0.5)
plt.legend()

plt.savefig("/home/hola/datos_seis/port_transfert/dtair_dswed_t_ens_"+reg+".png")
plt.savefig("/home/hola/datos_seis/port_transfert/dtair_dswed_t_ens_"+reg+".svg")

# DSWE_REL = f( DTAIR)
plt.figure()
for reg in reg_list:
    plt.plot(delta_data[reg][var_name][exp]["dtair"], delta_data[reg][var_name][exp]["dSWE_rel"], marker=".", color=plt_dict[reg]["col"], ms=12, mew=0, lw=0.8, label=reg)
    
plt.ylabel("dSWE (%)")
plt.xlabel("dT (ºC)")
plt.axis((0, 5, 0, 100))
plt.grid(True, lw=0.5)
plt.legend()

plt.savefig("/home/hola/datos_seis/port_transfert/dtair_dswedrel_t_ens_"+reg+"0.png")
plt.savefig("/home/hola/datos_seis/port_transfert/dtair_dswedrel_t_ens_"+reg+"0.svg")
plt.close()

# DSWE_REL = f( DTAIR) - 1 PLT BY REGION
for reg in reg_list:
    plt.figure()
    plt.plot(delta_data[reg][var_name][exp]["dtair"], delta_data[reg][var_name][exp]["dSWE_rel"], marker=".", color=plt_dict[reg]["col"], ms=12, mew=0, lw=0.8, label=reg)
    plt.plot(delta_data[reg]["mean_mstd"][exp]["dtair"], delta_data[reg]["mean_mstd"][exp]["dSWE_rel"], marker=".", color=plt_dict[reg]["col"], ms=12, mew=0, lw=0.8, label=reg)
    plt.plot(delta_data[reg]["mean_pstd"][exp]["dtair"], delta_data[reg]["mean_pstd"][exp]["dSWE_rel"], marker=".", color=plt_dict[reg]["col"], ms=12, mew=0, lw=0.8, label=reg)
    
    for ii,run2 in enumerate(["rcp26", "rcp45", "rcp85"]):
        if ii==0:
            plt.vlines(dT_2050[reg][run2], 0, 100, ls=":", lw=4, color=plt_dict[reg]["col"], label= "2050 likely dT")
        else:
            plt.vlines(dT_2050[reg][run2], 0, 100, ls=":", lw=4, color=plt_dict[reg]["col"])
    plt.ylabel("dSWE (%)")
    plt.xlabel("dT (ºC)")
    plt.axis((0, 5, 0, 100))
    plt.grid(True, lw=0.5)
    plt.legend()
    
    plt.savefig("/home/hola/datos_seis/port_transfert/dtair_dswedrel_t_ens_"+reg+".png")
    plt.savefig("/home/hola/datos_seis/port_transfert/dtair_dswedrel_t_ens_"+reg+".svg")
    plt.close()
    
# DPREC_REL = f( DTAIR)
plt.figure()
for reg in reg_list:
    plt.plot(delta_data[reg][var_name][exp]["dtair"], delta_data[reg][var_name][exp]["dprec_rel"], marker=".", color=plt_dict[reg]["col"], ms=12, mew=0, lw=0.8, label=reg)
       
plt.ylabel("dprec (%)")
plt.xlabel("dT (ºC)")
plt.axis((0, 5, 50, 110))
plt.grid(True, lw=0.5)
plt.legend()

plt.savefig("/home/hola/datos_seis/port_transfert/dtair_dprecrel_t_ens_"+reg+".png")
plt.savefig("/home/hola/datos_seis/port_transfert/dtair_dprecrel_t_ens_"+reg+".svg")
plt.close()

#################################################### 
        ## PLOT DSWE = f(DTAIR) ## MAP
#################################################### 

#data_ens_qm["ens"]["all"][reg][var] = var_concat_clean
# put that in a parallel function for all var
delta_data_map = {}
run = "ens"
exp = "all"

for reg in reg_list:
    delta_data_map[reg] = {}
    for var in ["mean", "mean_pstd", "mean_mstd"]:
        delta_data_map[reg][var] = {}
        print(reg+" - "+run)
        tt = data_ens_qm[run]["all"][reg]["tair"][var].sel(time=slice("1974-09-01","2004-08-31"))
        tair_ref = tt.sel(time=tt['time.month'].isin( months_to_select )).mean(dim="time").tair.assign_coords({"dtair":0})
        
        tt = data_ens_qm[run]["all"][reg]["swed"][var].sel(time=slice("1974-09-01","2004-08-31"))
        SWE_ref  = tt.sel(time=tt['time.month'].isin( months_to_select )).mean(dim="time").swed.assign_coords({"dtair":0})
    
        tt = data_ens_qm[run]["all"][reg]["prec"][var].sel(time=slice("1974-09-01","2004-08-31"))
        prec_ref = tt.sel(time=tt['time.month'].isin( months_to_select )).mean(dim="time").prec.assign_coords({"dtair":0})
        
        smod_ref = (data_ens_qm[run]["all"][reg]["smod"][var].sel(hyear=slice("1974","2003"))).mean(dim="hyear").smod.assign_coords({"dtair":0})
        
        SWE_list = [SWE_ref.copy(deep=True)]
        smod_list = [smod_ref.copy(deep=True)]
        for exp in ["all"]:
            zero_xr = (0*data_ens_qm[run][exp][reg]["tair"][var].sel(time=slice("2005-09-01","2035-09-03")).mean(dim="time").tair).assign_coords({"dtair":0}) #, "dprec":0})
            dtair_list     = [zero_xr.copy(deep=True)] # create point for ref, i.e. 0 change
            
            dSWE_list      = [zero_xr.copy(deep=True)]
            dSWE_rel_list  = [zero_xr.copy(deep=True)]
    
            dprec_list     = [zero_xr.copy(deep=True)]
            dprec_rel_list = [zero_xr.copy(deep=True)]
            
            zero_xr = (0*data_ens_qm[run][exp][reg]["smod"][var].sel(hyear=slice("2005","2006")).mean(dim="hyear").smod).assign_coords({"dtair":0}) #, "dprec":0})
            dsmod_list     = [zero_xr.copy(deep=True)]
    
            for ii, (time_min, time_max) in enumerate([("2005-09-01","2035-08-31"), ("2035-09-01","2065-08-31"), ("2065-09-01","2095-08-31")]):
                dtair_coord = delta_data[reg][var][exp]["dtair"][ii+1] # skip 0
                dprec_coord = delta_data[reg][var][exp]["dprec"][ii+1] # skip 0
                
                tt = data_ens_qm[run]["all"][reg]["tair"][var].sel(time=slice(time_min, time_max))
                tair = tt.sel(time=tt['time.month'].isin( months_to_select )).mean(dim="time").tair.assign_coords({"dtair":dtair_coord})
                
                tt = data_ens_qm[run]["all"][reg]["swed"][var].sel(time=slice(time_min, time_max))
                SWE = tt.sel(time=tt['time.month'].isin( months_to_select )).mean(dim="time").swed.assign_coords({"dtair":dtair_coord})
            
                tt = data_ens_qm[run]["all"][reg]["prec"][var].sel(time=slice(time_min, time_max))
                prec = tt.sel(time=tt['time.month'].isin( months_to_select )).mean(dim="time").prec.assign_coords({"dtair":dtair_coord})
                
                SWE_list.append(SWE)
    
      
                   
                dtair_list.append( (tair - tair_ref).assign_coords({"dtair":dtair_coord})) #, "dprec":dprec_coord}) )
                dSWE_list.append( (SWE - SWE_ref ).assign_coords({"dtair":dtair_coord})) #, "dprec":dprec_coord} ) )
                dSWE_rel_list.append( ( 100*( SWE - SWE_ref ) / SWE_ref ).astype(int).assign_coords({"dtair":dtair_coord})) #, "dprec":dprec_coord}) )
                dprec_list.append( (prec-prec_ref).assign_coords({"dtair":dtair_coord})) #, "dprec":dprec_coord}) )
                dprec_rel_list.append( ( 100*( prec - prec_ref ) / prec_ref ).astype(int).assign_coords({"dtair":dtair_coord})) #, "dprec":dprec_coord}) )
    
                hyear_min, hyear_max = time_min[:4], time_max[:4]
                smod = data_ens_qm[run][exp][reg]["smod"][var].sel(hyear=slice(hyear_min, hyear_max)).mean(dim="hyear").smod
                
                smod_list.append(  smod.assign_coords({"dtair":dtair_coord}) )
                dsmod_list.append( (smod-smod_ref).assign_coords({"dtair":dtair_coord}) )
    
            delta_data_map[reg][var][exp] = {"dtair":dtair_list, "dSWE":dSWE_list, "dSWE_rel":dSWE_rel_list, "dprec":dprec_list, "dprec_rel":dprec_rel_list, "SWE":SWE_list, "smod":smod_list, "dsmod":dsmod_list}


# PLOT FOR SWE
min_swe_mask = 0.1
var_name = "mean"

for reg in reg_list:
    print(reg)
    (x_parc, y_parc) = contour_parc[reg]
    
    in_HS = "/home/hola/datos_seis/nacional_parques/data/COP30/250m/limite_"+reg+"_COP30_250m_UTM_DEM_HS.tif"
    HS = xr.open_dataset( in_HS )

    dswed_dtair = xr.concat(delta_data_map[reg][var_name]["all"]["dSWE_rel"], dim = "dtair") # , "dprec"] )
    swed_dtair  = xr.concat(delta_data_map[reg][var_name]["all"]["SWE"], dim = "dtair") # , "dprec"] )

    dSWE_0 = dswed_dtair.interp( coords = {"dtair":0}, method = "nearest")
    SWE_0  = swed_dtair.interp( coords = {"dtair" :0}, method = "nearest")
        
    dSWE_rcp26 = dswed_dtair.interp( coords = {"dtair":dT_2050[reg]["rcp26"]}, method = "linear")
    SWE_rcp26  = swed_dtair.interp( coords = {"dtair":dT_2050[reg]["rcp26"]}, method = "linear")
    
    dSWE_rcp45 = dswed_dtair.interp( coords = {"dtair":dT_2050[reg]["rcp45"]}, method = "linear")
    SWE_rcp45  = swed_dtair.interp( coords = {"dtair":dT_2050[reg]["rcp45"]}, method = "linear")
    
    dSWE_rcp85 = dswed_dtair.interp( coords = {"dtair":dT_2050[reg]["rcp85"]}, method = "linear")
    SWE_rcp85  = swed_dtair.interp( coords = {"dtair":dT_2050[reg]["rcp85"]}, method = "linear")
    

    for ii, (SWE, dSWE) in enumerate([(SWE_rcp26, dSWE_rcp26), (SWE_rcp45, dSWE_rcp45), (SWE_rcp85, dSWE_rcp85)]):
        print(str(ii+1)+"/3")
        fig = plt.figure( figsize=( 9, 4) )
        
        ax1 = plt.subplot(131)
        data2im = np.flipud(SWE_0.to_numpy())
        data2im = np.ma.masked_where(data2im < min_swe_mask, data2im)    
        im1 = ax1.imshow( HS.band_data.squeeze(), cmap='Greys_r', vmin=0, vmax=255, extent = post_tools.get_xr_im_extent(HS), origin="upper", interpolation="nearest", alpha=0.8)
        im2 = ax1.imshow( data2im, cmap='viridis', vmin = 0 , vmax = 2, extent = post_tools.get_xr_im_extent(HS), origin="upper", interpolation="nearest")
        ax1.set_title('Reference period (1974-2004)',fontsize=10, horizontalalignment="center")
    
        ax2 = plt.subplot(132)
        data2im = np.flipud(SWE_rcp26.to_numpy())
        data2im = np.ma.masked_where(data2im < min_swe_mask, data2im)    
        im1 = ax2.imshow( HS.band_data.squeeze(), cmap='Greys_r', vmin=0, vmax=255, extent = post_tools.get_xr_im_extent(HS), origin="upper", interpolation="nearest", alpha=0.8)
        im3 = ax2.imshow( data2im, cmap='viridis', vmin = 0 , vmax = 2, extent = post_tools.get_xr_im_extent(HS), origin="upper", interpolation="nearest")
        ax2.set_title('2050 (2035-2064)',fontsize=10, horizontalalignment="center")
    
        ax3 = plt.subplot(133)
        data2im = np.flipud(dSWE_rcp26.where(SWE_0 > min_swe_mask, np.nan).to_numpy())
        im1 = ax3.imshow( HS.band_data.squeeze(), cmap='Greys_r', vmin=0, vmax=255, extent = post_tools.get_xr_im_extent(HS), origin="upper", interpolation="nearest", alpha=0.8)
        im4 = ax3.imshow( data2im, cmap='RdYlBu', vmin = -50, vmax = 50, extent = post_tools.get_xr_im_extent(HS), origin="upper", interpolation="nearest")
        ax3.set_title('2050 minus reference period',fontsize=10, horizontalalignment="center")
    
        for ax in fig.get_axes():
            ax.set_xticks([])
            ax.set_yticks([])
            ax.plot(x_parc, y_parc, color=plt_dict[reg]["col"], lw=0.8) #color="#f6cd61", lw=0.5 )
    
        scalebar = ScaleBar(0.001, "km", length_fraction=0.25, width_fraction=0.03, fixed_value=10, font_properties={"size":9})
        plt.gca().add_artist(scalebar)
        
        fig.subplots_adjust(bottom=0.1) #, right=0.8)  # Adjust as needed
        cbar_ax = fig.add_axes([0.15, 0.25, 0.3, 0.02])
        cb = fig.colorbar(im2, cax=cbar_ax, orientation='horizontal')
        cb.ax.set_title('Snow mass (m.w.e.)',fontsize=10, horizontalalignment="center")
    
        cbar_ax = fig.add_axes([0.50, 0.25, 0.3, 0.02])
        cb = fig.colorbar(im4, cax=cbar_ax, orientation='horizontal')
        cb.ax.set_title('Snow mass change (%)',fontsize=10, horizontalalignment="center")
        
        plt.tight_layout()    
        
        plt.savefig("/home/hola/datos_seis/port_transfert/swed_dswed_2050_"+str(ii)+"_temp_"+reg+"_report.png")
        plt.savefig("/home/hola/datos_seis/port_transfert/swed_dswed_2050_"+str(ii)+"_temp_"+reg+"_report.svg")
        plt.close()
        
        
# PLOT FOR SMOD
min_smod_mask = 100
for reg in reg_list:
    print(reg)
    (x_parc, y_parc) = contour_parc[reg]
    
    in_HS = "/home/hola/datos_seis/nacional_parques/data/COP30/250m/limite_"+reg+"_COP30_250m_UTM_DEM_HS.tif"
    HS = xr.open_dataset( in_HS )

    dsmod_dtair = xr.concat(delta_data_map[reg][var_name]["all"]["dsmod"], dim = "dtair") # , "dprec"] )
    smod_dtair  = xr.concat(delta_data_map[reg][var_name]["all"]["smod"], dim = "dtair") # , "dprec"] )

    dsmod_0 = dsmod_dtair.interp( coords = {"dtair":0}, method = "nearest")
    smod_0  = smod_dtair.interp( coords = {"dtair" :0}, method = "nearest")
        
    dsmod_rcp26 = dsmod_dtair.interp( coords = {"dtair":dT_2050[reg]["rcp26"]}, method = "linear")
    smod_rcp26  = smod_dtair.interp( coords = {"dtair":dT_2050[reg]["rcp26"]}, method = "linear")
    
    dsmod_rcp45 = dsmod_dtair.interp( coords = {"dtair":dT_2050[reg]["rcp45"]}, method = "linear")
    smod_rcp45  = smod_dtair.interp( coords = {"dtair":dT_2050[reg]["rcp45"]}, method = "linear")
    
    dsmod_rcp85 = dsmod_dtair.interp( coords = {"dtair":dT_2050[reg]["rcp85"]}, method = "linear")
    smod_rcp85  = smod_dtair.interp( coords = {"dtair":dT_2050[reg]["rcp85"]}, method = "linear")
    

    for ii, (smod, dsmod) in enumerate([(smod_rcp26, dsmod_rcp26), (smod_rcp45, dsmod_rcp45), (smod_rcp85, dsmod_rcp85)]):
        fig = plt.figure( figsize=( 9, 4) )
        
        ax1 = plt.subplot(131)
        data2im = np.flipud(smod_0.to_numpy())
        data2im = np.ma.masked_where(data2im < min_smod_mask, data2im)    
        im1 = ax1.imshow( HS.band_data.squeeze(), cmap='Greys_r', vmin=0, vmax=255, extent = post_tools.get_xr_im_extent(HS), origin="upper", interpolation="nearest", alpha=0.8)
        im2 = ax1.imshow( data2im, cmap='viridis', vmin = 100 , vmax = 365, extent = post_tools.get_xr_im_extent(HS), origin="upper", interpolation="nearest")
        ax1.set_title('Reference period (1974-2004)',fontsize=10, horizontalalignment="center")
    
        ax2 = plt.subplot(132)
        data2im = np.flipud(smod_rcp26.to_numpy())
        data2im = np.ma.masked_where(data2im < min_smod_mask, data2im)    
        im1 = ax2.imshow( HS.band_data.squeeze(), cmap='Greys_r', vmin=0, vmax=255, extent = post_tools.get_xr_im_extent(HS), origin="upper", interpolation="nearest", alpha=0.8)
        im3 = ax2.imshow( data2im, cmap='viridis', vmin = 100 , vmax = 365, extent = post_tools.get_xr_im_extent(HS), origin="upper", interpolation="nearest")
        ax2.set_title('2050 (2035-2064)',fontsize=10, horizontalalignment="center")
    
        ax3 = plt.subplot(133)
        data2im = np.flipud(dsmod_rcp26.where(smod_0 > min_smod_mask, np.nan).to_numpy())
        im1 = ax3.imshow( HS.band_data.squeeze(), cmap='Greys_r', vmin=0, vmax=255, extent = post_tools.get_xr_im_extent(HS), origin="upper", interpolation="nearest", alpha=0.8)
        im4 = ax3.imshow( data2im, cmap='RdYlBu', vmin = -50, vmax = 50, extent = post_tools.get_xr_im_extent(HS), origin="upper", interpolation="nearest")
        ax3.set_title('2050 minus reference period',fontsize=10, horizontalalignment="center")
    
        for ax in fig.get_axes():
            ax.set_xticks([])
            ax.set_yticks([])
            ax.plot(x_parc, y_parc, color=plt_dict[reg]["col"], lw=0.8) #color="#f6cd61", lw=0.5 )
    
        scalebar = ScaleBar(0.001, "km", length_fraction=0.25, width_fraction=0.03, fixed_value=10, font_properties={"size":9})
        plt.gca().add_artist(scalebar)
        
        #fig.subplots_adjust(right=0.7)
        fig.subplots_adjust(bottom=0.1) #, right=0.8)  # Adjust as needed
        cbar_ax = fig.add_axes([0.15, 0.25, 0.3, 0.02])
        cb = fig.colorbar(im2, cax=cbar_ax, orientation='horizontal')
    #    cb.set_ticks([0, 25, 50, 75, 100], labels=[0, 25, 50, 75, 100], fontsize=10)
        cb.ax.set_title('Snow melt-out date (day, 0=1st sept.)',fontsize=10, horizontalalignment="center")
    
    #    fig.subplots_adjust(right=0.8)
        cbar_ax = fig.add_axes([0.50, 0.25, 0.3, 0.02])
        cb = fig.colorbar(im4, cax=cbar_ax, orientation='horizontal')
    #    cb.set_ticks([0, 25, 50, 75, 100], labels=[0, 25, 50, 75, 100], fontsize=10)
        cb.ax.set_title('Snow melt-out change day)',fontsize=10, horizontalalignment="center")
        
        plt.tight_layout()    
        
        plt.savefig("/home/hola/datos_seis/port_transfert/smod_dsmod_2050_"+str(ii)+"_temp_"+reg+"_report.png")
        plt.savefig("/home/hola/datos_seis/port_transfert/smod_dsmod_2050_"+str(ii)+"_temp_"+reg+"_report.svg")
        plt.close()
    
    
    
# OUTPUT STAT
out_stat = {}
for reg in reg_list:
    print(reg)
    out_stat[reg] = {}
    for var in ["mean", "mean_pstd", "mean_mstd"]:
        out_stat[reg][var] = {"in_PN":{}, "all":{} }

        # dswed_dtair = xr.concat(delta_data_map[reg][var]["all"]["dSWE_rel"], dim = "dtair") # , "dprec"] )
        swed_dtair  = xr.concat(delta_data_map[reg][var]["all"]["SWE"], dim = "dtair") # , "dprec"] )
    
        # dSWE_0 = dswed_dtair.interp( coords = {"dtair":0}, method = "nearest")
        SWE_0  = swed_dtair.interp( coords = {"dtair" :0}, method = "nearest")
        out_stat[reg][var]["in_PN"]["ref"] = SWE_0.where(SWE_0.MSK==1).mean().values.flatten()[0]
        out_stat[reg][var]["all"]["ref"] = SWE_0.where(SWE_0.MSK==1).mean().values.flatten()[0]

        for exp in ["rcp26", "rcp45", "rcp85"]:
            # dSWE = dswed_dtair.interp( coords = {"dtair":dT_2050[reg][exp]}, method = "linear")
            SWE = swed_dtair.interp( coords = {"dtair":dT_2050[reg][exp]}, method = "linear")
            # SIMU
            out_stat[reg][var]["in_PN"][exp] = SWE.where(SWE.MSK==1).mean().values.flatten()[0]
            out_stat[reg][var]["all"][exp]   = SWE.where(SWE.MSK==1).mean().values.flatten()[0]

pd.DataFrame(out_stat).T.to_csv("/home/hola/datos_seis/port_transfert/swed_PN_2050_"+reg+".csv")

# loading precedent search
with open("/home/hola/datos_seis/nacional_parques/prod/pkl/delta_data_"+reg+".pkl", "wb") as handle:
    pickle.dump( delta_data, handle, protocol = pickle.HIGHEST_PROTOCOL)  

# PLOT ALL 
plt.figure(figsize=(6,3))
for ii, reg in enumerate(out_stat.keys()):
    plt.bar(ii-0.2, out_stat[reg]["mean"]["in_PN"]["ref"]  , 0.15, color=plt_dict[reg]["col"])
    plt.bar(ii+0.2, out_stat[reg]["mean"]["in_PN"]["rcp45"], 0.15, color=plt_dict[reg]["col"])
    plt.plot([ii+0.2, ii+0.2], [out_stat[reg]["mean"]["in_PN"]["rcp26"],out_stat[reg]["mean"]["in_PN"]["rcp85"]], "k") # color=plt_dict[reg]["col"])

plt.xticks(np.arange(0,5), ["TEIDE", "", "", "", ""])
plt.ylim((0, 0.75))
plt.grid(axis="y", lw=0.3)
plt.title("Snow mass change")
plt.ylabel("Mean snow mass - dec. to jun. (m.w.e.)")
plt.savefig("/home/hola/datos_seis/port_transfert/dswe_all_reg_report_"+reg+".png")
plt.savefig("/home/hola/datos_seis/port_transfert/dswe_all_reg_report_"+reg+".svg")

plt.ylim((0, 0.02))
plt.savefig("/home/hola/datos_seis/port_transfert/dswe_all_reg_report_"+reg+"_zoom.png")
plt.savefig("/home/hola/datos_seis/port_transfert/dswe_all_reg_report_"+reg+"_zoom.svg")

plt.close()

    

# PLOT AGAINST ELEVATION
# PLOT FOR SMOD
min_smod_mask = 100
for reg in reg_list:
    print(reg)
    (x_parc, y_parc) = contour_parc[reg]
    
    in_HS = "/home/hola/datos_seis/nacional_parques/data/COP30/250m/limite_"+reg+"_COP30_250m_UTM_DEM_HS.tif"
    HS = xr.open_dataset( in_HS )

    smod_dtair  = xr.concat(delta_data_map[reg][var_name]["all"]["smod"], dim = "dtair") # , "dprec"] )

    smod_0  = smod_dtair.interp( coords = {"dtair" :0}, method = "nearest")
        
    smod_rcp45  = smod_dtair.interp( coords = {"dtair":dT_2050[reg]["rcp45"]}, method = "linear")
    
    
    
    # REF PERIOD    
    data_grouped = smod_0.groupby_bins( 'DEM', bin_edges)
    
    boxplot_data, bin2plot = [], []
    for name, group in data_grouped:
        boxplot_data.append(group.values)
        bin2plot.append(name.mid)
        
    # Create the boxplot
    fig, ax = plt.subplots()
    bp_simu_ref = ax.boxplot(boxplot_data, positions=np.array(bin2plot)-35, widths=55, patch_artist=True, whis=1e9,
                boxprops = { "facecolor":plt_dict["SIMU"]["col"], "color":"k"},
                capprops = { "color":plt_dict["SIMU"]["col"]},
                whiskerprops = { "color":plt_dict["SIMU"]["col"]},
                medianprops = { "color":"w"},
                )
    
    # 2050 PERIOD    
    data_grouped = smod_rcp45.groupby_bins( 'DEM', bin_edges)
    
    boxplot_data, bin2plot = [], []
    for name, group in data_grouped:
        boxplot_data.append(group.values)
        bin2plot.append(name.mid)
        
    # Create the boxplot
    bp_simu = ax.boxplot(boxplot_data, positions=np.array(bin2plot)+35, widths=55, patch_artist=True, whis=1e9,
                boxprops = { "facecolor":plt_dict["SIMU"]["col"], "color":plt_dict["SIMU"]["col"]},
                capprops = { "color":plt_dict["SIMU"]["col"]},
                whiskerprops = { "color":plt_dict["SIMU"]["col"]},
                medianprops = { "color":"w"},
                )
    
    # Customize the appearance of the boxplot
    ax.set_xlabel('Elevation (m)')
    ax.set_ylabel('Snow melt-out date (0=1st sept.)')
    ax.set_title('Snow melt-out date per elevation - '+reg)
    ax.set_xticks(bin_edges)
    ax.set_xticklabels([str(int(x)) for x in bin_edges])
    ax.legend([bp_simu_ref["boxes"][0], bp_simu["boxes"][0]], ['Ref. period', '2050'], loc='upper right')
    ax.axis([600 ,3300, 0, 365])
    ax.grid(lw=0.4)  
    
    plt.savefig("/home/hola/datos_seis/port_transfert/smod_ref_2050_elevation_"+reg+"_report.png")
    plt.savefig("/home/hola/datos_seis/port_transfert/smod_ref_2050_elevation_"+reg+"_report.svg")
    

#################################################### 
        ## dSWE = f(POLAR PLOT) ## 
#################################################### 


#################################################### 
        ## PLOT SCA, TEMP = f(time) ## 
#################################################### 

gcm_rcm_list = list(data_all_run_qm.keys())
out_stat = {}
for reg in reg_list:
    print(reg)

    plt.figure(figsize=(8,3))

    ############
    # TEMPERATURE
    # plot ens mean
    plt.subplot(211)
    tt = data_ens_qm["ens"]["all"][reg]["tair"]["mean"]
    ts = tt.mean(dim=("lat","lon")).groupby("hyear").mean()
    ts = ts-ts.sel(hyear=1989) # ANOMALY COMPARED TO REF PERIOD

        
    tt = data_ens_qm["ens"]["all"][reg]["tair"]["mean_mstd"]
    ts_min = tt.mean(dim=("lat","lon")).groupby("hyear").mean()
    ts_min = ts_min - ts_min.sel(hyear=1989) # ANOMALY COMPARED TO REF PERIOD
            
    tt = data_ens_qm["ens"]["all"][reg]["tair"]["mean_pstd"]
    ts_max = tt.mean(dim=("lat","lon")).groupby("hyear").mean()
    ts_max = ts_max - ts_max.sel(hyear=1989) # ANOMALY COMPARED TO REF PERIOD
    
    plt.plot(ts.hyear, ts.tair, lw=2, ms=6, mew=0, color=plt_dict["SIMU"]["col"])
    plt.fill_between(ts_max.hyear, ts_min.tair, ts_max.tair, color=plt_dict["SIMU"]["col"], alpha=0.5)
    
    plt.ylim([-5, 15])
    plt.ylabel("Mean yearly temperature (ºC)")
    plt.grid(True, lw=0.4)
    
    # ############
    # # PRECIP
    # # plot ens mean
    # plt.subplot(312)
    # tt = data_ens_qm["ens"]["all"][reg]["prec"]["mean"]
    # ts = tt.mean(dim=("lat","lon")).groupby("hyear").sum()
    #
    # # tt = data_ens_qm["ens"]["all"][reg]["prec"]["mean_mstd"]
    # # ts_min = tt.mean(dim=("lat","lon")).groupby("hyear").sum()
    # #
    # # tt = data_ens_qm["ens"]["all"][reg]["prec"]["mean_pstd"]
    # # ts_max = tt.mean(dim=("lat","lon")).groupby("hyear").sum()
    #
    # plt.plot(ts.hyear, ts.prec, lw=2, ms=6, mew=0, color=plt_dict["SIMU"]["col"])
    # # plt.fill_between(ts_max.hyear, ts_min.prec, ts_max.prec, color=plt_dict["SIMU"]["col"], alpha=0.5) => does not work for precip
    #
    # plt.ylim([0, 3])
    # plt.ylabel("Mean yearly precipitation (m)")
    # plt.grid(True, lw=0.4)
        
    ############
    # SCA
    # plot ens mean
    plt.subplot(212)
    tt = data_ens_qm["ens"]["all"][reg]["sca"]["mean"]
    ts = tt.sum(dim=("lat","lon")).groupby("hyear").mean()*250*250/1e6
        
    tt = data_ens_qm["ens"]["all"][reg]["sca"]["mean_mstd"]
    ts_min = tt.sum(dim=("lat","lon")).groupby("hyear").mean()*250*250/1e6
            
    tt = data_ens_qm["ens"]["all"][reg]["sca"]["mean_pstd"]
    ts_max = tt.sum(dim=("lat","lon")).groupby("hyear").mean()*250*250/1e6
    
    
    plt.plot(ts.hyear, ts.sca, lw=2, ms=6, mew=0, color=plt_dict["SIMU"]["col"])
    plt.fill_between(ts_max.hyear, ts_min.sca, ts_max.sca, color=plt_dict["SIMU"]["col"], alpha=0.5)
   
    # Read MODIS data
    in_SCA_MODIS="/home/hola/datos_seis/nacional_parques/data/MODIS/Snow_Cover_Area_MODIS_"+reg+".csv"
    SCA = pd.read_csv(in_SCA_MODIS,names=["time","sca"],header=0)
    time_dt = [dt.datetime.strptime(dd, "%Y_%m_%d") for dd in SCA.time]
    SCA["time"]=time_dt
    SCA["hyear"] = [yy if mm<12 else yy+1 for yy,mm in zip(SCA.time.dt.year, SCA.time.dt.month) ]
    ts_MODIS = SCA.groupby("hyear").mean()

    plt.ylim([0, 300])
    plt.ylabel("Yearly mean SCA (km2)")
    plt.grid(True, lw=0.4)
    
    plt.savefig("/home/hola/datos_seis/port_transfert/temp_sca_"+reg+"_qm_report.png")
    plt.savefig("/home/hola/datos_seis/port_transfert/temp_sca_"+reg+"_qm_report.svg")
    plt.close()
    
    

#################################################### 
        ## PLOT SWED, TEMP = f(time) 30 yr rolling mean ## 
#################################################### 

gcm_rcm_list = list(data_all_run_qm.keys())
out_stat = {}
for reg in reg_list:
    print(reg)

    plt.figure(figsize=(8,4))

    ############
    # TEMPERATURE
    # plot ens mean
    plt.subplot(211)
    tt = data_ens_qm["ens"]["all"][reg]["tair"]["mean"]
    ts0 = tt.mean(dim=("lat","lon")).groupby("hyear").mean()
    ts0 = ts0.to_pandas().rolling(30, center=True).mean() 
    ts = ts0 - ts0.loc[1989] # ANOMALY COMPARED TO REF PERIOD

        
    tt = data_ens_qm["ens"]["all"][reg]["tair"]["mean_mstd"]
    tt = tt.mean(dim=("lat","lon")).groupby("hyear").mean()
    ts_min = tt.to_pandas().rolling(30, center=True).mean() 
    ts_min = ts_min - ts0.loc[1989] # ANOMALY COMPARED TO REF PERIOD
            
    tt = data_ens_qm["ens"]["all"][reg]["tair"]["mean_pstd"]
    tt = tt.mean(dim=("lat","lon")).groupby("hyear").mean()
    ts_max = tt.to_pandas().rolling(30, center=True).mean() 
    ts_max = ts_max - ts0.loc[1989] # ANOMALY COMPARED TO REF PERIOD
    
    plt.plot(ts.index, ts.tair, lw=2, ms=6, mew=0, color=plt_dict["SIMU"]["col"])
    plt.fill_between(ts_max.index, ts_min.tair, ts_max.tair, color=plt_dict["SIMU"]["col"], alpha=0.5)
    
    plt.hlines(0, 1970, 2100, color="k", lw=0.3)
    plt.axis([1970, 2100, -4, 10])
    plt.title("Mean yearly temperature anomaly (ºC)")
    plt.ylabel("Temperature anomaly (ºC)")
    plt.grid(True, lw=0.4)
    
        
    ############
    # SWED
    # plot ens mean
    plt.subplot(212)
    tt = data_ens_qm["ens"]["all"][reg]["swed"]["mean"]
    tt = tt.mean(dim=("lat","lon")).groupby("hyear").mean()
    ts0 = tt.to_pandas().rolling(30, center=True).mean() 
    ts = 100*(ts0 / ts0.loc[1989].swed) # ANOMALY COMPARED TO REF PERIOD

    tt = data_ens_qm["ens"]["all"][reg]["swed"]["mean_mstd"]
    tt = tt.mean(dim=("lat","lon")).groupby("hyear").mean()
    ts_min = tt.to_pandas().rolling(30, center=True).mean() 
    ts_min = 100*(ts_min / ts0.loc[1989].swed) # ANOMALY COMPARED TO REF PERIOD


    tt = data_ens_qm["ens"]["all"][reg]["swed"]["mean_pstd"]
    tt = tt.mean(dim=("lat","lon")).groupby("hyear").mean()
    ts_max = tt.to_pandas().rolling(30, center=True).mean() 
    ts_max = 100*(ts_max / ts0.loc[1989].swed) # ANOMALY COMPARED TO REF PERIOD


    
    plt.plot(ts.index, ts.swed, lw=2, ms=6, mew=0, color=plt_dict["SIMU"]["col"])
    plt.fill_between(ts_max.index, ts_min.swed, ts_max.swed, color=plt_dict["SIMU"]["col"], alpha=0.5)
   
    plt.axis([1970, 2100, 0, 220])
    plt.title("Yearly mean snow mass (1974-2004 as ref. 100)")
    plt.ylabel("Snow mass")
    plt.grid(True, lw=0.4)
    
    plt.tight_layout()
    
    plt.savefig("/home/hola/datos_seis/port_transfert/temp_swed_"+reg+"_qm_report.png")
    plt.savefig("/home/hola/datos_seis/port_transfert/temp_swed_"+reg+"_qm_report.svg")
    plt.close()

#################################################### 
          ##     PLOT VIZ EXAMPLE     ##   
#################################################### 
reg="AIGUE"
var="swed"
    
(time_min, time_max) = ("1974-09-01","2004-08-31") # in enumerate([("2005-09-01","2035-08-31"), ("2035-09-01","2065-08-31"), ("2065-09-01","2095-08-31")]):
#months_to_select = [12, 1, 2, 3, 4, 5]
SWE = data_ens_qm[run][exp][reg]["prec"]["mean"].sel( time=slice(time_min, time_max)).prec.sum(dim="time")/30
#SWE = SWE.sel(time=SWE['time.month'].isin( months_to_select )).to_array().sum(dim="time")/30
ax1 = plt.subplot()
    
data2im = np.flipud(SWE.to_numpy().squeeze())
data2im = np.ma.masked_where(data2im < 1e-3, data2im)    
im1 = ax1.imshow( HS.band_data.squeeze(), cmap='Greys_r', vmin=0, vmax=255, extent = post_tools.get_xr_im_extent(HS), origin="upper", interpolation="nearest", alpha=0.8)
im2 = ax1.imshow( data2im, cmap='viridis', extent = post_tools.get_xr_im_extent(SWE), origin="upper", interpolation="nearest")
# vmin = 0 , vmax = 2,
plt.colorbar(im2)
plt.savefig("/home/hola/datos_seis/port_transfert/tt_TEIDE_prec.png")
plt.close()

(time_min, time_max) = ("1974-09-01","2004-08-31") # in enumerate([("2005-09-01","2035-08-31"), ("2035-09-01","2065-08-31"), ("2065-09-01","2095-08-31")]):
months_to_select = [12, 1, 2]
SWE = data_ens_qm[run][exp][reg]["tair"]["mean"].sel( time=slice(time_min, time_max))
SWE = SWE.sel(time=SWE['time.month'].isin( months_to_select )).to_array().mean(dim="time")
ax1 = plt.subplot()
    
data2im = np.flipud(SWE.to_numpy().squeeze())
im1 = ax1.imshow( HS.band_data.squeeze(), cmap='Greys_r', vmin=0, vmax=255, extent = post_tools.get_xr_im_extent(HS), origin="upper", interpolation="nearest", alpha=0.8)
im2 = ax1.imshow( data2im, cmap='viridis', vmin=-5, vmax=30, extent = post_tools.get_xr_im_extent(SWE), origin="upper", interpolation="nearest")
# vmin = 0 , vmax = 2,
plt.colorbar(im2)
plt.savefig("/home/hola/datos_seis/port_transfert/tt_TEIDE_tair_winter.png")
plt.close()

months_to_select = [3, 4, 5]
SWE = data_ens_qm[run][exp][reg]["tair"]["mean"].sel( time=slice(time_min, time_max))
SWE = SWE.sel(time=SWE['time.month'].isin( months_to_select )).to_array().mean(dim="time")
ax1 = plt.subplot()
    
data2im = np.flipud(SWE.to_numpy().squeeze())
im1 = ax1.imshow( HS.band_data.squeeze(), cmap='Greys_r', vmin=0, vmax=255, extent = post_tools.get_xr_im_extent(HS), origin="upper", interpolation="nearest", alpha=0.8)
im2 = ax1.imshow( data2im, cmap='viridis', vmin=-5, vmax=30, extent = post_tools.get_xr_im_extent(SWE), origin="upper", interpolation="nearest")
# vmin = 0 , vmax = 2,
plt.colorbar(im2)
plt.savefig("/home/hola/datos_seis/port_transfert/tt_TEIDE_tair_spring.png")
plt.close()

months_to_select = [6, 7, 8]
SWE = data_ens_qm[run][exp][reg]["tair"]["mean"].sel( time=slice(time_min, time_max))
SWE = SWE.sel(time=SWE['time.month'].isin( months_to_select )).to_array().mean(dim="time")
ax1 = plt.subplot()
    
data2im = np.flipud(SWE.to_numpy().squeeze())
im1 = ax1.imshow( HS.band_data.squeeze(), cmap='Greys_r', vmin=0, vmax=255, extent = post_tools.get_xr_im_extent(HS), origin="upper", interpolation="nearest", alpha=0.8)
im2 = ax1.imshow( data2im, cmap='viridis', vmin=-5, vmax=30, extent = post_tools.get_xr_im_extent(SWE), origin="upper", interpolation="nearest")
# vmin = 0 , vmax = 2,
plt.colorbar(im2)
plt.savefig("/home/hola/datos_seis/port_transfert/tt_TEIDE_tair_summer.png")
plt.close()

tt = data_ens_qm[run]["all"][reg]["tair"]["mean"].sel(time=slice("1974-09-01","2004-08-31"))

import numpy as np
import matplotlib.pyplot as plt

# Create sample data (elevation and aspect)
elevation = np.linspace(1000, 3000, 5)
aspect = np.linspace(0, 360, 9)
var = np.random.rand(5, 9)  # Sample variable data

# Create a polar plot
fig = plt.figure(figsize=(8, 8))
ax = fig.add_subplot(111, projection='polar')

# Convert aspect to radians
aspect_rad = np.deg2rad(aspect)

# Create a meshgrid for elevation and aspect
elevation_grid, aspect_grid = np.meshgrid(elevation, aspect_rad)

# Plot the data using imshow
pcm = ax.imshow(var, extent=(aspect_rad.min(), aspect_rad.max(), elevation.min(), elevation.max()) ) #, aspect='auto', cmap='viridis')

# Customize the plot
ax.set_rmax(3000)
ax.set_rticks([1000, 2000, 3000])
ax.grid(True)

# Add colorbar
cbar = plt.colorbar(pcm)
cbar.set_label('Variable')