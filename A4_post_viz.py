import os
import glob
import sys
from xgrads import CtlDescriptor # replace with xarray
from xgrads import open_CtlDataset # replace with xarray
import datetime as dt
import calendar 
import numpy as np    
import matplotlib as mpl
mpl.rcParams.update({'text.usetex': False,"svg.fonttype": 'none'})
import pickle
import itertools
from multiprocessing import Pool
import xarray as xr
import blosc

import matplotlib.pyplot as plt
plt.rcParams.update({'font.size': 10})

import pandas as pd

# edit of sys.path required due to a weird bug/feature of python3.6 https://github.com/conda/conda/issues/6018
sys.path.append('/home/hola/project/nacional_parques/script')
from nivo_parc.toolbox import post_tools

            
# fixed parameters
cordex_domain = "EUR-11"  # CORDEX domain [STR] eg "EUR-44"
project = 'CORDEX' # eg CMIP6
time_frequency = "3hr" #'day'  # eg 3hr
res = int(250)      
pixel_size = res*res/1e6

run_dict = post_tools.read_run_list("/home/hola/project/nacional_parques/script/nivo_parc/toolbox/exp_list_EXP.txt")
out_dir = "/home/hola/datos_seis/nacional_parques/prod"

# variables
exp_list = ['historical', "rcp85"] #,'historical' 'rcp26', 'rcp45', 'rcp85']
reg_list = ["AIGUE", "GUADA", "ORDES", "PICOS", "SIERR"] 
var_list = ["prec", "relh", "rpre", "smlt", "ssub", "tair", "qlin", "roff", "sden", "snod", "swed", "wdir"]


plt_dict = {
    "historical":{"col":"#4a4e4d", "mrk":"v"},
    "rcp26":{"col":"#f6cd61", "mrk":"^"},
    "rcp85":{"col":"#fe8a71", "mrk":">"},
    "AIGUE":{"col":"#ee4035","mrk":"H"},
    "GUADA":{"col":"#f37736","mrk":"X"},
    "ORDES":{"col":"#ffe800","mrk":"o"},
    "PICOS":{"col":"#7bc043","mrk":"d"},
    "SIERR":{"col":"#0392cf","mrk":"s"},
    "DJF":{"col":"#0057e7","mrk":"*"},
    "MAM":{"col":"#008744","mrk":"s"},
    "JJA":{"col":"#d62d20","mrk":"."},
    "SON":{"col":"#ffa700","mrk":"^"},
    }

in2cm = 1/2.54

# prepare folder architecture to output figures
# for exp in exp_list:
#     run_list = run_dict[exp]
#     for run in run_list:
#         if not os.path.isdir( sm_fig_out_dir+"/"+run ):
#             os.mkdir( sm_fig_out_dir+"/"+run )
#
#     if not os.path.isdir( sm_fig_out_dir+"/"+run+"/"+exp ):
#         os.mkdir( sm_fig_out_dir+"/"+run+"/"+exp )


# regular run
# for exp in exp_list:
#     out_dir = "/home/hola/datos_seis/nacional_parques/prod" # datos => rcp85, datos2 => rcp26,hist
#     sm_out_dir = out_dir+"/sm/sm_Tlr_53_qm"
#     arg_list = [{"exp":xarg_list[0],
#                  "run":xarg_list[1],
#                  "reg":xarg_list[2],
#                  "var":xarg_list[3],
#                  "res":res,
#                  "sm_out_dir":sm_out_dir,
#                  "sm_par_dir":sm_par_dir} 
#                  for xarg_list in list(itertools.product([exp],run_list,reg_list,var_list))]
#
#
#     # write ctl file and dohy, hyear pkl file
#     with Pool(40) as p:
#         f_out = p.map(post_tools.f_prep_dohy_hyear, arg_list)
    


data_all_run_qm =  {}
sm_par_dir = "/home/hola/project/nacional_parques/sm_Tlr_53/par_qm"

for exp in ["historical", "rcp85"]: #, "rcp26", "rcp85"]:
    run_list = run_dict[exp]
    sm_out_dir = out_dir+"/sm/sm_Tlr_53_qm"
    print(exp)
    print(run_list)
    print(out_dir)
    data = post_tools.get_simu_single_exp(exp, res, sm_out_dir, sm_par_dir, run_list, reg_list, var_list)
    data_all_run_qm = post_tools.merge_data_runs(data_all_run_qm, data)

data_all_run_noqm =  {}
sm_par_dir = "/home/hola/project/nacional_parques/sm_Tlr_53/par"

for exp in ["historical", "rcp85"]: #, "rcp26", "rcp85"]:
    run_list = run_dict[exp]
    sm_out_dir = out_dir+"/sm/sm_Tlr_53"
    print(exp)
    print(run_list)
    print(out_dir)
    data = post_tools.get_simu_single_exp(exp, res, sm_out_dir, sm_par_dir, run_list, reg_list, var_list)
    data_all_run_noqm = post_tools.merge_data_runs(data_all_run_noqm, data)
    
    
# Calculate SCA
snod_threshold = 0.1
for exp in ["historical", "rcp85"]: 
    for reg in reg_list:
        print(reg)
        for run in data_all_run_qm.keys():
            snod = data_all_run_qm[run][exp][reg]["snod"]
            if len(snod)>0:
                data_all_run_qm[run][exp][reg]["sca"] = snod.copy(deep=True)
            
                # Create the binary mask using Xarray operations
                mask = (snod['snod'] > snod_threshold).astype(int)
                data_all_run_qm[run][exp][reg]["sca"]["snod"] = mask
            else:
                data_all_run_qm[run][exp][reg]["sca"] = {}
        
        for run in data_all_run_noqm.keys():
            snod = data_all_run_noqm[run][exp][reg]["snod"]
            if len(snod)>0:
                data_all_run_noqm[run][exp][reg]["sca"] = snod.copy(deep=True)
            
                # Create the binary mask using Xarray operations
                mask = (snod['snod'] > snod_threshold).astype(int)
                data_all_run_noqm[run][exp][reg]["sca"]["snod"] = mask
            else:
                data_all_run_noqm[run][exp][reg]["sca"] = {}



sm_par_dir = "/home/hola/project/nacional_parques/sm_Tlr_53/par_qm"
exp = "historical"    
run_list = run_dict[exp]
sm_out_dir = out_dir+"/sm/sm_Tlr_53_qm"
out_folder = "/home/hola/datos_seis/nacional_parques/prod/sm/sm_Tlr_53_qm/ens"

# cuida que satura la ram...
# post_tools.get_ensemble_stat( exp, res, sm_out_dir, sm_par_dir, run_list, reg_list, var_list, out_folder)

################################################### 
## Comparison SCA with and without QM correction ##
################################################### 

########################## 
# one run and region by plot
for reg in reg_list:
    print(reg)
    in_SCA_MODIS="/home/hola/datos_seis/nacional_parques/data/MODIS/Snow_Cover_Area_MODIS_"+reg+".csv"
    SCA = pd.read_csv(in_SCA_MODIS,names=["time","sca"],header=0)
    time_dt = [dt.datetime.strptime(dd, "%Y_%m_%d") for dd in SCA.time]
    SCA["time"]=time_dt
    SCA["hyear"] = [yy if mm<12 else yy+1 for yy,mm in zip(SCA.time.dt.year, SCA.time.dt.month) ]
    tt_MODIS = SCA.groupby("hyear").mean()
    
    for run in data_all_run_qm.keys():
        exp="historical"
        plt.figure(figsize=(8,3))

        # qm 
        sca = data_all_run_qm[run][exp][reg]["sca"]
        if len(snod)>0:
            #snod = clean_uncomplete_hyear(snod)
            #sca = (snod>0.2).astype(int)
            tt = sca.sum(dim=("lat","lon")).groupby("hyear").mean()*250*250/1e6
            plt.plot(tt.hyear, tt.snod, lw=0.2, marker=".", ms=6, mew=0, color=[0.1,0.1,0.9])
        # no qm
        sca = data_all_run_noqm[run][exp][reg]["sca"]
        if len(snod)>0:
            #snod = clean_uncomplete_hyear(snod)
            #sca = (snod>0.2).astype(int)
            tt = sca.sum(dim=("lat","lon")).groupby("hyear").mean()*250*250/1e6
            plt.plot(tt.hyear, tt.snod, lw=0.2, marker=".", ms=6, mew=0, color=[0.9,0.1,0.1])
        
        plt.plot(tt_MODIS.index, tt_MODIS.sca, lw=2, color="k")
        plt.ylim((0,plt.gca().get_ylim()[1]))
        plt.ylabel("Yearly mean SCA")
        plt.savefig("/home/hola/datos_seis/port_transfert/sca_"+reg+"_"+run+"_compqm.png")
        plt.savefig("/home/hola/datos_seis/port_transfert/sca_"+reg+"_"+run+"_compqm.svg")
        plt.close()

########################## 
# scatter plot mean SCA over common years (MODIS//historical)
common_years = [2000, 2001, 2002, 2003] #list(set(tt_MODIS.index) & set(tt.hyear.values))

for reg in reg_list:
    print(reg)
    in_SCA_MODIS="/home/hola/datos_seis/nacional_parques/data/MODIS/Snow_Cover_Area_MODIS_"+reg+".csv"
    SCA = pd.read_csv(in_SCA_MODIS, names = ["time", "sca"],header=0)
    time_dt = [dt.datetime.strptime(dd, "%Y_%m_%d") for dd in SCA.time]
    SCA["time"] = time_dt
    SCA["hyear"] = [yy if mm<12 else yy+1 for yy,mm in zip(SCA.time.dt.year, SCA.time.dt.month) ]
    tt_MODIS = SCA.groupby("hyear").mean()

    MODIS_filtered = tt_MODIS.loc[ common_years ]

    plt.figure(figsize=(8*in2cm, 8*in2cm))

    for run in data_all_run_qm.keys():
        exp="historical"

        # qm 
        sca = data_all_run_qm[run][exp][reg]["sca"]
        if len(sca)>0:
            tt = sca.sum(dim=("lat","lon")).groupby("hyear").mean()*250*250/1e6
            
            # Filter XR Dataset based on common years
            SCA_filtered = tt.sel( hyear=common_years )
        
            # Filter Pandas DataFrame based on common years
            plt.plot( MODIS_filtered.sca, SCA_filtered, "b.")
        
        # no qm
        sca = data_all_run_noqm[run][exp][reg]["sca"]
        if len(sca)>0:
            tt = sca.sum(dim=("lat","lon")).groupby("hyear").mean()*250*250/1e6

            # Filter XR Dataset based on common years
            SCA_filtered = tt.sel( hyear=common_years )
        
            # Filter Pandas DataFrame based on common years
            plt.plot( MODIS_filtered.sca, SCA_filtered, "r.")            


    x_min, x_max = plt.gca().get_xlim()
    y_min, y_max = plt.gca().get_ylim()
    xy_max = ((np.max([x_max,y_max])/50).round()+1)*50
    plt.plot([0, xy_max], [0, xy_max], lw=0.5, color="k")
    plt.axis("equal")
    plt.axis([0, xy_max, 0, xy_max])
    plt.grid(True)
    plt.xlabel("MODIS mean snow cover 2000-2004")
    plt.ylabel("Modeled mean snow cover 2000-2004")

    plt.savefig("/home/hola/datos_seis/port_transfert/sca_"+reg+"_compqm_scatter.png")
    plt.close()

########################## 
# temporal plot, all run on one plot, 2 subplot QM // no-QM
for reg in reg_list:
    print(reg)
    plt.figure(figsize=(16,3))
    exp="historical"
    in_SCA_MODIS="/home/hola/datos_seis/nacional_parques/data/MODIS/Snow_Cover_Area_MODIS_"+reg+".csv"
    SCA = pd.read_csv(in_SCA_MODIS,names=["time","sca"],header=0)
    time_dt = [dt.datetime.strptime(dd, "%Y_%m_%d") for dd in SCA.time]
    SCA["time"]=time_dt
    SCA["hyear"] = [yy if mm<12 else yy+1 for yy,mm in zip(SCA.time.dt.year, SCA.time.dt.month) ]
    tt_MODIS = SCA.groupby("hyear").mean()
    
    for run in data_all_run_qm.keys():
        plt.subplot(122)
        # qm
        sca = data_all_run_qm[run][exp][reg]["sca"]
        if len(sca)>0:
            tt = sca.sum(dim=("lat","lon")).groupby("hyear").mean()*250*250/1e6
            plt.plot(tt.hyear, tt.snod, lw=0.2, marker=".", ms=6, mew=0, color=[0.1,0.1,0.9])

    for run in data_all_run_noqm.keys():
        # no qm
        plt.subplot(121)

        sca = data_all_run_noqm[run][exp][reg]["sca"]
        if len(sca)>0:
            tt = sca.sum(dim=("lat","lon")).groupby("hyear").mean()*250*250/1e6
            plt.plot(tt.hyear, tt.snod, lw=0.2, marker=".", ms=6, mew=0, color=[0.9,0.1,0.1])
    
    plt.subplot(121)
    plt.plot(tt_MODIS.index, tt_MODIS.sca, lw=2, color="k")
    plt.ylim((0,plt.gca().get_ylim()[1]))
    plt.ylabel("Yearly mean SCA")
    plt.title("No quantile mapping correction")
    plt.legend()
    plt.axis([1990,2010,0,1000])
    
    plt.subplot(122)
    plt.plot(tt_MODIS.index, tt_MODIS.sca, lw=2, color="k", label="MODIS")
    plt.ylim((0,plt.gca().get_ylim()[1]))
    plt.ylabel("Yearly mean SCA")
    plt.title("Quantile mapping correction")
    plt.legend()
    plt.axis([1990,2010,0,1000])

    plt.savefig("/home/hola/datos_seis/port_transfert/sca_"+reg+"_allrun_compqm.png")
#    plt.savefig("/home/hola/datos_seis/port_transfert/sca_"+reg+"_allrun_compqm.svg")
    plt.close()
    
    
    
#################################################### 
         ## PLOT SNOD = f(dohy)  ##
#################################################### 


for reg in reg_list:
    in_SCA_MODIS="/home/hola/datos_seis/nacional_parques/data/MODIS/Snow_Cover_Area_MODIS_"+reg+".csv"
    SCA = pd.read_csv( in_SCA_MODIS, names=["time","sca"], header=0)
    time_dt = [dt.datetime.strptime(dd, "%Y_%m_%d") for dd in SCA.time]
    SCA["time"] = time_dt
    SCA["hyear"] = [yy if mm<12 else yy+1 for yy,mm in zip(SCA.time.dt.year, SCA.time.dt.month) ]
    SCA = SCA.set_index(SCA.time, drop = True).to_xarray()

    MODIS_filtered = SCA.where(SCA.hyear.isin(common_years), drop = True)

    for run in data_all_run_qm.keys():
        plt.figure()
        print(reg+" - "+run)
        
        sca = data_all_run_qm[run][exp][reg]["sca"]
        if len(sca) > 0:
            tt = sca.sum(dim=("lat","lon"))*250*250/1e6
            tt = tt.where( tt.hyear.isin(common_years), drop=True)

            plt.subplot(211)
            plt.plot(tt.time, tt.snod, color="b", lw=0.8)
            plt.ylabel("Snow cover area")
            #plt.xlabel("Day of hydrological year (1st sept.=0)")
            #plt.axis((0,365,0,plt.gca().get_ylim()[1]))

            plt.subplot(212)
            dSCA = tt.snod - MODIS_filtered.reindex(time=tt['time'], method='nearest').sca
            plt.hist(dSCA, bins = 100, range =[-1000,1000], histtype="step", color="b")
            
        sca = data_all_run_noqm[run][exp][reg]["sca"]
        if len(sca) > 0:
            tt = sca.sum(dim=("lat","lon"))*250*250/1e6
            tt = tt.where( tt.hyear.isin(common_years), drop=True)
            
            plt.subplot(211)
            plt.plot(tt.time, tt.snod, color="r", lw=0.8)
            
            plt.subplot(212)
            dSCA = tt.snod - MODIS_filtered.reindex(time=tt['time'], method='nearest').sca
            plt.hist(dSCA, bins = 100, range =[-1000,1000], histtype="step", color="r")
        
        plt.subplot(211)
        plt.plot(MODIS_filtered.time, MODIS_filtered.sca, "k", lw=0.5)
        plt.ylim([0, plt.gca().get_ylim()[1]])
        
        plt.savefig("/home/hola/datos_seis/port_transfert/snod_"+run+"_"+reg+"_spatialmean_tsdohy.png")
#        plt.savefig("/home/hola/datos_seis/port_transfert/snod_"+run+"_"+reg+"_spatialmean_tsdohy.svg")
        plt.close()
        


#################################################### 
     ## Comparison precip from station ##
#################################################### 
    

exp = "historical"
run_list = run_dict[exp]
sm_out_dir = out_dir+"/sm/sm_Tlr_53_qm"
for run in data_all_run_qm.keys():
    for reg in reg_list:
        print("Treating "+exp+ " - "+run+" - "+reg)
        dset_noqm = data_all_run_noqm[run][exp][reg]
        dset_qm = data_all_run_qm[run][exp][reg]

        # precipitation plots
        var_name = "prec"
        in_ctl = glob.glob(sm_out_dir+"/"+run+"/"+exp+"/"+reg+"/wo_assim/"+var_name+".ctl")[0]
        in_pkl_station="/home/hola/datos_seis/nacional_parques/prod/pkl/station_data_pr_region.pkl"
        post_tools.plot_prec_valid_station(dset_noqm, "prec", in_ctl, "/home/hola/datos_seis/port_transfert/", in_pkl_station = in_pkl_station)
            
####################################################
      ## Comparison with ERA5  ##
####################################################
    

exp = "historical"
run_list = run_dict[exp]
sm_out_dir = out_dir+"/sm/sm_Tlr_53"
for run in data_all_run_qm.keys():
    for reg in reg_list:
        print("Treating "+exp+ " - "+run+" - "+reg)
        dset_noqm = data_all_run_noqm[run][exp][reg]
        dset_qm = data_all_run_qm[run][exp][reg]

        # precipitation plots
        var_name = "prec"
        in_ctl = glob.glob(sm_out_dir+"/"+run+"/"+exp+"/"+reg+"/wo_assim/"+var_name+".ctl")[0]
    
        in_ERA5 = "/media/hola/datos_seis/nacional_parques/data/ERA5/year_season_mean_temp_cum_prec_"+reg+".csv"
        post_tools.plot_tair_prec_valid_ERA5_qm(dset_noqm, dset_qm, in_ctl, "/home/hola/datos_seis/port_transfert/", in_ERA5 = in_ERA5)
        post_tools.plot_tair_prec_valid_ERA5(dset_noqm, in_ctl, fig_out_dir, in_ERA5 = in_ERA5)


for exp in exp_list:
    for run in run_list:
        for reg in reg_list:
            print("Treating "+exp+ " - "+run+" - "+reg)
            dset = data_all_run_qm[run][exp][reg]
            fig_out_dir = "/home/hola/datos_seis/port_transfert/"

            # precipitation plots
            var_name = "prec"
            in_ctl = glob.glob(sm_out_dir+"/"+run+"/"+exp+"/"+reg+"/wo_assim/"+var_name+".ctl")[0]

            #post_tools.plot_snod(dset, "snod", in_ctl, fig_out_dir, res)
            post_tools.plot_snod(dset, "snod", exp, run, reg, fig_out_dir, res, ["1970-01-01","1999-12-31"])
            post_tools.plot_prec(dset, "prec", in_ctl, fig_out_dir, res, ["1970-01-01","1999-12-31"])
            post_tools.plot_tair(dset, "tair", in_ctl, fig_out_dir, res, ["1970-01-01","1999-12-31"])
            
            # make plot according to slope, aspect, elevation
            post_tools.plot_snod_topo(dset, "snod", in_ctl, fig_out_dir, res, ["1970-01-01","1999-12-31"])
            post_tools.plot_prec_topo(dset, "prec", in_ctl, fig_out_dir, res, ["1970-01-01","1999-12-31"])
            
            
                
########################## 
## PLOT DSWE = f(DTAIR) ##
########################## 

# put that in a parallel function for all var
data2plt = {}
for reg in reg_list:
    data2plt[reg] = {}
    for run in data_all_run_qm.keys():
#        if run != 'CNRM-CERFACS-CNRM-CM5__CNRM-ALADIN63': # FUCKING MISSING UAS, VAS
        data2plt[reg][run] = {}
        print(reg+" - "+run)
        if len(data_all_run_qm[run].keys())>1:
            kk_list = list(data_all_run_qm[run].keys())
            kk_list .remove("historical")
            
            tair_ref = data_all_run_qm[run]["historical"][reg]["tair"].sel(time=slice("1975-01-01","2005-01-01")).mean().tair.values
            SWE_ref = data_all_run_qm[run]["historical"][reg]["swed"].sel(time=slice("1975-01-01","2005-01-01")).mean().swed.values
            prec_ref = data_all_run_qm[run]["historical"][reg]["prec"].sel(time=slice("1975-01-01","2005-01-01")).mean().prec.values
    
            for exp in kk_list:
                dtair_list = []
                dSWE_list = []
                dSWE_rel_list = []
        
                prec_list = []
                prec_rel_list = []
                
                for (time_min, time_max) in [("2006-01-01","2036-01-01"), ("2037-01-01","2066-01-01"), ("2067-01-01","2096-01-01")]:
                    tair = data_all_run_qm[run][exp][reg]["tair"].sel(time=slice(time_min, time_max)).mean().tair.values
                    SWE = data_all_run_qm[run][exp][reg]["swed"].sel( time=slice(time_min, time_max)).mean().swed.values
                    
                    dtair_list.append(tair-tair_ref)
                    dSWE_list.append(SWE-SWE_ref)
                    dSWE_rel_list.append(int(100*(SWE-SWE_ref)/SWE_ref))
                                    
                tair = data_all_run_qm[run][exp][reg]["tair"].sel(time=slice("2006-01-01","2036-01-01")).mean().tair.values
                SWE = data_all_run_qm[run][exp][reg]["swed"].sel(time=slice("2006-01-01","2036-01-01")).mean().swed.values
                
                dtair_list.append(tair-tair_ref)
                dSWE_list.append(SWE-SWE_ref)
                dSWE_rel_list.append(int(100*(SWE-SWE_ref)/SWE_ref))
                
                tair = data_all_run_qm[run][exp][reg]["tair"].sel(time=slice("2037-01-01","2066-01-01")).mean().tair.values
                SWE = data_all_run_qm[run][exp][reg]["swed"].sel(time=slice("2037-01-01","2066-01-01")).mean().swed.values
                
                dtair_list.append(tair-tair_ref)
                dSWE_list.append(SWE-SWE_ref)
                dSWE_rel_list.append(int(100*(SWE-SWE_ref)/SWE_ref))
                            
                tair = data_all_run_qm[run][exp][reg]["tair"].sel(time=slice("2067-01-01","2096-01-01")).mean().tair.values
                SWE = data_all_run_qm[run][exp][reg]["swed"].sel(time=slice("2067-01-01","2096-01-01")).mean().swed.values
                
                dtair_list.append(tair-tair_ref)
                dSWE_list.append(SWE-SWE_ref)
                dSWE_rel_list.append(int(100*(SWE-SWE_ref)/SWE_ref))
                
                data2plt[reg][run][exp] = {"dtair":dtair_list, "dSWE":dSWE_list, "dSWE_rel":dSWE_rel_list}
                
                

plt.figure()
for reg in reg_list:
    for ii,exp in enumerate(data2plt[reg].keys()):
        if ii == 0:
            plt.plot(data2plt[reg][exp]["rcp85"]["dtair"], data2plt[reg][exp]["rcp85"]["dSWE"], marker=".", color=plt_dict[reg]["col"], ms=12, mew=0, lw=0.8, label=reg)
        else:
            plt.plot(data2plt[reg][exp]["rcp85"]["dtair"], data2plt[reg][exp]["rcp85"]["dSWE"], marker=".", color=plt_dict[reg]["col"], ms=12, mew=0, lw=0.8)

plt.ylabel("dSWE (m)")
plt.xlabel("dT (ºC)")
#plt.axis((0,4,-0.1,0))
plt.grid(True, lw=0.5)
plt.legend()

plt.savefig("/home/hola/datos_seis/port_transfert/dtair_dswed_t_"+run+".png")
plt.savefig("/home/hola/datos_seis/port_transfert/dtair_dswed_t_"+run+".svg")


def get_data_period_mean(args):
    print(args["run"]+" - "+args["reg"]+" - "+args["var"])
    data_ref = args["data_all_run"][ args["run"] ][ "historical" ][args["reg"]][ args["var"]].sel( time = slice( "1975-01-01", "2005-01-01" )).mean()[args["var"]].values
    data     = args["data_all_run"][ args["run"] ][ args["exp"] ][args["reg"]][ args["var"]].sel( time = slice( args["period"][0], args["period"][1])).mean()[args["var"]].values

    out = data-data_ref
    out_rel = (data-data_ref)/data_ref
    
    return (out, out_rel)

def get_data_period_mean_map(args):
    print(args["run"]+" - "+args["reg"]+" - "+args["var"])
    data_ref = args["data_all_run"][ args["run"] ][ "historical" ][args["reg"]][ args["var"]].sel( time = slice( "1975-01-01", "2005-01-01" )).mean(dim="time")[args["var"]]
    data     = args["data_all_run"][ args["run"] ][ args["exp"] ][args["reg"]][ args["var"]].sel( time = slice( args["period"][0], args["period"][1])).mean(dim="time")[args["var"]]

    out = data-data_ref
    out_rel = (data-data_ref)/data_ref
    
    return (out, out_rel)

arg_list = [{"exp":xarg_list[0],
             "run":xarg_list[1],
             "reg":xarg_list[2],
             "var":xarg_list[3],
             "period":xarg_list[4],
             "data_all_run":xarg_list[5]} 
             for xarg_list in list(itertools.product(["rcp85"], run_list, reg_list, ["tair", "prec", "swed"], [("2006-01-01","2036-01-01"), ("2037-01-01","2066-01-01"), ("2067-01-01","2096-01-01")], [data_all_run_qm]))
             ]

# write ctl file and dohy, hyear pkl file
with Pool(5) as p: # fails with 10
     f_out = p.map(get_data_period, arg_list)


# all run on one plot
for reg in reg_list:
    print(reg)
    plt.figure(figsize=(8,3))
    exp="historical"


    for run in data_all_run_noqm.keys():
        # qm
        snod = data_all_run_noqm[run][exp][reg]["snod"]
        if len(snod)>0:
            snod = clean_uncomplete_hyear(snod)
            sca = (snod>0.2).astype(int)
            tt = sca.sum(dim=("lat","lon"))*250*250/1e6
        
            plt.plot(tt.time, tt.snod, lw=0.2, marker=None, ms=6, mew=0, color=[0.9,0.1,0.1])
    
    for run in data_all_run_qm.keys():
        # qm
        snod = data_all_run_qm[run][exp][reg]["snod"]
        if len(snod)>0:
            snod = clean_uncomplete_hyear(snod)
            sca = (snod>0.2).astype(int)
            tt = sca.sum(dim=("lat","lon"))*250*250/1e6
        
            plt.plot(tt.time, tt.snod, lw=0.2, marker=None, ms=6, mew=0, color=[0.1,0.1,0.9])
    
    gcm_rcm_list = list(data_all_run_qm.keys())
    xr_list = [data_all_run_qm[gcm_rcm]["historical"]["AIGUE"]["snod"] for gcm_rcm in gcm_rcm_list]
    snod_concat = xr.concat(xr_list, pd.Index(gcm_rcm_list, name='gcm_rcm'))
    sca = (snod_concat.snod>0.2).astype(int)
    tt = sca.sum(dim=("lat","lon")).mean(dim="gcm_rcm")*250*250/1e6
    plt.plot(tt.time, tt.values, lw=0.8, marker=None, ms=6, mew=0, color=[0.1,0.1,0.9])


    in_SCA_MODIS = "/home/hola/datos_seis/nacional_parques/data/MODIS/Snow_Cover_Area_MODIS_" + reg + ".csv"
    
    SCA = pd.read_csv(in_SCA_MODIS,names=["time","sca"],header=0)
    time_dt = [dt.datetime.strptime(dd, "%Y_%m_%d") for dd in SCA.time]
    SCA["time"]=time_dt
    plt.plot(SCA.time, SCA.sca)
    
            
    plt.gca().set_xlim([dt.date(1999, 8, 1), dt.date(2006, 8, 1)])
    
    
    plt.savefig("/home/hola/datos_seis/port_transfert/SCA_time_"+reg+".png")
    plt.close()
    


# all run on one plot - MEAN GCM_RCM

for reg in reg_list:
    print(reg)
    plt.figure(figsize=(8,3))
    exp="historical"

    
    gcm_rcm_list = list(data_all_run_qm.keys())
    xr_list = [data_all_run_qm[gcm_rcm]["historical"][reg]["sca"] for gcm_rcm in gcm_rcm_list]
    sca = xr.concat(xr_list, pd.Index(gcm_rcm_list, name='gcm_rcm'))
    #sca = (snod_concat.snod>0.2).astype(int)
    tt = sca.sum(dim=("lat", "lon")).mean(dim="gcm_rcm")*250*250/1e6
    SCA_filtered = tt.sel( hyear=common_years )
    plt.plot(SCA_filtered.time, SCA_filtered.snod, lw=0.8, marker=None, ms=6, mew=0, color=[0.1,0.1,0.9])    
    
    gcm_rcm_list = list(data_all_run_noqm.keys())
    xr_list = [data_all_run_noqm[gcm_rcm]["historical"][reg]["sca"] for gcm_rcm in gcm_rcm_list]
    sca = xr.concat(xr_list, pd.Index(gcm_rcm_list, name='gcm_rcm'))
    #sca = (snod_concat.snod>0.2).astype(int)
    tt = sca.sum(dim=("lat", "lon")).mean(dim="gcm_rcm")*250*250/1e6
    SCA_filtered = tt.sel( hyear=common_years )
    plt.plot(SCA_filtered.time, SCA_filtered.snod, lw=0.8, marker=None, ms=6, mew=0, color=[0.9,0.1,0.1])


    in_SCA_MODIS="/home/hola/datos_seis/nacional_parques/data/MODIS/Snow_Cover_Area_MODIS_"+reg+".csv"
    
    SCA = pd.read_csv(in_SCA_MODIS,names=["time","sca"],header=0)
    time_dt = [dt.datetime.strptime(dd, "%Y_%m_%d") for dd in SCA.time]
    SCA["time"] = time_dt
    MODIS_filtered = SCA.loc[ common_years ]

    plt.plot(SCA.time, SCA.sca, "k", lw=0.8)
    
    plt.ylim([0, plt.gca().get_ylim()[1]])
    plt.xlim([dt.date(1999, 8, 1), dt.date(2006, 8, 1)])
    
    
    plt.savefig("/home/hola/datos_seis/port_transfert/SCA_meanrun_time_"+reg+".png")
    plt.close()
    
    

##############
# check if all run went well
arg_list = [{"exp":xarg_list[0],
             "run":xarg_list[1],
             "reg":xarg_list[2],
             "res":res,
             "sm_out_dir":sm_out_dir,
             "sm_par_dir":sm_par_dir} 
             for xarg_list in list(itertools.product(exp_list,run_list,reg_list))]

for args in arg_list:
    in_gdat = args["sm_out_dir"]+"/"+args["run"]+"/"+args["exp"]+"/"+args["reg"]+"/wo_assim/snod.gdat"
    if os.path.isfile(in_gdat) is False:
        #print(args)
        in_par="/home/hola/project/nacional_parques/sm_Tlr_53/par/"+args["run"]+"_"+args["exp"]+"_"+args["reg"]+".par"
        if os.path.isfile(in_par) is False:
            print("missing : "+in_par)
        else:
            print("not missing : "+in_par)


arg_list_clean = []
for arg in arg_list:
    if (arg["exp"]=="rcp85") & (arg["reg"]=="SIERR"): # & (arg["run"]=="CNRM-CERFACS-CNRM-CM5__ICTP-RegCM4-6") &
        pass
    else:
        arg_list_clean.append(arg)
        
for arg in arg_list_clean:
    f_prep_dohy_hyear(arg)
    
##############
# compare different models (run)
for exp in exp_list:
    for reg in reg_list:
        print("Treating "+exp+ " - all run - "+reg)
        #dset = data_all_run_qm[run][exp]
        fig_out_dir = sm_fig_out_dir+"/all_run/"
        plot_snod_allrun(data_all_run, var_name, exp, run, reg, fig_out_dir, res, period = None)
        
def plot_snod_allrun(dset_allrun, var_name, exp, run, reg, fig_out_dir, res, period = None):
    pixel_size = res*res/1e6
    plt.figure(figsize=(5.5,4.5))

    for run_name in dset_allrun.keys():
        print(run_name)
        dset = dset_allrun[run_name][exp][reg]
        var = get_var(dset, var_name, period)
        # figure of daily mean snow depth
        dd = var.mean(dim=("lat","lon")).groupby(var.time.dohy)
        dd.mean().plot.scatter(x="dohy",y=var_name,s=2,label=run_name)
        #q1 = [np.quantile(xx[1]["snod"].values,0.25) for xx in dd]
        #q3 = [np.quantile(xx[1]["snod"].values,0.75) for xx in dd]  
        #med = [np.median(xx[1]["snod"].values) for xx in dd]
        #plt.fill_between(dd.groups.keys(), q1, q3, color=[0.6,0.6,0.9], alpha=0.5,label="First-third quartile")
        #plt.plot(dd.groups.keys(), med, ".", label="median")
    plt.ylim([0,(int(plt.gca().get_ylim()[1]/1.0)+1)*1])
    plt.grid(True, lw=0.5, zorder=1000)
    plt.legend(fontsize="small")
    plt.savefig(fig_out_dir+"/"+var_name+"_"+reg+"_spatialmean_tsdohy.png")
    plt.close()
            

# compares two runs, for instance sm_ref and sm_T
import os
import glob
import matplotlib.pyplot as plt
import numpy as np

sm_out_dir_0 = "/home/hola/datos_seis/nacional_parques/prod/sm/sm_ref"
sm_out_dir_1 = "/home/hola/datos_seis/nacional_parques/prod/sm/sm_Tlr_53"
fig_out_dir = "/home/hola/datos_seis/nacional_parques/fig"
var_name = "snod"

for reg in reg_list:  
    print("Treating "+exp+ " - "+run+" - "+reg)
    in_ctl_0 = glob.glob(sm_out_dir_0+"/"+run+"/"+exp+"/"+reg+"/wo_assim/snod.ctl")[0]
    dset_0 = open_CtlDataset(in_ctl_0) # replace with xarray
    
    in_ctl_1 = glob.glob(sm_out_dir_1+"/"+run+"/"+exp+"/"+reg+"/wo_assim/snod.ctl")[0]
    dset_1 = open_CtlDataset(in_ctl_1) # replace with xarray
    # pixel size in km2 - assuming lat-lon to be actually UTM coord...

    # add a day of hydrological year variable !!! MOVE THIS IN A SPECIAL LOOP SO THAT WE DONT DO IT EVERY TIME WE NEED TO PLOT SOMETHING !!!
    dohy = []
    for dd in dset_0.time: # tested with map : it is 17% slower 
       if dd.dt.date.item()<dt.date(year=dd.dt.year.item(),month=9,day=1):
            dohy.append(dd.dt.dayofyear.item()+121)
       else:
           if calendar.isleap(dd.dt.year.item()) is False:
               dohy.append(dd.dt.dayofyear.item()-244)
           else:
               dohy.append(dd.dt.dayofyear.item()-245)
   
dset_0.time.dt.day.data
[calendar.isleap(yy) for yy in dset_0.time.dt.year.data]

    hyear = [ dd.dt.year.item() if (dd.dt.month>=9) else dd.dt.year.item()-1 for dd in dset_0.time]

        

            