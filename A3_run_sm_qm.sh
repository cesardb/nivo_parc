#!/bin/sh

### Preparation of the Cordex data
conda activate parc
chmod 777 /home/hola/project/nacional_parques/script/nivo_parc/toolbox/*
CordexToolbox="/home/hola/project/nacional_parques/script/nivo_parc/toolbox"

# Site array
declare -a site_arr=("AIGUE" "GUADA" "ORDES" "PICOS" "SIERR") # "TEIDE") 

#QM="_qm" #"_qm"
declare -a QM_arr=("_qm_T1_P1_S") #"_qm_T0_P0"  
 
 
# Init folders
#dataFolder="/home/hola/datos_seis/nacional_parques/data/cordex"

run_name="rcp85" # historical rcp85 rcp26
source $CordexToolbox/exp_list_${run_name}.txt

cordexFolder0="/home/hola/datos_seis/nacional_parques/prod/cordex"
#workFolder="/home/hola/project/nacional_parques/sm_Tlr_53"
workFolder="/home/hola/project/nacional_parques/sm_2024_01_28_4pn"

# sleep 2h

for QM in "${QM_arr[@]}"
do

outFolder0="/home/hola/datos_seis/nacional_parques/prod/sm/sm_20240128_Tlr_53"${QM} 

mkdir -p ${workFolder}"/par" ${outFolder0}"/log" #  ${workFolder}"/met_prod"

for exp_name in "${exp_arr[@]}"
do

for site_name in "${site_arr[@]}"
do
echo $exp_name "-" $site_name

# only if destination folder is empty
#path_to_par=${workFolder}"/par"${QM}"/"${exp_name}"_"${run_name}"_"${site_name}".par" 
#n_file=$(ll $path_to_par | wc -l)
#if [ "$n_file" -lt "1" ]
#then
# prepare .par
outFolder1=${outFolder0}/${exp_name}/${run_name}/${site_name}"/"
inMET=$(ls ${cordexFolder0}"/"${exp_name}"/"${run_name}"/"${site_name}"/dat"${QM}"/allvar_"*".dat")
res_simu="250m"
mkdir -p ${outFolder1}"/wo_assim" ${outFolder1}"/wi_assim" ${outFolder1}"/fig"  ${workFolder}"/par"${QM}"/"
${CordexToolbox}/sm_par_write.py -inVEG "/home/hola/datos_seis/nacional_parques/data/land_cover/CORINE/"${res_simu}"/U2012_CLC2006_V2020_20u1_"${res_simu}"_"${site_name}"_sm_label.asc" -inDEM "/home/hola/datos_seis/nacional_parques/data/COP30/"${res_simu}"/limite_"${site_name}"_COP30_"${res_simu}"_UTM_DEM.asc" -inMET $inMET -outPAR ${workFolder}"/par"${QM}"/"${exp_name}"_"${run_name}"_"${site_name}".par"  -outFolder ${outFolder1}

# run snowmodel
cd ${workFolder}

# only if destination folder is empty
#path_to_output=$(grep output_path_wo_assim ${workFolder}"/par"${QM}"/"${exp_name}"_"${run_name}"_"${site_name}".par" | cut -d'=' -f2)
#n_file=$(ll $path_to_output | wc -l)
#if [ "$n_file" -lt "2" ]
#then
nohup ./snowmodel  ${workFolder}"/par"${QM}"/"${exp_name}"_"${run_name}"_"${site_name}".par" > ${outFolder0}"/log/log_"${exp_name}"_"${run_name}"_"${site_name}".txt" &
#fi
  
done
done
done


######################
# post-treatment


ctl2nccomp(){
while [[ "$#" > 1 ]]; do case $1 in
    -inFILE) inFILE="$2";;
    *) break;;
  esac; shift; shift
done

NAME=`echo "$inFILE" | cut -d'.' -f1`;
EXT=`echo "$inFILE" | cut -d'.' -f2`;

if [ "$EXT" = "ctl" ]
then
cdo -f nc4 -z zip_9 import_binary $inFILE ${NAME}.nc
#ncks -4 -L 9 ${NAME}_0.nc ${NAME}.nc
elif [ "$EXT" = "gdat" ]
then
cdo -f nc4 -z zip_9 import_binary ${NAME}.ctl ${NAME}.nc
fi

#[ -f ${NAME}.nc ] && echo rm ${NAME}.gdat 
}

export -f ctl2nccomp

for QM in "${QM_arr[@]}"
do

sm_out_dir="/home/hola/datos_seis/nacional_parques/prod/sm/sm_20240128_Tlr_53"${QM}
sm_par_dir=${workFolder}"/par"${QM}
           
python ${CordexToolbox}/post_treatment_gdat_to_nc.py -sm_out_dir $sm_out_dir -sm_par_dir $sm_par_dir -exp rcp85  # historical rcp85

# !!! convert gdat to nc compress !!! 
find $sm_out_dir/*/*/*/* -type f -name "*.gdat" > inFILE_list.txt
done

parallel --jobs 10 ctl2nccomp -inFILE {} :::: inFILE_list.txt
rm inFILE_list.txt



calc_ens(){
while [[ "$#" > 1 ]]; do case $1 in
    -in_template) in_template="$2";;
    -out_dir) out_dir="$2";;
    -hist_rcp) hist_rcp="$2";;
    *) break;;
  esac; shift; shift
done
echo "Treating "$in_template
# create temp dir
tmp_dir=$(mktemp -d -p /home/hola/datos_seis/tmp)
# select all file and cut them right 
count=0
for ff in $(ls ${in_template})
do
echo $ff
NAME=`basename "$ff" | cut -d'.' -f1`;
EXT=`basename "$ff" | cut -d'.' -f2`;
let "count=count+1"

if [ $hist_rcp = "historical" ]; then
cdo -selyear,1971/2005 $ff $tmp_dir/$NAME"_"$count"."$EXT # 2006-2099 for rcp
elif [ $hist_rcp = "rcp85" ]; then
cdo -selyear,2006/2099 $ff $tmp_dir/$NAME"_"$count"."$EXT # 2006-2099 for rcp
fi

done

cdo ensmean $tmp_dir/$NAME"_"*"."$EXT  ${out_dir}/${NAME}_mean.nc
#cdo ensmedian $tmp_dir/$NAME"_"*"."$EXT  ${out_dir}/${NAME}_median.nc
cdo ensmin $tmp_dir/$NAME"_"*"."$EXT  ${out_dir}/${NAME}_min.nc
cdo ensmax $tmp_dir/$NAME"_"*"."$EXT  ${out_dir}/${NAME}_max.nc
cdo ensstd $tmp_dir/$NAME"_"*"."$EXT  ${out_dir}/${NAME}_std.nc
rm $tmp_dir/*.nc
}

export -f calc_ens

######################
# create ensemble netcdf
parallelTxtFolder="/home/hola/project/nacional_parques/script/parallelTxt"

declare -a run_arr=("historical" "rcp85")
declare -a var_arr=("snod" "tair" "prec" "rpre")
declare -a var_arr=("snod" "prec" "relh" "rpre" "smlt" "ssub" "tair" "qlin" "roff" "sden" "swed" "wdir" "wspd" "qsin" "albd" "var5" "var6")
declare -a site_arr=("AIGUE" "GUADA" "ORDES" "PICOS" "SIERR") # "TEIDE")


for QM in "${QM_arr[@]}"
do

sm_out_dir="/home/hola/datos_seis/nacional_parques/prod/sm/sm_20240128_Tlr_53"${QM} 

for run_name in ${run_arr[@]}
do

for site_name in ${site_arr[@]}
do

for var_name in ${var_arr[@]}
do
out_dir=${sm_out_dir}/ens/${run_name}/${site_name}
mkdir -p ${out_dir}
echo calc_ens -in_template \""${sm_out_dir}/*/${run_name}/${site_name}/wo_assim/${var_name}.nc"\" -out_dir ${out_dir} -hist_rcp $run_name >> ${parallelTxtFolder}/tmp_cal_ens.txt 

done
done
done
done
parallel --jobs 4 :::: ${parallelTxtFolder}/tmp_cal_ens.txt 

