import os
import glob

def write_ctl(in_gdat, in_par_folder="/home/hola/project/nacional_parques/sm/par"):
    """ Write the .ctl file further used to read the .gdat."""
    var_def = {
        "prec" : "prec  0  0 water-equivalent precipitation (m/time_step)",
        "rpre" : "rpre  0  0 liquid precipitation, rain (m/time_step)",
        "sden" : "sden  0  0 snow density (kg/m3)",
        "snod" : "snod  0  0 snow depth (m)",
        "spre" : "spre  0  0 solid precipitation, snowfall (m/time_step)",
        "swed" : "swed  0  0 snow-water-equivalent depth (m)",
        "tair" : "tair  0  0 air temperature (deg C)",
        "ssub" : "ssub  0 0 static-surface sublimation (m)",
        "roff" : "roff  0 0 runoff from base of snowpack (m/time_step)",
        "var5" : "csub  0 0 sublimation", # Qcs canopy sublimation
        "var6" : "wsub  0 0 sublimation" # wbal_qsubl wind sublimation
        }
    
    month_dic = {
        "1" : "jan", "2" : "feb", "3" : "mar", "4" : "apr",
        "5" : "may", "6" : "jun", "7" : "jul", "8" : "aug",
        "9" : "sep", "10" : "oct", "11" : "nov", "12" : "dec"
        }
    
    out_folder = os.path.dirname(in_gdat)

    var = in_gdat.split("/")[-1].replace(".gdat","")
    reg = "AIGUE"
    
    in_par = glob.glob(in_par_folder+"/*.par")[0]
    out_ctl = out_folder+"/"+var+".ctl"
    
    with open(in_par) as f:
        lines = f.readlines()
    
    for line in lines:
        if "nx =" in line:
            nx = line.split("nx = ")[1].replace("\n","")
            
        elif "ny =" in line:
            ny = line.split("ny = ")[1].replace("\n","")
            
        if "xmn =" in line:
            x_mn = line.split("xmn = ")[1].replace("\n","")
            
        elif "ymn =" in line:
            y_mn = line.split("ymn = ")[1].replace("\n","")
            
        elif "deltax =" in line:
            delta_x = line.split("deltax = ")[1].replace("\n","")
        
        elif "deltay =" in line:
            delta_y = line.split("deltay = ")[1].replace("\n","")
                
        elif "iyear_init =" in line:
            year0 = line.split("iyear_init = ")[1].replace("\n","")

        elif "imonth_init =" in line:
            month0 = line.split("imonth_init = ")[1].replace("\n","")

        elif "iday_init =" in line:
            day0 = line.split("iday_init = ")[1].replace("\n","")

        elif "xhour_init =" in line:
            hour0 = line.split("xhour_init = ")[1].replace("\n","")

        elif "max_iter =" in line:
            max_iter = line.split("max_iter = ")[1].replace("\n","")
            
        elif "print_inc =" in line:
            print_inc = line.split("print_inc = ")[1].replace("\n","")
    

    time_step = "1dy"
    date0 = hour0.zfill(2)+"Z"+day0.zfill(2)+month_dic[month0]+year0
    n_time = str(int(float(max_iter)/float(print_inc)))

    lines = []
    lines.append("DSET "+in_gdat+"\n")
    lines.append("TITLE SnowModel single-variable output file"+"\n")
    lines.append("UNDEF    -9999.0"+"\n")
    lines.append("XDEF       "+nx+" LINEAR     "+str(x_mn)+"        "+str(delta_x)+"\n")
    lines.append("YDEF       "+ny+" LINEAR     "+str(y_mn)+"        "+str(delta_y)+"\n")
    lines.append("ZDEF         1 LINEAR 1 1"+"\n")
    lines.append("TDEF    "+str(n_time)+" LINEAR "+date0+"  "+str(time_step)+"\n")  # 12Z01dec1949  1dy
    lines.append("VARS     1"+"\n")
    if var in var_def.keys():
        lines.append(var_def[var]+"\n")
    else:
        lines.append(var+" 0 0 unknown var\n")
    lines.append("ENDVARS")
    
    with open(out_ctl, "w") as f:
        f.writelines(lines)

in_gdat="/home/hola/datos_seis/nacional_parques/sm_test/wo_assim/smlt.gdat"
sm_par_dir="/home/hola/datos_seis/nacional_parques/sm_test"
for in_gdat in glob.glob("/home/hola/datos_seis/nacional_parques/sm_test/wo_assim/*.gdat"):
    write_ctl(in_gdat, in_par_folder=sm_par_dir)












