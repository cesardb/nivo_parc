
### Preparation of the Cordex data
conda activate parc
chmod 777 /home/hola/project/nacional_parques/script/nivo_parc/toolbox/*
chmod 777 /home/hola/project/nacional_parques/script/nivo_parc/test/*

CordexToolbox="/home/hola/project/nacional_parques/script/nivo_parc/toolbox"
CordexToolboxTest="/home/hola/project/nacional_parques/script/nivo_parc/test"

outFolder="/home/hola/datos_seis/nacional_parques/sm_test"
inVEG="/home/hola/datos_seis/nacional_parques/data/land_cover/CORINE/250m/U2012_CLC2006_V2020_20u1_250m_AIGUE_sm_label.asc"
inDEM="/home/hola/datos_seis/nacional_parques/data/COP30/250m/limite_AIGUE_COP30_250m_UTM_DEM.asc"
inOrog="/home/hola/datos_seis/nacional_parques/data/cordex/EUR11/ICHEC-EC-EARTH__ICTP-RegCM4-6/orog/orog_EUR-11_ICHEC-EC-EARTH_historical_r12i1p1_ICTP-RegCM4-6_v1_fx.nc"
inMETnc="/home/hola/datos_seis/nacional_parques/prod/cordex/ICHEC-EC-EARTH__ICTP-RegCM4-6/historical/AIGUE/allvar_qm_T1_P1_S/allvar_EUR-11_ICHEC-EC-EARTH_historical_r12i1p1_ICTP-RegCM4-6_v1_3hr_197001010300_end.nc"
cdo selyear,1990/1991 ${inMETnc} ${outFolder}/met.nc
${CordexToolboxTest}/prep_netcdf_meteo2micromet4test.py -inNC ${outFolder}/met.nc  -inDEM ${inDEM}   -inOrog ${inOrog} -siteName AIGUE -outFolder ${outFolder}/ -resDEM ${res_simu} -dist_buffer 15000


inMET=${outFolder}/met.dat
mkdir -p ${outFolder}"/wo_assim" ${outFolder}"/wi_assim" ${outFolder}"/fig" 
${CordexToolbox}/sm_par_write.py -inVEG ${inVEG} -inDEM ${inDEM} -inMET $inMET -outPAR ${outFolder}"/par_test.par"  -outFolder ${outFolder}

cd /home/hola/project/nacional_parques/sm_2024_01_28_4pn
nohup ./snowmodel  ${outFolder}"/par_test.par"  > ${outFolder}"/out_log.txt"  &

ctl2nccomp(){
while [[ "$#" > 1 ]]; do case $1 in
    -inFILE) inFILE="$2";;
    *) break;;
  esac; shift; shift
done

NAME=`echo "$inFILE" | cut -d'.' -f1`;
EXT=`echo "$inFILE" | cut -d'.' -f2`;

if [ "$EXT" = "ctl" ]
then
cdo -f nc4 -z zip_9 import_binary $inFILE ${NAME}.nc
#ncks -4 -L 9 ${NAME}_0.nc ${NAME}.nc
elif [ "$EXT" = "gdat" ]
then
cdo -f nc4 -z zip_9 import_binary ${NAME}.ctl ${NAME}.nc
fi

#[ -f ${NAME}.nc ] && rm ${NAME}.gdat 
}

export -f ctl2nccomp

python ${CordexToolbox}/post_treatment_gdat_to_nc.py -sm_out_dir $outFolder -sm_par_dir $outFolder -exp historical  # historical rcp85

# !!! convert gdat to nc compress !!! 
find $outFolder/*/* -type f -name "*.ctl" > inFILE_list.txt

parallel --jobs 2 ctl2nccomp -inFILE {} :::: inFILE_list.txt



